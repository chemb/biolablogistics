<?php 
	echo $this->Html->script('sorttable');
	//echo $this->html->css('style2');
	?>

<style>
.select label{ display:none;}
table.sortable thead {
	    font-size: smaller;
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
.st a{
color :white !important;
}
.inputt {
    border-bottom: 1px solid #000 !important;
    border-left: 0;
    border-right: 0;
    border-top: 0;
    outline: none;
	background-color: transparent;
    width:98% !important;
    //line-height:20px;
}
.search-btn {
	    height: 38px !important;
}
.add-btn {
	background: #008080 !important;
	color: white !important;
}
</style>
<div class="new-order ">

		<h2 class="col-lg-3">Orders</h2>
		<div class="col-sm-5" style="width:initial;"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text','id'=>'search',"placeholder"=>'Search by any header','class'=>'form-control search_box',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				
				<span id="submitbtn" class="invite-button nomargin submitsearch search-btn">Search</span>
					
			</div>
			
		</div>
		<br>
		<div class="col-sm-3"> 
		<a id="add_btn" style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('directorder') ?>" >Schedule Delivery</a>
		</div>
		<div class="clear"></div>
<?php echo $this->Session->flash(); ?>
	<div class="responsive-table">
		<table id="table" class="sortable table table-striped">
			<thead>
			<tr>
<th>&nbsp;</th>

                             <th class="st">Order ID</th>
                                <th class="st">From</th>
                                <th class="st">TO</th>
                                <!--<th class="st">Pickup</th>
                                <th class="st">Dropoff</th>  
                                <th class="st">Priority</th>   
                                <th class="st">Recepient</th>-->   
                                <th class="st">Order Status</th>   
                                <th class="st">Price</th>   
                                <th class="st">Payment Status</th>
                                <th>&nbsp;</th>          
			</tr>	
			 </thead>
                   <tbody>
       <?php  
      if(!empty($dodata)){
      $i=0;
       foreach($dodata as $order){
       //print_r($order);
       ?>
			<tr>
			<td style="width:19px;" class="class_toggle plus"></td>
<!--<td><?php  
 //echo $this->Form->checkbox('arc', array('hiddenField' => false,'value'=>$order['Directorder']['oid'],'myarray'=>$order['Directorder']['payment_status'],'class'=>'arc','id'=>'arc_'.$i));
 $i++;

?></td> -->
			<td><?php echo '#'.$order['Directorder']['oid'];?></td>
			<td><?php 
			$state1 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip']; ?></td>
			<td><?php 
			$state2 = $this->requestAction('App/getstate/'.$order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			echo $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'];
			?></td>
			<td><?php echo $order['Directorder']['order_status']; ?></td>
			<td><?php echo '$'.$order['Directorder']['price']; ?></td>
			<td><?php echo ($order['Directorder']['payment_status'] == 0)? 'Pending':'Payment Done'; ?></td>
	<td><?php if($order['Directorder']['payment_status'] == 0){ 
		echo '<div class="black-btn-outer pull-right">';
echo $this->Html->link("Make Payment",array("controller"=>"orders","action"=>'laborderpayment',$order['Directorder']['oid']),array("class"=>"black-btn"));
	echo '</div>';
}else{
	echo 'Payment Done';
}
		?>
		</td>
			</tr>
			<tr class="detail_data" style="display:none;">
			<td>&nbsp;</td>
			<td><?php
			$distance = $this->requestAction('App/getzipdistance/'.$order['Directorder']['pickup_zip'].'/'.$order['Directorder']['dropoff_zip']);
			 echo '<b>Distance</b><br>'.$distance; 
			 ?></td>
			<td><?php echo '<b>Pickup</b><br>'.$order['Directorder']['pickup']; ?></td>
			<td><?php echo '<b>Dropoff</b><br>'.$order['Directorder']['dropoff']; ?></td>
			<td><?php echo '<b>Priority</b><br>'.$order['Directorder']['priority']; ?></td>
			<td><?php echo '<b>Recepient</b><br>'.$order['Directorder']['recipient']; ?></td>
			<td colspan="3"><?php echo '<b>Note</b><br>'.$order['Directorder']['note']; ?></td>
			</tr>
			
			
			<?php } }else{ ?>
				<tr>
					<td colspan="7" style="color:red;">No Order Found</td>
				</tr> 
			<?php } ?>
			
			</tbody>
		</table>
	</div>

</div>


<script>

var $rows = $('#table tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

$(document).ready(
		function(){
			$(".class_toggle").click(function(){
			    $(this).parent().next().toggle();
				
					
					if($(this).parent().next().css('display') == 'none')
					{if($(this).hasClass("minus")){$(this).removeClass("minus");$(this).addClass("plus");}}
					else{
						if($(this).hasClass("plus")){$(this).removeClass("plus");$(this).addClass("minus");}
					}
				
			});
			
		}
		
		);



</script>




