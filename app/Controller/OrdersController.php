<?php 
App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @property Order $Order
 */
class OrdersController extends AppController {
	
	var $components = array("Stripe");
	
	
	function beforefilter() {
		parent::beforefilter();
		$admin = $this->Session->read("admin");
		$this->checklogin();
		$this->set('curruser', $this->Auth->user('id'));
		
	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->set("title_for_layout", "Orders");
		$this->bulkactions();
		/* code to perform search functionality */
		if (isset($this->data) && !empty($this->data['Order']['searchval'])) {
			
			$this->Session->write('searchval', $this->data['Order']['searchval']);
			$this->conditions = array("OR" => array("Order.order_manualid like" => "%" . $this->data['Order']['searchval'] . "%","Order.status like" => "%" . $this->data['Order']['searchval'] . "%"));
			$order = "Order.created desc";
		}
		if (isset($this->params['named']['page'])) {
			if ($this->Session->read('searchval')) {
				$this->conditions = array("OR" => array("Order.id like" => "%" . $this->Session->read('searchval') . "%","Order.status like" => "%" . $this->data['Order']['searchval'] . "%"));
				$this->data['Order']['searchval'] = $this->Session->read('searchval');
			}
		} elseif (empty($this->conditions)) {
			$this->Session->delete('searchval');
		}
		/* end of code to perform search functionality */

		$order = "Order.created desc";
		$this->Order->virtualFields = array("status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1");
		$fields = array("Order.status", "Order.*", "Lab.first_name", "Lab.last_name", "Lab.address", "Lab.city", "Lab.phone", "Doctor.first_name", "Doctor.last_name", "Doctor.address", "Doctor.city", "Doctor.phone");
		$this->paginate = array("fields" => $fields, "order" => $order);
		$this->set('orders', $this->paginate($this->conditions));
	}

	/**
	 * admin_viewtransaction method
	 *
	 * @return void
	 */
	function admin_viewtransaction($id = NULL) {
		$order = $this->Order->find("first", array("conditions" => array("Order.id" => $id)));
		/* code to perform search functionality */
		if (isset($this->data) && !empty($this->data['Order']['searchval'])) {
			$this->Session->write('searchval', $this->data['Order']['searchval']);
			$this->conditions = array("Order.paymentref like" => "%" . $this->data['Order']['searchval'] . "%");
		}

		if (isset($this->params['named']['page'])) {

			if ($this->Session->read('searchval')) {
				$this->conditions = array("OR" => array("Order.paymentref like" => "%" . $this->Session->read('searchval') . "%"));
				$this->data['Order']['searchval'] = $this->Session->read('searchval');
			}
		} elseif (empty($this->conditions)) {
			$this->Session->delete('searchval');
		}
		/* end of code to perform search functionality */
		if (!empty($order)) {
			$this->conditions = array_merge($this->conditions, array("OR" => array("Order.id" => $id, "Order.order_id" => $id)));
			$orders = $this->Order->find("all", array("conditions" => $this->conditions, "order" => "Order.order_id ASC"));
			$this->set(compact("orders"));
		} else {
			$this->Session->setFlash("Invalid transaction.");
			$this->redirect(array("action" => "index"));
		}
	}

	/*
	 * @function name	: reorder
	 * @purpose			: to reorder any order
	 * @arguments		: na
	 * @return			: none
	 * @created by		: Nishi
	 * @created on		: 21th March 2015
	 */

	function reorder($order_id = Null) {
		$this->render('order');
		$this->layout = "frontend_new";
		$this->loadModel('DoctorLab');
		$this->loadModel('Userdetail');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderAttachment');
		$this->loadModel('OrderRequest');
		$this->loadModel('OrderStatus');
		$this->loadModel('Notification');
		$i = 0;
		
		if (!empty($order_id)) {
			if ($this->request->is('post') || $this->request->is('put')) {
				
				$data['OrderStatus']['status_id'] = 11;
				$data['OrderStatus']['updated_by'] = $this->Session->read("Auth.User.id");
				$data['OrderStatus']['order_id'] = $order_id;
				$this->OrderStatus->save($data);
				$reorder_id =$order_id;
				$order_lab_id = $this->Order->find('all', array('conditions' => array('Order.id' => $order_id), 'fields' => array('Order.lab_id')));
				$data = $this->request->data;
				 $rush= $data['OrderDetail'][3]['RUSHCASE'];
				$orderdetails = serialize($data['OrderDetail']);
				unset($data['OrderDetail']);
				$data['Order']['order_id'] = $order_id;
				//$data['Order']['lab_id'] = $order_lab_id[0]['Order']['lab_id'];
				$show_id = $order_lab_id[0]['Order']['lab_id'];
				$data['Order']['doctor_id'] = $this->Session->read("Auth.User.id");
				$data['OrderStatus'][0]['status_id'] = 1;
				$data['OrderStatus'][0]['updated_by'] = $this->Session->read("Auth.User.id");			
				$data['OrderDetail'][0]['details'] = $orderdetails;
				$data['OrderRequest'][0]['order_id'] = $order_id;
				$data['OrderRequest'][0]['lab_id'] = $order_lab_id[0]['Order']['lab_id'];
				if ($this->Order->saveAll($data)) {
					 $imageid = $data['Order']['image_id'];  
					$order_id = $this->Order->getLastInsertId();
					
					//$order_attachments['OrderAttachment']['order_id'] = $imageid ;
					$this->loadModel('OrderAttachment');
					$num=strlen((string)$order_id);
					if($num==1)
					{
						$digit='000000'.$order_id;
					}
					if($num==2)
					{
						$digit='00000'.$order_id;
					}
					if($num==3)
					{
						$digit='0000'.$order_id;
					}
					if($num==4)
					{
						$digit='000'.$order_id;
					}
					 
				    $ftype=$data['ftype'] ;
				    
				    $date=date('y-m-d');
				    $date=explode('-',$date);
				    if($rush=='1' && $ftype='1')
				    {
						$manualid='FER-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='1' && $ftype='2')
				    {
						$manualid='RER-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='0' && $ftype='2')
				    {
						$manualid='RNR-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='0' && $ftype='1')
				    {
						$manualid='FNR-'.$date[1].$date[0].'-'.$digit;
					}
					
					//echo $order_id ;echo "<br>"; echo $manualid; echo "<br>"; echo $rush;  exit;
					
					$this->OrderAttachment->updateAll(array('order_id'=>$order_id), array('OrderAttachment.id'=>$imageid));
					
					$not_order['Notification']['order_id'] = $order_id;
					
					$not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
				
					$not_order['Notification']['status_id'] = 11;
				
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $show_id;
					$this->Notification->save($not_order);
					
					
					
					//$this->updateid($order_id,$manualid);
					
					$this->Order->updateAll(array('order_manualid'=>"'$manualid'"), array('Order.id'=>$order_id));
					
					
					//$lab_id2 = $this->OrderRequest->find("first",array("conditions"=>array("OrderRequest.id"=>$order_id))); 
					//$lab_id_ms = $lab_id2['OrderRequest']['lab_id'];
					$orderr = $this->getlabid($order_id);
				$rr = unserialize($orderr['OrderDetail']['details']);
				$lab_id_ms = $rr[156]['lab_id'];
					
					$lab_na1 = $this->companyname($lab_id_ms);
					$pat_na1 = $this->getpatient($order_id);
                    $pro = $this->getprocedure($order_id);
					$ord_st1 = 'Reorder';
					$pay_st1 = $this->getpaymentstatus($order_id);
					$pri_to1 = $this->getbudgetprize($order_id);
					//$pri_to1 = $prize_de2[0]['OrderPayment']['payment'];
					$new_pr1 = $this->getbudgetprize($reorder_id);
					$new_pa1 = $this->getpatient($reorder_id);
					$ordee1 = $this->getorderid($order_id);
				    $ordee2 = $this->getorderid($reorder_id);
					//exit;
					$this->getmaildata(15);
					$this->mailBody = str_replace("{USER}", 'Admin', $this->mailBody);
					$this->mailBody = str_replace("{DOCNAME}", $this->Session->read("Auth.User.Userdetail.first_name"), $this->mailBody);
					$this->mailBody = str_replace("{PREID}", $ordee1, $this->mailBody);
                    $this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $lab_na1, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $pat_na1, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $ord_st1, $this->mailBody);
					$this->mailBody = str_replace("{PRICE}", $pri_to1, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $pay_st1, $this->mailBody);
					//new order
					$this->mailBody = str_replace("{NEWID}", $ordee2, $this->mailBody);
					$this->mailBody = str_replace("{NPATIENT}", $new_pa1, $this->mailBody);
					$this->mailBody = str_replace("{NPRICE}", $new_pr1, $this->mailBody);
					
					
					$this->sendmail(ADMIN_EMAIL);
					$lab = $this->Userdetail->find('all', array('conditions' => array('Userdetail.user_id' => $order_lab_id[0]['Order']['lab_id']), 'fields' => array('Userdetail.first_name', 'User.username')));
					$this->getmaildata(15);
					$this->mailBody = str_replace("{USER}", $lab[0]['Userdetail']['first_name'], $this->mailBody);
					$this->mailBody = str_replace("{DOCNAME}", $this->Session->read("Auth.User.Userdetail.first_name"), $this->mailBody);
					$this->mailBody = str_replace("{PREID}", $ordee1, $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $lab_na1, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $pat_na1, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $ord_st1, $this->mailBody);
                    $this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					$this->mailBody = str_replace("{PRICE}", $pri_to1, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $pay_st1, $this->mailBody);
					
					//new order
					$this->mailBody = str_replace("{NEWID}", $ordee2, $this->mailBody);
					$this->mailBody = str_replace("{NPATIENT}", $new_pa1, $this->mailBody);
					$this->mailBody = str_replace("{NPRICE}", $new_pr1, $this->mailBody);
					
					$this->sendmail($lab[0]['User']['username']);
					
					if (!empty($data['OrderAttachment'][0]['filename']['name'])) {
						//echo "first"; 
						foreach ($data['OrderAttachment'] as $key => $file) {
							$destination = "img/" . $this->Session->read("Auth.User.id") . "/Orders/" . $order_id . "/";
							if ($this->uploadAttachments($file['filename'], $destination)) {
								$order_attachments['OrderAttachment'][$i]['filename'] = $file['filename']['name'];
								$order_attachments['OrderAttachment'][$i]['destination'] = $this->uploaddir . $this->imagename;
								$order_attachments['OrderAttachment'][$i]['order_id'] = $reorder_id;
								++$i;
							}
						}
						$this->OrderAttachment->saveAll($order_attachments['OrderAttachment']);
						//exit;
					}
					$this->Session->setFlash("You have been reorder the product.", 'default', array("class" => "success_message"));
					$this->redirect('/preview-orders');
				}
			} 
			
			else {
				$order_data = $this->OrderDetail->find('all', array('conditions' => array('OrderDetail.order_id' => $order_id)));
				$data = unserialize($order_data[0]['OrderDetail']['details']);
				$users = $this->DoctorLab->find('all', array('conditions' => array('DoctorLab.doctor_id' => $this->Session->read("Auth.User.id")), 'fields' => array('Userdetail.first_name', 'DoctorLab.lab_id')));
				$order_accepted = $this->Order->find('all', array('conditions' => array('Order.id' => $order_id), array('fields' => array('Order.lab_id'))));
				$accepted_user = $order_accepted[0]['Order']['lab_id'];
				$this->set(compact("accepted_user"));
				$this->set('users', $users);
				$this->layout = "frontend_new";
				$heading = "Reorder";
				$or_id = $order_id;			
				$this->set(compact("heading"));
				$this->set(compact("or_id"));
				$this->request->data['OrderDetail'] = $data;
				$this->render('order');
			}
		}
	}


function advancesearch()
{
	$allorder=array();
	$this->layout = "frontend_new";
	$this->loadModel('State');
	$this->loadModel('Userdetail');
	$this->loadModel('Userdetail');
	$this->loadModel('Order');
	$state=$this->State->find('all');
	$this->set('state', $state);
	$alldata ='';
	$this->set('order_data', $allorder);	
	
	if ($this->request->is('post')) {
		$data=$this->data;
		//print_r($data);
		if($data['orders']['searchtype']==1)
		{
   $allusers = $this->Userdetail->find('all', array('conditions' => array('Userdetail.state_id' =>$data['orders']['stateid'])
    ));
    
     foreach($allusers as $user)
     {
		
		if($user['User']['user_type']=='1')
		{
			
		$allorder = $this->Order->find('all', array('conditions' => array('Order.doctor_id' =>$user['Userdetail']['user_id'])
    ));	
		 	
	}
	
		 
	 }
	 
     $this->set('order_data', $allorder);		
		}
	if($data['orders']['searchtype']==2)
		{
			
   $allusers = $this->Userdetail->find('all', array('conditions' => array('Userdetail.city' =>$data['orders']['cityname'])
    ));
     foreach($allusers as $user)
     {
		
		if($user['User']['user_type']=='1')
		{
			
		$allorder = $this->Order->find('all', array('conditions' => array('Order.doctor_id' =>$user['Userdetail']['user_id'])
    ));	
		 	
	}
	
		 
	 }
	 
     $this->set('order_data', $allorder);		
		}	
		

	}

}
	/* end of function */

	/*
	 * @function name	: updateorder
	 * @purpose			: to update an order till the status is waiting 
	 * @arguments		: na
	 * @return			: none
	 * @created by		: Nishi
	 * @created on		: 26th March 2015
	 */

	function updateorder($order_id = Null) {
		$this->autoRender = false;
		$order_attachments = array();

		if (!empty($order_id)) {
			$this->loadModel('OrderDetail');
			$res = $this->OrderDetail->find('first', array('conditions' => array('OrderDetail.order_id' => $order_id), 'fields' => array('OrderDetail.id')));
			if (isset($this->data) && !empty($this->data)) {
				$data = $this->data;
                                    $price =$data['Order']['budget']; 
				//pr($data['OrderAttachment']); die;				
				$orderdetails = serialize($data['OrderDetail']);
				unset($data['OrderDetail']);
				$data1['OrderDetail']['details'] = $orderdetails;

				if (!empty($data['OrderAttachment'][0]['filename']['name'])) {
					$i = 0;
					foreach ($data['OrderAttachment'] as $key => $file) {
						$destination = "img/" . $this->Session->read("Auth.User.id") . "/Orders/" . $order_id . "/";

						if ($this->uploadAttachments2($file['filename'], $destination, $order_id)) {
							$order_attachments['OrderAttachment'][$i]['filename'] = $file['filename']['name'];
							$order_attachments['OrderAttachment'][$i]['destination'] = $this->uploaddir . $this->imagename;
							$order_attachments['OrderAttachment'][$i]['order_id'] = $order_id;
							++$i;
						}
					}
					$this->loadModel('OrderAttachment');
					$this->OrderAttachment->saveAll($order_attachments['OrderAttachment']);
				}
                                 $this->loadModel('Order');
                                 $this->Order->updateAll(array('budget'=>$price), array('Order.id'=>$order_id));
				$this->OrderDetail->create();
				$this->OrderDetail->id = $res['OrderDetail']['id'];
				if ($this->OrderDetail->save($data1)) {
					$this->Session->setFlash("Your order has been updated successfully.", 'default', array("class" => "success_message"));
					$this->redirect(array("action" => "viewOrder", $order_id));
				}
			}
		}
	}

	/* end of function */

	/*
	 * @function name	: order
	 * @purpose			: to place an order 
	 * @arguments		: na
	 * @return			: none
	 * @created by		: Nishi
	 * @created on		: 17th March 2015
	 */

	
	function order($order_id = Null) {	
		$this->Session->delete('reorder_msg');
		$this->loadModel('DoctorLab');
		$this->loadModel('Userdetail');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderRequest');
		$this->loadModel('User');
		$this->loadModel('Order');
		$this->loadModel('State');
		$this->loadModel('Notification');
		$this->chekAccess(1);
		$this->layout = "frontend_new";
		//echo $order_id."test";
		
		
		
		if ($this->check_profile_status()) {
			$labid = $this->Session->read('Auth.User.id');
			
			$user_d = $this->Userdetail->find('first', array('conditions'=>array('Userdetail.id'=>$labid)));
			$this->set('user_d', $user_d['Userdetail']);
			$user_s = $this->State->find('first', array('conditions'=>array('State.id'=>$user_d['Userdetail']['state_id'])));
			$user_ss = $this->User->find('first', array('conditions'=>array('User.id'=>$user_d['Userdetail']['user_id'])));
			$heading = "Place an Order";
			$this->set(compact("heading"));
			$order_attachments = array();
			$i = 0;
			$this->set('stuser', $user_ss);
			$this->set('state', $user_s);
			//	$users = $this->DoctorLab->find('all', array('conditions'=>array('DoctorLab.doctor_id'=>$this->Session->read("Auth.User.id")), 'fields'=>array('Userdetail.first_name','DoctorLab.lab_id')));
			$heading = "Place an Order";
			$this->set(compact("heading"));
			$order_attachments = array();
			$i = 0;
			//	$this->set('users', $users);
			$this->set('usersl', $order_id);
			if (isset($this->data) && !empty($this->data)) {
				
				$data = $this->data; 
				//print_r($data); exit;
				
				  $rush= $data['OrderDetail'][3]['RUSHCASE'];
				//pr($data['OrderDetail'][0]['patientName']); die;
				$orderdetails = serialize($data['OrderDetail']); 
				unset($data['OrderDetail']);
				$data['Order']['doctor_id'] = $this->Session->read("Auth.User.id");
				$data['OrderStatus'][0]['status_id'] = 1;
				$data['OrderStatus'][0]['updated_by'] = $this->Session->read("Auth.User.id");
				$data['OrderDetail'][0]['details'] = $orderdetails;
				
				$patient_name = unserialize($orderdetails);
				$data['OrderDetail'][0]['patient_name'] = $patient_name[0]['patientName'];
				$data['OrderRequest'][0]['lab_id'] = $order_id;
				$details['OrderDetail'] = unserialize($orderdetails);
				
				$patient_m = $data['OrderDetail'][0]['patient_name'];
				$bprize = $data['Order']['budget'];
				
				//$bprize_m1 = $bprize*5/100;
                //$bprize_m = "Budget Amt:".$bprize."<br>"."Prepay Amt:".$bprize_m1;
                $bprize_m = $bprize;
				//$bprize_m = $bprize-$bprize_m;
				if($bprize_m =='' && $bprize_m == null){
					$bprize_m = 'Null';
				}
				
				
				
				$order_s = '';
				if($data['OrderStatus'][0]['status_id'] == 1){
					$order_s = "Waiting"; 
				}
				
				$this->data = $details; 
				//print_r($details) ;exit;             
				if ($this->Order->saveAll($data)) {
					$imageid = $data['Order']['image_id'];
					//$order_attachments['OrderAttachment']['order_id'] = $imageid ;
					$this->loadModel('OrderAttachment');
					$order_id = $this->Order->getLastInsertId();
                   $num=strlen((string)$order_id);
					if($num==1)
					{
						$digit='000000'.$order_id;
					}
					if($num==2)
					{
						$digit='00000'.$order_id;
					}
					if($num==3)
					{
						$digit='0000'.$order_id;
					}
					if($num==4)
					{
						$digit='000'.$order_id;
					}
					 
				    $ftype=$data['ftype'] ;
				    $date=date('y-m-d');
				    $date=explode('-',$date);
				    if($rush=='1' && $ftype='1')
				    {
						$manualid='FE-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='1' && $ftype='2')
				    {
						$manualid='RE-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='0' && $ftype='2')
				    {
						$manualid='RN-'.$date[1].$date[0].'-'.$digit;
					}
					if($rush=='0' && $ftype='1')
				    {
						$manualid='FN-'.$date[1].$date[0].'-'.$digit;
					}
					//echo $order_id ;echo $manualid;  exit;
					
					$this->OrderAttachment->updateAll(array('order_id'=>$order_id), array('OrderAttachment.id'=>$imageid));
					
					//$this->updateid($order_id,$manualid);
					
					$this->Order->updateAll(array('order_manualid'=>"'$manualid'"), array('Order.id'=>$order_id));
					
					foreach ($data['OrderRequest'] as $k => $v) {
						if (!empty($v['lab_id'])) {
							$this->User->recursive = 0;
							$user = $this->getUserDetailOnFly($v['lab_id'], array("User.username", "Userdetail.first_name", "Userdetail.last_name"));
							$orderlink = "<a href=" . SITE_LINK . "view-orders-lab/" . $order_id . ">Click Here</a>";
                            $pro = $this->getprocedure($order_id);
                            
                            
                    $not_order['Notification']['order_id'] = $order_id;
					
					$not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
				
					$not_order['Notification']['status_id'] = 1;
				
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $user['User']['id'];
					$this->Notification->save($not_order);
$ordee = $this->getorderid($order_id);
$spayment_m = $this->getpaymentstatus($order_id);
							// code to send email to labs asscociated with doctor																			
							$this->getmaildata(9);
							
							$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name'], $this->mailBody);
							$this->mailBody = str_replace("{DOCNAME}", $this->Session->read("Auth.User.Userdetail.first_name"), $this->mailBody);
							$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
							//$this->mailBody = str_replace("{ORDERDESC}",$this->data['OrderDetail'][0]['details'],$this->mailBody);
							$this->mailBody = str_replace("{PATIENT}", $patient_m, $this->mailBody);
							$this->mailBody = str_replace("{ORDERS}", $order_s, $this->mailBody);
							$this->mailBody = str_replace("{PRIZE}", $bprize_m, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
							$this->mailBody = str_replace("{PAYMENTS}", $spayment_m, $this->mailBody);
							$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
							$this->sendmail($user['User']['username']);
						}
					}

					if (!empty($data['OrderAttachment'][0]['filename']['name'])) {
						foreach ($data['OrderAttachment'] as $key => $file) {
							$destination = "img/" . $this->Session->read("Auth.User.id") . "/Orders/" . $order_id . "/";
							if ($this->uploadAttachments($file['filename'], $destination)) {
								$order_attachments['OrderAttachment'][$i]['filename'] = $file['filename']['name'];
								$order_attachments['OrderAttachment'][$i]['destination'] = $this->uploaddir . $this->imagename;
								$order_attachments['OrderAttachment'][$i]['order_id'] = $order_id;
								++$i;
							}
						}

						$this->loadModel('OrderAttachment');
						$this->OrderAttachment->saveAll($order_attachments['OrderAttachment']);
					}

					$this->Session->setFlash("Your order has been placed successfully.", 'default', array("class" => "success_message"));
					$this->redirect("/preview-orders");
				}
			}
		} else {
			$this->render('profilestatusview');
		}
	}
	
	
	function new_order($order_id =null) {	
		$this->render('order');	
		$this->Session->delete('reorder_msg');
		$this->loadModel('DoctorLab');
		$this->loadModel('Userdetail');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderRequest');
		$this->loadModel('User');
		$this->chekAccess(1);
		$this->layout = "frontend_new";
		if ($this->check_profile_status()) {
			if(!empty($order_id)) {		   
			$order_attachments = array();
			$i = 0;					
			if (isset($this->data) && !empty($this->data)) {				
				$data = $this->data;  				
				$orderdetails = serialize($data['OrderDetail']); 
				unset($data['OrderDetail']);
				$data['Order']['doctor_id'] = $this->Session->read("Auth.User.id");
				$data['OrderStatus'][0]['status_id'] = 1;
				$data['OrderStatus'][0]['updated_by'] = $this->Session->read("Auth.User.id");
				$data['OrderDetail'][0]['details'] = $orderdetails;
				$patient_name = unserialize($orderdetails);
				$data['OrderDetail'][0]['patient_name'] = $patient_name[0]['patientName'];
				$data['OrderRequest'][0]['lab_id'] = $order_id;
				$details['OrderDetail'] = unserialize($orderdetails);
				
				$this->data = $details;              
				if ($this->Order->saveAll($data)) {
					$order_id = $this->Order->getLastInsertId();
					foreach ($data['OrderRequest'] as $k => $v) {
						if (!empty($v['lab_id'])) {
							$this->User->recursive = 0;
							$user = $this->getUserDetailOnFly($v['lab_id'], array("User.username", "Userdetail.first_name", "Userdetail.last_name"));
							$orderlink = "<a href=" . SITE_LINK . "view-orders-lab/" . $order_id . ">Click Here</a>";
$pro = $this->getprocedure($order_id);
$ordee = $this->getorderid($order_id);

							// code to send email to labs asscociated with doctor																			
							$this->getmaildata(9);
							$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name'], $this->mailBody);
							$this->mailBody = str_replace("{DOCNAME}", $this->Session->read("Auth.User.Userdetail.first_name"), $this->mailBody);
							$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
							//$this->mailBody = str_replace("{ORDERDESC}",$this->data['OrderDetail'][0]['details'],$this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
							$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
							$this->sendmail($user['User']['username']);
						}
					}

					if (!empty($data['OrderAttachment'][0]['filename']['name'])) {
						foreach ($data['OrderAttachment'] as $key => $file) {
							$destination = "img/" . $this->Session->read("Auth.User.id") . "/Orders/" . $order_id . "/";
							if ($this->uploadAttachments($file['filename'], $destination)) {
								$order_attachments['OrderAttachment'][$i]['filename'] = $file['filename']['name'];
								$order_attachments['OrderAttachment'][$i]['destination'] = $this->uploaddir . $this->imagename;
								$order_attachments['OrderAttachment'][$i]['order_id'] = $order_id;
								++$i;
							}
						}

						$this->loadModel('OrderAttachment');
						$this->OrderAttachment->saveAll($order_attachments['OrderAttachment']);
					}

					$this->Session->setFlash("Your order has been placed successfully.", 'default', array("class" => "success_message"));
					$this->redirect("/preview-orders");
				}
			} else {
				$res = $this->Order->find('all',array('conditions'=>array('Order.id' =>$order_id)));
				$data = unserialize($res['0']['OrderDetail']['0']['details']);				
					   
				$this->request->data['OrderDetail'] = $data;			
				$heading = "Place an Order";
				$this->set(compact("heading"));
				$this->render('order');
				$this->layout = "frontend_new";
			}
		   } 
		}
		
		else {
			$this->render('profilestatusview');
		}
		
	}

	/*
	 * @function name	: accept_order
	 * @purpose			: to display order listing for Lab
	 * @arguments		: na
	 * @return			: none
	 * @created by		: Nishi
	 * @created on		: 17th March 2015
	 */

	function accept_order($archive = 0) {		 
		$this->layout = "frontend_new";
		$this->loadModel('OrderStatus');
		if ($this->check_profile_status()) {
			//$this->Order->recursive = -1;
			$options['joins'] = array(
				array('table' => 'userdetails',
					'alias' => 'Doctor',
					'type' => 'inner',
					'conditions' => array(
						'Order.doctor_id = Doctor.user_id'
					)
				), array('table' => 'order_requests',
					'alias' => 'OrderRequest',
					'type' => 'Inner',
					'conditions' => array(
						'OrderRequest.order_id = Order.id'
					)
				),
				array('table' => 'order_details',
					'alias' => 'OrderDetail',
					'type' => 'Inner',
					'conditions' => array(
						'OrderDetail.order_id = Order.id'
					)
					),
				array('table' => 'order_statuses',
					'alias' => 'OrderStatus',
					'type' => 'Inner',
					'conditions' => array(
						'Order.id = OrderStatus.order_id'
					)
				), array('table' => 'statuses',
					'alias' => 'Status',
					'type' => 'Inner',
					'conditions' => array(
						'OrderStatus.status_id = Status.id'
					)
				), array('table' => 'order_payments',
					'alias' => 'OrderPayment',
					'type' => 'left',
					'conditions' => array(
						'OrderPayment.order_id = Order.id and OrderPayment.receiver = Order.lab_id'
					)
				)
			);
			$order_status = $this->OrderStatus->find('list',array('conditions'=>array("status_id"=>array(12,13)),"fields"=>array("id","OrderStatus.order_id")));		
            $this->Order->recursive = -1;
			$options['fields'] = array("OrderStatus.order_id", "OrderStatus.id");
			$orders = $this->Order->find('list', $options);			
			if ( $archive > 0 ) {		
				$options['conditions'] = array('or'=>array('OrderRequest.lab_id' => $this->Session->read("Auth.User.id"),'Order.lab_id' => $this->Session->read("Auth.User.id")),"OrderStatus.status_id"=>array(12,13),"OrderStatus.lstatus"=>1);
				$this->set('data',$archive);
			}else {
				$options['conditions'] = array('or'=>array('OrderRequest.lab_id' => $this->Session->read("Auth.User.id"),'Order.lab_id' => $this->Session->read("Auth.User.id")), 'OrderStatus.id' => $orders,"OrderStatus.lstatus"=>0);
			}
			
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$options['conditions'] = array_merge($options['conditions'], array("or" => array("Status.title like" => "%" . $this->request->query['searchval'] . "%", "OrderStatus.order_id" =>$this->request->query['searchval'], "Doctor.first_name like" => "%" . $this->request->query['searchval'] . "%","OrderDetail.patient_name like" => "%" . $this->request->query['searchval'] . "%")));
			} 
			$this->set("set_val", $this->request->query);
			$options['fields'] = array('Order.*', 'OrderRequest.*', 'Doctor.first_name', 'Doctor.last_name', 'Doctor.city', 'Doctor.state_id', 'Doctor.phone', 'Status.title', "Status.id", "OrderStatus.updated_by", 'OrderPayment.payment','OrderDetail.details','OrderDetail.patient_name');
			$options['order'] = array("OrderStatus.order_id"=>'DESC');
			$this->paginate = $options;
			$this->set('orders', $this->paginate('Order'));
			
		} else {
			$this->render('profilestatusview');
		}
	}

	/*
	 * @function name	: orderstatus
	 * @purpose			: to accept and reject order by lab and send email to the doctor
	 * @arguments		: order id and status for accept or reject
	 * @return			: none
	 * @created by		: sukhwinder
	 * @created on		: 4th March 2015
	 */

	 function orderstatus2($id = NULL, $status = NULL) {

		$this->autoRender = false;
		$this->loadModel("OrderStatus");
		$this->loadModel('User');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderPayment');
		$this->loadModel('OrderRequest');
		$this->loadModel('Notification');
		$this->loadModel('Order');
		
				
			
						
	if ($this->request->is('ajax')) {
         $id=$this->request->data['order_id']; 		
	       $status=$this->request->data['status_id']; 
				
		$check_ifcancel = $this->OrderStatus->find('first',array('conditions'=>array('OrderStatus.order_id'=>$id,"status_id"=>12)));
		if(!empty($check_ifcancel)){
			$this->Session->setFlash("Order has already been canceled by doctor.");
			//$this->redirect(array("action" => "viewOrderForLab",$id));			
		}		
		if (!empty($id)) {
			$is_valid_order = $this->Order->find('first', array('conditions' => array('Order.id' => $id), 'fields' => array('Order.lab_id')));
			if (!empty($is_valid_order['Order']['lab_id'])) {
				$this->Session->setFlash("Order has already been picked by other Lab.", 'default', array("class" => "success_message"));
				//$this->redirect(array("action" => "accept_order"));
			}
			$this->Order->create();
			$this->Order->id = $id;
			if ($status != 3) {
				$data['Order']['lab_id'] = $this->Session->read('Auth.User.id');
				$this->Order->save($data);
			}
			$idm = $this->Session->read('Auth.User.id');
		$labname_m = $this->User->find("first",array("conditions"=>array("User.id"=>$idm)));
		$patient_m = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$id)));
	
		$payment_m = $this->OrderPayment->find("first",array("conditions"=>array("OrderPayment.order_id"=>$id,"OrderPayment.receiver"=>$idm)));
		$statuses_m = $this->User->find("first",array("conditions"=>array("User.id"=>$idm)));
		
		
		$user_no = $labname_m['User']['id'];
		$patient_ms =$patient_m['OrderDetail']['patient_name'];
		$bprize_m = $patient_m['Order']['budget'];
		$labname_ms = $labname_m['User']['company_name'];
			$data1['OrderStatus']['updated_by'] = $this->Session->read('Auth.User.id');
			
			$data1['OrderStatus']['order_id'] = $id;
			$data1['OrderStatus']['status_id'] = $status;
			$data2['OrderStatus']['updated_by'] = $this->Session->read('Auth.User.id');
			
			$data2['OrderStatus']['order_id'] = $id;
			$data2['OrderStatus']['status_id'] = 4;
			$data3['OrderPayment']['order_id']=$id;
			//echo "<br>";
			$data3['OrderPayment']['payer']=-1;
			//echo "<br>";
			 $data3['OrderPayment']['receiver']=$this->Session->read('Auth.User.id');
			//echo "<br>";
		  $data3['OrderPayment']['payment']=$bprize_m; 
		  //print_r($patient_m);exit;
			//echo "<br>";
			//for order status

		$sorder_m = 'Accept';
			//$orderstatus_order_id = $this->OrderStatus->find('first',array('fields'=>array('OrderStatus.id'), 'conditions'=>array('OrderStatus.order_id'=>$id)));				
			$doctorId = $this->Order->find('first', array('fields' => array('Order.doctor_id'), 'conditions' => array('Order.id' => $id)));
			$user = $this->getUserDetailOnFly($doctorId['Order']['doctor_id'], array("User.username", "Userdetail.first_name"));
			//$this->OrderStatus->id = $orderstatus_order_id['OrderStatus']['id'];
			$orderlink = "<a href=" . SITE_LINK . "view-orders-lab/" . $id . ">Click Here</a>";
			if ($this->OrderStatus->save($data1)) {
				
				if ($status == 2) {
					$this->OrderStatus->save($data2);
				$this->OrderPayment->save($data3);
					$not_order['Notification']['order_id'] = $id;
					
					$not_order['Notification']['user_id'] = $user_no;
				
					$not_order['Notification']['status_id'] = 2;
				
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $user['User']['id'];
					
					$this->Notification->save($not_order);
$pro = $this->getprocedure($id);
$ordee = $this->getorderid($id);
//payment status
$payment_ms = $this->getpaymentstatus($id) ; 
	$this->getmaildata(10);
					$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'], $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $labname_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $patient_ms, $this->mailBody);
					$this->mailBody = str_replace("{PRIZE}", $bprize_m, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $payment_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $sorder_m, $this->mailBody);
                   $this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					//$this->mailBody = str_replace("{ORDERDESC}",$doctorId['OrderDetail'][0]['details'],$this->mailBody);
					$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
					$this->sendmail($user['User']['username']);
					$this->Session->setFlash("Order has been accepted and email sent to the doctor.", 'default', array("class" => "success_message"));
					//$this->redirect(array("action" => "accept_order"));
					
				} else {
					
					 $not_order['Notification']['order_id'] = $id;
					
					$not_order['Notification']['user_id'] = $user_no;
					
					$not_order['Notification']['status_id'] = 3;
					
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $user['User']['id'];
					
					$this->Notification->save($not_order);
					$sorderm = 'Lab Rejected';
$pro = $this->getprocedure($id);
$ordee = $this->getorderid($id);
$payment_ms = $this->getpaymentstatus($id);
					$this->getmaildata(11);
					$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'], $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $labname_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $patient_ms, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					$this->mailBody = str_replace("{PRIZE}", $bprize_m, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $payment_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $sorderm, $this->mailBody);
					//$this->mailBody = str_replace("{ORDERDESC}",$doctorId['OrderDetail'][0]['details'],$this->mailBody);
					$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
					$this->sendmail($user['User']['username']);
					$this->Session->setFlash("Order has been rejected and email sent to the doctor.");
					//$this->redirect(array("action" => "accept_order"));
				}
			}
		}
		
	
	return 1;
	}
	}
function orderstatus($id = NULL, $status = NULL) {
		$this->autoRender = false;
		$this->loadModel("OrderStatus");
		$this->loadModel('User');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderPayment');
		$this->loadModel('OrderRequest');
		$this->loadModel('Notification');
		$this->loadModel('Order');
		
				
			
				
				
		$check_ifcancel = $this->OrderStatus->find('first',array('conditions'=>array('OrderStatus.order_id'=>$id,"status_id"=>12)));
		if(!empty($check_ifcancel)){
			$this->Session->setFlash("Order has already been canceled by doctor.");
			$this->redirect(array("action" => "viewOrderForLab",$id));			
		}		
		if (!empty($id)) {
			$is_valid_order = $this->Order->find('first', array('conditions' => array('Order.id' => $id), 'fields' => array('Order.lab_id')));
			if (!empty($is_valid_order['Order']['lab_id'])) {
				$this->Session->setFlash("Order has already been picked by other Lab.", 'default', array("class" => "success_message"));
				$this->redirect(array("action" => "accept_order"));
			}
			$this->Order->create();
			$this->Order->id = $id;
			if ($status != 3) {
				$data['Order']['lab_id'] = $this->Session->read('Auth.User.id');
				$this->Order->save($data);
			}
			$idm = $this->Session->read('Auth.User.id');
		$labname_m = $this->User->find("first",array("conditions"=>array("User.id"=>$idm)));
		$patient_m = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$id)));
		$payment_m = $this->OrderPayment->find("first",array("conditions"=>array("OrderPayment.order_id"=>$id,"OrderPayment.receiver"=>$idm)));
		$statuses_m = $this->User->find("first",array("conditions"=>array("User.id"=>$idm)));
		
		
		$user_no = $labname_m['User']['id'];
		$patient_ms =$patient_m['OrderDetail']['patient_name'];
		$bprize_m = $patient_m['Order']['budget'];
		$labname_ms = $labname_m['User']['company_name'];
			$data1['OrderStatus']['updated_by'] = $this->Session->read('Auth.User.id');
			
			$data1['OrderStatus']['order_id'] = $id;
			$data1['OrderStatus']['status_id'] = $status;
			$data2['OrderStatus']['updated_by'] = $this->Session->read('Auth.User.id');
			
			$data2['OrderStatus']['order_id'] = $id;
			$data2['OrderStatus']['status_id'] = 4;
			$data3['OrderPayment']['order_id']=$id;
			$data3['OrderPayment']['payer']=-1;
			$data3['OrderPayment']['receiver']=$this->Session->read('Auth.User.id');
			$data3['OrderPayment']['payment']=$bprize_m;
			//for order status

		$sorder_m = 'Accept';
			//$orderstatus_order_id = $this->OrderStatus->find('first',array('fields'=>array('OrderStatus.id'), 'conditions'=>array('OrderStatus.order_id'=>$id)));				
			$doctorId = $this->Order->find('first', array('fields' => array('Order.doctor_id'), 'conditions' => array('Order.id' => $id)));
			$user = $this->getUserDetailOnFly($doctorId['Order']['doctor_id'], array("User.username", "Userdetail.first_name"));
			//$this->OrderStatus->id = $orderstatus_order_id['OrderStatus']['id'];
			$orderlink = "<a href=" . SITE_LINK . "view-orders-lab/" . $id . ">Click Here</a>";
			if ($this->OrderStatus->save($data1)) {
				
				if ($status == 2) {
					$this->OrderStatus->save($data2);
				$this->OrderPayment->save($data3);
					$not_order['Notification']['order_id'] = $id;
					
					$not_order['Notification']['user_id'] = $user_no;
				
					$not_order['Notification']['status_id'] = 2;
				
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $user['User']['id'];
					
					$this->Notification->save($not_order);
$pro = $this->getprocedure($id);
$ordee = $this->getorderid($id);
//payment status
$payment_ms = $this->getpaymentstatus($id);

					$this->getmaildata(10);
					$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'], $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $labname_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $patient_ms, $this->mailBody);
					$this->mailBody = str_replace("{PRIZE}", $bprize_m, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $payment_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $sorder_m, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					//$this->mailBody = str_replace("{ORDERDESC}",$doctorId['OrderDetail'][0]['details'],$this->mailBody);
					$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
					$this->sendmail($user['User']['username']);
					$this->Session->setFlash("Order has been accepted and email sent to the doctor.", 'default', array("class" => "success_message"));
					$this->redirect(array("action" => "accept_order"));
					
				} else {
					
					 $not_order['Notification']['order_id'] = $id;
					
					$not_order['Notification']['user_id'] = $user_no;
					
					$not_order['Notification']['status_id'] = 3;
					
					$not_order['Notification']['status'] = 1;
					$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
					$not_order['Notification']['show_id'] = $user['User']['id'];
					
					$this->Notification->save($not_order);
					$sorderm = 'Lab Rejected';
$pro = $this->getprocedure($id);
$ordee = $this->getorderid($id);
$payment_ms = $this->getpaymentstatus($id);
					$this->getmaildata(11);
					$this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'], $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $labname_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
					$this->mailBody = str_replace("{PATIENT}", $patient_ms, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
					$this->mailBody = str_replace("{PRIZE}", $bprize_m, $this->mailBody);
					$this->mailBody = str_replace("{PAYMENTS}", $payment_ms, $this->mailBody);
					$this->mailBody = str_replace("{ORDERS}", $sorderm, $this->mailBody);
					//$this->mailBody = str_replace("{ORDERDESC}",$doctorId['OrderDetail'][0]['details'],$this->mailBody);
					$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
					$this->sendmail($user['User']['username']);
					$this->Session->setFlash("Order has been rejected and email sent to the doctor.");
					$this->redirect(array("action" => "accept_order"));
				}
			}
		}
	}

	/* end of function */

	public function remove_attachment($attachment_id = NUll, $order_id = NULL, $file_name = NULL) {
		$this->autoRender = false;
		$this->loadModel('OrderAttachment');
		$destination = WWW_ROOT . "img/" . $this->Session->read("Auth.User.id") . "/Orders/" . $order_id . "/" . $file_name;
		unlink($destination);
		$this->OrderAttachment->create();
		$this->OrderAttachment->id = $attachment_id;
		if ($this->OrderAttachment->delete()) {
			$result = array('error' => false, 'message' => 'Selected file has been deleted.');
		} else {
			$result = array('error' => true, 'message' => 'An error occured. Please try later.');
		}
		return json_encode($result);
	}

	/*
	 * @function name	: orderForDoctor
	 * @purpose			: Listing of orders for specific doctor.
	 * @arguments		: none
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 5th March 2015
	 */

	public function orderForDoctor($archive = 0) {	
		
		$this->layout = "frontend_new";
		$this->loadModel('OrderStatus');
		$this->loadModel('OrderDetail');
		$this->loadModel('UserDetail');
        $this->loadModel('User');
		if ($this->check_profile_status()) {
			$doc_id = $this->Session->read('Auth.User.id');
			$this->Order->virtualFields = array("status" => ("select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1"),"p_name" =>"select patient_name from order_details inner join orders on orders.id = order_details.order_id where order_details.order_id = Order.id");
			$order_status = $this->OrderStatus->find('list',array('conditions'=>array('OrderStatus.updated_by'=>$doc_id,"status_id"=>array(12,13),"status"=>1),"fields"=>array("id","OrderStatus.order_id")));	
				
			if(count($order_status) == 1) {
				$order_status = array_merge($order_status,array(-1));
			}
			
			if ( $archive > 0 ) {		
				$options['conditions'] = array('Order.doctor_id' => $doc_id,"Order.id"=>$order_status);				
				$this->set('data',$archive);
			}else {
				$options['conditions'] = array('Order.doctor_id' => $doc_id,"Order.id not"=>$order_status);				
			}
			
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$options['conditions'] = array_merge($options['conditions'],array("OR" => array('Order.order_manualid like' => $this->request->query['searchval'], 'Order.status like' =>$this->request->query['searchval'],"Order.p_name like" => "%" . $this->request->query['searchval'])));
			} 	
			$this->set("set_val", $this->request->query);	
			$options['fields'] = array('Order.*',"Order.status");
			$options['order'] = array("Order.id"=>'DESC');
			$this->paginate = $options;

			$user_ss = $this->User->find('all', array('conditions'=>array('User.user_type'=>4)));
//print_r($user_ss); die;
			$this->set('user_ss' ,$user_ss);
		
			$this->set('order_data', $this->paginate('Order'));
		} else {
			$this->render('profilestatusview');
		}
	}
	
		/*
	 * @function name	: archived_orders
	 * @purpose			: to view order that is in completed and cancel status.
	 * @arguments		: none
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 10th April 2015
	 */

	public function archived_orders() {
		$this->layout = "frontend_new";
		if ($this->check_profile_status()) {
			$doc_id = $this->Session->read('Auth.User.id');			
			$this->Order->virtualFields = array("status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1","status_id" => "select statuses.id from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1");
	
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$options['conditions'] = array('Order.doctor_id' => $doc_id, array("OR" => array('Order.id like' => "%" . $this->request->query['searchval'] . "%", 'Order.status like' => "%" . $this->request->query['searchval'] . "%")));
			} else {
				$options['conditions'] = array('Order.doctor_id' => $doc_id);
			}
			$this->paginate = $options;
			$orders = $this->Order->find('all', $options);
			$this->set('order_data', $this->paginate('Order'));
			$this->render('archived_orders');
		} else {
			$this->render('profilestatusview');
		}
	}
	

	/* end of function */
	/*
	 * @function name	: orderForStaff
	 * @purpose			: Listing of orders for staff member.
	 * @arguments		: none
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 10th March 2015
	 */

	public function orderForStaff($data = Null) {
		$this->layout = "frontend_new";
		//$this->loadModel('OrderStatus');
		if ($this->check_profile_status()) {
			$options['joins'] = array(
				array(
					'table' => 'userdetails',
					'alias' => 'userdetail',
					'type' => 'inner',
					'conditions' => array(
						'Order.doctor_id = userdetail.user_id'
					)
				), array(
					'table' => 'order_statuses',
					'alias' => 'OrderStatus',
					'type' => 'Inner',
					'conditions' => array(
						'Order.id = OrderStatus.order_id'
					)
				), array(
					'table' => 'statuses',
					'alias' => 'Status',
					'type' => 'Inner',
					'conditions' => array(
						'Status.id = OrderStatus.status_id'
					)
				), array(
					'table' => 'order_details',
					'alias' => 'OrderDetail',
					'type' => 'Inner',
					'conditions' => array(
						'Order.id = OrderDetail.order_id'
					)
				)
			);

			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$this->Order->recursive = -1;
				$options['conditions'] = array('OrderStatus.status_id' => array(7, 8, 9, 11,12,13));
				$options['fields'] = array("OrderStatus.order_id", "OrderStatus.id");
				$options['order'] = array("OrderStatus.order_id"=>'DESC');
				$order_data = $this->Order->find('list', $options);
				$options['conditions'] = array('OrderStatus.id' => $order_data, "or" => array("Status.title like" => "%" . $this->request->query['searchval'] . "%", "OrderStatus.order_id like" => "%" . $this->request->query['searchval'] . "%","OrderDetail.patient_name like" => "%" . $this->request->query['searchval'] . "%"));
			} 
			else if($data){
				$this->Order->recursive = -1;
				$options['conditions'] = array('Status.id' => array(12,13));
				$options['fields'] = array("OrderStatus.order_id", "OrderStatus.id");
				$order_data = $this->Order->find('list', $options); 
				$options['conditions'] = array('OrderStatus.id' => $order_data);
			}		
			else {
				$this->Order->recursive = -1;
				$options['conditions'] = array('OrderStatus.status_id' => array(7, 8, 9, 11,12,13));
				$options['fields'] = array("OrderStatus.order_id", "OrderStatus.id");							
				$order_data = $this->Order->find('list', $options);						
				$options['conditions'] = array('OrderStatus.id' => $order_data);				
			}
			$this->set("set_val", $this->request->query);
			$options['fields'] = array("Order.*", "OrderDetail.*", "Status.*");
			$options['order'] = array("OrderStatus.order_id"=>'DESC');
			$this->paginate = $options;			 
			$orders = $this->Order->find('all', $options);
				//print_r($this->paginate('Order'));exit;
			$this->set('order_data', $this->paginate('Order'));
			if($data){
			$this->render('archived_order_member');
			}
		}
		
		 else {
			$this->render('profilestatusview');
		}
	}

	/* end of function */

	/*
	 * @function name	: view_order
	 * @purpose			: To view complete order details for doctor.
	 * @arguments		: order id.
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 5th March 2015
	 */

	function viewOrder($order_id = Null) {
		$this->Order->id = $order_id;
		if ($this->Order->exists()) {
			$this->layout = "frontend_new";
			$doc_id = $this->Session->read('Auth.User.id');
			$this->loadModel("OrderDetail");
			$this->loadModel("Status");
			$this->loadModel("OrderStatus");
			$this->loadModel("OrderAttachment");
			$this->loadModel("User");
			$this->loadModel("OrderRequest");
			$this->loadModel("UserRating");
			$this->OrderDetail->virtualFields = array("status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = OrderDetail.order_id order by order_statuses.id desc limit 1",
				"status_id" => "select status_id from order_statuses where order_id = '$order_id' order by created desc limit 1"
				);
				
			//$this->OrderDetail->bindModel(array("hasMany"=>array("OrderAttachment"=>array("className"=>"OrderAttachment","foreignKey"=>"order_id"))));

			$OrderAttachment = $this->OrderAttachment->find('all', array('conditions' => array('OrderAttachment.order_id' => $order_id)));
			//	pr($OrderAttachment); die;
			$this->OrderDetail->recursive = 1;

			$order_data = $this->OrderDetail->find("all", array("conditions" => array("Order.id" => $order_id, "Order.doctor_id" => $doc_id), "fields" => array("OrderDetail.*", "OrderDetail.status", "Order.*")));

			$data = unserialize($order_data[0]['OrderDetail']['details']);
			$this->request->data['OrderDetail'] = $data;
			$is_statusExist = $this->OrderStatus->find("list", array('conditions' => array('OrderStatus.order_id' => $order_id), 'fields' => array('OrderStatus.status_id')));
			$status = $this->Status->find("list", array('conditions' => array('Status.id NOT' => $is_statusExist, 'Status.status_type_id' => $this->Session->read('Auth.User.user_type'), 'Status.status' => '1')));
			
			$lab_id = $this->OrderRequest->find('first',array('conditions'=>array('OrderRequest.order_id' =>$order_data[0]['Order']['id']),'fields'=>array('OrderRequest.lab_id')));		
            $lab_n = $this->User->Userdetail->find('first',array('conditions'=>array('Userdetail.user_id'=>$lab_id['OrderRequest']['lab_id']),'fields'=>array('Userdetail.first_name','Userdetail.last_name','Userdetail.user_id',"User.company_name")));
            
            $user_rating =$this->UserRating->find('first',array('conditions'=>array('UserRating.user_id'=>$lab_id['OrderRequest']['lab_id'])));            
            
            $already_given = $this->UserRating->find('all',array('conditions'=>array('UserRating.order_id'=>$order_id,'UserRating.rater_id'=>$lab_id['OrderRequest']['lab_id'])));           
           
			
			if (isset($order_data[0]['Order']) && !empty($order_data[0]['Order']['id'])) {
				$this->set('order_data', $order_data[0]);
				$this->set('status', $status);
				$this->set('lab_name', $lab_n);
				$this->set('user_rating', $user_rating);
				$this->set('user_rating_already', $already_given);
				$this->set('OrderAttachment', $OrderAttachment);
			} else {
				throw new NotFoundException(__('Invalid Action'));
			}
		} else {
			throw new NotFoundException(__('Invalid Action'));
		}
	}

	/* end of function */

	/*
	 * @function name	: viewOrderForLab
	 * @purpose			: To view complete order details for Lab.
	 * @arguments		: order id.
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 9th March 2015
	 */

	function viewOrderForLab($order_id = Null) {

		$this->Order->id = $order_id;
		if ($this->Order->exists()) {
			$this->layout = "frontend_new";
			$lab_id = $this->Session->read('Auth.User.id');
			$this->loadModel("OrderDetail");
			$this->loadModel("Status");
			$this->loadModel("Order");
			$this->loadModel("OrderAttachment");
			$this->loadModel("OrderStatus");
			$this->OrderDetail->virtualFields = array(
				"status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = OrderDetail.order_id order by order_statuses.id desc limit 1",
				"is_requested" => "select id from order_requests where lab_id = " . $lab_id . " and order_id = " . $order_id,
				"is_reject_by" => "select id from order_statuses where status_id = 3 and updated_by = " . $lab_id . " and order_id = " . $order_id,
				"status_id" => "select status_id from order_statuses where order_id = '$order_id' order by created desc limit 1");
			//$this->OrderDetail->bindModel(array("hasMany"=>array("OrderAttachment"=>array("className"=>"OrderAttachment","foreignKey"=>"order_id"))));
			$OrderAttachment = $this->OrderAttachment->find('all', array('conditions' => array('OrderAttachment.order_id' => $order_id)));
			//$this->OrderDetail->recursive = 1;			
			$order_data = $this->Order->OrderDetail->find("all", array("conditions" => array("Order.id" => $order_id), "fields" => array("OrderDetail.*", "OrderDetail.is_requested", "OrderDetail.status", "Order.*","Doctor.first_name")));						
			$doc_email = $this->getUserDetailOnFly($order_data[0]['Order']['doctor_id']);						
			$doc_name = $this->getUserDetailOnFly($order_data[0]['Order']['doctor_id'],array("User.company_name"));	
			$data = unserialize($order_data[0]['OrderDetail']['details']);
			$this->request->data['OrderDetail'] = $data;
			$is_statusExist = $this->OrderStatus->find("list", array('conditions' => array('OrderStatus.order_id' => $order_id), 'fields' => array('OrderStatus.status_id')));
			$status = $this->Status->find("list", array('conditions' => array('Status.id NOT' => $is_statusExist, 'Status.status_type_id' => $this->Session->read('Auth.User.user_type'), 'Status.status' => '1')));
			if (isset($order_data[0]['Order']) && !empty($order_data[0]['Order']['id']) && ($order_data[0]['Order']['lab_id'] == $lab_id || !empty($order_data[0]['OrderDetail']['is_requested']))) {
				$this->set('order_data', $order_data[0]);
				$this->set('status', $status);
				$this->set('doc_email', $doc_email);
				$this->set('doc_name', $doc_name);
				$this->set('OrderAttachment', $OrderAttachment);
			} else {
				throw new NotFoundException(__('Invalid Action'));
			}
			$this->render('viewOrder');
		} else {
			throw new NotFoundException(__('Invalid Action'));
		}
	}

	/*
	 * @function name	: viewOrderStaff
	 * @purpose			: To view complete order details for Staff members.
	 * @arguments		: order id.
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 10th March 2015
	 */

	function viewOrderStaff($order_id = Null) {
		$this->Order->id = $order_id;
		if ($this->Order->exists()) {
			$this->layout = "frontend_new";
			$this->loadModel("OrderDetail");
			$this->loadModel("Status");
			$this->loadModel("OrderStatus");
			$this->Order->virtualFields = array("status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1",
				"status_id" => "select status_id from order_statuses where order_id = '$order_id' order by created desc limit 1");
			$this->Order->unbindModel(array('hasMany' => array('OrderStatus', 'Transaction', 'OrderDeliveryCharge')));
			$order_data = $this->Order->find("all", array("conditions" => array("Order.id" => $order_id)));


			$is_statusExist = $this->OrderStatus->find("list", array('conditions' => array('OrderStatus.order_id' => $order_id), 'fields' => array('OrderStatus.status_id')));
			$status = $this->Status->find("list", array('conditions' => array('Status.id NOT' => $is_statusExist, 'Status.status_type_id' => $this->Session->read('Auth.User.user_type'), 'Status.status' => '1')));

			if (isset($order_data[0]['Order']) && !empty($order_data[0]['Order']['id'])) {
				$this->set('order_data', $order_data[0]);
				$this->set('status', $status);
			} else {
				throw new NotFoundException(__('Invalid Action'));
			}
			$this->render('viewOrder');
		} else {
			throw new NotFoundException(__('Invalid Action'));
		}
	}

	/* end of function */



	/*
	 * @function name	: update_status
	 * @purpose			: To change the order status.
	 * @return			: error message true if any error occured otherwise return false.
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 9th March 2015
	 */

		function update_status() {		
		$this->autoRender = false;
		$this->loadModel('OrderDetail');
		$this->loadModel('Order');
		$this->loadModel('OrderStatus');
		$this->loadModel('OrderPayment');
		$this->loadmodel("Userdetail");
		$this->loadModel('OrderRequest');
		$this->loadModel('OrderDetail');
			$this->loadModel('Notification');
		
		if ($this->request->is('ajax')) {			 		
			if ($this->request->data['status_id'] == 7) {
				$data = $this->OrderDetail->find('first', array('fields' => array('OrderDetail.order_id,OrderDetail.details'), 'conditions' => 'OrderDetail.order_id =' . $this->request->data['order_id']));
				$orderlink = "<a href=" . SITE_LINK . "view-orders-lab/" . $this->request->data['order_id'] . ">Click Here</a>";
				$order_idrd = $data['OrderDetail']['order_id'];
				//$lab_id_m = $this->OrderRequest->find("first",array("conditions"=>array("OrderRequest.id"=>$order_idrd)));
		//$lab_id_mm = $lab_id_m['OrderRequest']['lab_id'];
				$orderr = $this->getlabid($order_idrd);
				$rr = unserialize($orderr['OrderDetail']['details']);
				$lab_id_mm = $rr[156]['lab_id'];
				$lab_name1 = $this->companyname($lab_id_mm);
				$pat_name1 = $this->getpatient($order_idrd);
				$order_sm1 = 'Ready to Dispatch';
				$prize_1 = $this->getbudgetprize($order_idrd);
				$payment_s1= $this->getpaymentstatus($order_idrd);
					$not_order['Notification']['order_id'] = $order_idrd;
				
					 $not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
				
				$not_order['Notification']['status_id'] = 7;
					
					 $not_order['Notification']['status'] = 1;
					 $not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
			 $not_order['Notification']['show_id'] = 13;
					$this->Notification->save($not_order);
				
$or_id = $data['OrderDetail']['order_id'];
$pro = $this->getprocedure($or_id);
$ordee = $this->getorderid($or_id);
				$this->getmaildata(12);
				$this->mailBody = str_replace("{LABNAME}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
				$this->mailBody = str_replace("{ORDERS}", $order_sm1, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $pat_name1, $this->mailBody);
				$this->mailBody = str_replace("{LAB}", $lab_name1, $this->mailBody);
				$this->mailBody = str_replace("{PRICE}", $prize_1, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $payment_s1, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
				$this->mailBody = str_replace("{LABNAME}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{LABEMAIL}", $this->Session->read('Auth.User.username'), $this->mailBody);
				$this->mailBody = str_replace("{LABPHONE}", $this->Session->read('Auth.User.Userdetail.phone') != '' ? $this->Session->read('Auth.User.Userdetail.phone') : 'N-A', $this->mailBody);
				$this->mailBody = str_replace("{LABADDRESS}", $this->Session->read('Auth.User.Userdetail.address') != '' ? $this->Session->read('Auth.User.Userdetail.address') : 'N-A', $this->mailBody);				
				$this->sendmail(ADMIN_EMAIL);
			}
			if ($this->request->data['status_id'] == 9) {
				$data = $this->OrderDetail->find('first', array('fields' => array('OrderDetail.order_id,OrderDetail.details'), 'conditions' => 'OrderDetail.order_id =' . $this->request->data['order_id']));				
				$orderlink = "<a href=" . SITE_LINK . "orders/viewOrder/" . $this->request->data['order_id'] . ">Click Here</a>";
				//ORDER STATUS
				//$ids = 9;
		//$sorder_mss = $this->OrderStatus->find("first",array("conditions"=>array("OrderStatus.id"=>$ids)));
		$order_id_m = $data['OrderDetail']['order_id'];
		$payment_m = $this->getprize($order_id_m);
		$pri1 = $payment_m[0]['OrderPayment']['payment'];
		$pri2 = $payment_m[1]['OrderPayment']['payment'];
		$total_pri = '$'.$pri1;
		$pay_status = $this->getpaymentstatus($order_id_m);
		//lab name
		//$lab_id_m = $this->OrderRequest->find("first",array("conditions"=>array("OrderRequest.id"=>$order_id_m)));
		//$lab_id_mm = $lab_id_m['OrderRequest']['lab_id'];
		
		$orderr = $this->getlabid($order_id_m);
		$rr = unserialize($orderr['OrderDetail']['details']);
		$lab_id_mm = $rr[156]['lab_id'];
		
		$lab_name_mm = $this->companyname($lab_id_mm);
		//patient name
		$patient_m = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.id"=>$order_id_m)));
		$patient_mm =$patient_m['OrderDetail']['patient_name'];
		//order status
		$sorder_mm = 'Delivered';
		
		$not_id = $this->Order->find("first",array("conditions"=>array("Order.id"=>$order_id_m)));
		
		 $not_order['Notification']['order_id'] = $order_id_m;
		$not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
		 $not_order['Notification']['status_id'] = 9;
		$not_order['Notification']['status'] = 1;
		$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
		$not_order['Notification']['show_id'] = $not_id['Order']['doctor_id'];
		$this->Notification->save($not_order);
		
$orde_id = $data['OrderDetail']['order_id'];
$pro = $this->getprocedure($orde_id);
$ordee = $this->getorderid($orde_id);
				$this->getmaildata(18);
				$this->mailBody = str_replace("{STAFFMEMBER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
				$this->mailBody = str_replace("{ORDERS}", $sorder_mm, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm, $this->mailBody);
				$this->mailBody = str_replace("{LAB}", $lab_name_mm, $this->mailBody);
				$this->mailBody = str_replace("{PRICE}", $total_pri, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);							
				$this->sendmail(ADMIN_EMAIL);
			}
			if ($this->request->data['status_id'] == 13) {
				$payment = $this->OrderPayment->find('first', array('fields' => array('OrderPayment.order_id,OrderPayment.payment,OrderPayment.receiver'), 'conditions' => array('OrderPayment.order_id =' . $this->request->data['order_id'], 'OrderPayment.payer = -1')));
				$this->loadmodel("Userdetail");
				$user = $this->Userdetail->find("all", array("conditions" => array("User.id" => $payment['OrderPayment']['receiver']), "fields" => array("User.id", "Userdetail.first_name", "Userdetail.phone", "Userdetail.address", "User.username")), array("recursive" => 0));
				$orderlink = "<a href=" . SITE_LINK . "admin>Click Here</a>";
				$order_idm = $this->request->data['order_id'];
				$pat_name = $this->getpatient($order_idm);
				$order_sm = 'Completed';
				$payment_sm = $this->getpaymentstatus($order_idm);
				/*  Email for rrl admin if doctor marked the order status as compleated */
$order_idm = $this->request->data['order_id'];
			$pro = $this->getprocedure($order_idm);	
$ordee = $this->getorderid($order_idm);
				$this->getmaildata(13);
				$this->mailBody = str_replace("{DOCNAME}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{PAYMENT}", $payment['OrderPayment']['payment'], $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $pat_name, $this->mailBody);
				$this->mailBody = str_replace("{ORDERS}", $order_sm, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $payment_sm, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
				$this->mailBody = str_replace("{LABNAME}", $user[0]['Userdetail']['first_name'], $this->mailBody);
				$this->mailBody = str_replace("{LABEMAIL}", $user[0]['User']['username'], $this->mailBody);
				$this->mailBody = str_replace("{LABPHONE}", $user[0]['Userdetail']['phone'] != '' ? $user[0]['Userdetail']['phone'] : 'N-A', $this->mailBody);
				$this->mailBody = str_replace("{LABADDRESS}", $user[0]['Userdetail']['address'] != '' ? $user[0]['Userdetail']['address'] : 'N-A', $this->mailBody);
				$this->sendmail(ADMIN_EMAIL);
				$paymentdoc = $this->OrderPayment->find('all', array('fields' => array('OrderPayment.payment'), 'conditions' => array('OrderPayment.order_id =' . $this->request->data['order_id'])));
				$totalPayment = $paymentdoc[0]['OrderPayment']['payment'];
				/*  Email for doctor for total payment reminder */
$order_idm = $this->request->data['order_id'];
			$pro = $this->getprocedure($order_idm);	
$ordee = $this->getorderid($order_idm);

if($payment_sm != 'Payment Accepted'){
				$this->getmaildata(14);
				$this->mailBody = str_replace("{DOCNAME}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
				$this->mailBody = str_replace("{LABNAME}", $user[0]['Userdetail']['first_name'], $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $pat_name, $this->mailBody);
				$this->mailBody = str_replace("{ORDERS}", $order_sm, $this->mailBody);
$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $payment_sm, $this->mailBody);
				$this->mailBody = str_replace("{OCHARGE}", $paymentdoc[0]['OrderPayment']['payment'], $this->mailBody);
				$this->mailBody = str_replace("{DCHARGE}", $paymentdoc[1]['OrderPayment']['payment'], $this->mailBody);
				$this->mailBody = str_replace("{TOTALPAYMENT}", $totalPayment, $this->mailBody);
				$this->sendmail($this->Session->read('Auth.User.username'));
				
				
			}
			
			}
			/* 	if($this->request->data['selected_text'] == 'Cancel'){
			  $id = $this->Order->find('first',array('conditions'=>array('Order.order_id'=>$this->request->data['order_id']),'fields'=>array('Order.id')));
			  $this->Order->id = $id['Order']['id'];
			  if ($this->Order->delete()) {
			  $result = array("error"=>false,"message"=>"Order has been cancelled.");
			  }
			  }
			 */
			
			if (!empty($this->request->data['status_id'])) {
				$this->request->data['updated_by'] = $this->Session->read('Auth.User.id');
				$this->OrderStatus->create();
				if ($this->OrderStatus->save($this->request->data)) {
					if($this->request->data['status_id'] == 7){
					$result = array("error" => false, "message" => "Order charges and Order status have been updated successfully.");	
					} else if($this->request->data['status_id'] == 8){
						/*
						$id = $this->request->data['order_id'];
						
	$order_idm = $this->getorderid($id);
					$order = $this->getlabid($id);
					$doc = $this->Order->find("first",array('conditions'=>array('Order.id'=>$id)));
					
					$doc_id = $doc['Order']['doctor_id'];
					$doc_detail = $this->companydetail($doc_id);
					$state_d = $this->getstate($doc_detail['Userdetail']['state_id']);
	$doc_add = 'Doctor Name :'.$doc_detail['Userdetail']['first_name'].'<br>Address :'.$doc_detail['Userdetail']['address'].', '.$doc_detail['Userdetail']['city'].'<br>'.$state_d[0]['State']['name'].', '.$doc_detail['Userdetail']['zipcode'];
					
					$rr = unserialize($order['OrderDetail']['details']);
					$lab_id = $rr[156]['lab_id'];
	$lab_name = $this->companyname($lab_id);
					$driver_id = $this->User->find("first",array('conditions'=>array('User.id'=>$lab_id)));
					$did = $driver_id['User']['driver_assign'];
					$driver_data = $this->companydetail($did);
	$driver_email = $driver_data['User']['username'];
	$driver_name = $driver_data['Userdetail']['first_name'];
					$staff_id = $this->Session->read('Auth.User.id');
					$staff_detail = $this->companydetail($staff_id);
	$staff_name = $staff_detail['Userdetail']['first_name'];
						$lab_detail = $this->companydetail($lab_id);
						$state = $this->getstate($lab_detail['Userdetail']['state_id']);
						
	$lab_add = $lab_detail['Userdetail']['address'].', '.$lab_detail['Userdetail']['city'].'<br>'.$state[0]['State']['name'].', '.$lab_detail['Userdetail']['zipcode'];
						
				
						
						
						
						//$user = 'brijesh.kumar@the-appideas.com';
						//= $this->User->find("all",array('conditions'=>array('User.user_type'=>4)));
						$this->getmaildata(21);
						$this->mailBody = str_replace("{ORDERID}", $order_idm, $this->mailBody);
						$this->mailBody = str_replace("{DRIVER}", $driver_name, $this->mailBody);
						$this->mailBody = str_replace("{STAFFMEMBER}", $staff_name, $this->mailBody);
						$this->mailBody = str_replace("{LAB}", $lab_name, $this->mailBody);
						$this->mailBody = str_replace("{ADDRESS}", $lab_add, $this->mailBody);
						$this->mailBody = str_replace("{DADDRESS}", $doc_add, $this->mailBody);
						$this->sendmail($driver_email);
						
						*/
						
						
					$result = array("error" => false, "message" => "Order delivery charges and Order status have been updated successfully.");	
					}
					else {
					$result = array("error" => false, "message" => "Order status has been updated successfully.");
				    }
				} else {
					$result = array("error" => true, "message" => "Order status has not been updated. Please try again later.");
				}
			} else {
				$result = array("error" => true, "message" => "Please select a valid status type.");
			}
			return json_encode($result);
		}
	}

	/*
	 * @function name	: save_member_detail
	 * @purpose			: To enter deleivery charges.	 
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 13th March 2015
	 */

	function save_member_detail() {
		$this->autoRender = false;
		$this->loadModel('Order');
		$this->loadModel('OrderPayment');
		if ($this->RequestHandler->isAjax()) {
			if (isset($this->data) && !empty($this->data)) {
				$doc_id = $this->Order->find('first', array('conditions' => array('Order.id' => $this->data['OrderPayment']['order_id']), 'fields' => array('Order.doctor_id')));

				$data['OrderPayment']['order_id'] = $this->data['OrderPayment']['order_id'];
				$data['OrderPayment']['payment'] = $this->data['OrderPayment']['deliver_charge'];
				$data['OrderPayment']['payer'] = $doc_id['Order']['doctor_id'];
				$data['OrderPayment']['receiver'] = -1;


				if ($this->OrderPayment->save($data)) {
					$result = array('error' => false, 'message' => 'Order status has been saved.');
				} else {
					$result = array('error' => true, 'message' => 'Order status has not been saved.Please try later.');
				}
				return json_encode($result);
			}
		}
	}

	/*
	 * @function name	: save_lab_detail
	 * @purpose			: To enter amount for order by Lab.	 
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 14th March 2015
	 */

	function save_lab_detail() {
		$this->autoRender = false;
		$this->loadModel('OrderPayment');
		if ($this->RequestHandler->isAjax()) {
			/*if (isset($this->data) && !empty($this->data)) {
				$data['OrderPayment']['order_id'] = $this->data['OrderPayment']['order_id1'];
				$data['OrderPayment']['payment'] = $this->data['OrderPayment']['payment_amount'];
				$data['OrderPayment']['payer'] = -1;
				$data['OrderPayment']['receiver'] = $this->Session->read('Auth.User.id');
				if ($this->OrderPayment->save($data)) {
					$result = array('error' => false, 'message' => 'Order charges has been saved.');
				} else {
					$result = array('error' => true, 'message' => 'Order charges has not been saved.Please try later.');
				}
				return json_encode($result);
			}*/
			$result = array('error' => false, 'message' => 'Order charges has been saved.');
			return json_encode($result);
		}
	}


   function orderChangeRequest(){
	   	$this->autoRender = false;		
		if ($this->RequestHandler->isAjax()) {
			if (isset($this->data) && !empty($this->data)) {										
				$this->getmaildata(16);
				$this->mailBody = str_replace("{DOCNAME}",$this->request->data['LabRequest']['doc_name'] , $this->mailBody);
				$this->mailBody = str_replace("{LABNAME}",$this->Session->read('Auth.User.Userdetail.first_name') , $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $this->request->data['LabRequest']['order_id2'], $this->mailBody);
				$this->mailBody = str_replace("{CHANGES}", $this->request->data['LabRequest']['query'], $this->mailBody);							
				if($this->sendmail($this->request->data['LabRequest']['doc_email'])){			
					$this->Order->create();
					$this->Order->id = $this->request->data['LabRequest']['order_id2'];
					$data['Order']['change_request'] = $this->request->data['LabRequest']['query'];
					$this->Order->save($data);
					$result = array('error' => false, 'message' => 'Your request for order changes has been sent.');
				} else {
					$result = array('error' => true, 'message' => 'An error occured. Please try later.');
				}
				return json_encode($result);
			}
		} 
    }
	/*
	 * @function name	: downlodfiles
	 * @purpose			: To download attached order files.	 
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 13th March 2015
	 */

	function downlodfiles($file_id = NULL) {
		$this->loadModel("OrderAttachment");
		$this->OrderAttachment->recursive = -1;
		$data = $this->OrderAttachment->find("first", array("conditions" => array("OrderAttachment.id" => $file_id)));
		if (!empty($data)) {
			$fullPath = $data['OrderAttachment']['destination'];
		}
		// Must be fresh start
		if (headers_sent()) {
			die('Headers Sent');
		}
		// Required for some browsers
		if (ini_get('zlib.output_compression')) {
			ini_set('zlib.output_compression', 'Off');
		}
		// File Exists?
		if (file_exists($fullPath)) {
			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
			// Determine Content Type
			switch ($ext) {
				case "pdf": $ctype = "application/pdf";
					break;
				case "exe": $ctype = "application/octet-stream";
					break;
				case "zip": $ctype = "application/zip";
					break;
				case "doc": $ctype = "application/msword";
					break;
				case "xls": $ctype = "application/vnd.ms-excel";
					break;
				case "ppt": $ctype = "application/vnd.ms-powerpoint";
					break;
				case "gif": $ctype = "image/gif";
					break;
				case "png": $ctype = "image/png";
					break;
				case "jpeg":
				case "jpg": $ctype = "image/jpg";
					break;
				default: $ctype = "application/force-download";
			}
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"" . basename($data['OrderAttachment']['filename']) . "\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $fsize);
			ob_clean();
			flush();
			readfile($fullPath);
		}
	}

	/* end of function */

	/*
	 * @function name	: track_order
	 * @purpose			: To display the order status to the doctor.
	 * @return			: none
	 * @created by		: sukhwinder
	 * @created on		: 10th March 2015
	 */

	function track_order() {
		$this->layout = "frontend_new";
		
		if ($this->check_profile_status()) {
			$this->loadModel('OrderStatus');
			$associateData = array();
			foreach ( $this->Order->belongsTo as $key1 => $val1 ) {
				$associateData['belongsTo'][] = $key1;
			}
			foreach ( $this->Order->hasMany as $key2 => $val2 ) {
				if ( $key2 != 'OrderStatus' ) {
					$associateData['hasMany'][] = $key2;
				}
			}
	
			$this->Order->unbindModel($associateData);
			$this->conditions = array("1"=>"1");
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$orderId = $this->request->query['searchval'];
				$this->conditions = array('Order.order_manualid' => $orderId);
			}
			$this->Order->OrderStatus->virtualFields = array(
				"updated_company_by"=>"Select User.company_name from users User where User.id = OrderStatus.updated_by",
				"updated_name_by"=>"Select UserDetail.first_name from userdetails UserDetail where UserDetail.user_id = OrderStatus.updated_by",
				"deliver_charges" => "select order_payments.payment from order_payments where order_payments.receiver = -1 and order_payments.order_id = OrderStatus.order_id",
				"order_payment" => "select order_payments.payment from order_payments where order_payments.payer = -1 and order_payments.order_id = OrderStatus.order_id"
			);
					
			$this->Order->virtualFields = array(
				"order_details" =>"select details from order_details where order_details.order_id = Order.id");
				
			$this->loadModel("Status");
			$this->set("status",$this->Status->find("list",array("fields"=>array("id","title"))));
			
			$this->set('orders', $this->paginate($this->conditions));
			
			
			/*if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$orderId = $this->request->query['searchval'];
				$this->conditions = array('OrderStatus.order_id' => $orderId);
			}
				
				$this->OrderStatus->recursive = -1;
				$options['joins'] = array(
					array('table' => 'statuses',
						'alias' => 'Status',
						'type' => 'left',
						'conditions' => array(
							'OrderStatus.status_id = Status.id'
						)
					), array('table' => 'orders',
						'alias' => 'Order',
						'type' => 'left',
						'conditions' => array(
							'OrderStatus.order_id = Order.id'
						)
					)
				);
				$this->set("set_val", $this->request->query);
				$this->OrderStatus->virtualFields = array(
					"update_by" => "select (case when userdetails.last_name <> '' then concat(userdetails.first_name,' ',userdetails.last_name) else userdetails.first_name end) from userdetails where userdetails.user_id = OrderStatus.updated_by",
					"deliver_charges" => "select order_payments.payment from order_payments where order_payments.receiver = -1 and order_payments.order_id = OrderStatus.order_id",
					"order_payment" => "select order_payments.payment from order_payments where order_payments.payer = -1 and order_payments.order_id = OrderStatus.order_id",
					"order_details" =>"select details from order_details where order_details.order_id = OrderStatus.order_id"
				);
			if($this->Session->read("Auth.User.user_type") != 3){
				$options['conditions'] = array_merge($this->conditions, array("OR" => array("Order.doctor_id" => $this->Session->read("Auth.User.id"), "Order.lab_id" => $this->Session->read("Auth.User.id"))));
			}
				$options['fields'] = array('OrderStatus.*', 'Status.title', 'Order.*');
				
				$order = $this->OrderStatus->find('all', $options);
				//pr($order);
				//die;
				
				$this->set('orders', $order);*/
		} else {
			$this->render('profilestatusview');
		}
	}

	/*
	 * @function name	: admin_view
	 * @purpose			: To display the order details to the admin.
	 * @return			: none
	 * @created by		: sukhwinder
	 * @created on		: 10th March 2015
	 */

	public function admin_view($id) {
		$this->Order->id = $id;
		if ($this->Order->exists()) {
			$this->Order->virtualFields = array("status" => "select statuses.title from statuses Inner JOIN order_statuses ON statuses.id = order_statuses.status_id where order_statuses.order_id = Order.id order by order_statuses.id desc limit 1");
			$order = $this->Order->find("all", array("conditions" => array("Order.id" => $id), "fields" => array("Order.status", "Order.*", "Lab.first_name", "Lab.last_name", "Lab.address", "Lab.city", "Lab.phone", "Doctor.first_name", "Doctor.last_name", "Doctor.address", "Doctor.city", "Doctor.phone")));			
			$this->set('order', $order[0]);
		}
	}

	/*
	 * @function name	: admin_invoice_for_lab
	 * @purpose			: To display the order invoice generated by lab to the admin.
	 * @return			: none
	 * @created by		: sukhwinder
	 * @created on		: 20th March 2015
	 */

	function admin_invoice_for_lab($id) {
		$this->Order->id = $id;
		if ($this->Order->exists()) {
			$options['joins'] = array(
				array('table' => 'order_payments',
					'alias' => 'OrderPayment',
					'type' => 'Inner',
					'conditions' => array(
						'OrderPayment.order_id = Order.id'
					)
				)
			);
			$options['conditions'] = array('Order.id' => $id, 'OrderPayment.payer' => -1);
			$options['fields'] = array('Order.id', 'Lab.first_name', 'Lab.last_name', 'OrderPayment.payment', 'OrderPayment.created', 'OrderPayment.payer');
			$this->paginate = $options;
			$this->set('labInvoices', $this->paginate('Order'));
		}
	}

	/*
	 * @function name	: admin_invoice_for_doctor
	 * @purpose			: To display the order invoice by the admin to the doctor.
	 * @return			: none
	 * @created by		: sukhwinder
	 * @created on		: 20th March 2015
	 */

	function admin_invoice_for_doctor($id) {
		$this->Order->id = $id;
		if ($this->Order->exists()) {
			$options['joins'] = array(
				array('table' => 'order_payments',
					'alias' => 'OrderPayment',
					'type' => 'Inner',
					'conditions' => array(
						'OrderPayment.order_id = Order.id'
					)
				), array('table' => 'order_payments',
					'alias' => 'DeliverCharges',
					'type' => 'inner',
					'conditions' => array(
						'DeliverCharges.order_id = Order.id'
					)
				)
			);

			$options['conditions'] = array('Order.id' => $id, 'OrderPayment.payer' => -1, 'DeliverCharges.receiver' => -1);
			$options['fields'] = array('Order.id', 'Doctor.first_name', 'Doctor.last_name', 'OrderPayment.payment', 'OrderPayment.created', 'DeliverCharges.payment', 'OrderPayment.payer');
			$options['group'] = array('OrderPayment.order_id');
			$this->paginate = $options;
			$this->set('labInvoices', $this->paginate('Order'));
		}
	}
	
	function payment_history($archive = 0) {
		$this->layout = "frontend_new";
		$this->loadModel("OrderPayment");
		$this->loadModel("OrderStatus");
		$this->loadModel("Userdetail");
		$options = array();
		//$options['conditions'] =array();
		//$this->loadHelper("Paginator"=>array("url"=>"/pending-payment"));
		
		if ( $this->Session->read("Auth.User.user_type") == 1 ) {
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$this->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer != ".$this->Session->read("Auth.User.id"),"order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1","lab_name"=>"select first_name from userdetails inner join order_payments on userdetails.user_id = order_payments.receiver where order_payments.order_id = OrderPayment.order_id and order_payments.payer = -1","order_detail"=>"select details from order_details inner join order_payments on order_details.order_id = order_payments.order_id where order_payments.order_id = OrderPayment.order_id and order_payments.payer = -1");			
				$options['conditions'] = array("payer"=>$this->Session->read("Auth.User.id"),'payment_status'=>$archive);
				$options['conditions'] = array_merge($options['conditions'], array("OR" => array("OrderPayment.order_id like" => "%" . $this->request->query['searchval'] . "%")));
			} else { 					
				$this->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer != ".$this->Session->read("Auth.User.id"),"order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1","lab_name"=>"select first_name from userdetails inner join order_payments on userdetails.user_id = order_payments.receiver where order_payments.order_id = OrderPayment.order_id and order_payments.payer = -1","order_detail"=>"select details from order_details inner join order_payments on order_details.order_id = order_payments.order_id where order_payments.order_id = OrderPayment.order_id and order_payments.payer = -1");				
				$options['conditions'] = array("payer"=>$this->Session->read("Auth.User.id"),'payment_status'=>$archive);			
			}
		} else if ( $this->Session->read("Auth.User.user_type") == 2 ) {
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$this->OrderPayment->virtualFields = array("order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1");
				$options['conditions'] = array("receiver"=>$this->Session->read("Auth.User.id"),'payment_status'=>$archive);
				$options['conditions'] = array_merge($options['conditions'], array("OR" => array("OrderPayment.order_id like" => "%" . $this->request->query['searchval'] . "%")));				
			} else {
				$this->OrderPayment->virtualFields = array("order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1");
				$options['conditions'] = array("receiver"=>$this->Session->read("Auth.User.id"),'payment_status'=>$archive);
			}
		 }
		 if (isset($this->request->query) && !empty($this->request->query)) {
			$this->set("set_val", $this->request->query);
		}
		 $this->OrderPayment->unBindModel(array('belongsTo' => array('Doctor')));
		$this->paginate = $options;
		if( $archive > 0 ){
			$this->set('data',$archive);
		}
		$this->set("payments",$this->paginate("OrderPayment"));
	}

	function admin_payments($user = NULL,$archive = 0)
	{	
		$this->loadModel("OrderPayment");
		$options = array();
		$this->conditions =array();		
		if ( $user == 1 ) {
			$this->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.receiver !=  -1 ","order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1");
			$options['conditions'] = array("receiver"=>-1);						
		} else if ( $user == 2 ) {
			$this->OrderPayment->virtualFields = array("order_status"=>"select status_id from order_statuses OS where OS.order_id = OrderPayment.order_id order by id desc limit 1");
			$options['conditions'] = array("payer"=>-1);
		}
		
        /*code to perform search functionality */
		if (isset($this->data) && !empty($this->data['Payment']['searchval'])) {			
			if($this->data['Payment']['searchval'] == 'Pending') { $res = 0; } else {  $res = 1; }
			$this->Session->write('searchval', $res);
			$options['conditions'] = array_merge($options['conditions'], array("OR" => array("OrderPayment.payment_status like" => "%" . $res . "%" )));
		}	
		/* end of code to perform search functionality */
			
		$this->set('user',$user);
		$this->paginate = $options;
		if( $archive > 0 ){
			$this->set('data',$archive);
		}
		$this->set("payments",$this->paginate("OrderPayment"));
		$this->render('admin_payments');
	}
function admin_labpayments()
	 {
	$this->loadModel('Labtransaction');
		$this->set('payment',$this->Labtransaction->find('all'));
		//$this->render('admin_payments');
	}
   function payment($order_id = null) 
   { 
	   $this->loadModel("OrderDetail");
	   $this->loadModel("Carddetail");
	  
	 // print_r( $_POST['ids']);
	    $pid=$this->Session->read('uniqid');
	 	 if(!empty($_POST['ids']))
	  {
		  $amount=0;
	      $data = $_POST['ids'];
	      $order_id=$_POST['ids'];
	      
	      foreach($data as $orderid)
	      {
	        $prizearray=$this->getbudgetprize($orderid);
	      // print_r($prizearray);	exit;
	      $amount=$amount+ $prizearray; 
	   } 
	  
	   //exit; 
       }
       else
       {
	 
	   $this->layout = "frontend_new";
	   $this->loadModel('OrderPayment');
	  // $this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ");
	   $checkpayment=$this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>'-1',"order_id"=>$order_id)));	 
	
	   if ( empty($checkpayment) )
	    {
	
		   $this->Session->setFlash("Invalid Transaction.");
		   $this->redirect(array("action" => "/payment"));
	   } 
	   else if ($checkpayment['OrderPayment']['payment_status'] == 1) 
	   {
		 
		   $this->Session->setFlash("Payment has already been made.");
		    $this->redirect(array("action" => ""));
	   } 
	   else if($this->request->is('post')) 
	   {
		     
			$data = $this->request->data['OrderPayment'];
			$data['amount'] = ($checkpayment['OrderPayment']['payment']+$checkpayment['OrderPayment']['order_payment']);
			$result = $this->Stripe->charge($data);
			$this->loadModel("Transaction");
			if ( !empty($result['stripe_id']) ) {
				$status['OrderPayment']['payment_status'] = 1;
				$res = $this->OrderPayment->find('first',array('conditions'=>array('OrderPayment.payer'=>$this->Session->read("Auth.User.id"),'OrderPayment.receiver' =>-1,'OrderPayment.order_id'=>$order_id),'fields'=>array('OrderPayment.id')));
				$this->OrderPayment->id = $res['OrderPayment']['id'];
				$this->OrderPayment->save($status);				
				$transaction["Transaction"]['order_payment_id'] = $checkpayment['OrderPayment']['id'];
				$transaction["Transaction"]['payment'] = ($checkpayment['OrderPayment']['payment']+$checkpayment['OrderPayment']['order_payment']);
				$transaction["Transaction"]['payment_reference'] = $result['stripe_id'];
				$transaction["Transaction"]['orderid'] = $order_id;
				$transaction["Transaction"]['payment_method'] = 'Stripe';
				if($this->Transaction->save($transaction)) {
					$this->Session->setFlash("Payment has been made successfully.", 'default', array("class" => "success_message"));
				    $this->redirect("/archive-payments");										
				}
			}
			
			
			
		}
		if($order_id) {
			$this->loadModel('OrderPayment');
			/* code to get payment amount and order data start*/
			//$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			//$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments //where order_payments.order_id = OrderPayment.order_id");
			$checkpayment = $this->OrderPayment->find("first",array("conditions"=>array("OrderPayment.payer"=>'-1',"OrderPayment.order_id"=>$order_id)));	
			
				
			/* code to get payment amount and order data end*/
			if ( empty($checkpayment) ) {
				$this->Session->setFlash("Invalid Transaction.");
				$this->redirect(array("action" => "/payment"));
			} else if ($checkpayment['OrderPayment']['payment_status'] == 1) {
				$this->Session->setFlash("Payment has already been made.");
				$this->redirect(array("action" => "/payment"));
			} else {			
				/* code for expresschecout start */
			//	$order_detail = unserialize($checkpayment['OrderPayment']['order_detail']);			
				$obj = $this->Components->load("ExpressCheckout");
				$obj->itemnumber = $order_id;
				//print_r($order_detail);
				//print_r($obj );
				//$obj->itemdesc = $order_detail[18]['details'];		
				//echo $obj->itemname = $order_detail[18]['details'];		
				$amount =  $checkpayment['OrderPayment']['payment'];
				
				
			}
		}
		
		
	
}
		$months =array();
		 $years = array();
		$curryear = date("Y");
		$years[$curryear] = $curryear;
		for ( $i = 1;$i<=20;$i++ ) {
			if ( $i < 13) {
				$months[str_pad($i, 2, 0, STR_PAD_LEFT)] = str_pad($i, 2, 0, STR_PAD_LEFT);
			}
			$yearcount = $curryear + $i;
			$years[$yearcount] = $yearcount;
		}
		//$or_id = $order_id;
		// $_SESSION['order']=$or_id;
		$this->set(compact(array('years','months','or_id')));

		$this->layout = 'frontend_new';
		$amount = ($amount*2.9/100)+$amount+0.30;
		$amount = number_format($amount, 2);
		//$amount = money_format('%(#10n', $amount);
	//echo ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php';
require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
	//require_once ''.App::path('Vendor').'/'.'_environment.php';
	$url=Braintree_TransparentRedirect::url();
	if(!is_array($order_id))
	 {
		 
	$tr_data = Braintree_TransparentRedirect::transactionData(
	 
              array('redirectUrl' => "http://biolablogistics.com/orders/getresult/".$order_id."/".$pid,'transaction' => array('amount' => $amount, 'type' => 'sale',  'options' => array(
    'submitForSettlement' => True
  )))) ;
		  }
		  else 
		  {
			  $tr_data = Braintree_TransparentRedirect::transactionData(
			  array('redirectUrl' => "http://biolablogistics.com/orders/getresult/".$pid,'transaction' => array('amount' => $amount, 
		'type' => 'sale',  'options' => array('submitForSettlement' => True),
   ))) ;
		  }
		  $carddata=$this->Carddetail->find("first",array("conditions"=>array("Carddetail.userid"=>$this->Session->read('Auth.User.id'))));	
			
		  
				$this->set('url', $url);
				$this->set('amount', $amount);
				$this->set('tr_data',$tr_data);	
				$this->set('orderids',$order_id);
				$this->set('carddata',$carddata);
			
		        //$this->Session->set('orderids',$order_id);
		        $this->Session->write('orderids', $order_id);
	
   }
   function labpayment($order_id = null) 
   { 
	     $this->loadModel("OrderDetail");
	     	   $this->loadModel("Carddetail");
	  
	 // print_r( $_POST['ids']);
	    $pid=$this->Session->read('uniqid');
	  
	 // print_r( $_POST['ids']);
	   
	 	 if(!empty($_POST['ids']))
	  {
		  
		  $amount=0;
	      $data = $_POST['ids'];
	      $order_id=$_POST['ids'];
	      
	      foreach($data as $orderid)
	      {
	        $prizearray=$this->getbudgetprize($orderid);
	      // print_r($prizearray);	exit;
	      $amount=$amount+ $prizearray; 
	   } 
	  
	   //exit; 
       }
       else
       {
		   
	 
	   $this->layout = "frontend_new";
	   $this->loadModel('OrderPayment');
	  // $this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ");
	   $checkpayment=$this->Order->OrderPayment->find("first",array("conditions"=>array("receiver"=>'-1',"order_id"=>$order_id)));	 
	  $amount=$checkpayment['OrderPayment']['payment']; 
	   
	
}
		$months =array();
		 $years = array();
		$curryear = date("Y");
		$years[$curryear] = $curryear;
		for ( $i = 1;$i<=20;$i++ ) {
			if ( $i < 13) {
				$months[str_pad($i, 2, 0, STR_PAD_LEFT)] = str_pad($i, 2, 0, STR_PAD_LEFT);
			}
			$yearcount = $curryear + $i;
			$years[$yearcount] = $yearcount;
		}
		//$or_id = $order_id;
		// $_SESSION['order']=$or_id;
		$this->set(compact(array('years','months','or_id')));
$amount = ($amount*2.9/100)+$amount+0.30;
//$amount = money_format('%(#10n', $amount);
$amount = number_format($amount, 2);
		$this->layout = 'frontend_new';
	//echo ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php';
require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
	//require_once ''.App::path('Vendor').'/'.'_environment.php';
	$url=Braintree_TransparentRedirect::url();
	if(!is_array($order_id))
	 {
	$tr_data = Braintree_TransparentRedirect::transactionData(
	 
              array('redirectUrl' => "http://biolablogistics.com/orders/labgetresult/".$order_id."/".$pid,'transaction' => array('amount' => $amount, 'type' => 'sale',  'options' => array(
    'submitForSettlement' => True
  )))) ;
  
  
		  }
		  else 
		  {
			  $tr_data = Braintree_TransparentRedirect::transactionData(
			  array('redirectUrl' => "http://biolablogistics.com/orders/labgetresult/".$pid,'transaction' => array('amount' => $amount, 'type' => 'sale',  'options' => array(
    'submitForSettlement' => True, 'storeInVault'    => true
  )))) ;
		  }
		  
		   $carddata=$this->Carddetail->find("first",array("conditions"=>array("Carddetail.userid"=>$this->Session->read('Auth.User.id'))));	
		  
				$this->set('url', $url);
				$this->set('amount', $amount);
				$this->set('tr_data',$tr_data);	
				$this->set('orderids',$order_id);
				$this->set('carddata',$carddata);
			
		        //$this->Session->set('orderids',$order_id);
		        $this->Session->write('orderids', $order_id);
	
	
   }
   function laborderpayment($order_id = null) 
   {
$this->loadModel("Directorder");
$this->loadModel("Carddetail");
	  
	 // print_r( $_POST['ids']);
	    $pid=$this->Session->read('uniqid');
$checkpayment= $this->Directorder->find("first",array('conditions'=>array('Directorder.oid'=>$order_id)));
	  $amount=$checkpayment['Directorder']['price']; 


		$months =array();
		 $years = array();
		$curryear = date("Y");
		$years[$curryear] = $curryear;
		for ( $i = 1;$i<=20;$i++ ) {
			if ( $i < 13) {
				$months[str_pad($i, 2, 0, STR_PAD_LEFT)] = str_pad($i, 2, 0, STR_PAD_LEFT);
			}
			$yearcount = $curryear + $i;
			$years[$yearcount] = $yearcount;
		}
		$this->set(compact(array('years','months','or_id')));
$amount = ($amount*2.9/100)+$amount+.30;
//$amount = money_format('%(#10n', $amount);
$amount = number_format($amount, 2);
		$this->layout = 'frontend_new';
require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
	$url=Braintree_TransparentRedirect::url();
			  $tr_data = Braintree_TransparentRedirect::transactionData(
			  array('redirectUrl' => "http://biolablogistics.com/orders/labordergetresult/".$order_id."/".$pid,'transaction' => array('amount' => $amount, 'type' => 'sale',  'options' => array(
    'submitForSettlement' => True
  )))) ;
  
   $carddata=$this->Carddetail->find("first",array("conditions"=>array("Carddetail.userid"=>$this->Session->read('Auth.User.id'))));	
		  
		  
		//  print_r($tr_data); exit;
				$this->set('url', $url);
				$this->set('amount', $amount);
				$this->set('order', $checkpayment);
				$this->set('orderids',$order_id);
				$this->set('tr_data',$tr_data);
				$this->set('carddata',$carddata);
   }
      
   
   function change_adminpaymentstatus(){	   
	   	$this->autoRender = false;		
		$this->loadModel('OrderPayment');
		if ($this->RequestHandler->isAjax()) {
			if (isset($this->data) && !empty($this->data)) {				
				$doc_id = $this->OrderPayment->find('first', array('conditions' => array('OrderPayment.order_id' => $this->data['order_id'],'OrderPayment.payer'=>-1), 'fields' => array('OrderPayment.id')));
                  $this->OrderPayment->id = $doc_id['OrderPayment']['id'];
                  $res['OrderPayment']['payment_status'] = 1;
				if ($this->OrderPayment->save($res)) {
					$result = array('error' => false, 'message' => 'Order Payment status has been updated.');
				} else {
					$result = array('error' => true, 'message' => 'An error occured. Please try again later.');
				}
				return json_encode($result);
			}
		}
	   
   }
   
   function admin_mark_paid($id = NULL) {
	   $this->loadModel('OrderPayment');
	   $this->loadModel('Transaction');
	   $checkpayment = $this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>-1,"OrderPayment.id"=>$id)));	 
		if($this->request->is('post')){		   
			$data = $this->request->data;	
			$data['Transaction']['order_payment_id'] = $id;			
			$data['Transaction']['payment'] = $checkpayment['OrderPayment']['payment'];			
		    $this->Transaction->save($data);  
			$doc_id = $this->OrderPayment->find('first', array('conditions' => array('OrderPayment.order_id' => $checkpayment['OrderPayment']['order_id'],'OrderPayment.payer'=>-1), 'fields' => array('OrderPayment.id')));
			$this->OrderPayment->id = $doc_id['OrderPayment']['id'];
			//$res['OrderPayment']['payment_status'] = 1;
			$res['OrderPayment']['adminpayment'] = 1;
		if ($this->OrderPayment->save($res)) {
			$this->Session->setFlash("Order Payment status has been updated.");
			
		} else {
			$this->Session->setFlash("An error occured. please try later.");
		}
		$this->redirect('/admin/orders/payments/2');
	   }
   }
   
   function admin_transaction($orderpayment_id,$order_id) {
	 $this->loadModel('OrderPayment');
	 $this->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.receiver !=  -1 ");
	   $this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));	
	   $checkpayment = $this->OrderPayment->find("first",array("conditions"=>array("receiver"=>-1,"order_id"=>$order_id)));	  		   
	   $this->set('transaction',$checkpayment);  	   
   }
   
      
   function admin_transactionlab($orderpayment_id,$order_id) {
	 $this->loadModel('OrderPayment');
	 $this->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.receiver !=  -1 ");
	  $this->OrderPayment->unbindModel(array('belongsTo' => array('Doctor')));	
	   $checkpayment = $this->OrderPayment->find("first",array("conditions"=>array("payer"=>-1,"order_id"=>$order_id)));	      
	   $this->set('transaction',$checkpayment);  	   
   }
   
   function save_rating() {	   
	   $this->autoRender = false;
	   $this->loadModel('UserRating');
	   if ($this->request->is('ajax')) {				   
		$res['UserRating']['rating'] = $this->data['OrderReview']['rating'];
		$res['UserRating']['user_id'] = $this->Session->read("Auth.User.id");
		$res['UserRating']['rater_id'] = $this->data['OrderReview']['lab_id'];
		$res['UserRating']['review'] = $this->data['OrderReview']['review_text'];
		$res['UserRating']['order_id'] = $this->data['OrderReview']['order_id'];
		$res['UserRating']['pricerating'] = $this->data['OrderReview']['pricerating'];
		$already_check = $this->UserRating->find('first',array('conditions'=>array('UserRating.order_id'=>$res['UserRating']['order_id'],'UserRating.user_id'=>$res['UserRating']['user_id'])));
		if(empty($already_check)) {
			if($this->UserRating->save($res)){
				$result = array('error' => false, 'message' => 'Rating and price rating has been saved.');
			} else {
				$result = array('error' => true, 'message' => 'An error occured. Please try later.');
			}
	    } else {
			$result = array('error' => true, 'message' => 'Rating and price rating has already given for this order.');
		}
		 return json_encode($result);  
	   }   
   }
   
   
   
   
   
   
   
   
   	/*
	 * @function name	: paywithPaypal express checkout
	 * @purpose			: To make payment with paypal.
	 * @return			: none
	 * @created by		: Nishi
	 * @created on		: 4th may 2015
	 */
   
   	public function paywithPaypal($order_id = NULL) {
		$this->autoRender = false;
		if($order_id) {		
			$this->loadModel('OrderPayment');
			/* code to get payment amount and order data start*/
			$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ","order_detail"=>"select details from order_details where order_details.order_id = OrderPayment.order_id and OrderPayment.receiver = -1");
			$checkpayment = $this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>$this->Session->read("Auth.User.id"),"order_id"=>$order_id)));			
			/* code to get payment amount and order data end*/
			if ( empty($checkpayment) ) {
				$this->Session->setFlash("Invalid Transaction.");
				$this->redirect(array("action" => "/payment"));
			} else if ($checkpayment['OrderPayment']['payment_status'] == 1) {
				$this->Session->setFlash("Payment has already been made.");
				$this->redirect(array("action" => "/payment"));
			} else {			
				/* code for expresschecout start */
				$order_detail = unserialize($checkpayment['OrderPayment']['order_detail']);			
				$obj = $this->Components->load("ExpressCheckout");
				$obj->itemnumber = $order_id;
				$obj->itemdesc = $order_detail[18]['details'];		
				$obj->itemname = $order_detail[18]['details'];		
				$obj->itemamt = $checkpayment['OrderPayment']['payment'] + $checkpayment['OrderPayment']['order_payment'];
				$obj->returnurl = SITE_LINK."orders/getresult/".$order_id;  // step3: Required.  URL to which the customer will be returned after returning from PayPal. 
				$obj->cancelurl = SITE_LINK."orders/payment/".$order_id;
				if ( $obj->setExpressCheckout() ) {			// step1, To set basic fields like brand name item number etc.
					$this->redirect($obj->result_url);		// step2, redirect to result_url that returned from component setExpressCheckout() function. This  returned us with the result, token and payer id and redirect to $obj->returnurl        		
				} else {	
					$this->Session->setFlash("You have been cancelled the transaction.");			
					$this->redirect($obj->cancelurl);
				}			
			}
		}		
	}	
	
	
	
	
	public function paybycheque()
	{
		$this->loadModel('OrderPayment');		
		$this->loadModel('Transaction');
		$this->loadModel('OrderRequest');
		
		$patient_mm='';
		$pro ='';
		$ordee2='';
		$pri2='';
		if($this->request->is('post'))
		{		   
		$data = $this->request->data;
		
		$tid=uniqid();
		if(preg_match('/\s/',$data['orderids']))
		{
			$ids=explode(' ',$data['orderids']);
		}
		else
		{
			$ids=$data['orderids'];
		}
			if(is_array($ids))
			{
				$ordee = '';
				$pri = '';
				foreach($ids as $orderid)
				{
				
		    $status['OrderPayment']['payment_status'] = 1;
			$res = $this->OrderPayment->find('first',array('conditions'=>array('OrderPayment.payer'=>'-1','OrderPayment.order_id'=>$orderid),'fields'=>array('OrderPayment.id')));
			$this->OrderPayment->id = $res['OrderPayment']['id'];
		    $this->OrderPayment->updateAll(array('payment_status'=>1), array('OrderPayment.order_id'=>$orderid));			
			/* code to get payment amount and order data start*/
			$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			//$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ","order_detail"=>"select details from order_details where order_details.order_id = OrderPayment.order_id and OrderPayment.receiver = -1");
			$checkpayment = $this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>'-1',"order_id"=>$orderid)));
			//print_r($checkpayment );exit; 
			 $transactiondata["Transaction"]['order_payment_id'] = $checkpayment['OrderPayment']['id'];
			$transactiondata["Transaction"]['payment'] = $checkpayment['OrderPayment']['payment'] ;
			$transactiondata["Transaction"]['orderid'] = $orderid;
			 $transactiondata["Transaction"]['payment_reference'] = $tid; //returned transaction key that we have sent from expresscheckout component.
			$transactiondata["Transaction"]['payment_method'] = 'By Cheque';
			$transactiondata["Transaction"]['chequenumber'] =$data['cqnumber'];
			
			$patient_mm .= $this->getpatient($orderid)."\n";			
			$pro .= $this->getprocedure($orderid)."\n";
			$ordee .= $this->getorderid($orderid)."\n";
			$pri .= '$ '.$this->getbudgetprize($orderid)."\n";
			// $this->Transaction->saveall($transactiondata);
		  } 
			$patient_mm2 =nl2br($patient_mm);
			$pro2=nl2br($pro);
			$ordee2 =nl2br($ordee);
			$pri2 =nl2br($pri);
		
		
		$orderlink = "<a href=" . SITE_LINK . "preview-orders>Click Here</a>";
		$pay_status = 'Payment Accepted';
		$user = $this->Session->read('Auth.User.username');
		$this->getmaildata(20);
				
				$total_pri = $checkpayment['OrderPayment']['payment'];
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee2, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm2, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tid, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro2, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $pri2, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
		        $this->sendmail($user);
		        $this->Session->setFlash("Payment has been made successfully.", 'default', array("class" => "success_message"));
				 return $this->redirect("/preview-orders");
	
	}
			
			
			
			else
			{
				if($data['type2'] == 'laborder'){
					$this->loadModel('Labtransaction');
					$this->loadModel('Directorder');
					
					$order_id = $data['orderids'];
					
		$res = $this->Directorder->find('first',array('conditions'=>array('Directorder.oid'=>$order_id)));
		$amount = $res['Directorder']['price'];
		$transactiondata['Labtransaction']['check_recieved'] = 10;
		$transactiondata["Labtransaction"]['pay_amount'] = $amount;
			$transactiondata["Labtransaction"]['order_id'] = '#'.$order_id;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'By Cheque';
			$transactiondata["Labtransaction"]['lab_name'] = $this->Session->read('Auth.User.company_name');
			 $this->Labtransaction->save($transactiondata);

$this->Directorder->updateAll(array('payment_status' => 1), array('Directorder.oid'=>$order_id));
				
		$this->Session->setFlash("Payment has been made successfully.", 'default', array("class" => "success_message"));
				return $this->redirect("/orders/directorderlist");
				}
				 
			$order_id=$data['orderids']; 
			$status['OrderPayment']['payment_status'] = 1;
			$res = $this->OrderPayment->find('first',array('conditions'=>array('OrderPayment.payer'=>$this->Session->read("Auth.User.id"),'OrderPayment.order_id'=>$order_id),'fields'=>array('OrderPayment.id')));
			$this->OrderPayment->id = $res['OrderPayment']['id'];
			$this->OrderPayment->updateAll(array('payment_status'=>1), array('OrderPayment.order_id'=>$order_id));	
			/* code to get payment amount and order data start*/
			$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			//$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ","order_detail"=>"select details from order_details where order_details.order_id = OrderPayment.order_id and OrderPayment.receiver = -1");
			$checkpayment = $this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>'-1',"order_id"=>$order_id)));
			//print_r($checkpayment );exit; 
			$transactiondata["Transaction"]['order_payment_id'] = $checkpayment['OrderPayment']['id'];
			$transactiondata["Transaction"]['payment'] = $checkpayment['OrderPayment']['payment'];
			$transactiondata["Transaction"]['orderid'] = $order_id;
			$transactiondata["Transaction"]['payment_reference'] = $tid; //returned transaction key that we have sent from expresscheckout component.
			$transactiondata["Transaction"]['payment_method'] = 'By Cheque';
			$transactiondata["Transaction"]['chequenumber'] =$data['cqnumber'];
		
            $patient_mm = $this->getpatient($order_id);	
            $pro = $this->getprocedure($order_id);
            $ordee = $this->getorderid($order_id);
            $total_pri = $checkpayment['OrderPayment']['payment'];
            $this->Transaction->save($transactiondata);
            $orderlink = "<a href=" . SITE_LINK . "preview-orders>Click Here</a>";
				$patient_mm = $this->getpatient($order_id);
				//$labid = $this->OrderRequest->find("first",array("conditions"=>array("OrderRequest.id"=>$order_id)));
				//$lab_id_mm = $labid['OrderRequest']['lab_id'];
				//$lab_name_mm = $this->companyname($lab_id_mm);
				$total_pri = $checkpayment['OrderPayment']['payment'];
				$pay_status = 'Payment Accepted';
				$tan = $transactiondata["Transaction"]['payment_reference'];
				$user = $this->Session->read('Auth.User.username');
				$this->getmaildata(20);
				
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $order_id, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tan, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $total_pri, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
				$this->sendmail($user);
				$this->Session->setFlash("Payment has been made successfully.", 'default', array("class" => "success_message"));
				return $this->redirect("/preview-orders");
				
		        
				
			}
				
		}
		
	}		
	public function getresult($order_id=null,$pid=null)
	 {
		ob_start();
		$order_id=$this->Session->read('orderids');		
		$this->loadModel('OrderPayment');		
		$this->loadModel('Transaction');
		$this->loadModel('OrderRequest');
		$this->loadModel('Carddetail');
		require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
        $result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
        $this->layout = 'frontend_new';
        $transaction = $result->transaction;
        $patient_mm = '';
        $pro = '';
        $ordee = '';
        $pri = '';
       if($result->success==1)
       {
		  
		   $mask_card = $result->transaction->creditCardDetails->maskedNumber;
		   
	$this->Carddetail->updateAll(array('mask_number' => "'$mask_card'"), array('Carddetail.userid' => $this->Session->read('Auth.User.id'))); 
		 
        $tid= $transaction->id;
        $this->Session->delete('uniqid');
	   }
	   else
	   {
		   if($this->Session->read('uniqid')!='')
		   {
		 $this->Carddetail->deleteAll(array('Carddetail.userid' => $this->Session->read('Auth.User.id'),'Carddetail.uniqid'=>$this->Session->read('uniqid')));  
		$this->Session->delete('uniqid');
	       }
	   }
        //print_r($order_id);
        if($result->success==1)
        {
		
			if(is_array($order_id))
			{
				
				foreach($order_id as $orderid)
				{
				
		 $status['OrderPayment']['payment_status'] = 1;
			$res = $this->OrderPayment->find('first',array('conditions'=>array('OrderPayment.payer'=>'-1','OrderPayment.order_id'=>$orderid),'fields'=>array('OrderPayment.id')));
			$this->OrderPayment->id = $res['OrderPayment']['id'];
			$this->OrderPayment->updateAll(array('payment_status'=>1), array('OrderPayment.order_id'=>$orderid));/* code to get payment amount and order data start*/
			//$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			//$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ","order_detail"=>"select details from order_details where order_details.order_id = OrderPayment.order_id and OrderPayment.receiver = -1");
			$checkpayment = $this->OrderPayment->find("first",array("conditions"=>array("OrderPayment.payer"=>'-1',"OrderPayment.order_id"=>$orderid)));
			$transactiondata["Transaction"]['order_payment_id'] = $checkpayment['OrderPayment']['id'];
			$transactiondata["Transaction"]['payment'] = $checkpayment['OrderPayment']['payment'] ;
			$transactiondata["Transaction"]['orderid'] = $orderid;
			 $transactiondata["Transaction"]['payment_reference'] = $tid; //returned transaction key that we have sent from expresscheckout component.
			$transactiondata["Transaction"]['payment_method'] = 'Braintree';
			$patient_mm .= $this->getpatient($orderid)."\n";			
			$pro .= $this->getprocedure($orderid)."\n";
			$ordee .= $this->getorderid($orderid)."\n";
			$pri .= '$ '.$this->getbudgetprize($orderid)."\n";
			// $this->Transaction->saveall($transactiondata);
		} 
			$patient_mm2 =nl2br($patient_mm);
			$pro2=nl2br($pro);
			$ordee2 =nl2br($ordee);
			$pri2 =nl2br($pri);
		$orderlink = "<a href=" . SITE_LINK . "preview-orders>Click Here</a>";
		$pay_status = 'Payment Accepted';
		$user = $this->Session->read('Auth.User.username');
		$this->getmaildata(20);
				$total_pri = $checkpayment['OrderPayment']['payment'];
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee2, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm2, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tid, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro2, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $pri2, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
		        $this->sendmail($user);
		
	}
	else
	{
		$status['OrderPayment']['payment_status'] = 1;
			$res = $this->OrderPayment->find('first',array('conditions'=>array('OrderPayment.payer'=>'-1','OrderPayment.order_id'=>$order_id),'fields'=>array('OrderPayment.id')));
			$this->OrderPayment->id = $res['OrderPayment']['id'];
			$this->OrderPayment->updateAll(array('payment_status'=>1), array('OrderPayment.order_id'=>$order_id));	
			/* code to get payment amount and order data start*/
			$this->OrderPayment->unbindModel(array('belongsTo' => array('Lab')));
			//$this->Order->OrderPayment->virtualFields = array("order_payment"=>"select payment from order_payments where order_payments.order_id = OrderPayment.order_id and order_payments.payer =  -1 ","order_detail"=>"select details from order_details where order_details.order_id = OrderPayment.order_id and OrderPayment.receiver = -1");
			$checkpayment = $this->Order->OrderPayment->find("first",array("conditions"=>array("payer"=>'-1',"order_id"=>$order_id)));
			//print_r($checkpayment );exit; 
			$transactiondata["Transaction"]['order_payment_id'] = $checkpayment['OrderPayment']['id'];
			$transactiondata["Transaction"]['payment'] = $checkpayment['OrderPayment']['payment'];
			$transactiondata["Transaction"]['orderid'] = $order_id;
			$transactiondata["Transaction"]['payment_reference'] = $tid; 
			$transactiondata["Transaction"]['payment_method'] = 'Braintree';
            $patient_mm = $this->getpatient($order_id);	
            $pro = $this->getprocedure($order_id);
            $ordee = $this->getorderid($order_id);
            $total_pri = $checkpayment['OrderPayment']['payment'];
            $this->Transaction->save($transactiondata);
            $orderlink = "<a href=" . SITE_LINK . "preview-orders>Click Here</a>";
				$patient_mm = $this->getpatient($order_id);
				$total_pri = $checkpayment['OrderPayment']['payment'] ;
				$pay_status = 'Payment Accepted';
				$tan = $transactiondata["Transaction"]['payment_reference'];
				$user = $this->Session->read('Auth.User.username');
				$this->getmaildata(20);
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $order_id, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tan, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $total_pri, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
				$this->sendmail($user);   
	}
			$this->Session->setFlash("Payment has been made successfully.", 'default', array("class" => "success_message"));
				$this->redirect("/preview-orders");

		}
		else
		{
			//$message= $result->message;
			//$this->redirect(array("action" => "/payment"));
			$this->Session->setFlash($result->message, 'default', array("class" => "error_message"));
			$this->redirect("/preview-orders");
		}
	}
	
	
	public function labgetresult($order_id=null,$pid=null)
	 {
		ob_start();
		$order_id=$this->Session->read('orderids');		
		$this->loadModel('OrderPayment');		
		$this->loadModel('Labtransaction');
		$this->loadModel('OrderRequest');
		$this->loadModel('Carddetail');
		require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
        $result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
        $this->layout = 'frontend_new';
        $transaction = $result->transaction;
        $patient_mm = '';
        $pro = '';
        $ordee = '';
        $pri = '';
       if($result->success==1)
       {
         $mask_card = $result->transaction->creditCardDetails->maskedNumber;
		   
	$this->Carddetail->updateAll(array('mask_number' => "'$mask_card'"), array('Carddetail.userid' => $this->Session->read('Auth.User.id'))); 
		 
        $tid= $transaction->id;
        $this->Session->delete('uniqid');
       
	   }
	   else
	   {
		     if($this->Session->read('uniqid')!='')
		   {
		 $this->Carddetail->deleteAll(array('Carddetail.userid' => $this->Session->read('Auth.User.id'),'Carddetail.uniqid'=>$this->Session->read('uniqid')));  
		$this->Session->delete('uniqid');
	       }
	   }
        //print_r($order_id);
        if($result->success==1)
        {
		
			if(is_array($order_id))
			{
				
				foreach($order_id as $orderid)
				{
				
		      $transactiondata['Labtransaction']['check_recieved'] = 1;
		 $transactiondata["Labtransaction"]['pay_amount'] = $this->getdeliveryprize($order_id);
		
			$transactiondata["Labtransaction"]['order_id'] = $order_id;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'Braintree';
			$transactiondata["Labtransaction"]['lab_name'] = $this->Session->read('Auth.User.company_name');
			
			
			
			$patient_mm .= $this->getpatient($order_id)."\n";			
			$pro .= $this->getprocedure($order_id)."\n";
			$ordee .= $this->getorderid($order_id)."\n";
			$pri .= '$ '.$this->getdeliveryprize($order_id)."\n";
			 $this->Labtransaction->save($transactiondata);
		    } 
			$patient_mm2 =nl2br($patient_mm);
			$pro2=nl2br($pro);
			$ordee2 =nl2br($ordee);
			$pri2 =nl2br($pri);
		$orderlink = "<a href=" . SITE_LINK . "view-orders>Click Here</a>";
		$pay_status = 'Payment Accepted';
		$user = $this->Session->read('Auth.User.username');
		$this->getmaildata(20);
				//$total_pri = $checkpayment['OrderPayment']['payment'];
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee2, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm2, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tid, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro2, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $pri2, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
		        $this->sendmail($user);
		
	}
	else
	{
		
		
		  $transactiondata['Labtransaction']['check_recieved'] = 1;
		 $transactiondata["Labtransaction"]['pay_amount'] = $this->getdeliveryprize($order_id);
		
			$transactiondata["Labtransaction"]['order_id'] = $order_id;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'Braintree';
			$transactiondata["Labtransaction"]['lab_name'] = $this->Session->read('Auth.User.company_name');
			 $this->Labtransaction->save($transactiondata);
			
				$patient_mm = $this->getpatient($order_id);			
			$pro = $this->getprocedure($order_id);
			$ordee = $this->getorderid($order_id);
			$pri = '$ '.$this->getdeliveryprize($order_id);
		$orderlink = "<a href=" . SITE_LINK . "view-orders>Click Here</a>";
		$pay_status = 'Payment Accepted';
		$user = $this->Session->read('Auth.User.username');
		$this->getmaildata(20);
				//$total_pri = $checkpayment['OrderPayment']['payment'];
				$this->mailBody = str_replace("{USER}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ORDERID}", $ordee, $this->mailBody);
				$this->mailBody = str_replace("{PATIENT}", $patient_mm, $this->mailBody);
				$this->mailBody = str_replace("{TAN}", $tid, $this->mailBody);
				$this->mailBody = str_replace("{PROCEDURE}", $pro, $this->mailBody);
				$this->mailBody = str_replace("{PRIZE}", $pri, $this->mailBody);
				$this->mailBody = str_replace("{PAYMENTS}", $pay_status, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
		        $this->sendmail($user);
		       // $this->sendmail('kidjusticesyed@gmail.com');
	}
			$this->Session->setFlash(" Delivery Payment has been made successfully.", 'default', array("class" => "success_message"));
				$this->redirect("/view-orders");

		}
		else
		{
			//$message= $result->message;
			//$this->redirect(array("action" => "/payment"));
			$this->Session->setFlash($result->message, 'default', array("class" => "error_message"));
			$this->redirect("/view-orders");
		}
	}
public function labordergetresult($order_id=null)
	 {
		ob_start();
		$this->loadModel('Labtransaction');
		$this->loadModel('Directorder');
		$this->loadModel('Carddetail');
		require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
        $result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
        $this->layout = 'frontend_new';
        $transaction = $result->transaction;
        $patient_mm = '';
        $pro = '';
        $ordee = '';
        $pri = '';
       if($result->success==1)
       {
        $tid= $transaction->id;
        $price = $transaction->amount;
        $mask_card = $result->transaction->creditCardDetails->maskedNumber;
		   
	$this->Carddetail->updateAll(array('mask_number' => "'$mask_card'"), array('Carddetail.userid' => $this->Session->read('Auth.User.id'))); 
       
	   }
	    else
	   {
		     if($this->Session->read('uniqid')!='')
		   {
		 $this->Carddetail->deleteAll(array('Carddetail.userid' => $this->Session->read('Auth.User.id'),'Carddetail.uniqid'=>$this->Session->read('uniqid')));  
		$this->Session->delete('uniqid');
	       }
	   }
        if($result->success==1)
        {
			

		  $transactiondata['Labtransaction']['check_recieved'] = 5;
		$transactiondata["Labtransaction"]['pay_amount'] = $price;
			$transactiondata["Labtransaction"]['order_id'] = '#'.$order_id;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'Braintree';
			$transactiondata["Labtransaction"]['lab_name'] = $this->Session->read('Auth.User.company_name');
			 $this->Labtransaction->save($transactiondata);

$this->Directorder->updateAll(array('payment_status' => 1), array('Directorder.oid'=>$order_id));

$orderlink = "<a href=" . SITE_LINK . "/orders/directorderlist>Click Here</a>";
$user = $this->Session->read('Auth.User.username');
$this->getmaildata(24);
				//$total_pri = $checkpayment['OrderPayment']['payment'];
				$this->mailBody = str_replace("{LAB}", $this->Session->read('Auth.User.Userdetail.first_name'), $this->mailBody);
				$this->mailBody = str_replace("{ID}", $order_id, $this->mailBody);
				$this->mailBody = str_replace("{LINKURL}", $orderlink, $this->mailBody);
		        $this->sendmail($user);


			$this->Session->setFlash(" Delivery Payment has been made successfully.", 'default', array("class" => "success_message"));
				$this->redirect("/orders/directorderlist");

		}
		else
		{
			$this->Session->setFlash($result->message, 'default', array("class" => "error_message"));
			$this->redirect("/orders/directorderlist");
		}
	}

function allnotification($user_id = null){
	$this->loadModel("Notification");
$this->layout = "frontend_new";
$qry = $this->Notification->find("all",array("conditions"=>array("Notification.show_id"=>$user_id)));
	if($qry){
	$this->set('notification_data',$qry);
	}

}

	function mahitable(){
		$this->loadModel("OrderStatus");
		//$this->OrderStatus->find("all",array("conditions"=>array("OrderStatus.order_id"=>$id)));
		//$this->set('data', 'goog');
        $this->autoRender = false;
        $res = $this->request->data['res'];
        $res1 = $this->OrderStatus->find("all",array("conditions"=>array("OrderStatus.order_id"=>$res)));
if($res1){
   echo '<table id="result_table" class="sortable table table-striped">';
   echo '<thead>';
   echo '<th>Date</th>';
   echo '<th>Order Status</th>';
   echo '</thead>';
   echo '<tbody>';
   for($i=0;$i<count($res1);$i++){
echo '<tr>';
echo '<td>';

echo (date("m/d/y",strtotime($res1[$i]['OrderStatus']['modified'])));
echo '</td>';
echo '<td>';
if($res1[$i]['Status']['title'] == 'Lab Rejected' Or $res1[$i]['Status']['title'] == 'Action Required'){
echo '<span style="color:red;">'.$res1[$i]['Status']['title'].'</span>';
}
elseif($res1[$i]['Status']['title'] == 'biolab Delivered'){
	echo '<span style="color:green;">'.$res1[$i]['Status']['title'].'</span>';
}else{
	echo $res1[$i]['Status']['title'];
}
echo '</td>';
echo '</tr>';
}
echo '</tbody>';
	echo '</table>';
	}

else{
	echo '<p>Waiting for Status....</p>';
}
}
function mahiarchive(){
	$this->loadModel("OrderStatus");
	$this->autoRender = false;
	$res = $this->request->data['res'];
	$st = 1;
	$qry = $this->OrderStatus->updateAll(array('status'=>$st), array('OrderStatus.order_id'=>$res));
	if($qry){
	echo 'Done';
	}
}
function mahiarchiverem(){
	$this->loadModel("OrderStatus");
	$this->autoRender = false;
	$res = $this->request->data['res'];
	$st = 0;
	$qry = $this->OrderStatus->updateAll(array('status'=>$st), array('OrderStatus.order_id'=>$res));
	if($qry){
	echo 'Done';
	}
}
function mahiarchivelab(){
	$this->loadModel("OrderStatus");
	$this->autoRender = false;
	$res = $this->request->data['res'];
	$st = 1;
	$qry = $this->OrderStatus->updateAll(array('lstatus'=>$st), array('OrderStatus.order_id'=>$res));
	if($qry){
	echo 'Done';
	}
}
function mahiarchiveremlab(){
	$this->loadModel("OrderStatus");
	$this->autoRender = false;
	$res = $this->request->data['res'];
	$st = 0;
	$qry = $this->OrderStatus->updateAll(array('lstatus'=>$st), array('OrderStatus.order_id'=>$res));
	if($qry){
	echo 'Done';
	}
}
function anotification(){
	$this->loadModel("Notification");
	$this->autoRender = false;
	$uid = $this->request->data['uid'];
	$st = 0;
	$qry = $this->Notification->updateAll(array('status'=>$st), array('Notification.show_id'=>$uid));
	if($qry){
	echo 'Done';
	}
}

function notification(){
	$this->loadModel("Notification");
	$this->autoRender = false;
	$id = $this->request->data['id'];
	$uid = $this->request->data['uid'];
	$status_id = $this->request->data['status_id'];
	$st = 0;
	$qry = $this->Notification->updateAll(array('status'=>$st), array('Notification.show_id'=>$uid,'Notification.order_id'=>$id,'Notification.status_id'=>$status_id));
	if($qry){
	echo 'Done';
	}
}

function delnotification(){
	$this->loadModel("Notification");
	$this->autoRender = false;
	$id = $this->request->data['id'];
	$uid = $this->request->data['uid'];
	$status_id = $this->request->data['status_id'];
	$st = 0;
	$qry = $this->Notification->deleteAll(array('Notification.show_id'=>$uid,'Notification.order_id'=>$id,'Notification.status_id'=>$status_id));
	if($qry){
	echo 'Done';
	}
}
function update_check_status()
{
	$this->loadModel('Order');
	
		if ($this->request->is('ajax')) {
         $id=$this->request->data['payment_id']; 
         //echo $id;
	
$this->Order->updateAll(
    array('check_recieved' => "1"), array('Order.id'=>$id));
   // echo "Done";
    
}
}
function update_priority(){
	$this->loadModel("Notification");
	$this->loadModel('Order');
	$this->autoRender = false;
	$id = $this->request->data['order_id'];
	$s_id = $this->request->data['status_id'];
		$not_order['Notification']['order_id'] = $id;
		$not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
		$not_order['Notification']['status_id'] = 20;
		$not_order['Notification']['status'] = 1;
		$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
		$not_order['Notification']['show_id'] = 13;
		$this->Notification->save($not_order);
		
	$qry = $this->Order->updateAll(array('priority' => $s_id), array('Order.id'=>$id));
	if($qry){
	echo 'Done';
	}
}
function update_recipient(){
	$this->loadModel("Notification");
	$this->loadModel('Order');
	$this->autoRender = false;
	$id = $this->request->data['order_id'];
	$s_id = $this->request->data['status_id'];
		$not_order['Notification']['order_id'] = '#'.$id;
		$not_order['Notification']['user_id'] = $this->Session->read("Auth.User.id");
		$not_order['Notification']['status_id'] = 21;
		$not_order['Notification']['status'] = 1;
		$not_order['Notification']['update_on'] = date("Y-m-d H:i:s");
		$not_order['Notification']['show_id'] = 13;
		$this->Notification->save($not_order);
	$qry = $this->Order->updateAll(array('recipient' => $s_id), array('Order.id'=>$id));
	if($qry){
	echo 'Done';
	}
}
function lab_doctor(){
	$this->loadModel("Labdoctor");
	$this->autoRender = false;
	$l_doctor = $this->Labdoctor->find('all');
echo '<select name="labdoctor" id="labdoctor" class="form-control" style="opacity:1;">';
echo '<option value="">Select Action</option>';
	foreach($l_doctor as $ld_data){
		echo '<option value="'.$ld_data['Labdoctor']['id'].'">'.$ld_data['Labdoctor']['name'].'</option>';
	}
echo '</select>';
}
function adddoctor(){
	$this->loadModel("Labdoctor");
	$this->loadModel("Order");
	$data = $this->data;
	$ldoc_id = $data['labdoctor'];
	$ordee =  $data['order']['orderid'];
	$qry = $this->Order->updateAll(array('labdoctor' => $ldoc_id), array('Order.id'=>$ordee));
	if($qry){
	$this->Session->setFlash("Doctor select successfully.", 'default', array("class" => "success_message"));
					$this->redirect('/view-orders');
				}
}

function notesave()
{
$this->loadModel('Order');
    $data=$this->request->data;
    //print_r($data);
    $n_data = $data['note']; 
    $id = $data['order_id'];     
$this->Order->updateAll(array('note' => "'$n_data'"), array('Order.id'=>$id));
$this->Session->setFlash("Your note has been save successfully.", 'default', array("class" => "success_message"));
$this->redirect("/view-orders");
}


function directorder()
{

	$this->loadModel('Directorder');
	$this->layout = 'frontend_new';
	$this->loadModel('State');
	$this->loadModel('Favorite');
	$this->loadModel('Userdetail');
	//pr($this->Session->read("Auth.User.Userdetail"));die;
	$labId = $this->Session->read("Auth.User.id");
	$labName = $this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name");
	$this->set("labName",$labName);
		$this->State->recursive = -1;
		$this->set("state", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));
   
    //get favorite doctors
	$entry = $this->Favorite->find('list', array(
    'fields' => array('Favorite.doctor_id'),
    'conditions' => array('Favorite.lab_id' => $labId,
                           'Favorite.status' => 1,
    	),
));
	//echo '<pre>';print_r($entry);die;
	
		$docList = $this->Userdetail->find('list',array("conditions" => array("Userdetail.user_id" => array_values($entry)), "fields" => array("Userdetail.user_id", "Userdetail.doctorService")));
        //echo '<pre>';print_r($docList);die;
        $this->set("docList",$docList);
		
		

if($this->request->is('post'))
	{
	if($this->data!=null)
	$this->Directorder->create();

	$order = $this->data;
	
	$order['Directorder']['lab_id'] = $this->Session->read('Auth.User.id');
	if(empty($order['Directorder']['dropoff'])){
		$order['Directorder']['dropoff'] = '0';
	}
	$pick_lat = $order['Directorder']['pick_lat'];
	$pick_lng = $order['Directorder']['pick_lng'];
	$drop_lat = $order['Directorder']['drop_lat'];
	$drop_lng = $order['Directorder']['drop_lng'];
	
	$order['Directorder']['distance'] = $this->distance($pick_lat, $pick_lng, $drop_lat, $drop_lng, "M");
	
	$order['Directorder']['doctor_id'] = ($order['Directorder']['doctor_id'] != '')?$order['Directorder']['doctor_id']:0;
	//pr($order);die;
	if($order['Directorder']['priority'] == 'Rush'){
		$del = $this->getdelivery($order['Directorder']['lab_id']);
		if($del == ''){
		$distance = $this->getzipdistance($order['Directorder']['pickup_zip'], $order['Directorder']['dropoff_zip']);
		$rush_price = (0.5)*(str_replace(' mi','',$distance));
		$delivery = ($order['Directorder']['pickup']*5)+($order['Directorder']['dropoff']*5);
		$order['Directorder']['price'] = $rush_price + $delivery;
		}else{
			
		$distance = $this->getzipdistance($order['Directorder']['pickup_zip'], $order['Directorder']['dropoff_zip']);
		$rush_price = ($del['Deliverycharge']['mile_amt'])*(str_replace(' mi','',$distance));
		$delivery = ($order['Directorder']['pickup']*$del['Deliverycharge']['delivery_amt'])+($order['Directorder']['dropoff']*$del['Deliverycharge']['delivery_amt']);
		$order['Directorder']['price'] = $rush_price + $delivery;
		}
	}else{
	$order['Directorder']['price'] = ($order['Directorder']['pickup']*5)+($order['Directorder']['dropoff']*5);
}
//print_r($order);die;
//echo '<pre>';print_r($order);die;

	$this->Directorder->save($order);

	$order_id = '#'.$this->Directorder->getLastInsertId();
					$state2 = $this->getstate($order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			$to = $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'];
					$state1 = $this->getstate($order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			$from = $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip'];
	$pri =$order['Directorder']['priority'];
	$rec =$order['Directorder']['recipient'];
	$lab_name = $this->Session->read('Auth.User.company_name');
	$this->getmaildata(23);
					$this->mailBody = str_replace("{ORDERID}", $order_id, $this->mailBody);
					$this->mailBody = str_replace("{PICKUPA}", $from, $this->mailBody);
					$this->mailBody = str_replace("{DROPA}", $to, $this->mailBody);
					$this->mailBody = str_replace("{PRI}", $pri, $this->mailBody);
					$this->mailBody = str_replace("{PICKUP}", $order['Directorder']['pickup'], $this->mailBody);
					$this->mailBody = str_replace("{DROP}", $order['Directorder']['dropoff'], $this->mailBody);
					$this->mailBody = str_replace("{PRICE}", $order['Directorder']['price'], $this->mailBody);
					$this->mailBody = str_replace("{REC}", $rec, $this->mailBody);
					$this->mailBody = str_replace("{LAB}", $lab_name, $this->mailBody);
					
					//$this->sendmail(ADMIN_EMAIL);
	
		$this->Session->setFlash("Your order has been submitted successfully.", 'default', array("class" => "success_message"));
		$this->redirect('directorderlist');
	}
	
}
function directorderlist($archive = null) {
	$locationData = array();
    $destination = '';
    $origin = '';
	$start_end = array();
	$driverOrder = array();
	
    $this->loadModel('Directorder');
$this->loadModel('UserDetail');
$this->loadModel('User');
$this->loadModel('Gpslocation');
    $this->layout = 'frontend_new';
	
	/*$this->Directorder->bindModel(array(
	          'belongsTo' => array(
			          'DoctorDetail' => array(
					       'className' => 'User',
						   'foreignKey' => 'doctor_id',
					  )
			  )
	));
	
	$data = $this->Directorder->find('all', array(
                'contain' => array('DoctorDetail', 'DoctorDetail.Userdetail'=>array('fields'=>array('Userdetail.first_name')))
 ));*/
	
	
    if($this->Session->read('Auth.User.user_type') == 1){
		$this->Directorder->bindModel(array(
	          'belongsTo' => array(
			          'OrderDetail' => array(
					       'className' => 'User',
						   'foreignKey' => 'lab_id',
					  )
			  )
	    ));
        if ($archive == null) {
        $dodata=$this->Directorder->find("all",array('conditions'=>array(
		'NOT'=>array('Directorder.payment_status' => 1), 
		'Directorder.doctor_id'=>$this->Session->read('Auth.User.id')),
		'contain' => array('OrderDetail', 'OrderDetail.Userdetail'=>array('fields'=>array('Userdetail.full_name'))),
		'order'=>array('Directorder.oid'=>'desc')));
		
		
	    } else {
	        $dodata=$this->Directorder->find("all",array('conditions'=>array(
			'Directorder.payment_status'=>1, 
			'Directorder.doctor_id'=>$this->Session->read('Auth.User.id')),
			'contain' => array('OrderDetail', 'OrderDetail.Userdetail'=>array('fields'=>array('Userdetail.full_name'))),
			'order'=>array('Directorder.oid'=>'desc')));      		
	    }
		
		//Map co-ordinates
		$i = 0;
		foreach ($dodata as $data) {
			$dodata[$i]['Directorder']['lab'] = $this->UserDetail->find("first", array('conditions'=> array('UserDetail.user_id'=>$data['Directorder']['lab_id'])));
			$dodata[$i]['Directorder']['doctor'] = $this->UserDetail->find("first", array('conditions'=> array('UserDetail.user_id'=>$data['Directorder']['doctor_id'])));
			$i++;
		}
		$driverList = $this->Directorder->find('list', array(
		'fields'=>array('driver_id'),
		'conditions'=>array(
		             'Directorder.doctor_id'=>$this->Session->read('Auth.User.id'),
					 'Directorder.driver_id !=' => 0
					 
					 ),  
	    'group'	=> array('driver_id')			 
		));
		//pr($driverList);die;
			//var_dump ($driverList);
		if(!empty($driverList)){
			foreach($driverList as $drivers){
				$locationData = array();
				$mapdata=$this->Directorder->find("all",array(
				'conditions'=>array(
						'Directorder.driver_id'=>$drivers,
						//'Directorder.order_status !='=> 'Delivered',
						'Directorder.doctor_id'=> $this->Session->read('Auth.User.id'),
				), 
						'order'=>array('Directorder.distance'=>'asc'),
				));
				//pr($mapdata);	die;	
				if(count($mapdata) > 0){
					$origin = '';
					$destination = '';
					$i=1;
					foreach($mapdata as $locations){
						$location_data = array();
						if(count($mapdata) == 1){
							$destination = $locations['Directorder']['pickup_address'];
							$origin = $locations['Directorder']['dropoff_address'];
							$location_data['location'] = $locations['Directorder']['pickup_address'];
							$locationData[] = $location_data;
						}else{
							if(count($mapdata) == $i){
								$destination = $locations['Directorder']['pickup_address'];
								$origin = $locations['Directorder']['dropoff_address'];
							}else{
								$location_data['location'] = $locations['Directorder']['pickup_address'];
								$locationData[] = $location_data;
							}
						}
						$i++;
					}
					$start_end['origin'][] = $origin;
					$start_end['destination'][] = $destination;
					$driverOrder[] = $locationData;
				}			
				
			}
	    }
				
    }else{
		$this->Directorder->bindModel(array(
	          'belongsTo' => array(
			          'OrderDetail' => array(
					       'className' => 'User',
						   'foreignKey' => 'doctor_id',
					  )
			  )
	    ));

    	if ($archive == null) {
        $dodata=$this->Directorder
			->find("all",array(
					'conditions'=>array(
							'NOT'=>array('Directorder.payment_status' => 1),
							'Directorder.lab_id'=>$this->Session->read('Auth.User.id')
					),
					'contain' => array('OrderDetail', 'OrderDetail.Userdetail'=>array('fields'=>array('Userdetail.full_name'))),
					'order'=>array('Directorder.oid'=>'desc'),
			));
		
	    } else {
	        $dodata=$this->Directorder->find("all",array('conditions'=>array(
			'Directorder.payment_status'=>1, 
			'Directorder.lab_id'=>$this->Session->read('Auth.User.id')),
			'contain' => array('OrderDetail', 'OrderDetail.Userdetail'=>array('fields'=>array('Userdetail.full_name'))),
			'order'=>array('Directorder.oid'=>'desc'))); 		
	    }
		//Map co-ordinates
		$i = 0;
		foreach ($dodata as $data) {
			$dodata[$i]['Directorder']['lab'] = $this->UserDetail->find("first", array('conditions'=> array('UserDetail.user_id'=>$data['Directorder']['lab_id'])));
			$dodata[$i]['Directorder']['doctor'] = $this->UserDetail->find("first", array('conditions'=> array('UserDetail.user_id'=>$data['Directorder']['doctor_id'])));
			$i++;
		}
		$driverList = $this->Directorder->find('list', array(
		'fields'=>array('driver_id'),
		'conditions'=>array(
		             'Directorder.lab_id'=>$this->Session->read('Auth.User.id'),
					 'Directorder.driver_id !=' => 0
					 
					 ),  
	    'group'	=> array('driver_id')			 
		));
		//pr($driverList);die;
		if(!empty($driverList)){
			foreach($driverList as $drivers){
				$locationData = array();
				$mapdata=$this->Directorder->find("all",array(
				'conditions'=>array(
						'Directorder.driver_id'=>$drivers,
						//'Directorder.order_status !='=> 'Delivered',
						'Directorder.lab_id'=> $this->Session->read('Auth.User.id'),
				), 
						'order'=>array('Directorder.distance'=>'asc'),
				));
				//pr($mapdata);	die;	
				if(count($mapdata) > 0){
					$origin = '';
					$destination = '';
					$i=1;
					foreach($mapdata as $locations){
						$location_data = array();
						if(count($mapdata) == 1){
							$destination = $locations['Directorder']['dropoff_address'];
							$origin = $locations['Directorder']['pickup_address'];
							$location_data['location'] = $locations['Directorder']['dropoff_address'];
							$locationData[] = $location_data;
						}else{
							if(count($mapdata) == $i){
								$destination = $locations['Directorder']['dropoff_address'];
								$origin = $locations['Directorder']['pickup_address'];
							}else{
								$location_data['location'] = $locations['Directorder']['dropoff_address'];
								$locationData[] = $location_data;
							}
						}
						$i++;
					}
					$start_end['origin'][] = $origin;
					$start_end['destination'][] = $destination;
					$driverOrder[] = $locationData;
				}			
				
			}
	    } 	
    }
     //print_r($start_end)	;	
     //print_r($driverOrder);die;

$this->User->bindModel(array(
    'hasMany' => array('Gpslocation'=>array(
                   'foreignKey' => 'userName',
                   'order' => array('Gpslocation.lastUpdate' => 'DESC'), 
                   'limit' => 1
    	))
));
$user_ss = $this->User->find('all', array('conditions'=>array('User.user_type'=>4)));

//echo '<pre>'; print_r($dodata);die;

$locationDatajson = json_encode($driverOrder);
$start_end = json_encode($start_end);
//echo $start_end;die; 

$this->set('start_end' ,$start_end);
$this->set('locationData' ,$locationDatajson);

$this->set('archive' ,$archive);
$this->set('user_ss' ,$user_ss);
$this->set('dodata' ,$dodata );
if($this->Session->read('Auth.User.user_type') == 1){
$this->render('directorderlistfrodoctor');
}
}

public function ajaxmap(){
		if(isset($_POST['uid'])){
			$this->loadmodel('Userdetail');
			$user_locations = $this->Userdetail->find('first',array(
					 'conditions' => array('Userdetail.user_id' => $_POST['uid'],
					 
		)));
		if(empty($user_locations['Userdetail']['latitude'])){
			$address = str_replace(" ", "+", $user_locations['Userdetail']['address']);

			$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
			$json = json_decode($json);
            
			$add = $user_locations['Userdetail']['address'];
			$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		}else{
			$lat = $user_locations['Userdetail']['latitude'];
			$lng = $user_locations['Userdetail']['longitude'];
			$add = $user_locations['Userdetail']['address'];
		}
		    
		}else{
			$lat = $_POST['lat'];
			$lng = $_POST['lng'];
			$add = $_POST['address'];
		}
		//pr($user_locations);die;
		
		
    	$this->set(compact('lat', 'lng', 'add'));
		echo $this->render('ajaxmap');
    } 
	
	
	public function getAddress(){
		$this->autoRender = false;
		if(isset($_POST['uid'])){
			$this->loadmodel('Userdetail');
			$user_locations = $this->Userdetail->find('first',array(
					 'conditions' => array('Userdetail.user_id' => $_POST['uid'],
					 
		)));
		if(empty($user_locations['Userdetail']['latitude'])){
			$address = str_replace(" ", "+", $user_locations['Userdetail']['address']);

			$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
			$json = json_decode($json);
            
			$user_locations['Userdetail']['latitude'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$user_locations['Userdetail']['longitude'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		}
		    
		
		//pr($user_locations);die;
		
		echo json_encode($user_locations);
        } 
	}


function directorderdoctorlist() {
$this->loadModel('OrderStatus');
		$this->loadModel('User');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderPayment');
		$this->loadModel('OrderRequest');
		$this->loadModel('Notification');
		$this->loadModel('Order');
		
    $this->layout = 'frontend_new';
    
        $this->set('dodata',$this->Order->find("all"));
 }
function driverorders($archive = null) {
    $this->layout = 'frontend_new';
    $this->loadModel('Directorder');
    $user_id = $this->Session->read('Auth.User.id');
    if ($archive == 'archive') {
        $order = $this->Directorder->find("all",array("conditions"=>array("Directorder.driver_id"=>$user_id, "Directorder.order_status"=>"Delivered"),'order'=>array('Directorder.oid'=>'desc')));
    } else {
        $order = $this->Directorder->find("all",array("conditions"=>array("Directorder.driver_id"=>$user_id, 'NOT'=>array('Directorder.order_status' => "Delivered")),'order'=>array('Directorder.oid'=>'desc')));        
    }
    $this->set('orders', $order);
    
    if ($archive == null) {
        $button = 'Archive Orders';
    } else {
        if ($archive == 'archive') {
            $button = 'Unarchived Orders';
        } else {
            $button = 'Archive Orders';
        }
    }
    $this->set('button', $button);
}

function cardsave()
{
	$this->autoRender = false;
	
	if($this->request->is('ajax'))
	{
		
	  $this->loadModel('Carddetail');
	  $id=$this->request->data;	
	  $data=$this->Carddetail->find('all', array('conditions' => array('Carddetail.userid' =>$id['uid'])));
	  $numUsers = sizeof($data);
	  $pid=uniqid();
	  $this->Session->write('uniqid',$pid);
	  if($numUsers=='0')
	  {
	
		 $querydata['Carddetail']['userid']= $id['uid'];
		 $querydata['Carddetail']['cardnumber']= $id['cardnumber'];
		// $querydata['Carddetail']['cvv']= $id['cvv'];
		 $querydata['Carddetail']['exmonth']= $id['exmonth'];
		 $querydata['Carddetail']['exyear']= $id['exyear'];
		 $querydata['Carddetail']['email']= $id['email'];
		 $querydata['Carddetail']['fname']= $id['fname'];
		 $querydata['Carddetail']['lname']= $id['lname'];
		 $querydata['Carddetail']['uniqid']= $pid;
		 $this->Carddetail->save($querydata);
		
	  }
	/*if($numUsers=='1')
	 {
		$cardnumber=$id['cardnumber'];
		$cvv=$id['cvv'];
		$exmonth=$id['exmonth'];
		$exyear=$id['exyear'];
		$modify=date('Y-m-d h:i:s');
		$uid=$id['uid'];
		  $this->Carddetail->updateAll(array('cardnumber'=>"'$cardnumber'",'cvv'=>"'$cvv'",'exmonth'=>"'$exmonth'",'exyear'=>"'$exyear'",'modified_date'=>"'$modify'",'unique'),array('userid'=>$uid));
			
    }*/
}	
}

function carddelete()
{
	if($this->request->is('ajax'))
	{
	$this->loadModel('Carddetail');
	 $id=$this->request->data;
	 $userid=$id['userid']	;
	 $this->Carddetail->deleteAll(array('Carddetail.userid' => $userid)); 
	 //echo "ok"; 
	}
}

function stafflaborder($archive = null)
{
		$this->loadModel('Directorder');
		$this->layout = 'frontend_new';
if($archive == null){
$this->set('dodata',$this->Directorder->find("all",array('conditions'=>array('Directorder.order_status !='=>'Delivered'),'order'=>array('Directorder.oid'=>'desc'))));
}else{
	$this->set('dodata',$this->Directorder->find("all",array('conditions'=>array('Directorder.order_status '=>'Delivered', 'Directorder.payment_status '=>'1'),'order'=>array('Directorder.oid'=>'desc'))));
}
$this->set('archive' ,$archive);
}

function counter()
	{

		if($this->request->is('ajax'))
	{
		$this->loadModel('Counter');
//$this->autoRender = false;
$data = $this->Counter->find('all', array("conditions"=>array('Counter.counterid'=>'1')));
if($data)
{
return 1;
}
}
}
function dirorderdoc()
{
		
		$this->loadModel('OrderStatus');
		$this->loadModel('User');
		$this->loadModel('OrderDetail');
		$this->loadModel('OrderPayment');
		$this->loadModel('OrderRequest');
		$this->loadModel('Notification');
		$this->loadModel('Order');
		
    $this->layout = 'frontend_new';
    
        $this->set('test',$this->Order->find("all"));
     
    //$this->set('archive' ,$archive);
	//$this->set('test','from order controller');
}

}