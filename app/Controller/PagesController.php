<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
 
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';
	
/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	
	function beforefilter() {
		parent::beforefilter();
		$this->Auth->allow();
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function index() {
	/*	$obj = $this->Components->load("ExpressCheckout");
		$obj->returnurl = "http://localhost/rrl/trunk/pages/getresult";
		$obj->cancelurl = "http://localhost/rrl/trunk/pages/cancelresult";
		if ( $obj->setExpressCheckout() ) {
			$this->redirect($obj->result_url);
		} */

		
		$this->layout = 'frontend_new';
		$this->set("title_for_layout","Welcome to Bio Labs");
		$this->loadModel('Configuration');
$this->loadModel("OrderStatus");
		$year=date('Y');
		$unix=strtotime($year);
	    $data=$this->OrderStatus->find('all', array("conditions"=>array('OrderStatus.status_id'=>'9')));
	    
	  //echo "<pre>"; print_r($data); echo "</pre>";
	    $count=0;
	    foreach($data as $record)
	    {
			
			$modified=$record['OrderStatus']['modified'];
			$modifiedexplode=explode('-',$modified);
			$year2=$modifiedexplode[0];
			if($year2==$year)
			{
				$count=$count+1;
			}
			
			
		}
	    
	    $deliveredcount=$count;
	    $this->set('count',$deliveredcount);

/*$this->loadModel('Cmspages');
		$f_pages = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl','Cmspages.showinheader','Cmspages.showinfooter'), 'conditions'=>array('Cmspages.showinfooter'=>1), 'order'=>'Cmspages.name'));
			
		$this->set('footer_pages',$f_pages);

$h_pages = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl','Cmspages.showinheader','Cmspages.showinheader'), 'conditions'=>array('Cmspages.showinheader'=>1), 'order'=>'Cmspages.name'));
			
		$this->set('pages',$h_pages);*/
	}
	
	
	public function getresult() {
		pr($this->request->query);
		$obj = $this->Components->load("ExpressCheckout");
		if ( $obj->doExpressCheckout($this->request->query['token'],$this->request->query['PayerID']) ) {
			echo $obj->transactionkey;
		}
		die("here");
	}
	
	public function cancelresult() {
		die("here");
	}
	
	
	
/* Displays a static page 
 * @param slug of the page
 * @return page text
 */
	public function staticpages($slug){
		//$this->layout = 'frontend';
		$this->layout = "frontend_new";
		$this->loadModel('Cmspages');
		$pagesName = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl'), 'conditions'=>array('Cmspages.showinleft'=>1, 'Cmspages.status'=>1)));
		$pageContent = $this->Cmspages->find('first', array('conditions'=>array('Cmspages.seourl'=>$slug)));
		$this->set("title_for_layout",$pageContent['Cmspages']['name']);
		$this->set(compact('pagesName','pageContent'));
	}
	public function sitemap() {
		$this->layout = 'frontend';
		$this->set("title_for_layout","Site Map");
		$this->loadModel("Category");
		$categories = $this->Category->find("list",array("conditions"=>array("Category.status"=>1),"fileds"=>array("Category.id","Category.title"),'order'=>'Category.sort,Category.title'));
		$this->set(compact("categories"));
		$this->loadModel('Cmspages');
		$pagesName = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl'), 'conditions'=>array('Cmspages.showinleft'=>1, 'Cmspages.status'=>1)));
		$this->set(compact('pagesName'));
	}
	
	function contactus() {
		$this->autoRender = false;
		$result = array("error"=>true,"message"=>"Something went wrong, Please try again.");
		if($this->RequestHandler->isAjax()){			
			if (isset($this->data) && !empty($this->data)) {
				$this->mailBody .= "Sender Name: ".nl2br(strip_tags($this->data['Page']['first_name']))."<br/>";
				if (!empty($this->data['Page']['company'])) {
					$this->mailBody = "Company Name: ".$this->data['Page']['company']."<br/>";
				}
				$this->mailBody .= "Sender Email: ".nl2br(strip_tags($this->data['Page']['email']))."<br/>";
				$this->mailBody .= "Sender Phone: ".nl2br(strip_tags($this->data['Page']['phone']))."<br/>";
				$this->mailBody .= "Type:".nl2br(strip_tags($this->data['Page']['type']))."<br/>";
				$this->mailBody .= "Message:<br/>".nl2br(strip_tags($this->data['Page']['message']));
				$this->from = $this->data['Page']['email'];
				$this->subject = "Contact Request regarding ".$this->data['Page']['type'];
				$this->to = ADMIN_EMAIL;
				if(!empty($this->to) && !empty($this->from)) {
					$flag = $this->sendmail($this->to,NULL,nl2br(strip_tags($this->data['Page']['first_name'],"<p>,<br/>")));
				} else {
					$flag = false;
				}
				if ($flag) {					
					$result = array("error"=>false,"message"=>"Your contact request has been submitted, We will get back to you soon.");
				} else {
					$result = array("error"=>true,"message"=>"Oops, Something went wrong, Please try again.");
				}
			}
		}
		return json_encode($result);
	}
	
	
	
	function support() {
		$this->layout = "frontend";
		$this->set("title_for_layout","Support");
		if (isset($this->data) && !empty($this->data)) {
			$this->mailBody .= "Sender Name: ".nl2br(strip_tags($this->data['Page']['first_name']))."<br/>";
			if (!empty($this->data['Page']['company'])) {
				$this->mailBody = "Company Name: ".$this->data['Page']['company']."<br/>";
			}
			$this->mailBody .= "Sender Email: ".nl2br(strip_tags($this->data['Page']['email']))."<br/>";
			$this->mailBody .= "Sender Phone: ".nl2br(strip_tags($this->data['Page']['phone']))."<br/>";
			$this->mailBody .= "Type:".nl2br(strip_tags($this->data['Page']['type']))."<br/>";
			$this->mailBody .= "Message:<br/>".nl2br(strip_tags($this->data['Page']['message']));
			$this->from = $this->data['Page']['email'];
			$this->subject = "Support Request regarding ".$this->data['Page']['type'];
			$this->to = CONTACT_MAIL;
			if(!empty($this->to) && !empty($this->from)) {
				$flag = $this->sendmail($this->to,NULL,nl2br(strip_tags($this->data['Page']['first_name'],"<p>,<br/>")));
			} else {
				$flag = false;
			}
			if ($flag) {
				$this->Session->setFlash("Your query has been submitted.", 'default', array("class"=>"success_message"));
				$this->redirect("/support");
			} else {
				$this->Session->setFlash("Your query can not be submitted, please try again.");
			}
		}
		$this->render("contact-us");
	}
	
	function bulkemail(){
		/*$emails = array('sajal.jaiswal@cii.in','
		hr@netshellinfosystem.com','
		jyoti@it7solutions.com','
		charu@bebotechnologies.com','
		shaista@suventure.in','
		swapna.gadgil@cuelogic.co.in','
		sunitasingh@ibtechnology.com','
		shakti.teotia@fisglobal.com','
		SSinha@seasiainfotech.com','
		jasleenb@smartdatainc.net','
		hr@brillinfosystems.com','
		Neha@a-square.co.in','
		hr@deftsoft.com','
		hitesh@xicom.biz','
		tdhawan@cogniter.com','
		hr@softprodigy.com','
		ankita.sharma@trantorinc.com','
		tiresh@weboum.com','
		hr@richestsoft.com','
		hr@sebiz.net','
		hr@netsolutionsindia.com','
		hr@kindlebit.com','
		hrd@nanowebtech.com','
		hr@osvin.com','
		shanky@slinfy.com','
		hr@classicinformatics.com','
		contact@viennaadvantage.com','
		balvar.pooni@yesbank.in','
		info@aaxisnano.com','
		info@ablysoft.com','
		mgi@nda.vsnl.net.in','
		aksips45@gmail.com','
		contact@allumezinfotech.com','
		info@attoloinfotech.com','
		contactus@andelindia.com','
		md@andelindia.com','
		Info@BA-DataSolutions.com','
		info@chandanhospital.com','
		info@clerisysolutions.com','
		info@Coatecindia.com','
		credaipunjab@yahoo.com','
		contact@cricketershop.com','
		dismohali@rediffmail.com','
		rajnish@econ.co.in','
		roy@emobx.com','
		malkiatsinghvirdee@yahoo.co.in','
		info@gbgroup.co.in','
		contact@igef.net','
		info@jhhapsindustries.com','
		info@soap-packaging.com','
		info@biscuit-packaging.com','
		info@paper-packaging.com','
		herbalremedies123@yahoo.com','
		info@lpsmohali.com','
		info@localdiaries.com','
		sales@logicerp.com','
		support@logicerp.com','
		luthraengineer@yahoo.co.in','
		info@luthraengineer.com','
		info@manglas.com','
		manglasons@gmail.com','
		moielmhl@glide.net.in','
		press@nebero.com','
		support@netsetsoftware.com','
		paramount.exhibitors@gmail.com','
		reachus@pingakshotechnologies.com','
		hr@pingakshotechnologies.com','
		info@precisionengggroups.com','
		info@pressideas.com','
		mail@pla.co.in','
		secretarial@infotelconnect.com','
		regal_taps@yahoo.com','
		rollcon@hotmail.com','
		rollcon@rediffmail.com','
		info@saindustries.co.in','
		info@satgurutechnologies.com','
		hr@seasiainfotech.com','
		info@seasiainfotech.com','
		saplmoh@sappl.co.in','
		plant@sfnindia.com','
		simson@simsononline.com','
		hr@smartdatainc.com','
		hr@softprodigy.com','
		info@softobiz.com','
		info@finikelights.com','
		info@swanirubber.com','
		hr@tdsgroup.in','
		gloss@sify.com','
		info@techaids.in','
		hr@tynorindia.com','
		info@vertexinfosoft.com','
		info@vishwastubes.com','
		vishwastubes@rediffmail.com','
		info@wwicsgroup.com','
		info@avantgardeinfotech.com','
		info@abeertech.com','
		info@vistechgroup.com','
		info@anzerfurniture.com','
		hr@netsolutionsindia.com','
		hr@mastersoftwaresolutions.com','
		business@ayushvedainformatics.com','
		info@chemicalresources.net','
		contact@consortbuilders.com','
		drish@drish.com','
		info@electronicsindia.co.in','
		info@fqccertification.com','
		info@grazitti.com','
		contact@grazitti.com','
		mktg@pavl.co.in','
		info@easymarkers.com','
		mktg@pharmaffiliates.com','
		admin@pharmaffiliates.com','
		parv@parvtravels.com','
		parvtravels@gmail.com','
		sales@mobilewebpixels.com','
		mitoyain@yahoo.co.in','
		info@mitoyaindia.com','
		rishabhealthcare@yahoo.com','
		spscpkl@gmail.com',
		'ghosh@sgassociates.in','
		kunaalarora@rediffmail.com','
		info@sukataseats.com','
		corporateoffice@theonpharma.com','
		info@venusremedies.com','
		ib@venusremedies.com','
		info@webartindia.com','
		info@indswift.com','
		info@alchemist.co.in','
		aop144@gmail.com','
		solartech@susolartech.com','
		tcilchd@gmail.com','
		info@3mapl.net','
		adhunikengineers@yahoo.com','
		allinone.telecom@in.airtel.com','
		ajayintl@ajayinternational.com','
		info@alphaitworld.co.in','
		export@astralighting.co.in','
		atop@atopfasteners.com','
		info@audichandigarh.in','
		info@beckondelve.com','
		info@beyond-dimensions.com','
		bdppl@yahoo.com','
		contact@click-labs.com','
		info@competentindia.net','
		hello@cueblocks.com','
		dkentpkl@gmail.com','
		usha@dsl-india.com','
		ems@cdil.com','
		info@diplast.com','
		info@channeldivya.com','
		contacts@drishinfo.com','
		durgaseedfarm@gmail.com','
		info@durgaseedfarm.com','
		info@edlivetechnologies.com','
		support@emiliatechnologies.com','
		contact@esferasoft.com','
		contactus@essessindia.com','
		contact@eespl.com','
		info@graycelltech.com','
		info@pumpkart.com','
		contact@hnhtech.com','
		hemantsgalav@gmail.com','
		info@narainindustries.com','
		drsharma@idspl.com',
		'sunanda@orionesolutions.com',
		'shivamsharma@zapbuild.com',
		'hr@zapbuild.com',
		'swatimunjal@zapbuild.com',
		'amritpalsingh@zapbuild.com',
		'execution@zapbuild.com');*/
		$this->getmaildata(19);
		//$maildata = array("shivamsharma@zapbuild.com","bgmai_chd@butterflyindia.com","vijay@destm.com","vijay@destm.com","shivani.verma@dataguise.com","shilpa.mahajan@vtlrewa.com","hr@redalkemi.com","contact@sun-softwares.com","shivamsharma@zapbuild.com");
		$maildata = array("shivamsharma@zapbuild.com","hr@competentgroove.com","shivamsharma@zapbuild.com");
		echo $this->mailBody;
		echo $this->subject;
		echo $this->from;
		//die;
		foreach ( $maildata as $key=>$val ) {
			$this->sendmail($val,$template = 'email',$fromname = 'Chandigarh Supports Nepal',"campaign");
		}
		die;
	}
	
	
	function dummyResponse() {
		$this->loadModel("OrderDetail");
		$data['OrderDetail']['details'] = file_get_contents('php://input');
		$this->OrderDetail->save($data);
		die('here');
	}
	function counter()
	{
	 $this->autoRender = false;
	 $this->loadModel('Counter');
     $data = $this->Counter->find('all', array("conditions"=>array('Counter.counterid'=>'1')));
     if($data)
      {
		 // print_r($data);
	  return $data[0]['Counter']['counternumber'];
      }
      else
      {
	echo "okkk";
      }

}


	
}
