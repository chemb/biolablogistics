<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @version		: 1.0
 * @created by	: shivam sharma
 */
class ServicesController extends AppController {


	/* end of function */


	/*
	 * @function name	: admin_index
	 * @purpose			: to show listing of services for lab
	 * @arguments		:na	 
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 1st may 2015
	 * @description		: NA
	 */

	public function admin_index() {
		$this->bulkactions();
		$heading = 'Manage Services';
		/* code to perform search functionality */
		if (isset($this->data) && !empty($this->data['Service']['searchval'])) {
			$this->Session->write('searchval', $this->data['Service']['searchval']);
			$this->conditions = array("OR" => array("Service.name like" => "%" . $this->data['Service']['searchval'] . "%"));
		}

		if (isset($this->params['named']['page'])) {

			if ($this->Session->read('searchval')) {
				$this->conditions = array("OR" => array("Service.name like" => "%" . $this->Session->read('searchval') . "%"));
				$this->data['Service']['searchval'] = $this->Session->read('searchval');
			}
		} elseif (empty($this->conditions)) {
			$this->Session->delete('searchval');
		}
		
		$this->set(compact("heading"));
		/* end of code to perform search functionality */
		$this->paginate = array("order" => "Service.created desc");
		$this->Service->recursive = 0;
		$this->set('services', $this->paginate($this->conditions));
	}
		/* end of function */

	/*
	 * @function name	: admin_edit
	 * @purpose			: Edit lab Services from admin panel .	
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 1st may 2015	
	 */

	public function admin_edit($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		} else {
			
			$serviceInfo = $this->Service->find("first", array("conditions" => array('Service.id' => $id)));
					
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->Service->id = $this->request->data['Service']['id'];
				if ($this->Service->saveAll($this->request->data)) {
					$this->Session->setFlash("Service has been updated successfully.", 'default', array("class" => "success_message"));
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->request->data = $serviceInfo;
			}
		}
	}

	/* end of function */
   	public function admin_delete($id = null) {		
		$this->Service->id = $id;
		if ($this->Service->delete()) {
			$this->Session->setFlash(__('Service has been deleted successfully.', 'default', array("class" => "success_message")));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Service was not deleted.'));
		$this->redirect(array('action' => 'index'));
	}
	
			/* end of function */

	/*
	 * @function name	: admin_edit
	 * @purpose			: Add lab Services from admin panel .	
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 1st may 2015	
	 */

	public function admin_add() {						
			if ($this->request->is('post') || $this->request->is('put')) {					
				if ($this->Service->saveAll($this->request->data)) {
					$this->Session->setFlash("Service has been added successfully.", 'default', array("class" => "success_message"));
					$this->redirect(array('action' => 'index'));
				}
			} 
		}
	

	
}
