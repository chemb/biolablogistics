<?php
/**
 * StripeComponent
 *
 * A component that handles payment processing using Stripe.
 *
 * PHP version 5
 *
 * @package		StripeComponent
 * @author		Gregory Gaskill <gregory@chronon.com>
 * @license		MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link		https://github.com/chronon/CakePHP-StripeComponent-Plugin
 */

App::uses('Component', 'Controller');

/**
 * StripeComponent
 *
 * @package		StripeComponent
 */
class StripeComponent extends Component {

/**
 * Default Stripe mode to use: Test or Live
 *
 * @var string
 * @access public
 */
	public $mode = 'Test';

/**
 * Default currency to use for the transaction
 *
 * @var string
 * @access public
 */
	public $currency = 'usd';

/**
 * Default mapping of fields to be returned: local_field => stripe_field
 *
 * @var array
 * @access public
 */
	public $fields = array('stripe_id' => 'id');

/**
 * The required Stripe secret API key
 *
 * @var string
 * @access public
 */
	public $key = 'sk_test_GJVrm2qzbu6FPlS7tXpyVGPq';

/**
 * Valid parameters that can be included in the call to Stripe_Charge::create
 *
 * @var array
 * @access protected
 */
	protected $_chargeParams = array(
		'amount',
		'currency',
		'customer',
		'card',
		'description',
		'metadata',
		'capture',
		'statement_descriptor',
		'receipt_email',
		'application_fee',
		'shipping'
	);

/**
 * The charge method prepares data for Stripe_Charge::create and attempts a
 * transaction.
 *
 * @param array	$data Must contain 'amount' and 'stripeToken'.
 * @return array $charge if success, string $error if failure.
 * @throws CakeException
 * @throws CakeException
 */
	public function charge($data) {
		// load the stripe vendor class IF it hasn't been autoloaded (composer)
		App::import('Vendor', 'Stripe', array(
				'file' => 'stripe'.DS.'stripe-php'. DS . 'lib' . DS . 'Stripe.php')
			);
		if (!class_exists('Stripe')) {
			throw new CakeException('Stripe API library is missing or could not be loaded.');
		}
		// $data MUST contain 'stripeToken' or 'stripeCustomer' (id) to make a charge.
		if (!isset($data['stripeToken']) && !isset($data['stripeCustomer'])) {
			throw new CakeException('The required stripeToken or stripeCustomer fields are missing.');
		}

		// if amount is missing or not numeric, abort.
		if (!isset($data['amount']) || !is_numeric($data['amount'])) {
			throw new CakeException('Amount is required and must be numeric.');
		}

		// format the amount, in cents.
		$data['amount'] = $data['amount'] * 100;

		Stripe::setApiKey($this->key);
		$error = null;

		$chargeData = array();
		foreach ($this->_chargeParams as $param) {
			if (isset($data[$param])) {
				$chargeData[$param] = $data[$param];
			}

		}

		if (!isset($chargeData['currency'])) {
			$chargeData['currency'] = $this->currency;
		}

		if (isset($data['stripeToken'])) {
			$chargeData['card'] = $data['stripeToken'];
		} else {
			$chargeData['customer'] = $data['stripeCustomer'];
		}

		try {
			$charge = Stripe_Charge::create($chargeData);

		} catch(Stripe_CardError $e) {
			$body = $e->getJsonBody();
			$err = $body['error'];
			CakeLog::error(
				'Charge::Stripe_CardError: ' . $err['type'] . ': ' . $err['code'] . ': ' . $err['message'],
				'stripe'
			);
			$error = $err['message'];

		} catch (Stripe_InvalidRequestError $e) {
			$body = $e->getJsonBody();
			$err = $body['error'];
			CakeLog::error(
				'Charge::Stripe_InvalidRequestError: ' . $err['type'] . ': ' . $err['message'],
				'stripe'
			);
			$error = $err['message'];

		} catch (Stripe_AuthenticationError $e) {
			CakeLog::error('Charge::Stripe_AuthenticationError: API key rejected!', 'stripe');
			$error = 'Payment processor API key error.';

		} catch (Stripe_ApiConnectionError $e) {
			CakeLog::error('Charge::Stripe_ApiConnectionError: Stripe could not be reached.', 'stripe');
			$error = 'Network communication with payment processor failed, try again later';

		} catch (Stripe_Error $e) {
			CakeLog::error('Charge::Stripe_Error: Stripe could be down.', 'stripe');
			$error = 'Payment processor error, try again later.';

		} catch (Exception $e) {
			CakeLog::error('Charge::Exception: Unknown error.', 'stripe');
			$error = 'There was an error, try again later.';
		}

		if ($error !== null) {
			// an error is always a string
			return (string)$error;
		}

		CakeLog::info('Stripe: charge id ' . $charge->id, 'stripe');

		return $this->_formatResult($charge);
	}


/**
 * Returns an array of fields we want from Stripe's response objects
 *
 *
 * @param object $response A successful response object from this component.
 * @return array The desired fields from the response object as an array.
 */
	protected function _formatResult($response) {
		$result = array();
		foreach ($this->fields as $local => $stripe) {
			if (is_array($stripe)) {
				foreach ($stripe as $obj => $field) {
					if (isset($response->$obj->$field)) {
						$result[$local] = $response->$obj->$field;
					}
				}
			} else {
				if (isset($response->$stripe)) {
					$result[$local] = $response->$stripe;
				}
			}
		}
		// if no local fields match, return the default stripe_id
		if (empty($result)) {
			$result['stripe_id'] = $response->id;
		}
		return $result;
	}

}
