<?php
App::uses('AppController', 'Controller');
/**
 * Admins Controller
 *
 * @property Admin $Admin
 */
class AdminsController extends AppController {

	public $components	= array("Session","Cookie","Email","RequestHandler");
	var $helpers		= array("Js");
	public $value		= array();
	public $mailBody	= '';
	public $template	= '';
	public $content		= '';
	public $conditions	= array();
	public $delarr		= array();
	public $updatearr	= array();
	
	function beforefilter() {
		parent::beforefilter();
		$this->Auth->allow();
		$allowed = array("assignlabdriver","login","logout","forgotpassword","confirmation","getcountrystatecity", "updateassignlabdriver");
		$this->checklogin($allowed);
		$this->adminbreadcrumb();
	}
	
	
	
/**
 * index method
 *
 * @return void
 */
	public function login() {
		$this->set('title_for_layout',"Admin Login");
		if($this->Session->read('admin')) {
			$this->response->disableCache();
			$this->redirect("/admin/dashboard");
		}
		if (isset($this->data) && !empty($this->data)) {
			$admin = $this->__login($this->data['Admin']);
			if (!empty($admin)){
				$this->Session->write('admin',$admin);
				if(isset($this->data['Admin']['remember_me']) && $this->data['Admin']['remember_me'] == 1) {
					$cookie = array();
					$cookie['remembertoken']	= $this->encryptpass($this->data['Admin']['username']);
					$this->Cookie->write('Admin', $cookie,false, '+2 weeks');
					$this->Admin->create();
					$this->Admin->id	= $admin['Admin']['id'];
					$admin['Admin']['remembertoken'] = $this->encryptpass($this->data['Admin']['username']);
					$this->Admin->save($admin);
				}
				$this->response->disableCache();
				$this->redirect("/admin/dashboard");
			} else {
				$this->Session->setFlash('Invalid email or password. Please try again', 'default', array("class"=>"success_message"));
			}
		} elseif($this->Cookie->read('Admin')) {
			$cookie	= $this->Cookie->read('Admin');
			if (!empty($cookie)) {
				$admin	= $this->__login($cookie,true);
				if(!empty($admin)){
					$this->Session->write('admin',$admin);
					$this->response->disableCache();
					$this->redirect("/admin/dashboard");
				} else {
					$this->Session->setFlash('Invalid email or password. Please try again', 'default', array("class"=>"success_message"));
				}
			}
		}
		
	}


/*
 * @function name	: login
 * @purpose			: validate an autheticate user
 * @arguments		: none
 * @return			: logged in user details
 * @created by		: shivam sharma
 * @created on		: 26Apr 2013
 * @description		: it is a private function 
*/
	function __login($data,$rememberme = false){
		$this->Admin->recursive	=	0;
		if($rememberme){
			$admin = $this->Admin->find("first",array("conditions"=>array("remembertoken"=>$data['remembertoken'],"status"=>1)));
		}else{
			//echo ($this->encryptpass($data['password']));
			if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') $admin = $this->Admin->find("first",array("conditions"=>array("username"=>$data['username'],"status"=>1)));
			else $admin = $this->Admin->find("first",array("conditions"=>array("username"=>$data['username'],"password"=>$this->encryptpass($data['password']),"status"=>1)));
		}
		if($admin['Admin']['passwordstatus'] == 1){
			$admin['Admin']['passwordstatus'] = 0;
			$this->Admin->create();
			$this->Admin->id = $admin['Admin']['id'];
			$this->Admin->save($admin);
		}
		if(!empty($admin)){
			
			$date = date('F j, Y');
			$admin['Admin']['lastlogin'] = $date;
		}
		return $admin;
	}
/* end of function */


/*
 * @function name	: logout
 * @purpose			: logout of admin
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 26Apr 2013
 * @description		: NA
*/
	function logout() {
		$this->layout = false;
		$this->Session->destroy('admin');
		$this->Session->delete('admin');
		$this->Cookie->destroy();
		$this->response->disableCache();
		$this->redirect("/admin");
	}
/* end of function 	*/
	

/*
 * @function name	: forgotpassword
 * @purpose			: display form of forgot password and also performs password change functionlity
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 3rd Jan 2013
 * @description		: this function is to generate a new password and send to admin email
*/
	function forgotpassword(){
		$this->set("title_for_layout","Forgot Password");
		if($this->Session->read('admin')) {
			$this->response->disableCache();
			$this->redirect("/admin/dashboard");
		}
		if(isset($this->data) && !empty($this->data)){
			$arr = $this->Admin->find("first",array("conditions"=>array("username"=>$this->data['Admin']['email'])));
			if(empty($arr)){
				$this->Session->setFlash(INVALID_EMAIL_FORGOT_PASSWORD);
			} else {
				$arr['Admin']['passwordstatus'] = 1;
				$new_password = rand(100000, 999999);
				$arr['Admin']['password'] = $this->encryptpass($new_password);
				/* code to send email confirmation for signup */
				$user	= "Administrator";
				$this->getmaildata(2);
				$this->mailBody = str_replace("{USER}",$user,$this->mailBody);
				$this->mailBody = str_replace("{PASSWORD}",$new_password,$this->mailBody);
				$flag = $this->sendmail($arr['Admin']['username']);
				/* code to send email confirmation for signup */
				if ($flag) {
					$this->Session->setFlash(NEW_SENT_FORGOT_PASSWORD, 'default', array("class"=>"success_message"));
					$this->Admin->save($arr);
				}
				else {
					$this->Session->setFlash(FAIL_SENT_FORGOT_PASSWORD);
				}
				
			}
			$this->redirect("/admins/forgotpassword");
		}
	}
/* end of function 	*/


/*
 * @function name	: dashboard
 * @purpose			: display dashboard of admin
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 5th oct 2012
 * @description		: NA
*/
	function dashboard(){
		$this->set("title_for_layout","Admin Dashboard");
	}
/* end of function 	*/


/*
 * @function name	: changepassword
 * @purpose			: display form of change password and also performs password change functionlity
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 3rd Jan 2012
 * @description		: NA
*/
	function changepassword() {
		$admin = $this->Session->read("admin");
		$this->set("title_for_layout","Change Password");
		$this->Admin->recursive = 0;
		if (isset($this->data) && !empty($this->data)) {
			$password = $this->encryptpass($this->data['Admin']['currentpassword']);			
			$user = $this->Admin->find("first",array("conditions"=>array("id"=>$this->data['Admin']['id'],"password"=>$password)));
			$new_pass = $this->encryptpass($this->data['Admin']['newpassword']);			
			if (empty($user)) {
				$this->Session->setFlash('Current password is not correct');
			} elseif(empty($this->data['Admin']['newpassword']) || empty($this->data['Admin']['confirmpassword'])) {
				$this->Session->setFlash('New and confirm password do not match');
			} elseif($password == $new_pass){
			  	$this->Session->setFlash('New password can not be same as current password.');
			} elseif($this->data['Admin']['newpassword'] != $this->data['Admin']['confirmpassword']) {
				$this->Session->setFlash('CONFIRM_PASSWORD_ERROR');
			} else {
				$this->Admin->create();
				$this->Admin->id = $user['Admin']['id'];
				$data['Admin']['password'] =  $this->encryptpass($this->data['Admin']['newpassword']);
				$this->Admin->save($data);
				$this->Session->setFlash('Password has been updated successfully', 'default', array("class"=>"success_message"));
			}
				$this->redirect("/admin/changepassword");
		}
	}
/* end of function 	*/

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Admin->recursive = 0;
		$this->conditions	= array(array("Admin.account_id"=>$this->Session->read("admin.Admin.account_id")));
		$this->bulkactions();
	/* code to perform search functionality */
		if(isset($this->data) && !empty($this->data['Admin']['searchval'])){
			$this->Session->write('searchval',$this->data['Admin']['searchval']);
			$this->conditions	= array("OR"=>array("username like"=>"%".$this->data['Admin']['searchval']."%","domain like"=>"%".$this->data['Admin']['searchval']."%","date(created)"=>$this->data['Admin']['searchval']));
		}
		
		if(isset($this->params['named']['page'])){
			
			if($this->Session->read('searchval')){
				$this->conditions	= array("OR"=>array("username like"=>"%".$this->Session->read('searchval')."%","domain like"=>"%".$this->Session->read('searchval')."%"));
				$this->data['Admin']['searchval'] = $this->Session->read('searchval');
			}
		}elseif(empty($this->conditions)){
			$this->Session->delete('searchval');
		}
	/* end of code to perform search functionality */
		$this->set('admins', $this->paginate($this->conditions));
	}
	

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Admin->id = $id;
		if (!$this->Admin->exists()) {
			throw new NotFoundException(__('Invalid admin'));
		}
		$this->set('admin', $this->Admin->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Admin->create();
			$data = array();
			$data = $this->request->data;
			$data['Admin']['account_id'] = $this->Session->read("admin.Admin.account_id");
			$data['Admin']['password'] = $this->encryptpass($data['Admin']['password']);
			$data['Admin']['remembertoken'] = $this->encryptpass($data['Admin']['username']);
			$data['Admin']['confirm password'] = $this->encryptpass($data['Admin']['confirm password']);
			$data['Admin']['admintype'] = 'Sub';
			
			if ($this->Admin->save($data)) {
				$this->Admindetail->create();
				$detaildata['Admindetail']['admin_id'] = $this->Admin->getLastInsertId();
				$this->Admindetail->save($detaildata);
			/* code to send email */
				$link			= "<a href='http://".$_SERVER['SERVER_NAME']."/checkrope/admin/confirmation/".$this->encryptpass($data['Admin']['username'])."'>Click Here</a>";
				$mail			= $this->Cmsemail->find("first",array("conditions"=>array("mailsubject"=>'Creation of Admin Account')));
				$from			= (isset($mail['Cmsemail']['mailfrom']) && !empty($mail['Cmsemail']['mailfrom']))?$mail['Cmsemail']['mailfrom']:FORGFOTEMAILFROM;
				$subject		= (isset($mail['Cmsemail']['mailsubject']) && !empty($mail['Cmsemail']['mailsubject']))?$mail['Cmsemail']['mailsubject']:FORGOTEMAILSUBJECT;
				$this->content	= (isset($mail['Cmsemail']['mailcontent']) && !empty($mail['Cmsemail']['mailcontent']))?$mail['Cmsemail']['mailcontent']:'';
				if (!empty($this->content)) {
					$this->mailBody	=  str_replace('{USER}',$data['Admin']['username'],$this->content);
					$this->mailBody	=  str_replace('{EMAIL}',$data['Admin']['username'],$this->mailBody);
					$this->mailBody	=  str_replace('{LINK}',$link,$this->mailBody);
				} else {
					$this->value["user"]		= $data['Admin']['username'];
					$this->value["link"]		= $link;
					$this->template	= 'confirmadmin';
				}
				$to			=  $data['Admin']['username'];
				$flag		=  false;
				$flag		=  $this->sendmail($from,$subject,$to,$this->mailBody,$this->template,$this->value);
			/* end of code to send email */
				$this->Session->setFlash(__('The admin has been saved.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('controller'=>'admins','action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admin could not be saved. Please, try again.'));
			}
		}
	}

	function confirmation($token = null){
		$this->Admin->recursive = 0;
		$validateAdmin = $this->Admin->find("first",array("conditions"=>array("remembertoken"=>$token,"activationtoken"=>'')));
		if (!empty($validateAdmin)) {
			$this->Admin->create();
			$this->Admin->id = $validateAdmin['Admin']['id'];
			$admin['Admin']['status'] 			= 1;
			$admin['Admin']['activationtoken']	= $token;
			$this->Admin->save($admin);
			$this->Session->setFlash(__('Your account has been activated.'), 'default', array("class"=>"success_message"));
		} else {
			$this->Session->setFlash(__('You have already activate your account.'));
		}
		$this->redirect("/admin/");
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Admin->recursive = 0;
		$this->Admin->id = $id;
		if (!$this->Admin->exists()) {
			throw new NotFoundException(__('Invalid admin'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Admin->save($this->request->data)) {
				$this->Session->setFlash(__('The admin has been saved.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admin could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Admin->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		
		$this->Admin->id = $id;
		if (!$this->Admin->exists()) {
			throw new NotFoundException(__('Invalid admin'));
		}
		if ($this->Admin->delete()) {
			$this->Session->setFlash(__('Admin deleted.'), 'default', array("class"=>"success_message"));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Admin was not deleted.'));
		$this->redirect(array('action' => 'index'));
	}
	
	
public function driver_delete($id=null)
	{
		$this->Admin->id =$id;
        $this->Admin->delete();
        if ($this->Admin->delete()) {
        $this->Session->setFlash(__('Driver deleted.'), 'default', array("class"=>"success_message"));
			$this->redirect(array('action'=>'listdriver'));
		}
		$this->Session->setFlash(__('Admin was not deleted.'));
		$this->redirect(array('action' => 'listdriver'));
	}

	public function order_delete($id=null)
	{
		$this->loadModel("Directorders");
		$this->Directorders->primaryKey = 'oid';
		$this->Directorders->id = $id;

		if (!$this->Directorders->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->Directorders->delete()) {
			$this->Session->setFlash(__('Order deleted.'), 'default', array("class"=>"success_message"));
			$this->redirect(array('action'=>'assignlabdriver'));
		}
		$this->Session->setFlash(__('Order was not deleted.'));
		$this->redirect(array('action' => 'assignlabdriver'));
	}

	public function editprofile($id = null) {
		$admin = $this->Session->read("admin");
		if (isset($this->data) && !empty($this->data)) {
			$data = $this->data;
			if (isset($this->data['Admindetail']['company_logo']['name']) && !empty($this->data['Admindetail']['company_logo']['name'])) {
				$data['Admindetail']['company_logo'] = $this->data['Admindetail']['company_logo']['name'];
				$data['Admindetail']['company_logo'] = ($this->uploadimage($this->data['Admindetail']['company_logo'],$admin['Admin']['id'],NULL,NULL,'admin','logo'))?$this->uploaddir.$this->imagename:'';
			} else {
				unset($data['Admindetail']['company_logo']);
			}
			if(isset($this->data['Admindetail']['image']['name']) && !empty($this->data['Admindetail']['image']['name'])) {
				$data['Admindetail']['image'] = $this->data['Admindetail']['image']['name'];
				$data['Admindetail']['image'] = ($this->uploadimage($this->data['Admindetail']['image'],$admin['Admin']['id'],NULL,NULL,'admin','profilepic'))?$this->uploaddir.$this->imagename:'';
			} else {
				unset($data['Admindetail']['image']);
			}
			$this->uploaddir = $this->imagename = '';
			$this->Admindetail->create();
			if (isset($this->data['Admindetail']['id']) && !empty($this->data['Admindetail']['id'])) {
				$this->Admindetail->id = $this->data['Admindetail']['id'];
			} else {
				$data['Admindetail']['admin_id'] = $admin['Admin']['id'];
			}
			if ($this->Admindetail->save($data)) {
				$_SESSION['admin']['Admindetail'] = $data['Admindetail'];
			}
			$this->Session->setFlash("Profile has been saved successfully.", 'default', array("class"=>"success_message"));
			$this->redirect("/admin/editprofile");
		} else {
			$this->Admin->recursive = 1;
			$admindetail = $this->Admin->Admindetail->find("all",array("conditions"=>array("Admin.id"=>$admin['Admin']['id'])));
			if(isset($admindetail[0]) && !empty($admindetail[0])){
				$this->data = $admindetail[0];
			}
			
		}
		$this->set("addressdata",$this->getcountrystatecity());
	}
	
	public function getcountrystatecity($id = '',$opt='') {
		$result = array();
		if ($this->RequestHandler->isAjax()) {
			if (strtolower($_REQUEST['opt']) == 'country') {
				$result = $this->State->find("list",array("conditions"=>array("State.status"=>1,"State.country_id"=>$_REQUEST['id']),"order"=> "State.name asc"));
			} else {
				$result = $this->City->find("list",array("conditions"=>array("City.status"=>1,"City.state_id"=>$_REQUEST['id']),"order"=> "City.name asc"));
			}
			$this->set("result",$result);
			$this->set("option",(strtolower($_REQUEST['opt']) == 'country'?'Please select state':'Please select city'));
			$this->render("countrystatecity");
		}elseif(empty($id)){
			$result["Countries"]	= $this->Country->find("list",array("conditions"=>array("status"=>1),"order"=> "name asc"));
			$result["States"]		= $this->State->find("list",array("conditions"=>array("status"=>1),"order"=> "State.name asc"));
			$result["Cities"]		= $this->City->find("list",array("conditions"=>array("status"=>1),"order"=> "City.name asc"));
			return $result;
		}
		
		
	}
	
	public function configurations() {
		$this->loadModel("Configuration");
		if (isset($this->data) && !empty($this->data)) {
			foreach ($this->data['Configuration'] as $key=>$val) {
				if(is_array($val) && !empty($val['name'])){
					$val = ($this->uploadimage($val, NULL,NULL,"banners",NULL,NULL,"bannerimage".$key,1600, 268))?($this->uploaddir.$this->imagename):'';
					$config[$key] = array("id"=>$key,"value"=>$val);
				} elseif(is_array($val)){
					unset($config[$key]);
				} else{
					$config[$key] = array("id"=>$key,"value"=>$val);
				}
			}
			$this->Configuration->create();
			if ($this->Configuration->saveAll($config) ){
				$file = WWW_ROOT."custom/config.php";
				if(file_exists($file)) {
					//unlink($file);
				}
				//$this->configurations();
			}
			$this->Session->setFlash("Configuration has been updated.", 'default', array("class"=>"success_message"));
			$this->redirect("/admin/configurations");
		}
		$configuration = $this->Configuration->find("all");
		$this->set("configurations",$configuration);
		$this->render("configuration");
		//die;
	}
	
	function newsletter() {
		$this->loadModel("User");
		if($this->request->is("post")){
			$this->mailBody = !empty($this->request->data['Newsletter']['content'])?$this->request->data['Newsletter']['content']:'Hello';
			$this->subject = (isset($this->request->data['Newsletter']['subject']) && !empty($this->request->data['Newsletter']['subject']))?$this->request->data['Newsletter']['subject']:'Newsletter';
			$this->from = (isset($this->request->data['Newsletter']['from']) && !empty($this->request->data['Newsletter']['from']))?$this->request->data['Newsletter']['from']:'contact@1337institute.com';
			$users = explode(",",$this->request->data['Newsletter']['sentusers']);
			foreach($users as $user) {
				if($this->sendmail($user)) {
					//echo("<br/>".$user." <-----> sent<br/>");
				}
			}
			$this->Session->setFlash("Email has been sent to the users.", 'default', array("class"=>"success_message"));			
			$this->redirect(array("action"=>"newsletter"));
		}
		$this->User->recursive = 0;
		$users = $this->User->find("all",array("fields"=>array("Userdetail.first_name","Userdetail.last_name","User.username","Userdetail.image","Userdetail.email")));
		$this->set(compact("users"));
	}
	
	
	 function listdriver() {
		$this->autoRender = true;
		$this->loadModel("User");
		$pro = $this->User->find("all",array('conditions'=>array('User.user_type'=>4)));
		$this->set('pro' ,$pro);
		
		
}


function assigndriver() {
	
	//$this->autoRender = true;
		$this->loadModel("User");
		$this->loadModel("OrderStatus");
		$this->loadModel("Order");
		$this->loadModel("OrderDetail");
		$this->loadModel("Driver");
		$driver_data = array();
		$lab_detail = '';
		$doc_detail = '';
		if ($this->request->is('post') || $this->request->is('put'))
		 {
			$data=$this->data;
			
			//print_r($data);
			
			if($data['Admin']['select_action'] == 1){
				
			$driver_data['Driver']['order_id'] = $data['Admin']['order_id'];
			$driver_data['Driver']['doc_id'] = $data['Admin']['doc_id'];
			$driver_data['Driver']['lab_id'] = $data['Admin']['lab_id'];
			$driver_data['Driver']['driver_id'] = $data['Admin']['driver_id'];
			$driver_data['Driver']['order_date'] = $data['Admin']['date'];
			$driver_data['Driver']['update_on'] = date("Y-m-d H:i:s");
			
			
			$this->Driver->save($driver_data);
			
$this->User->updateAll(array('driver_assign'=>$data['Admin']['driver_id'], 'driver_status'=>'1'), array('User.id'=>$data['Admin']['lab_id']));
			
			
		$order_id = $this->getorderid($data['Admin']['order_id']);
		
		$l_data = $this->companydetail($data['Admin']['lab_id']);
		$area_id = $this->getstate($l_data['Userdetail']['state_id']);
		$area_name = $area_id[0]['State']['name'].', '.$area_id[0]['State']['code'];
		$lab_detail .= 'Lab name: '.$l_data['User']['company_name'].'<br><hr>'.$l_data['Userdetail']['address'].'<br>'.$l_data['Userdetail']['city'].', '.$area_name.'-'.$l_data['Userdetail']['zipcode'];
		
		$d_data = $this->companydetail($data['Admin']['doc_id']);
		$darea_id = $this->getstate($d_data['Userdetail']['state_id']);
		$darea_name = $darea_id[0]['State']['name'].', '.$darea_id[0]['State']['code'];
		$doc_detail .= 'Doctor name: '.$d_data['User']['company_name'].'<br><hr>'.$d_data['Userdetail']['address'].'<br>'.$d_data['Userdetail']['city'].', '.$darea_name.'-'.$d_data['Userdetail']['zipcode'];


//$lab_id for from and $doc_id for to 

			$driver = $this->companydetail($data['Admin']['driver_id']);
	$driver_name = $driver['Userdetail']['first_name'];
	$driver_email = $driver['User']['username'];
$title = 'You are assigned for labs regarding biolab order pickup on';
		//$user = 'mahavir.singh@the-appideas.com';
		$this->getmaildata(22);
					$this->mailBody = str_replace("{DRIVER}", $driver_name, $this->mailBody);
					$this->mailBody = str_replace("{LABS}", $lab_detail, $this->mailBody);
					$this->mailBody = str_replace("{DOCTORS}", $doc_detail, $this->mailBody);
					$this->mailBody = str_replace("{TITLE}", $title, $this->mailBody);
					//$this->mailBody = str_replace("{DNAME}", $data['Admin']['labdoctorname'], $this->mailBody);
					$this->mailBody = str_replace("{ID}", $order_id, $this->mailBody);
					$this->mailBody = str_replace("{DATE}", $data['Admin']['date'], $this->mailBody);
					$this->sendmail($driver_email);
}
else
{
	
$driver_data['Driver']['order_id'] = $data['Admin']['order_id'];
			$driver_data['Driver']['doc_id'] = $data['Admin']['lab_id'];
			$driver_data['Driver']['lab_id'] = $data['Admin']['doc_id'];
			$driver_data['Driver']['driver_id'] = $data['Admin']['driver_id'];
			$driver_data['Driver']['order_date'] = $data['Admin']['date'];
			$driver_data['Driver']['update_on'] = date("Y-m-d H:i:s");
			
			$this->Driver->save($driver_data);
			
$this->User->updateAll(array('driver_assign'=>$data['Admin']['driver_id'], 'driver_status'=>'1'), array('User.id'=>$data['Admin']['lab_id']));
			
			
		$order_id = $this->getorderid($data['Admin']['order_id']);
		
		$l_data = $this->companydetail($data['Admin']['lab_id']);
		$area_id = $this->getstate($l_data['Userdetail']['state_id']);
		$area_name = $area_id[0]['State']['name'].', '.$area_id[0]['State']['code'];
		$lab_detail .= 'Lab name: '.$l_data['User']['company_name'].'<br><hr>'.$l_data['Userdetail']['address'].'<br>'.$l_data['Userdetail']['city'].', '.$area_name.'-'.$l_data['Userdetail']['zipcode'];
		
		$d_data = $this->companydetail($data['Admin']['doc_id']);
		$darea_id = $this->getstate($d_data['Userdetail']['state_id']);
		$darea_name = $darea_id[0]['State']['name'].', '.$darea_id[0]['State']['code'];
		$doc_detail .= 'Doctor name: '.$d_data['User']['company_name'].'<br><hr>'.$d_data['Userdetail']['address'].'<br>'.$d_data['Userdetail']['city'].', '.$darea_name.'-'.$d_data['Userdetail']['zipcode'];




			$driver = $this->companydetail($data['Admin']['driver_id']);
	$driver_name = $driver['Userdetail']['first_name'];
	$driver_email = $driver['User']['username'];
$title = 'You are assigned for Remake order pickup from doctor on';
		//$user = 'mahavir.singh@the-appideas.com';
		$this->getmaildata(22);
					$this->mailBody = str_replace("{DRIVER}", $driver_name, $this->mailBody);
					$this->mailBody = str_replace("{LABS}", $doc_detail, $this->mailBody);
					$this->mailBody = str_replace("{DOCTORS}", $lab_detail, $this->mailBody);
					$this->mailBody = str_replace("{TITLE}", $title, $this->mailBody);
					//$this->mailBody = str_replace("{DNAME}", $data['Admin']['labdoctorname2'], $this->mailBody);
					$this->mailBody = str_replace("{ID}", $order_id, $this->mailBody);
					$this->mailBody = str_replace("{DATE}", $data['Admin']['date'], $this->mailBody);
					$this->sendmail($driver_email);


				}
		}
$driverlist= $this->User->find("all",array('conditions'=>array('User.user_type'=>4)));
$this->set('driverlist' ,$driverlist);

$order_data = $this->OrderStatus->find("all",array('fields'=>'DISTINCT OrderStatus.order_id'));
$data=array();
foreach($order_data as $id)
{
	//echo $this->getstatus($id['OrderStatus']['order_id']); 
	$test = $this->getstatus($id['OrderStatus']['order_id']);
	if( $test=='Biolab Pickup' || $test =='Remake Submitted')
	{
	$driver= $this->Driver->find("first",array('conditions'=>array('Driver.order_id'=>$id['OrderStatus']['order_id'])));
	if($driver){
	}else{
	$data[]=$id['OrderStatus']['order_id'];
}
}
	
}

$this->set('order_data', $data);


}

function prepay() {
$this->loadModel("Prepaid");
$this->loadModel("User");
$this->loadModel("Userdetail");
if ($this->request->is('post') || $this->request->is('put'))
		 {
$data=$this->data;
foreach($data['admin']['labname'] as $lab_id)
{
$pre= $this->Prepaid->find("first",array('conditions'=>array('Prepaid.lab_id'=>$lab_id)));
if($pre)
{
$this->Prepaid->updateAll(array('prepay_amt'=>$data['admin']['amt']), array('Prepaid.lab_id'=>$lab_id));
}
else
{
	$data2['Prepaid']['update_on'] = date("Y-m-d H:i:s");
	$data2['Prepaid']['prepay_amt'] = $data['admin']['amt'];
	$data2['Prepaid']['lab_id'] = $lab_id;
	$this->Prepaid->save($data2);

}
}
			 
			 
			 }

$lab_data= $this->User->find("all",array('conditions'=>array('User.user_type'=>2,'User.profile_status'=>1)));
$this->set('lab_data' ,$lab_data);




}
function assignorder() {
		$this->loadModel("Driver");
if ($this->request->is('post') || $this->request->is('put'))
		 {
//$data=$this->data;

			 }
$order= $this->Driver->find("all");
$data=array();
foreach($order as $id)
{
	//echo $this->getstatus($id['OrderStatus']['order_id']); 
	$test = $this->getstatus($id['Driver']['order_id']);
	if($test == 'Biolab Pickup' || $test =='Remake Submitted')
	{
	$driver= $this->Driver->find("first",array('conditions'=>array('Driver.order_id'=>$id['Driver']['order_id'])));
	if($driver){
			$data[]=$driver;
	}else{

}
}
	
}

//$this->set('order_data', $data);
$this->set('orders' ,$data);



}




function labname()
{
$this->loadModel("Order");
$this->loadModel("User");
$this->loadModel("Labdoctor");
	$this->autoRender = false;
	$order_id = $this->request->data['res'];

	$qry1 = $this->Order->find("first",array('conditions'=>array('Order.id'=>$order_id)));
	$id = $qry1['Order']['lab_id'];
	$id2 = $qry1['Order']['labdoctor'];
	$qry2 = $this->Labdoctor->find("first",array('conditions'=>array('Labdoctor.id'=>$id2)));
	
	$qry = $this->User->find("first",array('conditions'=>array('User.id'=>$id)));
	$qry['User']['doc_id'] = $qry1['Order']['doctor_id'];
	$user = $this->User->find("first",array('conditions'=>array('User.id'=>$qry1['Order']['doctor_id'])));
	$doc_name = $user['Userdetail']['first_name'];
	$qry['User']['doc_name'] = $doc_name;
	$qry['User']['labdoctor'] = $qry2['Labdoctor']['name'];
	echo json_encode($qry['User']);
}

function assignlabdriver($archive = null) {
    $this->loadModel("User");
    $this->loadModel("Directorder");
    
    $driverlist= $this->User->find("all",array('conditions'=>array('User.user_type'=>4)));
    $this->set('driverlist' ,$driverlist);
    
    if ($archive == null) {
        $orders= $this->Directorder->find("all",array('conditions'=>array('NOT'=>array('Directorder.payment_status'=>1)),'order'=>array('Directorder.oid'=>'desc')));
        require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
        $url=Braintree_TransparentRedirect::url();
        foreach ($orders as $order) {
            $aprize[$order['Directorder']['oid']]= $order['Directorder']['aprice'];
            $prize[$order['Directorder']['oid']]= $order['Directorder']['price'];
            $status[$order['Directorder']['oid']]= $order['Directorder']['payment_status'];
            if ($aprize[$order['Directorder']['oid']] != '') {
                if ($status[$order['Directorder']['oid']] == '1') {
                    $orderiid[$order['Directorder']['oid']] = $order['Directorder']['oid'];
                    $labid[$order['Directorder']['oid']] = $order['Directorder']['lab_id'];
                    $amount = $aprize[$order['Directorder']['oid']] - $prize[$order['Directorder']['oid']];
                    $total_p[$order['Directorder']['oid']] = number_format((($amount*2.9/100)+$amount+0.30), 2);
                    $tr_data[$order['Directorder']['oid']] = Braintree_TransparentRedirect::transactionData(array(
                        'redirectUrl' => "http://biolablogistics.com/admins/getresult/".$labid[$order['Directorder']['oid']],
                        'transaction' => array(
                            'orderId'=>$orderiid[$order['Directorder']['oid']],
                            'amount' => $total_p[$order['Directorder']['oid']],
                            'type' => 'sale',
                            'options' => [
                                'submitForSettlement' => True
                            ],
                        )
                    ));
                } else {
                    $orderiid[$order['Directorder']['oid']] = $order['Directorder']['oid'];
                    $labid[$order['Directorder']['oid']] = $order['Directorder']['lab_id'];
                    $amount = $aprize[$order['Directorder']['oid']];
                    $total_p[$order['Directorder']['oid']] = number_format((($amount*2.9/100)+$amount+0.30), 2);
                    $tr_data[$order['Directorder']['oid']] = Braintree_TransparentRedirect::transactionData(array(
                        'redirectUrl' => "http://biolablogistics.com/admins/getresult/".$labid[$order['Directorder']['oid']],
                        'transaction' => array(
                            'orderId'=>$orderiid[$order['Directorder']['oid']],
                            'amount' => $total_p[$order['Directorder']['oid']],
                            'type' => 'sale',
                            'options' => [
                                'submitForSettlement' => True
                            ],
                        )
                    ));
                }
            } else {
                $orderiid[$order['Directorder']['oid']] = $order['Directorder']['oid'];
                $labid[$order['Directorder']['oid']] = $order['Directorder']['lab_id'];
                $amount = $prize[$order['Directorder']['oid']];
                $total_p[$order['Directorder']['oid']] = number_format((($amount*2.9/100)+$amount+0.30), 2);
                $tr_data[$order['Directorder']['oid']] = Braintree_TransparentRedirect::transactionData(array(
                    'redirectUrl' => "http://biolablogistics.com/admins/getresult/".$labid[$order['Directorder']['oid']],
                    'transaction' => array(
                        'orderId'=>$orderiid[$order['Directorder']['oid']],
                        'amount' => $total_p[$order['Directorder']['oid']], 
                        'type' => 'sale',
                        'options' => [
                            'submitForSettlement' => True
                        ],
                    )
                ));
            }
        }
        
        $this->set('url' ,$url);
        $this->set('total_p' ,$total_p);
        $this->set('tr_data' ,$tr_data);
    } else {
        $orders= $this->Directorder->find("all",array('conditions'=>array('Directorder.payment_status'=>1), 'order'=>array('Directorder.oid'=>'desc')));
    }
    
    $this->set('archive' ,$archive);
    $this->set('orders' ,$orders);
}

function getresult($labid = null){
	ob_start();
require_once( ROOT. DS . 'vendors' . DS . 'braintree' . DS . '_environment.php');
$result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
//print_r($result);
//echo $orderid;

if($result->success==1)
       {
		   
		$this->loadModel('Labtransaction');
		$this->loadModel('Directorder');
		 $transaction = $result->transaction;
		   
		 $tid= $transaction->id;
        $price = $transaction->amount;
         $orderid = $transaction->orderId;



        

$ordata= $this->Directorder->find("first",array('conditions'=>array('Directorder.oid'=>$orderid)));

if($ordata['Directorder']['aprice'] != ''){
	if($ordata['Directorder']['payment_status'] == '1'){
		$ordee = '#'.$orderid;
$this->Directorder->updateAll(array('astatus' => 1), array('Directorder.oid'=>$orderid)); 
$this->Labtransaction->updateAll(array('adminpay_amt' => $price), array('Labtransaction.order_id'=>$ordee)); 
}
else{
	$lab = $this->companyname($labid);
		   $transactiondata['Labtransaction']['check_recieved'] = 5;
			$transactiondata["Labtransaction"]['pay_amount'] = $price;
			$transactiondata["Labtransaction"]['order_id'] = '#'.$orderid;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'Braintree';
			$transactiondata["Labtransaction"]['lab_name'] = $lab;
			$this->Labtransaction->save($transactiondata);
	
	$this->Directorder->updateAll(array('astatus' => 1), array('Directorder.oid'=>$orderid));
$this->Directorder->updateAll(array('payment_status' => 1), array('Directorder.oid'=>$orderid));
}
}
else{
$lab = $this->companyname($labid);
		   $transactiondata['Labtransaction']['check_recieved'] = 5;
			$transactiondata["Labtransaction"]['pay_amount'] = $price;
			$transactiondata["Labtransaction"]['order_id'] = '#'.$orderid;
			$transactiondata["Labtransaction"]['transaction_id'] = $tid;
			$transactiondata["Labtransaction"]['pay_method'] = 'Braintree';
			$transactiondata["Labtransaction"]['lab_name'] = $lab;
			$this->Labtransaction->save($transactiondata);

$this->Directorder->updateAll(array('payment_status' => 1), array('Directorder.oid'=>$orderid));
}

		$this->Session->setFlash('Transaction Successfully done', 'default', array("class" => "error_message"));
		$this->redirect("/admins/assignlabdriver");
	   }
	   else{
		   
		   $this->Session->setFlash($result->message, 'default', array("class" => "error_message"));
		  $this->redirect("/admins/assignlabdriver"); 
	   }
}



function updateassignlabdriver(){
	$this->autoRender = false;
	$this->loadModel("Directorder");
	if($this->request->is('ajax')){
	$order_s = $this->request->data['action'];
	$order_id = $this->request->data['orderid'];
	$pays = $this->request->data['pays'];
	
if($order_s == 'Pickup'){

	if($pays == 1){
		$driver = $this->request->data['driver'];
	$qry = $this->Directorder->updateAll(array('order_status'=> "'$order_s'", 'driver_id' => $driver), array('Directorder.oid'=>$order_id));
	
	$order= $this->Directorder->find("first",array('conditions'=>array('Directorder.oid'=>$order_id)));

$state2 = $this->getstate($order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			$to = $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'].'<hr><b>Pickup : </b>'.$order['Directorder']['dropoff'];
					$state1 = $this->getstate($order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			$from = $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip'].'<hr><b>Dropoff : </b>'.$order['Directorder']['pickup'];


$driver = $this->companydetail($driver);
$driver_name = $driver['Userdetail']['first_name'];
$driver_email = $driver['User']['username'];
$title = 'You are assigned for pickup and dropoff packages with following detail.'.'<br>'.'<b>Priority : </b>'.$order['Directorder']['priority'].'<br>'.'<b>Recepient : </b>'.$order['Directorder']['recipient'].'<br><b>Date :</b>';
		//$user = 'mahavir.singh@the-appideas.com';
		$this->getmaildata(22);
					$this->mailBody = str_replace("{DRIVER}", $driver_name, $this->mailBody);
					$this->mailBody = str_replace("{LABS}", $from, $this->mailBody);
					$this->mailBody = str_replace("{DOCTORS}", $to, $this->mailBody);
					$this->mailBody = str_replace("{TITLE}", $title, $this->mailBody);
					$this->mailBody = str_replace("{ID}", '#'.$order_id, $this->mailBody);
					$this->mailBody = str_replace("{DATE}", date('m/d/y'), $this->mailBody);
					$this->sendmail($driver_email);
		
		
	
}else
{
		
	$driver = $this->request->data['driver'];
	$price = $this->request->data['price'];
	$pickup = $this->request->data['pickup'];
	$dropoff = $this->request->data['dropoff'];

	$qry = $this->Directorder->updateAll(array('order_status'=> "'$order_s'", 'driver_id' => $driver, 'price' => $price, 'pickup' => $pickup, 'dropoff' => $dropoff), array('Directorder.oid'=>$order_id));
	
$order= $this->Directorder->find("first",array('conditions'=>array('Directorder.oid'=>$order_id)));

$state2 = $this->getstate($order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			$to = $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'].'<hr><b>Dropoff : </b>'.$order['Directorder']['dropoff'];
					$state1 = $this->getstate($order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			$from = $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip'].'<hr><b>Pickup : </b>'.$order['Directorder']['pickup'];


$driver = $this->companydetail($driver);
$driver_name = $driver['Userdetail']['first_name'];
$driver_email = $driver['User']['username'];
$title = 'You are assigned for pickup and dropoff packages with following detail.'.'<br>'.'<b>Priority : </b>'.$order['Directorder']['priority'].'<br>'.'<b>Recepient : </b>'.$order['Directorder']['recipient'].'<br><b>Date :</b>';
		//$user = 'mahavir.singh@the-appideas.com';
		$this->getmaildata(22);
					$this->mailBody = str_replace("{DRIVER}", $driver_name, $this->mailBody);
					$this->mailBody = str_replace("{LABS}", $from, $this->mailBody);
					$this->mailBody = str_replace("{DOCTORS}", $to, $this->mailBody);
					$this->mailBody = str_replace("{TITLE}", $title, $this->mailBody);
					$this->mailBody = str_replace("{ID}", '#'.$order_id, $this->mailBody);
					$this->mailBody = str_replace("{DATE}", date('m/d/y'), $this->mailBody);
					$this->sendmail($driver_email);
}
}
else{
    $qry = $this->Directorder->updateAll(array('order_status'=> "'".$order_s."'"), array('Directorder.oid'=>$order_id));
}



		echo ($qry)?'Done':'Data Loading...';
                exit;
	
	}

}

function deliverycharge() {
$this->loadModel("Deliverycharge");
$this->loadModel("User");
$this->loadModel("Userdetail");
if ($this->request->is('post') || $this->request->is('put'))
		 {
$data=$this->data;
//print_r($data);
//exit;
foreach($data['admin']['labname'] as $lab_id)
{
$pre= $this->Deliverycharge->find("first",array('conditions'=>array('Deliverycharge.lab_id'=>$lab_id)));
if($pre)
{
	if(!empty($data['admin']['delivery_amt'])){
$this->Deliverycharge->updateAll(array('delivery_amt'=>$data['admin']['delivery_amt']), array('Deliverycharge.lab_id'=>$lab_id));
}
	if(!empty($data['admin']['mile_amt'])){
$this->Deliverycharge->updateAll(array('mile_amt'=>$data['admin']['mile_amt']), array('Deliverycharge.lab_id'=>$lab_id));
}
}
else
{
	$data2['Deliverycharge']['update_on'] = date("Y-m-d H:i:s");
	if(!empty($data['admin']['delivery_amt'])){
	$data2['Deliverycharge']['delivery_amt'] = $data['admin']['delivery_amt'];
}else{ $data2['Deliverycharge']['delivery_amt'] = '5'; }
if(!empty($data['admin']['mile_amt'])){
	$data2['Deliverycharge']['delivery_amt'] = $data['admin']['mile_amt'];
}else{ $data2['Deliverycharge']['mile_amt'] = '0.50'; }
	$data2['Deliverycharge']['lab_id'] = $lab_id;
	$this->Deliverycharge->save($data2);

}
}
 }

$lab_data= $this->User->find("all",array('conditions'=>array('User.user_type'=>2,'User.profile_status'=>1)));
$this->set('lab_data' ,$lab_data);

}

function updatedirectorder(){
$this->loadModel("Directorder");
$data = $this->data;
//print_r($data);

if($data['priority'] == 'Rush'){
		$del = $this->getdelivery($data['labid']);
		if($del == ''){
		$distance = $this->getzipdistance($data['pickzip'], $data['dropzip']);
		$rush_price = (0.5)*(str_replace(' mi','',$distance));
		$delivery = ($data['apickup']*5)+($data['adropoff']*5);
		$prize = $rush_price + $delivery;
		}else{
			
		$distance = $this->getzipdistance($data['pickzip'], $data['dropzip']);
		$rush_price = ($del['Deliverycharge']['mile_amt'])*(str_replace(' mi','',$distance));
		$delivery = ($data['apickup']*$del['Deliverycharge']['delivery_amt'])+($data['adropoff']*$del['Deliverycharge']['delivery_amt']);
		$prize = $rush_price + $delivery;
		}
	}else{
	$prize = ($data['apickup']*5)+($data['adropoff']*5);
}

$this->Directorder->updateAll(array('apickup'=>$data['apickup'], 'adropoff'=>$data['adropoff'], 'aprice'=>$prize), array('Directorder.oid'=>$data['orderid']));

$this->redirect('/admins/assignlabdriver');
}


}
