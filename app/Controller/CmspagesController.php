<?php
App::uses('AppController', 'Controller');
/**
 * Cmspages Controller
 *
 * @property Cmspage $Cmspage
 */
class CmspagesController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Cmspage.name' => 'asc'
		)
	);
	function beforefilter() {
		parent::beforefilter();
		$allowed = array();
		$this->checklogin($allowed);
		$this->adminbreadcrumb();
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set("title_for_layout",'Static Pages');
		$this->bulkactions();
		/* code to perform search functionality */
		if(isset($this->data) && !empty($this->data['Cmspage']['searchval'])){
			$this->Session->write('searchval',$this->data['Cmspage']['searchval']);
			$this->conditions	= array("OR"=>array("Cmspage.name like"=>"%".$this->data['Cmspage']['searchval']."%"));
		}
		
		if(isset($this->params['named']['page'])){
			
			if($this->Session->read('searchval')){
				$this->conditions	= array("OR"=>array("Cmspage.name like"=>"%".$this->Session->read('searchval')."%"));
				$this->data['Cmspage']['searchval'] = $this->Session->read('searchval');
			}
		}elseif(empty($this->conditions)){
			$this->Session->delete('searchval');
		}
		/* end of code to perform search functionality */
		$this->set('cmspages', $this->paginate($this->conditions));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		$this->set("title_for_layout",'Add Page');
		if ($this->request->is('post')) {
			$this->Cmspage->create();
			if ($this->Cmspage->save($this->request->data)) {
				$this->Session->setFlash(__('Page has been created successfully.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Page has not been created, please try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->set("title_for_layout",'Edit Pade');
		$this->Cmspage->id = $id;
		if (!$this->Cmspage->exists()) {
			//throw new NotFoundException(__('Invalid cms page'));
			$this->Session->setFlash(__('Invalid cms page.'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$data['Cmspage']['content'] = str_replace('&nbsp;','',strip_tags($data['Cmspage']['content']));
			$this->Cmspage->set($data);
			if($this->Cmspage->validates()){
				if ($this->Cmspage->save($this->request->data)) {
					$this->Session->setFlash(__('Page has been updated successfully.'), 'default', array("class"=>"success_message"));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Page has not been updated successfully.'));
				}
			}
		} else {
			$this->request->data = $this->Cmspage->read(null, $id);
		}
	}


	public function admin_newcmspage($id = null) {
		$this->set("title_for_layout",'New Page');
		if ($this->request->is('post') || $this->request->is('put')) {
			
				if ($this->Cmspage->save($this->request->data)) {
					$this->Session->setFlash(__('Page has been created successfully.'), 'default', array("class"=>"success_message"));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Page has not been created successfully.'));
				}
			}
		
		
	}
	
	public function admin_popcontent(){
		$this->set("title_for_layout",'Terms & conditions');
		$this->loadmodel('Term');

		if ($this->request->is('post') || $this->request->is('put')) {
			
			$ddata = $this->request->data;
			$data = $ddata['Cmspage']['content'];
$ddone = $this->Term->updateAll(array('content'=>"'$data'"), array('Term.id'=>'1'));

				if ($ddone) {
					$this->Session->setFlash(__('Terms & Conditions has been successfully.'), 'default', array("class"=>"success_message"));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Terms & Conditions has not been created successfully.'));
				}
			}
}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	/*public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Cmspage->id = $id;
		if (!$this->Cmspage->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->Cmspage->delete()) {
			$this->Session->setFlash(__('Page has been deleted successfully.'), 'default', array("class"=>"success_message"));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('The page could not be deleted. Please, try again.'));
		$this->redirect(array('action' => 'index'));
	}*/
}
