<?php
App::uses('AppController', 'Controller');
/**
 * Userdetails Controller
 *
 * @property Userdetail $Userdetail
 */
class UserdetailsController extends AppController {
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Userdetail->recursive = 0;
		$this->set('userdetails', $this->paginate());
	}
	
	function beforefilter() {
		parent::beforefilter();
		$this->checklogin();
		$this->adminbreadcrumb();
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Userdetail->id = $id;
		if (!$this->Userdetail->exists()) {
			throw new NotFoundException(__('Invalid userdetail'));
		}
		$this->set('userdetail', $this->Userdetail->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Userdetail->create();
			if ($this->Userdetail->save($this->request->data)) {
				$this->Session->setFlash(__('The userdetail has been saved.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The userdetail could not be saved. Please, try again.'));
			}
		}
		$users = $this->Userdetail->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_profile_account($id = null) {
		$this->layout = "frontend_new";
		//$this->layout = "frontend";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("Country");
		$this->loadModel("State");
		$this->loadModel("User");
		if (isset($this->data) && !empty($this->data) ) {
			if (isset($this->data['Userdetail']['email'])) {
				$this->Userdetail->id = $this->Session->read("Auth.User.Userdetail.id");
				$useremail['Userdetail']['email'] = $this->data['Userdetail']['email'];
				if ($this->Userdetail->save($useremail)) {
					$this->Session->write("Auth.User.Userdetail.email",$useremail);
					$this->Session->setFlash("Your email has been updated.", 'default', array("class"=>"success_message"));
					$this->redirect("/account");
				}
			} else {
				$data = $this->data;
				$data['User']['currentpassword'] = (!empty($data['User']['currentpassword'])?$this->Auth->password($data['User']['currentpassword']):'');
				$data['User']['password'] = (!empty($data['User']['password'])?$this->Auth->password($data['User']['password']):'');
				$data['User']['confirmpassword'] = (!empty($data['User']['confirmpassword'])?$this->Auth->password($data['User']['confirmpassword']):'');
				$this->User->set($data);
				if ($this->User->validates()) {
					$this->User->create();
					$this->User->id = $this->Session->read("Auth.User.id");
					if($this->User->save($data, array('validate' => false))) {
						$this->Session->setFlash("Password has been updated successfully.", 'default', array("class"=>"success_message"));
						//$this->redirect("changepassword");
					} else {
						$this->Session->setFlash("Password can not updated successfully, Please try again.");
					}
				} 		
			}
		}
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.state_id","Userdetail.country_id","Userdetail.email","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.image")),array("recursive"=>0));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];
		/* to create dropdown for countries */
		$this->Country->recursive = -1;
		$this->set("country",$this->Country->find("list",array("conditions"=>array("Country.status"=>1),"fields"=>array("Country.id","Country.name"))));
		/* end here */
		/* to create dropdown for countries */
		$this->State->recursive = -1;
		$this->set("state",$this->State->find("list",array("conditions"=>array("State.status"=>1,"State.country_id"=>$user[0]["Userdetail"]["country_id"]),"fields"=>array("State.id","State.name"))));
		/* end here */
		
	}
	
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function premium_instructor($id = null) {
		$this->layout = "frontend";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		if (isset($this->data) && !empty($this->data) ) {
			if (isset($this->data['Userdetail']['paypalaccount'])) {
				$this->Userdetail->id = $this->Session->read("Auth.User.Userdetail.id");
				$useremail['Userdetail']['paypalaccount'] = $this->data['Userdetail']['paypalaccount'];
				if ($this->Userdetail->save($useremail)) {
					$this->Session->write("Auth.User.Userdetail.paypalaccount",$useremail);
					$this->Session->setFlash("Your paypal email has been updated.", 'default', array("class"=>"success_message"));
					$this->redirect("/paypal-account");
				}
			} 
		}
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.paypalaccount","Userdetail.state_id","Userdetail.country_id","Userdetail.email","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.heading","Userdetail.image","User.newsletter")),array("recursive"=>0));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Userdetail->id = $id;
		if (!$this->Userdetail->exists()) {
			throw new NotFoundException(__('Invalid userdetail'));
		}
		if ($this->Userdetail->delete()) {
			$this->Session->setFlash(__('Userdetail deleted.', 'default', array("class"=>"success_message")));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Userdetail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Userdetail->recursive = 0;
		$this->set('userdetails', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Userdetail->id = $id;
		if (!$this->Userdetail->exists()) {
			throw new NotFoundException(__('Invalid userdetail'));
		}
		$this->set('userdetail', $this->Userdetail->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Userdetail->create();
			if ($this->Userdetail->save($this->request->data)) {
				$this->Session->setFlash(__('The userdetail has been saved.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The userdetail could not be saved. Please, try again.'));
			}
		}
		$users = $this->Userdetail->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Userdetail->id = $id;
		if (!$this->Userdetail->exists()) {
			throw new NotFoundException(__('Invalid userdetail'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Userdetail->save($this->request->data)) {
				$this->Session->setFlash(__('The userdetail has been saved.'), 'default', array("class"=>"success_message"));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The userdetail could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Userdetail->read(null, $id);
		}
		$users = $this->Userdetail->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Userdetail->id = $id;
		if (!$this->Userdetail->exists()) {
			throw new NotFoundException(__('Invalid userdetail.'));
		}
		if ($this->Userdetail->delete()) {
			$this->Session->setFlash(__('Userdetail deleted.'), 'default', array("class"=>"success_message"));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Userdetail was not deleted.'));
		$this->redirect(array('action' => 'index'));
	}
	
/**
 * edit_profile method
 * @Modified by		: Nishi Kant Sharma
 * @Modified on		: 9th feb 2015
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_profile(){
		$this->layout = "frontend_new";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		// save user details
		if(isset($this->data) && !empty($this->data)) { 
			$address = $this->getaddress($this->data['Userdetail']['zipcode']);
			$data = $this->data;
			/*$data['Userdetail']['latitude'] = $address['lat'];
			$data['Userdetail']['longitude'] = $address['long'];*/
			$turnarounds = array();
			if ( isset($this->data['Userdetail']['turnarounds']) && !empty($this->data['Userdetail']['turnarounds']) ) {
				foreach ( $this->data['Userdetail']['turnarounds'] as $key=>$val ) {
					if ( !empty($val['name']) ) {
						$turnarounds[] = $val;
					}
				}
			}
			if ( !empty($turnarounds) ) {
				$data['Userdetail']['turnarounds'] = serialize($turnarounds);
			}
			if ( isset($this->data['Userdetail']['timings']) ) {
				$data['Userdetail']['timings'] = serialize($this->data['Userdetail']['timings']);
			}
			$this->Userdetail->set($data);
			if($this->Userdetail->validates()){
				// find user detail id with user_id
				$userdetail = $this->Userdetail->find('first', array('fields' => array('Userdetail.id',"Userdetail.user_id"), 'conditions' => 'Userdetail.user_id='.$id));
		        $data['Userdetail']['id'] = $userdetail['Userdetail']['id'];
				$this->request->data = $data;		
				if($this->Userdetail->save($this->request->data)){
					$this->User->id = $userdetail['Userdetail']['user_id'];
					$this->User->save($this->request->data);
					$this->getuserdetail();
					$this->Session->setFlash(__('Profile has been updated successfully.'), 'default', array("class"=>"success_message"));
				}
			}
		}
		
		// fetch user details
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.company_name","User.created","Userdetail.id","Userdetail.user_id","Userdetail.state_id","Userdetail.country_id","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.image","Userdetail.designation","Userdetail.biography","Country.name","State.name","Userdetail.address","Userdetail.zipcode","Userdetail.webLink","Userdetail.serviceTags","Userdetail.turnarounds","Userdetail.timings", "Userdetail.latitude", "Userdetail.longitude")),array("recursive"=>0));
		$this->set("turnarounds",unserialize($user[0]['Userdetail']['turnarounds']));
		$this->set("timings",unserialize($user[0]['Userdetail']['timings']));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];			
		$this->loadModel('State');
		$this->State->recursive = -1;
		$this->loadModel("Service");
		$i = 1;
		$time = array();
		do {
			if ( $i < 13 ) { 
				$time[str_pad($i,2,0,STR_PAD_LEFT).":00 AM"] = str_pad($i,2,0,STR_PAD_LEFT).":00 AM";
			} else {
				$time[str_pad($i - 12,2,0,STR_PAD_LEFT).":00 PM"] = str_pad($i - 12,2,0,STR_PAD_LEFT).":00 PM";
			}
			$i++;
		} while ($i < 25);
		$this->set("time",$time);
		$weekArray = array(6=>"Sunday",0=>"Monday",1=>"Tuesday",2=>"Wednesday",3=>"Thursday",4=>"Friday",5=>"Saturday");
		$services = $this->Service->find("list",array("conditions"=>array("status"=>1),"fields"=>array("name","name")));
		$services['Other'] = "Other";
		$this->set("weekArray",$weekArray);
		$this->set("services",$services);
		$this->set("state", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));

	}
	
	
		/*
	 * @function name	: get_states
	 * @purpose			: To get state data related to country. 	
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 9th feb 2015
	 * @description		: NA
	 */
	 
	public function get_states()
		{
			$this->autoRender=false;
			$this->layout='ajax';
			if( $this->request->is('ajax') ) {
				$country_id = $this->request->data['country_id'];
				$this->loadModel('State');
				$this->State->recursive = -1;
				$res_data = $this->State->find("list", array("conditions" => array("State.country_id" => $country_id), "fields" => array("State.id", "State.name")));
				echo json_encode($res_data);
		   }			
		}
	

/**
 * edit_profile_photo method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	
	public function edit_profile_photo($id=null){
		//$this->layout = "frontend";
		$this->layout = "frontend_new";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("Country");
		$this->loadModel("State");
		$this->loadModel("User");
		
		if (isset($this->data) && !empty($this->data)) {
			if(!empty($this->data['Userdetail']['image']['name'])){
				$count = 0;
				$userid = $id;
				$userdetails['Userdetail'] = $this->data['Userdetail'];
				$userdetails['Userdetail']['image'] = $userdetails['Userdetail']['image']['name'];
				$this->Userdetail->set($userdetails);
				if ($this->Userdetail->validates()) {
					$count++;
				}
				if($count == 1) {					
				
					if (isset($this->data['Userdetail']['image']) && !empty($this->data['Userdetail']['image']['name'])) {
						$userdetails['Userdetail']['image'] = ($this->uploadimage($this->data['Userdetail']['image'],$userid,NULL,NULL,NULL,"profileimg","profile",210,210))?$this->uploaddir.$this->imagename:'';
					} else {
						unset($userdetails['Userdetail']['image']);
					}				
					$this->Userdetail->create();
					$this->Userdetail->id = $userdetails['Userdetail']['id'];					
					$this->Userdetail->save($userdetails, array('validate' => false));
					$this->User->id = $userid;
					$this->User->save($this->data);
					$countinds = 0;
					$this->getuserdetail();
					$this->Session->setFlash("Profile photo has been updated successfully.", 'default', array("class"=>"success_message"));
				}		
			} else{
				$this->Session->setFlash("Please select a photo to upload.");
			}
		}
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.state_id","Userdetail.country_id","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.image")),array("recursive"=>0));
		$this->Session->write("Auth.User.Userdetail.image", $user[0]["Userdetail"]["image"]);
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];
		/* to create dropdown for countries */
		$this->Country->recursive = -1;
		$this->set("country",$this->Country->find("list",array("conditions"=>array("Country.status"=>1),"fields"=>array("Country.id","Country.name"))));
		/* end here */
		/* to create dropdown for countries */
		$this->State->recursive = -1;
		$this->set("state",$this->State->find("list",array("conditions"=>array("State.status"=>1,"State.country_id"=>$user[0]["Userdetail"]["country_id"]),"fields"=>array("State.id","State.name"))));
		/* end here */
		
		
	}

/**
 * edit_profile_privacy method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	
	public function edit_profile_privacy(){
		//$this->layout='frontend';
		$this->layout = "frontend_new";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		//echo $id;
		// save privacy settings
		if ($this->request->is('put')) {
			if($this->Userdetail->updateAll(array('Userdetail.privacy'=> "'".serialize($this->request->data['Userdetail'])."'"),array('Userdetail.user_id'=>$id))){
				$this->Session->setFlash('Your privacy settings has been updated.', 'default', array("class"=>"success_message"));
			}
		}
		
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.paypalaccount","Userdetail.state_id","Userdetail.country_id","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.heading","Userdetail.image","User.newsletter","Userdetail.privacy")),array("recursive"=>0));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];
		
		
		
	}

/**
 * edit_profile_notification method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	
	public function edit_profile_notification(){
		$this->layout='frontend';
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		// for user's information in left panel
		$this->loadModel("User");
		$this->loadModel("Usernotification");
		$this->loadModel("Notification");
		
		
		// save notification settings
		if ($this->request->is('put') && isset($this->request->data['Userdetail'])) {
			//find userdetail id as per user_id
			$userdetail = $this->Userdetail->find('first', array('fields' => array('Userdetail.id'), 'conditions' => 'Userdetail.user_id='.$id));
			
			$this->request->data['Userdetail']['id'] = $userdetail['Userdetail']['id'];
			
			if(!isset($this->request->data['Userdetail']['dontSendNotification'])){
				$this->request->data['Userdetail']['notification'] = serialize($this->request->data['Userdetail']['notifications']);
			} else{
				$this->request->data['Userdetail']['notification'] = '';
			}
			
			if($this->Userdetail->save($this->request->data['Userdetail'])) {
				$this->getuserdetail();
				$this->Session->setFlash("Notification settings has been saved.", 'default', array("class"=>"success_message"));
			} else {
				$this->Session->setFlash("Notification settings couldn't be saved.");
			}
		}
		
		// fetch user details for view
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.paypalaccount","Userdetail.state_id","Userdetail.country_id","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.heading","Userdetail.image","Userdetail.notification","User.newsletter")),array("recursive"=>0));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];
		$this->set('notifi', unserialize($this->data['Userdetail']['notification']));
		// display user profile notification
		
		$notifications = $this->Notification->find('all',array('conditions'=>array('Notification.enable'=>1)));
		$this->set('notifications' , $notifications);
		
		
	}

/**
 * edit_profile_dangerzone method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	
	public function edit_profile_dangerzone(){
		//$this->layout='frontend';
		$this->layout = "frontend_new";
		$this->set("title_for_layout",$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"));
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		// delete account with all the dependants
		if ($this->request->is('put') || $this->request->is('post')) {
			if ($this->User->delete($id)) {
				if(is_dir(WWW_ROOT."img/".$id)) {
					exec("rm -rf ".WWW_ROOT."img/".$id);
				}
			}
			$this->Session->delete("Auth.User.id");
			$this->Session->setFlash("Your account has been permanently deleted.", 'default', array("class"=>"success_message"));
			$this->redirect('/');
		}
		// fetch user details
	
		$user = $this->Userdetail->find("all",array("conditions"=>array("User.id"=>$id),"fields"=>array("User.username","User.status","User.created","Userdetail.id","Userdetail.user_id","Userdetail.state_id","Userdetail.country_id","Userdetail.first_name","Userdetail.last_name","Userdetail.about","Userdetail.phone","Userdetail.city","Userdetail.image","User.newsletter")),array("recursive"=>0));
		$this->set("content_for_title",$user[0]["Userdetail"]["first_name"]." ".$user[0]["Userdetail"]["first_name"]);
		$this->data = $user[0];

	}

/**
 * function name :save_doc_lab
 * @Modified by		: Nishi Kant Sharma
 * @Modified on		: 2nd March 2015
 * description      : To add labs for specific doctor
 */
	public function save_doc_lab($lab_id = Null,$op_type = Null){		
		$result = array("error"=>true,"message"=>"An error occured, Please try again.");	
		$this->autoRender=false;		
		if( $this->request->is('ajax') ) {	
			$this->loadModel('DoctorLab');
			if ( $op_type == 'remove' ) {
				if($this->DoctorLab->deleteAll(array("lab_id"=>$lab_id,"doctor_id"=>$this->Session->read("Auth.User.id")))){
					$result = array("error"=>false,"message"=>"Lab has been removed successfully.");
				}
			} elseif ( $op_type == 'add' ) {
				$data['DoctorLab']['doctor_id'] = $this->Session->read('Auth.User.id');
				$data['DoctorLab']['lab_id'] = $lab_id;
				//$conditions = array('doctor_id' => $this->Session->read('Auth.User.id'));
				//$totallabs = $this->DoctorLab->find('list', array('conditions' => $conditions,'fields'=>array('id','lab_id')));				
					$this->DoctorLab->create();
					if( $this->DoctorLab->save($data) ) {					
						$result = array("error"=>false,"message"=>"Lab has been added successfully.");					
					} else {
						$result = array("error"=>true,"message"=>"Lab has not been added successfully.");		
					}							
				
			}
			return json_encode($result);
	   }
    }
    
    
   public function seldoctor()
   {
	   $this->layout = "frontend_new";
	   $this->loadModel('State');
	   $this->loadModel('Labdoctor');
	   	$state = $this->State->find('all');
	   	$this->set('state',$state);
	   if ($this->request->is('post')) 
	   {
		   
		  $data= $this->data;
		  $arr['Labdoctor']['name']= $data['User']['name'];
		  $arr['Labdoctor']['email']= $data['User']['email'];
		  $arr['Labdoctor']['address']= $data['User']['address'];     
          $arr['Labdoctor']['city']= $data['User']['city']; 
          $arr['Labdoctor']['state']= $data['User']['state']; 
          $arr['Labdoctor']['phone']= $data['User']['phone']; 
          $arr['Labdoctor']['lab_id']= $data['User']['id']; 
         if($this->Labdoctor->save($arr))
           {			
			   		
						$this->Session->setFlash("Doctor has been added successfully.", 'default', array("class" => "success_message"));					
					} else {
						$this->Session->setFlash("Doctor has not been added successfully.", 'default', array("class" => "error_message"));		
					}	
				$this->redirect('/Seldoctor');
		  
	   }
	   
	   
   } 
function changecard(){
	$this->layout = "frontend_new";
	$this->loadmodeal('Carddetail');
echo 'hello';
exit;
}





}
