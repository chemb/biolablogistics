<?php

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Users Controller
 *
 * @property User $User
 * @version		: 1.0
 * @created by	: shivam sharma
 */
class UsersController extends AppController {

    public $userstatus = '';

    /*
     * @function name	: beforefilter
     * @purpose			: index page for site user
     * @arguments		: none
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    function beforefilter() {
        if ($this->params['action'] == 'login') {
            if ($this->Cookie->read('Auth.User') && !$this->Session->read("Auth.User.id")) {
                $this->login();
            }
        }
        if ($this->params['action'] == 'logout') {
            $this->logout();
        }
        parent::beforefilter();
        $this->Auth->allow("login", "logout", "signup", "uniqueuser", "newpassword", "requestaccount", "forgotpassword", "getindustry", "getstates", "confirmregisteration", "Captcha", "choosetype", "viewprofile", "loginwith", "search", "show_reviews", "upload");
        $admin = $this->Session->read("admin");
        $this->set('curruser', $this->Auth->user('id'));

        $this->checklogin();
        //pr($this->params);
    }

    /* end of function */


    /*
     * @function name	: index
     * @purpose			: index page for site user
     * @arguments		: none
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    function index() {
        $this->layout = "frontend";
        $user = $this->Session->read("Auth.User.id");
        $this->set("index", "1");
        if (empty($user) && $this->Cookie->read('Auth.User')) {
            $this->__login();
        }
    }

    /* end of function */



    /*
     * @function name	: login
     * @purpose			: to show login page for users
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10th Jan 2013
     * @description		: NA
     */

    function login($url = NULL) {
        $this->layout = "frontend_new";
        $this->set("title_for_layout", "User Login");
        if ($this->Session->read('Auth.User.id')) {
            $this->response->disableCache();
            $this->redirect("/");
        }
        if ((isset($this->data) && !empty($this->data))) {
            $this->__login();
        } elseif ($this->Cookie->read('Auth.User')) {
            $user = $this->Session->read("Auth.User.id");
            if (empty($user)) {
                $this->__login();
            }
        }
    }

    /* end of function */


    /*
     * @function name	: __login
     * @purpose			: to check user authentication
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: it is a private function
     */

    private function __login($url = NULL) {
        if (isset($this->data) && !empty($this->data)) {
            if ($this->Auth->login()) {
                $this->loginedUserInfo = $this->Auth->user();
                if (!empty($this->loginedUserInfo) && $this->loginedUserInfo['status'] == -1 || $this->loginedUserInfo['status'] == 0) {
                    $this->Session->setFlash("Your account is not activated yet.");
                    $this->Auth->logout();
                    $this->redirect(array("action" => "login"));
                    exit;
                }
            } else {
                $this->Session->setFlash("Invalid Email or Password.");
                $this->Auth->logout();
                $this->redirect(array("action" => "login"));
                exit;
            }
        }

        /* code to perform remeber me functinality " remember_login " */
        //condition to check if remember me checkbox is checked or not, if checked a cookie Auth.Member will be written
        if (isset($this->data['User']['remember_me']) && !empty($this->data['User']['remember_me']) && !empty($this->loginedUserInfo)) {
            $cookie = array();
            $cookie['remembertoken'] = $this->encryptpass($this->data['User']['username']) . "^" . base64_encode($this->data['User']['password']);
            $data['User']['remembertoken'] = $this->encryptpass($this->data['User']['username']);
            $this->User->create();
            $this->User->id = $this->loginedUserInfo['id'];
            $this->User->save($data);
            $this->Cookie->write('Auth.User', $cookie, false, '+2 weeks');
        }
        //condition to check if cookie Auth.Member is set or not if set then automatically logged in
        if (empty($this->data)) {
            $cookie = $this->Cookie->read('Auth.User');
            if (!is_null($cookie)) {
                $cookie = explode("^", $cookie['remembertoken']);
                $this->User->recursive = 0;
                $user = $this->User->find("first", array("conditions" => array("User.remembertoken" => $cookie[0], "User.status" => 1), "fields" => array("User.username", "User.password")));
                $user['User']['password'] = base64_decode($cookie[1]);
                unset($user['User']['id']);
                $this->data = $user;

                if ($this->Auth->login()) {
                    $this->loginedUserInfo = $this->Auth->user();
                    //  Clear auth message, just in case we use it.
                    $this->Session->delete('Message.auth');
                }
                //die("here");
            } else {
                $this->Auth->logout();
            }
        }
        /* end of code */
        if ($this->loginedUserInfo['user_type'] == 1) {
            $this->redirect("/preview-orders");
        } else if ($this->loginedUserInfo['user_type'] == 2) {
            $this->redirect("/orders/directorderlist");
        } else if ($this->loginedUserInfo['user_type'] == 3) {
            $this->redirect("/preview-orders-staff");
        } else if ($this->loginedUserInfo['user_type'] == 4) {
            $this->redirect("/preview-orders-driver");
        }
    }

    /* end of function */

    function dashboard() {
        $this->layout = false;
        if ($this->Session->read("Auth.User.loginfrom") == 'Twitter') {
            $this->render("twitterredirect");
        } else {
            $this->set("title_for_layout", "Dashboard");
            $this->render("dashboard");
        }
    }

    function choosetype() {
        if (isset($this->data) && !empty($this->data)) {
            $this->User->create();
            $this->User->id = $this->Session->read("Auth.User.id");
            if ($this->User->save($this->data)) {
                $this->Session->write("Auth.User.type", $this->data['User']['type']);
                $this->redirect("/dashboard");
            }
        }
    }

    /*
     * @function name	: logout
     * @purpose			: to logout from user panel
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    function logout() {
        $this->Auth->logout();
        if ($this->Cookie->read("Auth.User")) {
            $this->Cookie->destroy();
        }
        $this->Session->delete("Auth");
        $this->Session->delete("Profile");
        $this->response->disableCache();
        $this->redirect("/");
    }

    /* end of function */


    /*
     * @function name	: signup
     * @purpose			: to register new user account
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 3rd May 2013
     * @modified by		: Nishi Kant Sharma
     * @modified on		: 2nd feb 2015
     * @description		: This function is responsible to create new user account.
     */

    function signup($user_type = -1) {
        $this->layout = "frontend_new";
        $accountstatus = '';
        $this->set("title_for_layout", "User Signup");
        $this->set(compact("user_type"));
        $this->loadModel('State');
        $this->loadModel('Carddetail');
        $this->State->recursive = -1;
        $this->set("state", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));
        if ($this->Session->read('Auth.User.id')) {
            $this->response->disableCache();
            $this->redirect("/");
        }
        $this->loadModel("Service");
        $services = $this->Service->find("list", array("conditions" => array("status" => 1), "fields" => array("name", "name")));
        $services['Other'] = "Other";
        $this->set("services", $services);

        $months = array();
        $years = array();
        $curryear = date("Y");
        $years[$curryear] = $curryear;
        for ($i = 1; $i <= 20; $i++) {
            if ($i < 13) {
                $months[str_pad($i, 2, 0, STR_PAD_LEFT)] = str_pad($i, 2, 0, STR_PAD_LEFT);
            }
            $yearcount = $curryear + $i;
            $years[$yearcount] = $yearcount;
        }

        $this->set(compact(array('years', 'months', 'or_id')));




        if (isset($this->data) && !empty($this->data)) {
            $this->loadModel("Userdetail");
            $this->User->set($this->data);
            $this->Userdetail->set($this->data);
            $errflag = 0;
            if ($this->User->validates()) {
                $errflag += 1;
            }
            if ($this->Userdetail->validates()) {
                $errflag += 1;
            }
            if ($errflag == 2) {
                $user = $this->data;
                //$address = $this->getaddress($this->data['Userdetail']['zipcode']);
                //$user['Userdetail']['latitude'] = $address['lat'];
                //$user['Userdetail']['longitude'] = $address['long'];

                $user['User']['password'] = $this->Auth->password($user['User']['password']);
                $user['User']['passwordstatus'] = $passwordstatus = $this->encryptpass($user['User']['username']);
                $user['User']['status'] = 0;
                $user['User']['user_type'] = $user_type;
				
//echo '<pre>';print_r($user);die;
                if ($this->User->save($user, array('validate' => false))) {

                    $this->Carddetail->save($user);

                    $userdetails = $this->data;
					$userdetails['Userdetail']['email'] =  $user['User']['username'];
                    $userdetails['Userdetail']['user_id'] = $this->User->getLastInsertId();
                    $this->Userdetail->save($userdetails);

                    $user['Carddetail']['fname'] = $userdetails['Userdetail']['first_name'];
                    $user['Carddetail']['lname'] = $userdetails['Userdetail']['last_name'];
                    $user['Carddetail']['email'] = $user['User']['username'];
                    $user['Carddetail']['userid'] = $userdetails['Userdetail']['user_id'];
                    $this->Carddetail->save($user);
                    /* code to send email confirmation for signup */
                    $user = $userdetails['Userdetail']['first_name'];
                    $confirmlink = "<a href=" . SITE_LINK . "users/confirmregisteration/" . $passwordstatus . ">Click Here</a>";
                    $linkurl = SITE_LINK . "users/confirmregisteration/" . $passwordstatus;
                    $this->getmaildata(1);
                    $this->mailBody = str_replace("{USER}", $user, $this->mailBody);
                    $this->mailBody = str_replace("{LINK}", $confirmlink, $this->mailBody);
                    $this->mailBody = str_replace("{LINKURL}", $linkurl, $this->mailBody);
                    $this->sendmail($this->data['User']['username']);
                    /* code to send email confirmation for signup */
                    $this->Session->setFlash("Your registration with biolab has been successfully done and a verification link has been sent to your registered email.", 'default', array("class" => "success_message"));
                    $redirt = (($user_type == 1) ? "/signup-as-doctor" : "/signup-as-lab");
                    $this->redirect($redirt);
                }
            }
        }
    }

    /* end of function */

    /*
     * @function name	: search
     * @purpose			: Display listing for doctors and labs
     * @arguments		: NA
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 18th feb 2015
     * @description		: NA
     */

//mahi
    function search($user_type = null) {
        
        if (!$this->Session->read("Auth.User.id")) {
            $this->Session->setFlash("You need to login before view labs or doctors listing.");
            $this->redirect("/login");
        }
if($user_type == 1){
    $labId = $this->Session->read("Auth.User.id");
    $this->set("labId", $labId);
}

        $searchval = '';
        if (isset($this->params->query['searchval']) && !empty($this->params->query['searchval'])) {
            $searchval = $this->params->query['searchval'];
        }
        $this->set("searchval", $searchval);
        $this->layout = "frontend_new";
        //$order = "User.created desc";
        $order = "";
        $flag = false;
        $sort = 'asc';
        if (isset($this->request->query['sort']) && !empty($this->request->query['sort'])) {
            $sort = $this->request->query['sort'];
        }
        $this->set("sort", $sort);

        if ($user_type != 3) {
            $this->conditions = array("User.user_type" => $user_type, "User.profile_status" => 1, "User.status" => 1);
        } else {
            $this->conditions = array("User.user_type" => array(1, 2), "User.profile_status" => 1, "User.status" => 1);
        }
        /* code to perform search functionality */

        if (isset($this->request->query) && !empty($this->request->query)) {  // Read data from query string.
            if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
                $address = $this->getaddress($this->request->query['searchval'], true);

                if (!empty($address)) {
                    $this->conditions = array_merge($this->conditions, array("OR" => array("Userdetail.first_name like" => "%" . $this->request->query['searchval'] . "%", "User.company_name like" => "%" . $this->request->query['searchval'] . "%", "Userdetail.city like" => "%" . $this->request->query['searchval'], "User.StateName like" => "%" . $this->request->query['searchval'] . "%", "Userdetail.serviceTags like" => "%" . $this->request->query['searchval'] . "%", "Userdetail.zipcode like" => "%" . $this->request->query['searchval'] . "%")));
                }
            }
            if (isset($this->request->query['search_specialization']) && !empty($this->request->query['search_specialization'])) {
                $order = "User.created " . $sort;
                $this->conditions = array_merge($this->conditions, array('Userdetail.serviceTags' => $this->request->query['search_specialization']));
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Labs') {
                $order = "User.created " . $sort;
                $this->conditions = array_merge($this->conditions, array('User.user_type' => 2));
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Doctors') {
                $order = "User.created " . $sort;
                $this->conditions = array_merge($this->conditions, array('User.user_type' => 1));
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Rating') {
                $order = "User.rating " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Pricerating') {
                $order = "User.pricerating " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'City') {
                $order = "Userdetail.city " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'State') {
                $order = "User.StateName " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Zip') {
                $order = "Userdetail.zipcode " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Name') {
                $order = "User.company_name " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Distance') {
                $order = "User.distance " . $sort;
            }
            if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'All') {
                $order = "User.created " . $sort;
            } elseif ($flag) {
                
            }
            $this->set("set_val", $this->request->query);
        }



        if (!$flag && $this->Session->read("Auth.User.id")) {
            $address = $this->getaddress($this->Session->read("Auth.User.Userdetail.zipcode"), true);

            if (!empty($address)) {
                $lat = $address['lat'];
                //$this->Session->write("latit", $lat);

                $lon = $address['long'];
                if (empty($order)) {
                    $order = "User.distance desc";
                }
                $flag = true;
            }
        }

        /* end of code to perform search functionality */
        $add_type = ($user_type == 1) ? "Doctors" : (($user_type == 2) ? "Labs" : "Members");
        $fields = array("User.id", "User.user_type", 'User.company_name', "User.rating", "Userdetail.first_name", "Userdetail.first_name", "Userdetail.last_name", "Userdetail.city", "Userdetail.serviceTags", "Userdetail.address", "Userdetail.zipcode", "Userdetail.image", "User.profile_status", "Userdetail.state_id", "Userdetail.phone", "Userdetail.country_id", "User.StateName", "User.CountryName", "User.pricerating");
        $this->User->recursive = 0;
        $this->User->virtualFields = array(
            "rating" => "select AVG(rating) as average from user_ratings where rater_id = User.id",
            //"StateName" => "select concat(state.name,', ',state.code) from states state INNER JOIN userdetails Userdetail on state.id = Userdetail.state_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
            "StateName" => "select state.code from states state INNER JOIN userdetails Userdetail on state.id = Userdetail.state_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
            "CountryName" => "select country.name from countries country INNER JOIN userdetails Userdetail on country.id = Userdetail.country_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
            "pricerating" => "select AVG(pricerating) as averageprice from user_ratings where rater_id = User.id",
        );

        if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type") == 1 && $user_type == 2) {
            $this->User->virtualFields = array_merge($this->User->virtualFields, array("is_member" => "SELECT count(*) FROM `doctor_labs` dl INNER JOIN users u WHERE dl.lab_id = u.id and u.user_type = 2 and dl.doctor_id = " . $this->Session->read("Auth.User.id") . " and dl.lab_id = User.id"));
            array_push($fields, 'User.is_member');
        }

        if ($flag) {
            $this->User->virtualFields = array_merge($this->User->virtualFields, array("distance" => 'ROUND( 69.0 * DEGREES(ACOS(COS(RADIANS(' . $lat . '))			
                 * COS(RADIANS(latitude))
                 * COS(RADIANS(' . $lon . ') - RADIANS(longitude))
                 + SIN(RADIANS(' . $lat . '))
                 * SIN(RADIANS(latitude)))),1)'));
            array_push($fields, 'User.distance');
            $shwDistance = 'yes';
            $this->set(compact("shwDistance"));
        }
        if (empty($this->request->query['sort_rating']) && in_array('User.distance', $fields)) {
            //$order = "User.distance asc";		
        }

        $this->User->unBindModel(array('hasMany' => array('Adduser')));
        $this->paginate = array("fields" => $fields, "order" => $order, "limit" => 250);
        //echo $this->paginate($this->conditions; exit; 
		//echo '<pre>';print_r($this->paginate($this->conditions));die;
        $this->set('users', $this->paginate($this->conditions)); 
            
        $this->set(compact("add_type"));

        $this->loadModel("Service");
        $services = $this->Service->find("list", array("conditions" => array("status" => 1), "fields" => array("name", "name")));
        $services[] = "All";
        $services['Other'] = "Other";
        
  
        $this->set("services", $services);

        $this->loadModel("Favorite");
        $favDoctors = $this->Favorite->find('list', array('conditions' => array('Favorite.lab_id' => $this->Session->read('Auth.User.id'), 'Favorite.status' =>1), 'fields' => array('Favorite.doctor_id')));
       $this->set("favDoctors", $favDoctors);
        $this->render('view_lab_doctor');
    }


    public function makeFavorite(){
		$result = array("error"=>true,"message"=>"An error occured, Please try again.");
        $this->autoRender=false;
		
         if($this->RequestHandler->isAjax()){
             Configure::write('debug', 0);
          }
        $this->loadModel('User');
        $this->loadModel('Favorite');

        $favorites = $this->Favorite->find('first', array(
                            'conditions'=> array(
                                  'Favorite.lab_id' => $this->request->data['lab_id'],
                                  'Favorite.doctor_id' => $this->request->data['doctor_id'],
                            ),
            ));
        if(empty($favorites)){
            $data['Favorite']['lab_id'] = $this->request->data['lab_id'];
            $data['Favorite']['doctor_id'] = $this->request->data['doctor_id'];
            $data['Favorite']['status'] = ($this->request->data['type'] == 'add')?1:0;
            $this->Favorite->create();
        }else{
            $data['Favorite']['id'] = $favorites['Favorite']['id'];
            $data['Favorite']['status'] = ($this->request->data['type'] == 'add')?1:0;;
        }
        

        if($this->Favorite->save($data)){
            if($this->request->data['favorite'] == 'add'){
               $result = array("error"=>false,"message"=>"Lab has been added successfully.");
            }else{
               $result = array("error"=>false,"message"=>"Lab has been removed successfully.");
            }
            
        }else{
			$result = array("error"=>true,"message"=>"Lab has not been added successfully.");	
		}

        return json_encode($result);
        
    }

    /* end of function */

    /*
     * @function name	: favourite_lab
     * @purpose			: Display listing of favourite labs for doctor
     * @arguments		: NA
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 25th March 2015
     * @description		: NA
     */

//mahi
    function favourite_lab() {
        $this->layout = "frontend_new";
        $this->loadModel('doctorLab');
        $flag = false;
        if (!$flag && $this->Session->read("Auth.User.id")) {
            $address = $this->getaddress($this->Session->read("Auth.User.Userdetail.zipcode"), true);
            if (!empty($address)) {
                $lat = $address['lat'];
                //$this->Session->write("latit", $lat);

                $lon = $address['long'];
                if (empty($order)) {
                    $order = "User.distance desc";
                }
                $flag = true;
            }
        }


        $labs = $this->doctorLab->find('list', array('conditions' => array('doctorLab.doctor_id' => $this->Session->read('Auth.User.id')), 'fields' => array('doctorLab.lab_id')));
        $fields = array("User.id", "Userdetail.first_name", 'User.company_name', "User.rating", "Userdetail.user_id", "Userdetail.zipcode", "Userdetail.first_name", "Userdetail.last_name", "Userdetail.serviceTags", "Userdetail.city", "Userdetail.address", "Userdetail.image", "User.profile_status", "Userdetail.state_id", "Userdetail.serviceTags", "Userdetail.phone", "Userdetail.country_id", "User.StateName", "User.CountryName", "User.pricerating");
        $this->User->virtualFields = array(
            "rating" => "select AVG(rating) as average from user_ratings where rater_id = User.id",
            "StateName" => "select concat(state.name,', ',state.code) from states state INNER JOIN userdetails Userdetail on state.id = Userdetail.state_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
            "CountryName" => "select country.name from countries country INNER JOIN userdetails Userdetail on country.id = Userdetail.country_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
            "pricerating" => "select AVG(pricerating) as averageprice from user_ratings where rater_id = User.id",
        );
        if ($flag) {
            $this->User->virtualFields = array_merge($this->User->virtualFields, array("distance" => 'ROUND(69.0 * DEGREES(ACOS(COS(RADIANS(' . $lat . '))			
                 * COS(RADIANS(latitude))
                 * COS(RADIANS(' . $lon . ') - RADIANS(longitude))
                 + SIN(RADIANS(' . $lat . '))
                 * SIN(RADIANS(latitude)))),1)'));
            array_push($fields, 'User.distance');
            $shwDistance = 'yes';
            $this->set(compact("shwDistance"));
        }

        $this->paginate = array("fields" => $fields);
        $this->conditions = array('User.id' => $labs);
        if (isset($this->request->query) && !empty($this->request->query)) {  // Read data from query string.
            if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
                $this->conditions = array_merge($this->conditions, array("OR" => array("Userdetail.first_name like" => "%" . $this->request->query['searchval'] . "%", "User.company_name like" => "%" . $this->request->query['searchval'] . "%", "Userdetail.zipcode like" => "%" . $this->request->query['searchval'] . "%")));
            } else if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Rating') {
                $this->paginate = array_merge($this->conditions, array("order" => "User.rating desc"));
            } else if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'All') {
                $this->paginate = array_merge($this->conditions, array("order" => "User.rating asc"));
            } else {
                $this->paginate = array_merge($this->conditions, array("order" => "User.created desc"));
            }
            $this->set("set_val", $this->request->query);
        }
        $this->set('users', $this->paginate($this->conditions));
    }

    /* end of function */

    function confirmregisteration($id = null) {
        $this->layout = "frontend_new";
        //$this->layout = "frontend";
        $this->User->recursive = -1;
        if (!empty($id)) {
            $user = $this->User->find("first", array("conditions" => array("User.passwordstatus" => $id)));
        }
        if (empty($user)) {
            $this->Session->setFlash("Invalid User.");
        } elseif ($user['User']['status'] == 1) {
            $this->Session->setFlash("Account is already activated.");
        } else {
            $this->User->create();
            $this->User->id = $user['User']['id'];
            $updateuser['User']['status'] = 1;
            $updateuser['User']['passwordstatus'] = 0;
            $this->User->save($updateuser);
            $this->Session->setFlash("Verification is successful please login & request for approval.", 'default', array("class" => "success_message"));
        }
        $this->redirect(array("action" => "login"));
    }

    function captcha() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        if (!isset($this->Captcha)) { //if Component was not loaded throug $components array()
            $this->Captcha = $this->Components->load('Captcha', array(
                'width' => 150,
                'height' => 50,
                'theme' => 'default', //possible values : default, random ; No value means 'default'
            )); //load it
        }
        $this->Captcha->create();
    }

    /*
     * @function name	: newpassword
     * @purpose			: to set password after registration of user
     * @arguments		: token will be given with aet password url.
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 15rd Jan 2013
     * @description		: This function is responsible set new password for newly registered user. User can set password only once.
     */

    function newpassword($token) {
        //$this->layout = "frontend";
        $this->layout = "frontend_new";
        $this->User->recursive = 0;
        $user = $this->User->find("first", array("conditions" => array("User.remembertoken" => $token)));
        if (isset($this->data) && !empty($this->data)) {
            $this->User->create();
            $this->User->id = $this->data['User']['id'];
            $data['User']['password'] = $this->Auth->password($this->data['User']['password'], $this->data['User']['username']);
            $data['User']['confirmpassword'] = $this->Auth->password($this->data['User']['confirmpassword'], $this->data['User']['username']);
            $data['User']['status'] = 1;
            if ($this->User->save($data)) {
                $this->Cookie->destroy();
                $this->Session->destroy();
                $this->Session->setFlash("Your account is successfully confirmed.", 'default', array("class" => "success_message"));
                $this->redirect("/login");
            }
        } elseif (!empty($user)) {
            if (!empty($user['User']['password'])) {
                $this->Session->setFlash("You have already set your password.");
                $this->redirect("/login");
            } else {
                $this->data = $user;
            }
        } else {
            $this->Session->setFlash("Invalid link.");
            $this->redirect("/login");
        }
    }

    /* end of function */


    /*
     * @function name	: admin_index
     * @purpose			: to show listing of users for admin of site
     * @arguments		: Following arguments have been passed:
     * status	: status of users listed like 'Apprroved','Rejected'
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    public function admin_index($type = NULL) {
        $this->bulkactions();
        $heading = $type == 'request' ? 'Approve Users' : 'Users';
        /* code to perform search functionality */
        if (isset($this->data) && !empty($this->data['User']['searchval'])) {
            $this->Session->write('searchval', $this->data['User']['searchval']);
            $this->conditions = array("OR" => array("User.username like" => "%" . $this->data['User']['searchval'] . "%"));
        }

        if (isset($this->params['named']['page'])) {

            if ($this->Session->read('searchval')) {
                $this->conditions = array("OR" => array("User.username like" => "%" . $this->Session->read('searchval') . "%"));
                $this->data['User']['searchval'] = $this->Session->read('searchval');
            }
        } elseif (empty($this->conditions)) {
            $this->Session->delete('searchval');
        }
        if (!empty($type) && $type == 'request') {
            $this->conditions = array_merge($this->conditions, array("User.profile_status" => 0));
        } else {
            $this->conditions = array_merge($this->conditions, array("User.profile_status" => 1));
        }
        $this->set(compact("heading"));
        /* end of code to perform search functionality */
        $this->paginate = array("order" => "User.created desc");
        $this->User->recursive = 0;
        $this->set('users', $this->paginate($this->conditions));
    }

    /* end of function */


    /*
     * @function name	: admin_addstaffmember
     * @purpose			: Add staff member, Doctor,Labs from admin panel .	
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 3rd feb 2015
     * @description		: Here admin can add staff members,Doctor,Labs from admin panel and send mail, once account get created.
     */

    function admin_addstaffmember() {
        if ($this->request->is('post')) {
            $password = rand(100000, 999999); //Genrate random token no. for password.			                      
            $this->request->data['User']['password'] = $this->Auth->password($password);
            $this->request->data['User']['status'] = 1;   //status for created account is active.        
            $user_type = ($this->request->data['User']['user_type'] == 1) ? 'Doctor' : (($this->request->data['User']['user_type'] == 2) ? 'Lab' : 'Staff Member');

            if ($this->User->saveAll($this->request->data)) {
                /* code to send email confirmation for signup */
                $this->getmaildata(5);
                $this->mailBody = str_replace("{USER}", $this->request->data['Userdetail']['first_name'], $this->mailBody);
                $this->mailBody = str_replace("{USERNAME}", $this->request->data['User']['username'], $this->mailBody);
                $this->mailBody = str_replace("{PASSWORD}", $password, $this->mailBody);
                $this->sendmail($this->request->data['User']['username']);
                $this->Session->setFlash(__($user_type . ' has been successfully created and an email containing email address and password has been sent.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Staff member could not be saved. Please, try again.'));
            }
        }
    }

    function admin_adddriver() {
        $this->loadmodel("State");
        $this->State->recursive = -1;
        $this->set("states2", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));
        //$this->autoRender=false;
        if ($this->request->is('post')) {
            //$password = rand(100000, 999999); //Genrate random token no. for password.			                      
            //$this->request->data['User']['password'] = $this->Auth->password($password);
            //echo $this->data;

            $this->request->data['User']['status'] = 1;   //status for created account is active.        
            $user_type = ($this->request->data['User']['user_type'] == 1) ? 'Doctor' : ($this->request->data['User']['user_type'] == 2) ? 'Lab' : ($this->request->data['User']['user_type'] == 3) ? 'Staff Member' : 'Driver';

            $user = $this->request->data;


            $this->getmaildata(25);
            $driver = $user['Userdetail']['first_name'];
            $email = $user['User']['username'];
            $pass = $user['User']['password'];
            $this->mailBody = str_replace("{DRIVER}", $driver, $this->mailBody);
            $this->mailBody = str_replace("{USER}", $email, $this->mailBody);
            $this->mailBody = str_replace("{PASS}", $pass, $this->mailBody);

            $this->sendmail($email);


            $user['User']['password'] = $this->Auth->password($user['User']['password']);

            if ($this->User->saveAll($user)) {
                /* code to send email confirmation for signup */

                $this->Session->setFlash(__($user_type . ' has been successfully created .'));
                $this->redirect('adddriver');
            } else {
                $this->Session->setFlash(__('Driver member could not be saved. Please, try again.'));
            }
        }
    }

    function admin_listdriver() {

        $this->autoRender = true;
        $this->loadModel("User");
        $pro = $this->User->find("all", array('conditions' => array('User.user_type' => 4)));
        $this->set('pro', $pro);
        //$this->redirect('listdriver');
    }

    /* end of function */


    /*
     * @function name	: confirmuser
     * @purpose			: to aprove or disapprove the user from user panel
     * @arguments		: This function require following arguments:
     * id		: user id
     * opt		: status of user
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: This function is responsible to approve and disapprove a user from manager user.
     */

    function confirmuser($id, $opt) {
        $this->layout = "frontend";
        $admin = $this->Session->read("admin");
        $this->User->id = $id;

        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        } else {
            $this->User->recursive = 0;
            $user = $this->User->find("first", array("conditions" => array("User.id" => $id, "User.account_id" => $this->Session->read("Auth.User.account_id"))));
            $status = ($opt == 1) ? 'Approved and a notification has been sent to user on email ' . $user['User']['username'] : 'Rejected and a notification has been sent to user on email ' . $user['User']['username'];
            $status1 = ($opt == 1) ? 'Approved' : 'Rejected';
            if ($this->sendpasswordmail($user, $opt)) {
                $this->User->create();
                $this->User->id = $user['User']['id'];
                $userdata['User']['remembertoken'] = $this->encryptpass($user['User']['username']);
                $userdata['User']['approved'] = ($opt == 1) ? "A" : "N";
                $this->User->save($userdata);
                $this->Session->setFlash("The user account has been " . $status . ".", 'default', array("class" => "success_message"));
            } else {
                $this->Session->setFlash("The user account can not be " . $status1 . ", please try again.");
            }
        }

        $this->redirect("/viewusers", (($opt == 1) ? "N" : "A"));
    }

    /* end of function */

    /*
     * @function name	: admin_delete
     * @purpose			: to delete user profile from admin panel
     * @arguments		: This function require following arguments:
     * id		: user id
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    public function admin_delete($id = null) {
        $this->validateuser($id);
        $this->User->id = $id;
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User has been deleted successfully.', 'default', array("class" => "success_message")));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted.'));
        $this->redirect(array('action' => 'index'));
    }

    function admin_driverdelete($id = null) {
        $this->validateuser($id);
        $this->User->id = $id;
        if ($this->User->delete()) {
            $this->Session->setFlash(__('Driver has been deleted successfully.', 'default', array("class" => "success_message")));
            $this->redirect(array('action' => 'listdriver'));
        }
        $this->Session->setFlash(__('Driver was not deleted.'));
        $this->redirect(array('action' => 'listdriver'));
    }

    /* end of function */

    /*
     * @function name	: admin_view
     * @purpose			: to view user profile from admin panel
     * @arguments		: This function require following arguments:
     * id		: user id
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    public function admin_view($id = null) {
        $this->validateuser($id);
        $this->loadmodel("Userdetail");
        $user = $this->Userdetail->find("all", array("conditions" => array("User.id" => $id), "fields" => array("User.id", "User.username", "User.user_type", "User.status", "User.profile_status", "User.created", "Userdetail.first_name", "Userdetail.last_name", "Userdetail.phone", "Userdetail.biography", "Userdetail.city", "Userdetail.about", "Userdetail.city", "Userdetail.address", "Userdetail.image", "State.name", "Country.name")), array("recursive" => 0));

        $this->set("user", $user[0]);
    }

    public function admin_driveredit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        } else {
            //$userInfo = $this->User->findById($id);		
            $this->loadModel("Userdetail");
            $userInfo = $this->Userdetail->find("first", array("conditions" => array('User.id' => $id)/* ,"fields"=>array("State.name","Country.name") */));
            //$this->request->data['User']['status'] = array(-1,0,1);		
            if ($this->request->is('post') || $this->request->is('put')) {

                $this->User->id = $this->request->data['User']['id'];
                if ($this->User->saveAll($this->request->data)) {
                    $this->Session->setFlash("Driver has been updated successfully.", 'default', array("class" => "success_message"));
                    $this->redirect(array('action' => 'listdriver'));
                }
            } else {
                $this->request->data = $userInfo;
            }
        }
    }

    /* end of function */

    /*
     * @function name	: admin_edit
     * @purpose			: Edit User from admin panel .	
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 4th feb 2015
     * @description		: Here admin can edit user from admin panel like status active/inactive.
     */

    public function admin_edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        } else {
            //$userInfo = $this->User->findById($id);		
            $this->loadModel("Userdetail");
            $userInfo = $this->Userdetail->find("first", array("conditions" => array('User.id' => $id)/* ,"fields"=>array("State.name","Country.name") */));
            //$this->request->data['User']['status'] = array(-1,0,1);		
            if ($this->request->is('post') || $this->request->is('put')) {

                $this->User->id = $this->request->data['User']['id'];
                if ($this->User->saveAll($this->request->data)) {
                    $this->Session->setFlash("User has been updated successfully.", 'default', array("class" => "success_message"));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->request->data = $userInfo;
            }
        }
    }

    /* end of function */

    public function viewprofile($id = Null, $profile = Null) {
        $this->layout = "frontend_new";
        $this->loadmodel("Userdetail");
        $this->loadmodel("userRating");
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['userRating']['user_id'] = $this->Session->read("Auth.User.id");
            $data['userRating']['rater_id'] = $id;
            if (!empty($data['userRating']['id'])) {
                $this->userRating->id = $data['userRating']['id'];
            }
            $this->userRating->save($data);
            $this->redirect(SITE_LINK . $this->params->url);
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $user = $this->Userdetail->virtualFields = array(
            "rating" => "select AVG(rating) as average from user_ratings where user_ratings.rater_id = " . $id,
            "pricerating" => "select AVG(pricerating) as averageprice from user_ratings where user_ratings.rater_id = " . $id
        );

        if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type") == 1) {
            $this->Userdetail->virtualFields = array_merge($this->Userdetail->virtualFields, array("is_member" => "SELECT count(*) FROM `doctor_labs` dl INNER JOIN users u WHERE dl.lab_id = u.id and u.user_type = 2 and dl.doctor_id = " . $this->Session->read("Auth.User.id") . " and dl.lab_id = Userdetail.user_id"));
        }

        $data = $this->Userdetail->find('first', $options);
        $weekArray = array(6 => "Sunday", 0 => "Monday", 1 => "Tuesday", 2 => "Wednesday", 3 => "Thursday", 4 => "Friday", 5 => "Saturday");
        $this->set(compact('weekArray'));
        $this->show_reviews($id);
        if (!empty($data)) {
            $review = $this->userRating->find('count', array('conditions' => array('userRating.rater_id' => $data['User']['id'], "userRating.review <>" => ''), 'fields' => array('userRating.review', 'userRating.rater_id')));
            $this->set('review_count', $review);

            if (empty($data['User']['profile_status']) && ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.id") == $data['User']['id'] )) {
                $this->set('user', $data);
            } elseif (!empty($data['User']['profile_status'])) {
                $this->set('user', $data);
            } else {
                $this->Session->setFlash('Invalid Profile.', 'default', array("class" => "home_error_flash"));
                $this->redirect("/");
            }
        } else {
            $this->Session->setFlash('Invalid Profile.', 'default', array("class" => "home_error_flash"));
            $this->redirect("/");
        }
    }

    function show_reviews($rater_id = null) {
        if ($this->request->is('post')) {
            $this->loadmodel("userRating");
            $data = $this->request->data;
            $this->userRating->create();
            $this->userRating->id = $this->request->data['userRating']['id'];
            $this->userRating->save($data);
            $this->redirect(SITE_LINK . $this->params->url);
        }
        $this->loadmodel("UserRating");
        $this->layout = "frontend_new";
        $options['fields'] = array('UserRating.*', 'Userdetail.city', 'Userdetail.state_id', 'User.company_name');
        $options['conditions'] = array('UserRating.rater_id' => $rater_id, 'UserRating.review <>' => '');
        $options['order'] = array('UserRating.created desc');
        //$this->paginate = $options;
        $this->loadModel('State');
        $this->State->recursive = -1;
        $this->set("state", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));
        $this->set('reviews', $this->UserRating->find('all', $options));
    }

    /*
     * @function name	: forgotpassword
     * @purpose			: to generate and send a random password to user who is requesting for forgot password
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    function forgotpassword() {
        $this->layout = "frontend_new";
        $this->set("title_for_layout", "Forgot Password");
        if ($this->Session->read('Auth.User.id')) {
            $this->response->disableCache();
            $this->redirect("/");
        }
        if (isset($this->data) && !empty($this->data)) {
            $this->loadModel("Userdetail");
            $arr = $this->User->Userdetail->find("first", array("conditions" => array("username" => $this->data['User']['username'])));
            if (empty($arr)) {
                $this->Session->setFlash('Email address not exists.');
            } else {
                if ($arr['User']['status'] == 1) {
                    $arr['User']['passwordstatus'] = 1;
                    $new_password = rand(100000, 999999);
                    $arr['User']['password'] = $this->Auth->password($new_password);
                    /* code to send email confirmation for signup */
                    $user = $arr['Userdetail']['first_name'] . ' ' . $arr['Userdetail']['last_name'];
                    $this->getmaildata(2);
                    $this->mailBody = str_replace("{USER}", $user, $this->mailBody);
                    $this->mailBody = str_replace("{PASSWORD}", $new_password, $this->mailBody);
                    $flag = $this->sendmail($arr['User']['username']);
                    /* code to send email confirmation for signup */

                    if ($flag) {
                        $this->Session->setFlash('Your password has been sent to your registered email.', 'default', array("class" => "success_message"));
                        $this->User->save($arr);
                    } else {
                        $this->Session->setFlash('An error occured, please try later.');
                    }
                } else {
                    $this->Session->setFlash("This account has not been activated yet.");
                }
            }
            $this->redirect("/forgotpassword");
        }
    }

    /* end of function */

    function deleteprofile() {
        $this->layout = "frontend_new";
        if ($this->Session->read("Auth.User.id")) {
            $this->User->create();
            $this->User->id = $this->Session->read("Auth.User.id");
            $this->User->delete();
        }
        $this->redirect(array("controller" => "users", "action" => "logout"));
    }

    /*
     * @function name	: changepassword
     * @purpose			: to perform change password functionality in user panel, also validate old password and confirm if new and confirm password match or not 
     * @arguments		: NA
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 10rd Jan 2013
     * @description		: NA
     */

    function changepassword() {
        $this->layout = "frontend_new";
        //$this->layout = "frontend_dummy";
        $this->set("title_for_layout", "Change Password");
        if (isset($this->data) && !empty($this->data)) {
            $data = $this->data;
            $data['User']['currentpassword'] = (!empty($data['User']['currentpassword']) ? $this->Auth->password($data['User']['currentpassword']) : '');
            $data['User']['password'] = (!empty($data['User']['password']) ? $this->Auth->password($data['User']['password']) : '');
            $data['User']['confirmpassword'] = (!empty($data['User']['confirmpassword']) ? $this->Auth->password($data['User']['confirmpassword']) : '');
            $this->User->set($data);
            if ($this->User->validates()) {
                $this->User->create();
                $this->User->id = $this->Session->read("Auth.User.id");
                if ($this->User->save($data, array('validate' => false))) {
                    $this->Session->setFlash("Password has been updated successfully.", 'default', array("class" => "success_message"));
                    $this->redirect("changepassword");
                } else {
                    $this->Session->setFlash("Password can not updated successfully, Please try again.");
                }
            }
        }
    }

    /* end of function */

    function validateuser($id = Null) {
        if ($this->Session->read("admin.Admin")) {
            $this->User->recursive = -1;
            $users = $this->User->find("first", array("conditions" => array("User.id" => $id)));
            if (empty($users)) {
                $this->Session->setFlash("Invalid User.");
                $this->redirect(array("action" => "index"));
            }
        } else {
            $this->Session->setFlash("You are not autheticate to perform this action.");
            $this->redirect(array("action" => "index"));
        }
    }

    function getusers($keyword = NULL) {
        if ($this->RequestHandler->isAjax()) {
            $keyword = trim($this->request->data['keyword']);
            $this->User->recursive = 0;
            if (!empty($keyword)) {
                $searchusers = $this->User->find("all", array("conditions" => array("(CONCAT(Userdetail.first_name,' ',Userdetail.last_name) like '" . $keyword . "%' OR Userdetail.first_name like '" . $keyword . "%')", "User.username <>" => $this->Session->read("Auth.User.username")), "fields" => array("Userdetail.first_name", "Userdetail.image", "Userdetail.last_name", "User.id"), "order" => "Userdetail.first_name asc"));
                $this->set("users", $searchusers);
            }
        }
    }

    /*
     * @function name	: uniqueuser
     * @purpose			: to perform check if email is already exists in thne database on signup
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder
     * @created on		: 2nd feb 2015
     * @description		: NA
     */

    function uniqueuser() {
        $this->autoRender = false;
        $this->layout = null;
        $user = $this->User->find('count', array('conditions' => array('User.username' => $_REQUEST['data']['User']['username'])));
        if ($user)
            echo 'false';
        else
            echo 'true';
        exit;
    }

    /* end of function */

    /*
     * @function name	: uniquemember
     * @purpose			: to perform check if email is already exists in adduer table.
     * @arguments		: NA
     * @created by		: Nishi Kant Sharma
     * @created on		: 25th feb 2015
     * @description		: NA
     */

    function uniquemember() {
        $this->autoRender = false;
        $this->layout = null;
        $this->loadModel('Adduser');
        $member = $this->Adduser->find('count', array('conditions' => array('Adduser.email' => $_REQUEST['data']['Adduser']['email'])));
        if ($member)
            echo 'false';
        else
            echo 'true';
        exit;
    }

    /* end of function */

    /*
     * @function name	: uniqueuser
     * @purpose			: to perform avtive and deactive user request by admin
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder	
     * @created on		: 3rd feb 2015
     * @Modified by    : Nishi Kant Sharma
     * @Modified on    : 10th feb 2015
     * @description		: NA
     */

    function admin_userstatus($id = NULL, $profile_status = NULL) {
        $this->autoRender = false;
        if (!empty($id)) {
            $user = $this->User->find("first", array("recursive" => 1, "conditions" => array("User.id" => $id), 'fields' => array("Userdetail.first_name", "User.username")));
            $status = ($profile_status == 0) ? 0 : 1;
            $data = array('User' => array('id' => $id, 'profile_status' => $profile_status, 'status' => $status));
            if ($this->User->save($data)) {
                if ($profile_status == 0) {
                    $this->getmaildata(6);
                } else {
                    $this->getmaildata(3);
                }
                $this->mailBody = str_replace("{USER}", $user['Userdetail']['first_name'], $this->mailBody);
                $flag = $this->sendmail($user['User']['username']);
                if ($flag) {
                    if ($profile_status == 0) {
                        $this->Session->setFlash('The profile request has been rejected.');
                    } else {
                        $this->Session->setFlash('The profile request has been approved.');
                    }
                } else {
                    $this->Session->setFlash('Email could not sent to user, please try again');
                }
            } else {
                $this->Session->setFlash('There is some problem with registration request approval, Please try again.');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    /* end of function */

    /*
     * @function name	: requestToAddUser
     * @purpose			: to sent request to admin to add a new lab or doctor
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder
     * @created on		: 6rd feb 2015
     * @Modified by		: Nishi Kant Sharma
     * @Modified on		: 18th feb 2015
     * @description		: NA
     */

    function requesteduser($type = NULL) {
        $this->layout = "frontend_new";
        if ($this->request->is('post') && !empty($type)) {
            $this->loadModel('Adduser');
            $this->request->data['Adduser']['user_id'] = $this->Session->read("Auth.User.id");
            if ($type == 3) {
                $this->request->data['Adduser']['user_type'] = $this->request->data['Adduser']['user_type'];
            } else {
                $this->request->data['Adduser']['user_type'] = $type;
            }
            $created_user = $this->Session->read('Auth.User.Userdetail.first_name');

            if ($this->Adduser->save($this->request->data)) {
                /* code to send email */
                $this->getmaildata(7);
                $this->mailBody = str_replace("{SENDER}", $created_user, $this->mailBody);
                $this->mailBody = str_replace("{USERNAME}", $this->request->data['Adduser']['name'], $this->mailBody);
                $this->mailBody = str_replace("{USEREMAIL}", $this->request->data['Adduser']['email'], $this->mailBody);
                if ($type == 1) {
                    $requester = 'Doctor';
                } elseif ($type == 2) {
                    $requester = 'Lab';
                } else {
                    if ($this->request->data['Adduser']['user_type'] == 1) {
                        $requester = 'Doctor';
                    } else {
                        $requester = 'Lab';
                    }
                }
                $this->mailBody = str_replace("{USERTYPE}", $requester, $this->mailBody);
                $this->mailBody = str_replace("{PHONE}", $this->request->data['Adduser']['phone'], $this->mailBody);
                $this->mailBody = str_replace("{ADDRESS}", $this->request->data['Adduser']['address'], $this->mailBody);
                $this->mailBody = str_replace("{DES}", $this->request->data['Adduser']['description'], $this->mailBody);
                $this->sendmail(ADMIN_EMAIL);
                $this->Session->setFlash("Your request has been sent to admin.", 'default', array("class" => "success_message"));
                $redirt = ($type == 1) ? '/add-doctor' : (($type == 2) ? '/add-lab' : '/add-members');
                $this->redirect($redirt);
            } else {
                $this->Session->setFlash(__('There is an error occured. Please, try again.'));
            }
        }
    }

    /* end of function */

    /*
     * @function name	: admin_requestedUser
     * @purpose			: to display list of all the requested a new lab or doctor to admin
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder
     * @created on		: 6rd feb 2015
     * @Modified by     : Nishi Kant Sharma
     * @Modified on     : 16th feb 2015
     * @description		: NA
     */

    function admin_requestedUser() {
        //$this->bulkactions();		
        $this->loadModel("Adduser");
        $users = $this->Adduser->find("all");
        /* code to perform search functionality */
        if (isset($this->data) && !empty($this->data['Adduser']['searchval'])) {
            $this->Session->write('searchval', $this->data['Adduser']['searchval']);
            $this->conditions = array("OR" => array("Adduser.email like" => "%" . $this->data['Adduser']['searchval'] . "%", "Adduser.name like" => "%" . $this->data['Adduser']['searchval'] . "%"));
        }
        if (isset($this->params['named']['page'])) {
            if ($this->Session->read('searchval')) {
                $this->conditions = array("OR" => array("Adduser.email like" => "%" . $this->Session->read('searchval') . "%", "Adduser.name like" => "%" . $this->data['Adduser']['searchval'] . "%"));
                $this->data['Adduser']['searchval'] = $this->Session->read('searchval');
            }
        } elseif (empty($this->conditions)) {
            $this->Session->delete('searchval');
        }
        $this->paginate = array("order" => "Adduser.created desc");
        $this->set('newUser', $this->paginate('Adduser', $this->conditions));
    }

    /* end of function */


    /*
     * @function name	: admin_requestedUser
     * @purpose			: to view the requested a new lab or doctor to admin
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder
     * @created on		: 9th feb 2015
     * @Modified by		: Nishi Kant Sharma
     * @description		: NA
     */

    function admin_view_userrequest($id = null) {
        $this->loadModel("Adduser");
        $user = $this->Adduser->find("all", array("conditions" => array("Adduser.id" => $id), "fields" => array("Adduser.id", "Adduser.name", "Adduser.user_type", "Adduser.email", "Adduser.phone", "Adduser.address", "Adduser.description", "Adduser.created")));
        $this->set("user", $user[0]);
    }

    /* end of function */

    function admin_bannerImage() {
        if ($this->request->is('post')) {
            //pr($this->data); die;
            $filename = $this->data['User']['Userfile']['name'];
            if ($this->data['User']['Userfile']['error'] == 0) {
                $upload_path = getcwd() . "/img/files/" . $filename;
                if (move_uploaded_file($this->data['User']['Userfile']['tmp_name'], $upload_path)) {
                    $this->Session->setFlash("file has been uploaded successfully.");
                    $this->redirect('/admin/users/bannerImage');
                } else {
                    $this->Session->setFlash("An error occured!!");
                }
            }
        }
    }

    /*
     * @function name	: addRating
     * @purpose			: to save the user rating 
     * @arguments		: NA
     * @return			: none
     * @created by		: sukhwinder
     * @created on		: 9th feb 2015
     * @description		: NA
     */

    function addRating() {
        $this->autoRender = false;
        $this->loadModel("UserRating");
        if ($this->request->is('ajax')) {
            $data['UserRating']['user_id'] = $this->Session->read("Auth.User.id");
            $data['UserRating']['rater_id'] = $this->request->data('id');
            $data['UserRating']['rating'] = $this->request->data('response_rating');

            $ratingId = $this->request->data('ratingId');
            if ($this->UserRating->exists($ratingId)) {
                $this->UserRating->create();
                $this->UserRating->id = $ratingId;
                $this->UserRating->save($data);
            } else
            if (!empty($data)) {
                $this->UserRating->create();
                $this->UserRating->save($data, false);
            }
        }
    }

    /* end of function */


    /*
     * @function name	: send_emailforapproval
     * @purpose			: to send an email to admin for activate account.
     * @arguments		: NA
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 19th feb 2015 
     * */

    function send_emailforapproval() {
        $this->loadModel("User");
        $this->layout = "frontend_new";
        if ($this->request->is('post')) {
            $id = $this->Session->read("Auth.User.id");
            $email = $this->Session->read("Auth.User.username");
            $name = $this->Session->read("Auth.User.Userdetail.first_name");
            // $link	= "<a href=".SITE_LINK."profile/".$id.">Click Here</a>";				
            $this->getmaildata(8);
            $this->mailBody = str_replace("{NAME}", $name, $this->mailBody);
            $this->mailBody = str_replace("{EMAIL}", $email, $this->mailBody);
            if ($this->sendmail(ADMIN_EMAIL)) {
                $this->Session->setFlash("Your approval request has been sent to admin.", 'default', array("class" => "success_message"));

                $this->User->updateAll(array('wait_approve' => 1), array('User.id' => $id));
                $this->redirect("/logout");
            } else {
                $this->Session->setFlash('An error occured, please try later.');
            }
            $this->redirect("/send-email");
        }
    }

    /* end of function */


    /*
     * @function name	: uploadcsv
     * @purpose			: to register users by csv file upload by staff member.
     * @arguments		: NA
     * @return			: none
     * @created by		: Nishi Kant Sharma
     * @created on		: 30 april 2015 
     * */

    function uploadcsv() {
        $this->loadModel('User');
        $this->autoRender = false;
        $handle = fopen($this->data['Uploadcsv']['file']['tmp_name'], "r");
        $data = fgetcsv($handle, 1000, ",");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if (isset($data[0]) && isset($data[1])) {
                $str = $data[1];
                if ($str == 'doctor') {
                    $type = 1;
                } else if ($str == 'lab') {
                    $type = 2;
                } else {
                    $type = 3;
                }
                $rand_pass = rand(100000, 999999);
                $res['User']['username'] = $data[0];
                $res['User']['user_type'] = $type;
                $res['User']['status'] = 1;
                $res['User']['password'] = $this->Auth->password($rand_pass);
                $email_exist = $this->User->find('first', array('conditions' => array('User.username' => $res['User']['username'])));
                if (empty($email_exist)) {
                    $this->User->create();
                    $this->User->save($res);
                    $this->getmaildata(17);
                    $linkurl = "<a href=" . SITE_LINK . "login>Click Here</a>";
                    $this->mailBody = str_replace("{EMAIL}", $res['User']['username'], $this->mailBody);
                    $this->mailBody = str_replace("{USERNAME}", $res['User']['username'], $this->mailBody);
                    $this->mailBody = str_replace("{PASSWORD}", $rand_pass, $this->mailBody);
                    $this->mailBody = str_replace("{LINKURL}", $linkurl, $this->mailBody);
                    $this->sendmail($res['User']['username']);
                }
            }
        }
        $this->Session->setFlash("Csv file data has been saved.", 'default', array("class" => "success_message"));
        $this->redirect("/members");
    }

    function changecarddetail() {
        $this->layout = "frontend_new";

        $months = array();
        $years = array();
        $curryear = date("Y");
        $years[$curryear] = $curryear;
        for ($i = 1; $i <= 20; $i++) {
            if ($i < 13) {
                $months[str_pad($i, 2, 0, STR_PAD_LEFT)] = str_pad($i, 2, 0, STR_PAD_LEFT);
            }
            $yearcount = $curryear + $i;
            $years[$yearcount] = $yearcount;
        }

        $this->set(compact(array('years', 'months', 'or_id')));
        if ($this->request->is('post')) {
            $this->loadModel('Carddetail');
            $data = $this->data;
//print_r($data);

            $qry = $this->Carddetail->updateAll(array('cardnumber' => $data['Carddetail']['cardnumber'], 'cvv' => $data['Carddetail']['cvv'], 'exmonth' => $data['Carddetail']['exmonth'], 'exyear' => $data['Carddetail']['exyear']), array('Carddetail.userid' => $this->Session->read('Auth.User.id')));
            $this->Session->setFlash("Card detail update successfully", 'default', array("class" => "success_message"));
        }
    }
    
    public function admin_upload() {
        $fileToSave = date('Y-m-d-His');
        $dir = new Folder(WWW_ROOT . 'upload', true, 0777);
        $destination = $dir->path . DS . $fileToSave . '.csv';
        if (Cache::read('errorLines')) {
            $this->set('errors', Cache::read('errorLines'));            
        }
        $show_error = false;
        if ($this->request->is('post')) {
            if ($this->request->data['User']['CsvFile']['error'] == 0) {
                $type = explode('.', $this->request->data['User']['CsvFile']['name']);
                if (strtolower(end($type)) != 'csv') {
                    $this->Session->setFlash(__('The file was not a valid CSV!'), 'alert');
                } else {
                    $mimes = ['application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv', 'text/comma-separated-values'];
                    if (in_array($this->request->data['User']['CsvFile']['type'], $mimes)) {
                        $move = move_uploaded_file($this->request->data['User']['CsvFile']['tmp_name'], $destination);
                        if ($move) {
                            $fileData = [];
                            $filename = $destination;
                            $handle = fopen($filename, "r");
                            $header = fgetcsv($handle);
                            $success = 0;
                            $error = 0;
                            $errorLines = [];
                            while (($row = fgetcsv($handle)) !== FALSE) {
                                $pass = true;
                                foreach ($header as $key => $value) {
                                    if ($value == 'Email') {
                                        if ($row[$key] != '') {
                                            $targetUname = $row[$key]; 
                                        } else {
                                            $pass = false;
                                        }
                                    } elseif ($value == 'Password') {
                                        if ($row[$key] == '') {
                                            $pass = false;
                                        } else {
                                            $fileData['User']['password'] = $this->Auth->password($row[$key]);
                                        }
                                    }
                                }

                                if ($pass) {
                                    $this->loadModel('User');
                                    $check = $this->User->find('first', array(
                                        'conditions' => array(
                                            'username' => $targetUname
                                        )
                                    ));
                                    if (!$check) {
                                        $fileData['User']['username'] = $targetUname;                                    
                                        $fileData['Userdetail']['email'] = $targetUname;
                                    } else {
                                        $pass = false;
                                    }

                                    if ($pass) {
                                        foreach ($header as $key => $value) {
                                            if ($value == 'Company Name') {
                                                $fileData['User']['company_name'] = $row[$key];
                                            } elseif ($value == 'First Name') {
                                                $fileData['Userdetail']['first_name'] = $row[$key];
                                            } elseif ($value == 'Last Name') {
                                                $fileData['Userdetail']['last_name'] = $row[$key];
                                            } elseif ($value == 'Service Tags') {
                                                $fileData['Userdetail']['serviceTags'] = $row[$key]; 
                                            } elseif ($value == 'Phone') {
                                                $fileData['Userdetail']['phone'] = $row[$key];
                                            } elseif ($value == 'Address') {
                                                $fileData['Userdetail']['address'] = $row[$key];
                                            }  elseif ($value == 'City') {
                                                $fileData['Userdetail']['city'] = $row[$key];
                                            } elseif ($value == 'State Id') {
                                                $fileData['Userdetail']['state_id'] = $row[$key];
                                            } elseif ($value == 'Zip Code') {
                                                $fileData['Userdetail']['zipcode'] = $row[$key];
                                            } elseif ($value == 'Latitude') {
                                                $fileData['Userdetail']['latitude'] = $row[$key];
                                            } elseif ($value == 'Longitude') {
                                                $fileData['Userdetail']['longitude'] = $row[$key];
                                            }elseif ($value == 'Licence') {
                                                $fileData['Userdetail']['licence'] = $row[$key];    
                                            } elseif ($value == 'User Type') {
                                                $fileData['User']['user_type'] = $row[$key];
                                            } elseif ($value == 'Website Link') {
                                                $fileData['Userdetail']['webLink'] = $row[$key];
                                            }                                            
                                        }
                                        //set status false by default
                                        $fileData['User']['status'] = 0;
                                        $fileData['User']['profile_status'] = 0;
                                        
                                        if ($this->User->saveAll($fileData, array('validate' => false))) {
                                            $success++;
                                        } else {
                                            $errorLines[] = $row;
                                            $error++;
                                        }
                                    } else {
                                        $errorLines[] = $row;
                                        $error++; 
                                    }
                                } else {
                                    $errorLines[] = $row;
                                    $error++;
                                }
                            }
                            
                            if ($error > 0) {
                                $show_error = true;
                                Cache::write('errorLines', $errorLines);
                            } else {
                                Cache::delete('errorLines');                                   
                            }
                            $this->Session->setFlash($success . __(' new record(s) uploaded. Error in ') . $error . __(' record(s).'), 'alert', ['params' => ['close' => true, 'error' => $show_error]]); 
                            return $this->redirect(['prefix'=>'admin', 'action'=>'upload']);                        
                        } else {
                            $this->Session->setFlash(__('An error occurred! Please try again.'), 'alert', ['params' => ['close' => true, 'error' => $show_error]]);
                        }
                    } else {
                        $this->Session->setFlash(__('The file was not a valid CSV!'), 'alert', ['params' => ['close' => true, 'error' => $show_error]]);
                    }
                }
            } else {
                $this->Session->setFlash(__('An error occurred! Please try again with a valid CSV file.'), 'alert', ['params' => ['close' => true, 'error' => $show_error]]);
            }
        }
    }
    /* end of function */
}
