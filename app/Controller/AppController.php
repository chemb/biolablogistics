<?php
session_start();
error_reporting(0);
App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');	//Used for sanitizing the data

class AppController extends Controller {
	
	public $helpers		= array("Form","Html","Session");
	public $components	= array("Session","Cookie","Email","RequestHandler","Auth" => array(
			'loginAction' => array(
						'controller' => 'users',
						'action' => 'login'
					),
			'authError' => 'Did you really think you are allowed to see that?',
			'authenticate' => array(
				'Form' => array(
					'userModel' => 'User',
					'fields' => array(
						'username' => 'username',
						'password' => 'password'
					)
				)
			),		
			'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
			'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
		));
	
/* public array variable to be used in admin panel for searching conditions */
	public $conditions		= array();
/* public array variable to be used to get ids of records to be deleted */	
	public $delarr			= array();
/* public array variable to be used to get ids of records to be updated */	
	public $updatearr		= array();
/* public variable to be used to get image name while uploading for edit profile,add/edit post etc */	
	public $imagename		= '';
/* public variable to be used to upload dir name while uploading for edit profile,add/edit post etc */	
	public $uploaddir		= '';
/* public array variable to be used to get user information while logging in */	
	public $loginedUserInfo	= array();
/* Public variable used for email functionality */
	public $value		= array();
	public $mailBody	= '';
	public $from		= '';
	public $subject		= '';
	public $template	= '';
	public $content		= '';
	public $userdomain  = '';
	public $initial		= 0;
	public $viewcourse	= 10;
/* Public variable used for Authorize user info */
	public $profileid	= '';
	public $paymentprofileid	= '';
	public $shippingprofileid	= '';
	public $dummycount =0;

/*
 * @function name	: beforefilter
 * @purpose			: used to check if user is logged in as admin or site user
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 3rd Jan 2013
 * @description		: NA
*/
	function beforefilter(){
		//pr($this->params);
		//die;
		//if (!empty($this->params->base) && $this->params->base == "/IOT1337/trunk") {
			if(!defined('SITE_LINK')) {
				define("SITE_LINK", "http://".$_SERVER['SERVER_NAME'].$this->params->base."/");
				define("FILE_LINK", "http://".$_SERVER['SERVER_NAME'].$this->params->base."/");
			}
			
		/*} else { 
			if(!defined('SITE_LINK')) {
				define("SITE_LINK","http://".$_SERVER['HTTP_HOST']."/");
				define("FILE_LINK","http://".$_SERVER['HTTP_HOST']."/");
			}
		}*/
		
		

		//PHP 5.3
		if ($this->params['controller'] == 'admins' || $this->params['prefix'] == 'admin') {
			$this->Auth->allow();
			$this->layout="admin";
		} else { 
			$this->configuration();
			if ($this->Cookie->read('Auth.User') && !$this->Session->read("Auth.User.id")) {
				$this->redirect("/login");
			}
			$this->getstaticpage();
$this->footermenu();
			if (!$this->Session->read("Auth.User.Userdetail")) {
				$this->getuserdetail();
			}
			
		  
		   
			$this->layout="frontend";
		}
		if ($this->request->is('requested') && $this->request->params['action'] == 'getstate') {
        $this->Auth->allow(array('getstate'));
    }
	}
	
	
	function check_profile_status() {
		$this->loadModel("User");
		$user = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Session->read("Auth.User.id")),'fields'=>array('User.profile_status')));
		if($user['User']['profile_status'] == 0) {
			return false;
		} else{
		    return true;	
		}
	}
	
	
/* end of function */
	
/*
 * @function name	: encryptpass
 * @purpose			: encrypt a password for admin for custom login check
 * @arguments		: Following are the arguments to be passed:
	 password to encrypt as $password
	 encryption method as $method 
	 $crop to define if encrypted password will be croped or not if true then croped otherwise not
	 $start and $stop will define starting and ending point of croping
 * @return			: encrypted password
 * @created by		: shivam sharma
 * @created on		: 3rd Jan 2013
 * @description		: while encrypting password,password has been encrypted then croped and then again encrypted to make it more secure because even in md5 encryption if a user is setting some random password like 123456 etc can be decrypted using some online tools
*/
	function encryptpass($password,$method = 'md5',$crop = true,$start = 4, $end = 10){
		if($crop){
			$password = $method(substr($method($password),$start,$end));
		}else{
			$password = $method($password);
		}
		return $password;
	}
/* end of function */


/*
 * @function name	: checklogin
 * @purpose			: redirect to dashboard if user is already login and trying to access login page or forgotpassword page
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 3rd Jan 2013
 * @description		: NA
*/
	function checklogin($action = array()) {
		if ($this->params['controller'] == 'admins' || (isset($this->params['prefix']) && $this->params['prefix'] == 'admin')) {
			$currentAction = $this->params['action'];
			if (!in_array($currentAction,$action)) {
				if($this->Session->read('admin.Admin')) {
					
				} else {
					$this->redirect("/admin");
				}
			}
		} 
	}
/* end of function */


/*
 * @function name	: sendmail
 * @purpose			: sending email for various actions
 * @arguments		: Following are the arguments to be passed:
	 * from		: contain email address from which email is sending
	 * Subject	: Subject of Email
	 * to		: Email address to whom the email is sending
	 * body		: content of email
	 * template : if defining a html template for sending email else false.
	 * values	: to be given in email template like username etc.	 
 * @return			: true if email sending successfull else return false.
 * @created by		: shivam sharma
 * @created on		: 10th Jan 2013
 * @description		: NA
*/
	function sendmail($to,$template = 'email',$fromname = 'biolab',$layout = "default") {
		App::uses('CakeEmail', 'Network/Email');
		if(isset($this->params->base) && !empty($this->params->base)) {
			$email = new CakeEmail("gmail");
		} else {
			$email = new CakeEmail();
		}
		if(filter_var($to, FILTER_VALIDATE_EMAIL)) {
			$email->from(array($this->from => $fromname));
			$email->to($to);
			$email->subject($this->subject);
			$headers[]  = 'MIME-Version: 1.0';
			$headers[]  = 'Content-type: text/html; charset=iso-8859-1';
			$email->addHeaders($headers);
			$email->emailFormat('both');
			if (empty($template)) {
				try {
					$email->send($this->mailBody);
					return true;
				} catch (Exception $e) {
					$e->getMessage();
					return false;
				}
			} else {
				
				if(!empty($this->mailBody)) {
					$email->viewVars(array("mail"=>$this->mailBody));
				}
				$email->template($template,$layout);
				try {
					$email->send();
					return true;
				} catch (Exception $e) {
					$e->getMessage();
					return false;
				}
			}
		} else {
			return false;
		}
	}
/* end of function */

/*
 * @function name	: getmaildata
 * @purpose			: getting email data for various purposes
 * @arguments		: Following are the arguments to be passed:
	 * id		: id of email templates from cmsemail table
 * @return			: NONE
 * @created by		: shivam sharma
 * @created on		: 10th June 2013
 * @description		: function will assign value to global variables like mailbody,from, subject which will be used while sending email
*/
	
	function getmaildata($id = null) {
		$this->loadModel("Cmsemail");
		$cmsemail = $this->Cmsemail->find("first",array("conditions"=>array("Cmsemail.id"=>$id)));
		if (!empty($cmsemail)) {
			$this->mailBody = $cmsemail['Cmsemail']['mailcontent'];
			$this->from = $cmsemail['Cmsemail']['mailfrom'];
			$this->subject = $cmsemail['Cmsemail']['mailsubject'];
		}
	}
/* end of function */


/*
 * @function name	: adminbreadcrumb
 * @purpose			: to create bread crumb of admin module
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 4th jan 2013
 * @description		: NA
*/
	function adminbreadcrumb(){
		$this->loadModel("Breadcrumb");
		$breadcrumb = $this->Breadcrumb->find("all",array("conditions"=>array("controller"=>$this->params['controller'],"action"=>$this->params['action'])));
		if(!empty($breadcrumb)) {
			$this->set("breadcrumb",$breadcrumb);
		}
	}
/* end of function */


/*
 * @function name	: bulkactions
 * @purpose			: to perform multiple change status and delete in admin panel
 * @arguments		: flag which will indicate some special cases like users controller in which conditions we need to send confimation emails to users
 * @return			: will return array containing user ids but only when flag will be set true
 * @created by		: shivam sharma
 * @created on		: 4th Jun 2013
 * @description		: NA
*/
	function bulkactions($flag = false) {
		/* code to change status and delete by checking data from page */
		$controller = is_array($this->data)?array_keys($this->data):'';
		$statuskey  = '';
		$controller = isset($controller[0])?$controller[0]:'';
		$allowedarr = array("Account","User");
		if (isset($this->data[$controller]) && !empty($this->data[$controller]['options']) && !empty($controller)) {
                    if ($this->data[$controller]['options'] == 'Approve') {
                        foreach ($this->data['id'] as $key=>$val) {
                            if ($val > 0) {
                                $this->$controller->id = $key;
                                $this->$controller->saveField('profile_status', true);
                            }
                        }                        
                    } else {
                        foreach ($this->data['id'] as $key=>$val) {
				
				if ($val > 0) {
					$this->delarr[]	= $key;
					if ($flag) {
						$statuskey		= ($this->data[$controller]['options']);
						$this->updatearr[$controller][$key]	= array("id"=>$key,"approve"=>($this->data[$controller]['options']));
						
					} else {
						$statuskey		= ($this->data[$controller]['options'] == 'Active'?1:0);
						$this->updatearr[$controller][$key]	= array("id"=>$key,"status"=>($this->data[$controller]['options'] == 'Active'?1:0));
						$this->Session->setFlash('Record has been updated successfully.');
						 
					}
				} 
			}
			if (isset($this->data[$controller]['options']) && $this->data[$controller]['options'] == 'Delete') {
				if($flag == 1){
					if($this->unlinkDB($this->delarr)){
						$this->$controller->delete($this->delarr);
					}
				}
				else{
					$this->$controller->delete($this->delarr);
					$this->Session->setFlash('Record has been deleted successfully.');
				}
				$statuskey = -1;
			} else {
				$this->$controller->saveAll($this->updatearr[$controller]);
			}
			if (empty($this->data['Admin']['searchval'])) {
				$this->data = array();
			}
                    }
		} 
		if (in_array($controller,$allowedarr) && $statuskey > -1) {
			$arr['keys'] 	= $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
		if ($flag) {
			$arr['keys'] = $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
                
                return;
		/* end of code to change status and delete by checking data from page */
	}
/* end of function */


/*
 * @function name	: bulkactions
 * @purpose			: to perform multiple change status and delete in admin panel
 * @arguments		: flag which will indicate some special cases like users controller in which conditions we need to send confimation emails to users
 * @return			: will return array containing user ids but only when flag will be set true
 * @created by		: shivam sharma
 * @created on		: 4th Jun 2013
 * @description		: NA
*/
	function frontendbulkactions($flag = false) {
		/* code to change status and delete by checking data from page */
		$controller = is_array($this->data)?array_keys($this->data):'';
		$statuskey  = '';
		$controller = isset($controller[0])?$controller[0]:'';
		$allowedarr = array("Account","User");
		if (isset($this->data[$controller]) && !empty($this->data[$controller]['options']) && !empty($controller)) {
			
			foreach ($this->data['id'] as $key=>$val) {
				if ($val > 0) {
					$this->delarr[]	= $key;
					if($flag) {
						$optvalue = isset($this->data[$this->data[$controller]['options']][$val])?$this->data[$this->data[$controller]['options']][$val]:'';
						if ($this->data[$controller]['options'] == 'messagestatus' && $optvalue == 'Delete') {
							$optvalue = 'Read';
						}
						if ($controller == 'Campaign' && $this->data[$controller]['options'] == 'delete') {
							$optvalue = '1';
						}
						$this->updatearr[$controller][$key]	= array("id"=>$key,$this->data[$controller]['options']=>$optvalue);
					} else {
						$this->updatearr[$controller][$key]	= array("id"=>$key,"status"=>($this->data[$controller]['options'] == 'Active'?1:0));
					}
				}
			}
			if (isset($this->data[$controller]['options']) && $this->data[$controller]['options'] == 'Delete' && !$flag) {
				$this->$controller->delete($this->delarr);
				$statuskey = -1;
			} else {
				$this->$controller->saveAll($this->updatearr[$controller]);
			}
			if (empty($this->data['Admin']['searchval'])) {
				$this->data = array();
			}
		}
		/* end of code to change status and delete by checking data from page */
	}
/* end of function */


/*
 * @function name	: uploadimage
 * @purpose			: to upload image files for various functionalities like 
 * @arguments		: Following are the arguments to be passed:
		* file			: File array to be uploaded
		* userid		: id/type/domain of user uploading the image
		* size			: allowed file size by user
		* destination	: destination folder name in which the image will be uploaded
		* usertype		: admin or user
		* imagename		: name of image going to upload
		* height		: used to resize image height 
		* width			: used to resize image height 
		* allowed		: allowed type to upload
 * @return			: image name with full destination detail on server
 * @created by		: shivam sharma
 * @created on		: 16th jan 2013
 * @description		: NA
*/
	function uploadimage($file, $userid = null, $size = null, $destination = null, $usertype=null, $imagetype = '', $imagename=null, $height = 200, $width = 200, $allowed = array("jpg","jpeg","png","gif")) {
		//Configure::write('debug',2); 
		//code to upload with resizesing image//
		if (!is_dir(WWW_ROOT."img/tmp")) {
			exec("mkdir ".WWW_ROOT."img/tmp");
			exec("chmod 777 tmp");
		}
		
		if(($file['error'] == 0) && (move_uploaded_file($file['tmp_name'],WWW_ROOT."img/tmp/".$file['name']))){
			$orgfile = explode(".",$file['name']);
			if (empty($imagename)) {
				$this->imagename .= strtotime(date("y-m-d h:i:s")).".".$orgfile[count($orgfile)-1];
			} else {
				$this->imagename = $imagename.".".$orgfile[count($orgfile)-1];
			}
			
			if (!empty($userid)) {
				if (!is_dir(WWW_ROOT."img/".$userid)) {
					exec("mkdir ".WWW_ROOT."img/".$userid);
					if (!empty($imagetype)) {
						exec("mkdir ".WWW_ROOT."img/".$userid."/".$imagetype);
					}
					exec("chmod -R 777 ".WWW_ROOT."img/".$userid);
				} else {
					if (!empty($imagetype) && !is_dir(WWW_ROOT."img/".$userid."/".$imagetype)) {
						exec("mkdir ".WWW_ROOT."img/".$userid."/".$imagetype);
					}
					exec("chmod -R 777 ".WWW_ROOT."img/".$userid);
				}
				if($usertype == 'admin' && !is_dir(WWW_ROOT."img/".$userid."/".$imagetype)) {
					exec("mkdir ".WWW_ROOT."img/".$userid."/".$imagetype);
					exec("chmod -R 777 ".WWW_ROOT."img/".$userid);
				}
				$imagetype .= empty($imagetype)?'':"/";
				$this->uploaddir = "/img/".$userid."/".$imagetype;
			} elseif(!empty($destination)) {
				$destArr = explode("/",$destination);
				$deststr = '';
				if(!empty($destArr)) {
					foreach($destArr as $key=>$val) {
						if (!empty($val)) {
							$deststr .= "/".$val;
							if(!is_dir(WWW_ROOT."img".$destination)) {
								exec("mkdir ".WWW_ROOT."img".$deststr);
								exec("chmod 777 ".WWW_ROOT."img".$deststr);
							}
						}
					}
				}
				if (!is_dir(WWW_ROOT."img/".$destination)) {
					exec("mkdir ".WWW_ROOT."img/".$destination);
					exec("chmod 777 ".WWW_ROOT."img/".$destination);
				}
				$this->uploaddir = "/img/".$destination."/";
				
			} else {
				$this->uploaddir = "/img/";
			}
			exec("chmod -R 777 ".WWW_ROOT."img/");
			/* Thumbs of Profile and Course */
			if($imagetype == 'profileimg/'){
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.LargeProfileImagePrefix.$this->imagename,LargeProfileImage,LargeProfileImage);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.MediumProfileImagePrefix.$this->imagename,MediumProfileImage,MediumProfileImage);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.MediumSmallProfileImagePrefix.$this->imagename,MediumSmallProfileImage,MediumSmallProfileImage);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.SmallImageProfilePrefix.$this->imagename,SmallProfileImage,SmallProfileImage);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.ThumbImageProfilePrefix.$this->imagename,ThumbProfileImage,ThumbProfileImage);
			} elseif($imagetype == 'coverimg'){
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.LargeCourseImagePrefix.$this->imagename,LargeCourseImageWidth,LargeCourseImageHeight);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.MediumCourseImagePrefix.$this->imagename,MediumCourseImageWidth,MediumCourseImageHeight);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.MediumSmallCourseImagePrefix.$this->imagename,MediumSmallCourseImageWidth,MediumSmallCourseImageHeight);
				$this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.SmallCourseImagePrefix.$this->imagename,SmallCourseImage,SmallCourseImage);
			}
			/* Thumbs of Profile and Course */
			if ($this->Image->resize(WWW_ROOT."/img/tmp/".$file['name'],WWW_ROOT.$this->uploaddir.$this->imagename,$height,$width)){
				unlink(WWW_ROOT."/img/tmp/".$file['name']);
				return true;
			} else {
				unlink(WWW_ROOT."/img/tmp/".$file['name']);
				return false;
			}
		}
		//end here//
	}
/* end of function */


	function uploadAttachments($file , $destination = NULL, $filetypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'stl', 'cad')) {
		$flag = false;
		$file_ext = explode(".",$file['name']);
		$file_ext = strtolower(end($file_ext));
		$this->imagename = $this->uploaddir =  '';
		if ( in_array($file_ext,$filetypes) ) {
			if ( !empty($destination) && !is_dir($destination) ) {
				mkdir(WWW_ROOT.$destination,0777,true);
			}
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$file_ext;
			$this->uploaddir = $destination;
			if ( move_uploaded_file($file['tmp_name'],WWW_ROOT.$destination.$this->imagename) ) {
				$flag = true;
			} 
		} else {
			$this->Session->setFlash("Please select valid file to upload.", 'default', array("class"=>"success_message"));
			$this->redirect("order");
		}
		return $flag; 
	}
	
	
   	function uploadAttachments2($file , $destination = NULL,$order_id = NULL , $filetypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'stl', 'cad')) {
		$flag = false;
		$file_ext = explode(".",$file['name']);
		$file_ext = strtolower(end($file_ext));
		$this->imagename = $this->uploaddir =  '';
		if ( in_array($file_ext,$filetypes) ) {
			if ( !empty($destination) && !is_dir($destination) ) {
				mkdir(WWW_ROOT.$destination,0777,true);
			}
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$file_ext;
			$this->uploaddir = $destination;
			if ( move_uploaded_file($file['tmp_name'],WWW_ROOT.$destination.$this->imagename) ) {
				$flag = true;
			} 
		} else {
			$this->Session->setFlash("Please select valid file to upload.", 'default', array("class"=>"success_message"));
			$this->redirect(array("action"=>"viewOrder",$order_id));
		}
		return $flag; 
	}
	

/*
 * @function name	: getaddress
 * @purpose			: to get detail of address according to Zip code used while adding post
 * @arguments		: Following are the arguments to be passed:
		* zip			: zip code from which adress will be searched
		* address		: to be returned
 * @return			: adress containing zip and other information like city,state,country
 * @created by		: shivam sharma
 * @created on		: 20th jan 2013
 * @description		: NA
*/
	function getaddress($zip,$flag = true)	{ 	
		$curl_handle=curl_init();
		$address=urlencode($zip);
		$result = array();
		$this->loadModel('UsZipCode');
		$getZip = array();
		$getZip = $this->UsZipCode->find("first", array('fields'=>array('id','zip','latitude','longitude'), 'conditions'=>array('UsZipCode.zip'=>$zip)));
		//$getZip = $this->UsZipCode->query(' SELECT `UsZipCode`.`id`, `UsZipCode`.`zip`, `UsZipCode`.`latitude`, `UsZipCode`.`longitude` FROM `rrl`.`us_zip_codes` AS `UsZipCode` WHERE `UsZipCode`.`zip` = "'.$zip.'" LIMIT 1');
		if( isset ( $getZip[0] ) && !empty ( $getZip[0] ) ) {
			$result['lat']   = $getZip[0]['UsZipCode']['latitude'];
			$result['long']  = $getZip[0]['UsZipCode']['longitude'];
		} else {
			$url="http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false";
			curl_setopt($curl_handle,CURLOPT_URL,$url);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$data = curl_exec($curl_handle);
			curl_close($curl_handle);
			$data =  json_decode($data);
			if(!empty($data) && $data->status == 'OK' ) {
				$result['lat']   = $data->results[0]->geometry->location->lat;
				$result['long']  = $data->results[0]->geometry->location->lng;
			}
			try {
				if(isset($data->results[0]->formatted_address)) {
					$address = $data->results[0]->formatted_address;
				} else {
					$address = '';
				}
			} catch(Exception $e) {
				$address = 1;
			}
		}
		//pr($result);
		
		return $result;
	}
	
	
/* end of function */

	
/*
 * @function name	: getuserdetail
 * @purpose			: to get detail of logged in user
 * @arguments		: Following are the arguments to be passed:
		* id			: id of logged in user
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 20th june 2013
 * @description		: this function will fetch detail from userdetail table and append it with auth session
*/	
	function getuserdetail($id = null,$model = NULL) {
		if ($this->Session->read("Auth.User.id")) {
			$id = $this->Session->read("Auth.User.id");
		}
		if(!empty($id)) {
			$this->loadModel("Userdetail");
			$this->Userdetail->recursive = 0;
			$userdetail = $this->Userdetail->find("all",array("conditions"=>array("Userdetail.user_id"=>$id),"recursive"=>"-1"));
			if ($this->Session->read("Auth.User.userdetail")) {
				$this->Session->delete("Auth.User.userdetail");
			}
			$this->Session->write("Auth.User.userdetail",$userdetail[0]['Userdetail']);
			$this->Session->write("Auth.User.Userdetail",$userdetail[0]['Userdetail']);
		}
	}
/* end of function */	

/*
 * @function name	: username
 * @purpose			: to get name of logged in user
 * @arguments		: Following are the arguments to be passed:
		* id			: id of logged in user
 * @return			: name of logged in user
 * @created by		: shivam sharma
 * @created on		: 20th june 2013
 * @description		: NA
*/		
	function username($id) {
		$this->loadModel("Userdetail");
		$this->Userdetail->recursive = -1;
		$userdetail = $this->Userdetail->find("first",array("conditions"=>array("Userdetail.user_id"=>$id),"fields"=>array("Userdetail.first_name","Userdetail.last_name")));
		return ($userdetail['Userdetail']['first_name'].' '.$userdetail['Userdetail']['last_name']);
	}
/* end of function */


/*
 * @function name	: Get Enter form budget prize
 * @created by		: mahavir singh
 * @created on		: 24-sep-2015
 * @description		: NA
*/		
	function getbudgetprize($id) {
		$this->loadModel("OrderDetail");
		$budget = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$id)));
				$this->set('bud_pri',$budget['Order']['budget']);
				return ($budget['Order']['budget']);
	}
	
	function getdeliveryprize($id) {
		$this->loadModel("OrderPayment");
		$budget = $this->OrderPayment->find("first",array("conditions"=>array("OrderPayment.order_id"=>$id, "OrderPayment.receiver"=>'-1')));
				$this->set('bud_pri',$budget['OrderPayment']['payment']);
				return ($budget['OrderPayment']['payment']);
	}
	function getlabstatus($id) {
		$this->loadModel("Labtransaction");
		$l_status = $this->Labtransaction->find("first",array("conditions"=>array("Labtransaction.order_id"=>$id)));
		$this->set('lab_status',$l_status['Labtransaction']['check_recieved']);
		return ($l_status['Labtransaction']['check_recieved']);
	}
/* end of function */
/*
 * @function name	: company name
 * @purpose			: to get name of company name in user
 * @return			: name of logged in user
 * @created by		: mahavir singh
 * @created on		: 23-sep-2015
 * @description		: NA
*/		
	function companyname($uid) {
		$this->loadModel("User");
		
		$companyname = $this->User->find("first",array("conditions"=>array("User.id"=>$uid)));
		$this->set('lab_name',$companyname['User']['company_name']);
		return ($companyname['User']['company_name']);
	}
	
	
	function companydetail($uid) {
		$this->loadModel("User");
		
		$companydetail = $this->User->find("first",array("conditions"=>array("User.id"=>$uid)));
		$this->set('companydetail',$companydetail);
		return ($companydetail);
	}
/* end of function */


/*
 * @function name	: Prize detail
 * @purpose			: to get name of company name in user
 * @arguments		: Following are the arguments to be passed:
 * @return			: total prize
 * @created by		: mahavir singh
 * @created on		: 23-sep-2015
 * @description		: NA
*/		
	function getprize($id=null) {
		$this->loadModel('OrderPayment');
		
		$prize = $this->OrderPayment->find("all",array("conditions"=>array("OrderPayment.order_id"=>$id)));
		if($prize)
		{
			return ($prize);
		}
		else
		{
			$prize = 'Waiting..';
		}
		//Use like this
		//$payment_m[0]['OrderPayment']['payment'];
		//$payment_m[1]['OrderPayment']['payment'];
	
		return ($prize);
	}
/* end of function */
/*
 * @function name	: payment status
 * @purpose			: to get payment status by order is
 * @arguments		: order id
 * @return			: payment status
 * @created by		: mahavir singh
 * @created on		: 24-sep-2015
*/		
	function getpaymentstatus($id=null) {
		$this->loadModel('OrderPayment');
		$status = '';
		$payment_status = $this->OrderPayment->find("all",array("conditions"=>array("OrderPayment.order_id"=>$id)));
		if($payment_status)
		{
			if($payment_status[0]['OrderPayment']['payment_status'] == 0 && $payment_status[0]['OrderPayment']['payment_status'] == 0){
			$status = 'Payment Required';
			$this->set('pay_st',$status);
			return $status;
		}
		else{
			$status = 'Payment Accepted';
			$this->set('pay_st',$status);
			return $status;
		}
		
		}
		else
		{
			$status = 'Pending';
			$this->set('pay_st',$status);
			return $status;
		
		}
		
	$status = 'Payment Accepted';
	$this->set('pay_st',$status);
		return ($status);
	}
/* end of function */

/*
 * @function name	: Get the order status status by id 
 * @purpose			: to get name of company name in user
 * @arguments		: Following are the arguments to be passed:
 * @return			: total prize
 * @created by		: mahavir singh
 * @created on		: 23-sep-2015
 * @description		: NA
*/		
	function getstatus($id) {
		$this->loadModel("OrderStatus");
		$status_mm = $this->OrderStatus->find('all', array('conditions' => array('OrderStatus.order_id' => $id),'order' => array('OrderStatus.status_id' => 'DESC'),'limit'=>1));
		
		return ($status_mm['0']['Status']['title']);
		
	}
	//Use like this
	//$hello = $this->getstatus($id);
	//$hello['0']['Status']['title'];
/* end of function */
/*
 * @function name	: Get the patient name by order id
 * @arguments		: Following are the arguments to be passed:
 * @return			: patient name
 * @created by		: mahavir singh
 * @created on		: 24-sep-2015
 * @description		: NA
*/		
	function getpatient($id) {
		$this->loadModel("OrderDetail");
		$patient_name = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$id)));
		$p_name = unserialize($patient_name['OrderDetail']['details']);
		return $p_name[0]['patientName'];
	
	}
	//Use like this
	//$hello = $this->getstatus($id);
	//$hello['0']['Status']['title'];
/* end of function */


function getUserDetailOnFly($user_id = NULL,$keys = array('User.username')){
	$this->loadModel("User");
	$this->User->recursive = 0;
	$details = $this->User->find('first',array("conditions"=>array("User.id"=>$user_id),"fields"=>$keys)); 
	return $details;
}

/*
 * @function name	: createinvoice
 * @purpose			: to get create an invoice number for every transaction
 * @arguments		: Following are the arguments to be passed:
		* userid		: id of logged in user
 * @return			: invoice number
 * @created by		: shivam sharma
 * @created on		: 20th june 2013
 * @description		: NA
*/
	function createinvoice($userid = NULL) {
		$this->loadModel("Order");
		if(empty($userid)) {
			$orderid = $this->Order->find("count",array("conditions"=>array("Order.buyer_id"=>$this->Session->read("Auth.User.id"))));
			$invoice = "1337IOT/".date("Y-m-d")."/".$this->Session->read("Auth.User.id")."/".($orderid+1);
		} else { 
			$orderid = $this->Order->find("count",array("conditions"=>array("Order.buyer_id"=>$userid)));
			$invoice = "1337IOT/".date("Y-m-d")."/".$userid."/".($orderid+1);
		}
		
		return $invoice;
	}
/* end of function */


/*
 * @function name	: makeurl
 * @purpose			: to get a seo friendly url
 * @arguments		: Following are the arguments to be passed:
		* str		: string to parse as url
 * @return			: string
 * @created by		: shivam sharma
 * @created on		: 20th june 2013
 * @description		: it will remove spaces from string and replace it with "-"
*/
	function makeurl($str = NULL) {
		if(!empty($str)) {
			$str = strtolower(str_replace(' ','-',substr(strip_tags($str),0,80)));
		}
		return $str;
	}
/* end of function */


/*
 * @function name	: configuration
 * @purpose			: to make default variables as global variable which will be used throu-out the site
 * @arguments		: none
 * @return			: none
 * @created by		: shivam sharma
 * @created on		: 15th june 2013
 * @description		: it will define some global variables specified by "default_header" from configurations table
*/	
	function configuration() { 
		$this->loadModel("Configuration");
		$configurations = $this->Configuration->find("all");
		$str = "";
		foreach($configurations as $key=>$val) {
			if(!defined($val['Configuration']['default_header'])) {
				//$str .= "define('".$val['Configuration']['default_header']."','".$val['Configuration']['value']."');".'\n';
				$str .= 'define("'.$val['Configuration']['default_header'].'","'.$val['Configuration']['value'].'");'."\n";
			}
		}
		if(!empty($str)) {
			$str .= "define('RETURN_URL','http://".$_SERVER['SERVER_NAME'].$this->params->base."/');\n";
			$str .= "define('CENCEL_URL','http://".$_SERVER['SERVER_NAME'].$this->params->base."/');\n";
			$str = "<?php \n".$str."?>";
			$file = WWW_ROOT."custom/config.php";
			if(file_exists($file)) {
				unlink($file);
			}
			file_put_contents($file, $str, FILE_APPEND | LOCK_EX);
		}
	}
/* end of function */



/*
 * @function name	: unlinkDB
 * @purpose			: to remove the db files which were downloaded as backup
 * @arguments		: Following are the arguments to be passed:
		* ids			: ids of backup table records
 * @return			: none
 * @created by		: sandeep kaur
 * @created on		: 28th july 2013
 * @description		: NA
*/
	function unlinkDB($ids = null){
		foreach($ids as $id){
			$this->loadModel("Backupdb");
			$filename  = $this->Backupdb->find("first", array("fields"=>array("filename"), "conditions"=>array("id"=>$id)));
			$file = $filename['Backupdb']['filename'];
			if(file_exists(WWW_ROOT.'backupdb/'.$file)){
				unlink(WWW_ROOT.'/backupdb/'.$file);
			}
		}
		return true;
	}
/* end of function */


/*
 * @function name	: getstaticpage
 * @purpose			: to get content for static pages according to slugs
 * @arguments		: Following are the arguments to be passed:
		* slugs			: slug content in cmspages database
 * @return			: none
 * @created by		: sandeep kaur
 * @created on		: 28th july 2013
 * @description		: NA
*/
	function getstaticpage($slug = null){
		$this->loadModel('Cmspages');
		$pages = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl'), 'conditions'=>array('Cmspages.status'=>1, 'Cmspages.showinheader'=>1), 'order'=>'Cmspages.name'));
		$pageContent = $this->Cmspages->find('first', array('conditions'=>array('Cmspages.seourl'=>$slug)));
		$this->set("title_for_layout",$pageContent['Cmspages']['name']);
		$this->set(compact('pages','pageContent'));
	}

    function footermenu($slug = null){
		
		$this->loadModel('Cmspages');
		
$f_pages = $this->Cmspages->find('all', array('fields'=>array('Cmspages.name', 'Cmspages.seourl'), 'conditions'=>array('Cmspages.status'=>1, 'Cmspages.showinfooter'=>1), 'order'=>'Cmspages.name'));
$pageContent = $this->Cmspages->find('first', array('conditions'=>array('Cmspages.seourl'=>$slug)));
		$this->set("title_for_layout",$pageContent['Cmspages']['name']);
		$this->set(compact('f_pages','pageContent'));
	}

/* end of function */


	function chekAccess($type=1) {
		if ( $this->Session->read('Auth.User.user_type') != $type )  {
			throw new NotFoundException(__('Invalid Action'));
		}
	}
	function getprocedure($id) {
		$this->loadModel("OrderDetail");
		$pro = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$id)));
		$procc = unserialize($pro['OrderDetail']['details']);
		$pro_name = '';
		if(!$procc[4]['IMPRESSION'] == 0){$pro_name .= 'IMPRESSION'.'</br>';}
		if(!$procc[5]['WAXUP'] == 0){$pro_name .= 'WAXUP'.'</br>';}
		if(!$procc[6]['DENTURE'] == 0){$pro_name .= 'DENTURE'.'</br>';}if(!$procc[7]['CROWN'] == 0){$pro_name .= 'CROWN'.'</br>';}
		if(!$procc[8]['ARTICULATOR'] == 0){$pro_name .= 'ARTICULATOR'.'</br>';}if(!$procc[9]['BITE'] == 0){$pro_name .= 'BITE'.'</br>';}
		if(!$procc[10]['FRAMEWRK'] == 0){$pro_name .= 'WAXUP'.'</br>';}if(!$procc[11]['PYMT'] == 0){$pro_name .= 'PYMT'.'</br>';}
		if(!$procc[12]['MODELS'] == 0){$pro_name .= 'MODELS'.'</br>';}
if(!$procc['BITEBLK'] == 0){$pro_name .= 'BITEBLK'.'</br>';}
		if(!$procc['PARTIAL'] == 0){$pro_name .= 'PARTIAL'.'</br>';}
if(!$procc['CASESPOSTAGE'] == 0){$pro_name .= 'WAXUP'.'</br>';}
		
		$this->set($pro_name,$pro_name);
		return ($pro_name);
	
	}
	
	
	function getorderid($order_id)
	{
		$this->loadModel('Order');
		$idname = $this->Order->find("first",array("conditions"=>array("Order.id"=>$order_id)));
                    $this->set($idname,$idname['Order']['order_manualid']);
		    return ($idname['Order']['order_manualid']);
	}
	
	
	function getlabid($order_id)
	{
		$this->loadModel('OrderDetail');
		$lab_id = $this->OrderDetail->find("first",array("conditions"=>array("OrderDetail.order_id"=>$order_id)));
        $this->set('lab',$lab_id);
		    return ($lab_id);
	}
	function getcountdispatch($lab_id)
	{
		$this->loadModel('OrderStatus');
		$status= $this->OrderStatus->find("all",array('conditions'=>array('OrderStatus.status_id'=>7,'OrderStatus.updated_by'=>$lab_id)));
		$st_count = count($status);
		$this->set('lstatus',$st_count);
         return ($lstatus);
		 
	}
function notificationmsg($id) {
		$this->loadModel('Notificationtitle');
		$noti = $this->Notificationtitle->find("all",array("conditions"=>array("Notificationtitle.id"=>$id)));
		$this->set('noti',$noti);
		return $noti;
	}


	
	function notificationcount($uid) {
		
		$this->loadModel('Notification');
		
		$noti = $this->Notification->find("all",array("conditions"=>array("Notification.show_id"=>$uid, "Notification.status"=>1)));
		
		$this->set('noti',$noti);
	
		return $noti;
	}
function notificationdata($uid) {
		$this->loadModel('Notification');
		$this->loadModel('Notificationtitle');
		$noti_data = array();
		$norder_id = array();
		$noti = $this->Notification->find("all",array("conditions"=>array("Notification.show_id"=>$uid, "Notification.status"=>1)));
		for($i=0;$i<count($noti);$i++ ){
		$no_id = $noti[$i]['Notification']['status_id'];
	$noti_data[] = $this->Notificationtitle->find("all",array("conditions"=>array("Notificationtitle.id"=>$no_id)));
		$norder_id[] = $noti[$i]['Notification']['order_id'];
	}
	$this->set('noti',compact('noti_data','norder_id'));
	
		return compact('noti_data','norder_id');
		
	}
	
	function getstate($id) {
		
		$this->loadModel('State');
		
		$state = $this->State->find("all",array("conditions"=>array("State.id"=>$id,)));
		
		$this->set('state',$state);
	
		return $state;
	}
function getprepay($lid) {
		$this->loadModel('Prepaid');
		$prepay = $this->Prepaid->find("first",array("conditions"=>array("Prepaid.lab_id"=>$lid,)));
		
		$this->set('prepay',$prepay['Prepaid']['prepay_amt']);
	
		return $prepay['Prepaid']['prepay_amt'];
	}
function getdelivery($lid) {
		$this->loadModel('Deliverycharge');
	$dd = $this->Deliverycharge->find("first",array("conditions"=>array("Deliverycharge.lab_id"=>$lid,)));
		return $dd;
	}


	
	function getdriver($id) {
		$this->loadModel('Driver');
		$this->loadModel('User');
		$driver_detail = $this->Driver->find('first',array("conditions"=>array("Driver.order_id"=>$id)));
		$driver_id = $driver_detail['Driver']['driver_id'];
		$user_detail = $this->User->find('first',array("conditions"=>array("User.id"=>$driver_id)));
		$driver_name = $user_detail['Userdetail']['first_name'];
		return $driver_name;
	}
	
	function getorderpayment($orderid)
	{
		$this->loadModel('OrderPayment');
		$order_detail = $this->OrderPayment->find('first',array("conditions"=>array("OrderPayment.order_id"=>$orderid)));
		return $order_detail;
	}
	
	function curl_request($sURL,$sQueryString=null)
	{
	        $cURL=curl_init();
	        curl_setopt($cURL,CURLOPT_URL,$sURL.'?'.$sQueryString);
	        curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
	        $cResponse=trim(curl_exec($cURL));
	        curl_close($cURL);
	        return $cResponse;
	}
	
	function getdistance($labid=null)
	{
		$this->loadModel("Userdetail");
		$labdata = $this->Userdetail->find("first",array("conditions"=>array('Userdetail.user_id'=>$labid)));
		//return $labdata;
		$labzip=$labdata['Userdetail']['zipcode'];
		$docdata = $this->Userdetail->find("first",array("conditions"=>array('Userdetail.user_id'=>$this->Session->read('Auth.User.id'))));
		$doczip=$docdata['Userdetail']['zipcode'];
		/*$lab_l = $this->getaddress($labzip,$flag = true);
		$lab_lat = $lab_l['lat'];
		$lab_long = $lab_l['long'];
		
		$doc_l = $this->getaddress($doczip,$flag = true);
		$doc_lat = $doc_l['lat'];
		$doc_long = $doc_l['long'];*/
$sResponse=$this->curl_request('http://maps.googleapis.com/maps/api/distancematrix/json','origins="'.$doczip.'"&destinations="'.$labzip.'"&mode=driving&units=imperial&sensor=false');
		$oJSON=json_decode($sResponse);
		$dic = $oJSON->rows[0]->elements[0]->distance->text;
if($oJSON->rows[0]->elements[0]->distance->text == '1 ft'){
	$dic = '0 mi';
}
	 return $dic;
	
		
		//return 'lab<br>  : '.$labzip.'<br> Doctor<br>  : '.$doczip;
		/*
		$theta = $lab_long - $doc_long;
  $dist = sin(deg2rad($lab_lat)) * sin(deg2rad($doc_lat)) +  cos(deg2rad($lab_lat)) * cos(deg2rad($doc_lat)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
    return $miles; 
    */
}

function gettransaction($id)
{
	
		$this->loadModel('Transactions');
		$order_detail = $this->Transactions->find('first',array("conditions"=>array("Transactions.order_payment_id"=>$orderid)));
		return $order_detail;
}

function getorderdata($id)
{
	
		$this->loadModel('Order');
		$order_detail = $this->Order->find('first',array("conditions"=>array("Order.id"=>$id)));
		return $order_detail;
}

function getpickupdata($docid,$labid,$statusid)
{
	   $this->loadModel("OrderStatus");
	   $this->loadModel("OrderRequest");
		$this->loadModel('Order');
		$order_detail = $this->Order->find('all',array("conditions"=>array("Order.doctor_id"=>$docid)));
		$count=0;
		foreach ($order_detail as  $data2)
		{
		$order_detail2 = $this->OrderRequest->find('first',array("conditions"=>array("OrderRequest.lab_id"=>$labid,"OrderRequest.order_id"=>$data2['Order']['id'])));
		
		if($order_detail2['OrderRequest']['order_id'] == $data2['Order']['id']){
		$status_mm = $this->OrderStatus->find('first', array('conditions' => array('OrderStatus.order_id' =>$order_detail2['OrderRequest']['order_id']),'order' => array('OrderStatus.status_id' => 'DESC'),'limit'=>1));
		if($statusid==$status_mm['OrderStatus']['status_id'])
		{	
		$count=$count+1;
	    }
	}	
		
	}
		return $count;
		
}
function getzipdistance($zip1=null, $zip2=null)
	{
$sResponse=$this->curl_request('http://maps.googleapis.com/maps/api/distancematrix/json','origins="'.$zip1.'"&destinations="'.$zip2.'"&mode=driving&units=imperial&sensor=false');
		$oJSON=json_decode($sResponse);
		$dic = $oJSON->rows[0]->elements[0]->distance->text;
if($oJSON->rows[0]->elements[0]->distance->text == '1 ft'){
	$dic = '0 mi';
}
	 return $dic;
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

function carddata($labid){
$this->loadModel("Carddetail");
	
$carddata=$this->Carddetail->find("first",array("conditions"=>array("Carddetail.userid"=>$labid)));	
return $carddata;
}


function labpriceshow($orderid){
$this->loadModel("Labtransaction");
	
$pdata=$this->Labtransaction->find("first",array("conditions"=>array("Labtransaction.order_id"=>$orderid)));	
return $pdata;
}


	function delivereditems()
{
	$this->loadModel("OrderStatus");
	$data=$this->OrderStatus->find('all', array("conditions"=>array('OrderStatus.status_id'=>'9')));
	$deliveredcount=count($data);
	return $deliveredcount;
	
}
function counter()
{
	return 1;
	/*	
$this->loadModel('Counter');
$this->autoRender = false;
$data = $this->Counter->find('all', array("conditions"=>array('Counter.counterid'=>'1')));
if($data)
{
return 1;
}
*/		
}
}

	
	
	


/* end of function */
/*
 * @function name	: Get Enter form Procedure prize
 * @created by		: mahavir singh
 * @created on		: 05-oct-2015
 * @description		: NA
*/		

?>
