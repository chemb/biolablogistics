<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
	Router::connect('/join-our-faculty', array('controller' => 'pages', 'action' => 'join'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/admin', array('controller' => 'admins', 'action' => 'login'));
	Router::connect('/admin/dashboard', array('controller' => 'admins', 'action' => 'dashboard'));
	Router::connect('/admin/forgotpassword', array('controller' => 'admins', 'action' => 'forgotpassword'));
	Router::connect('/admin/changepassword', array('controller' => 'admins', 'action' => 'changepassword'));
	Router::connect('/admin/confirmation/*', array('controller' => 'admins', 'action' => 'confirmation'));
	Router::connect('/admin/configurations/*', array('controller' => 'admins', 'action' => 'configurations'));
	Router::connect('/admin/editprofile', array('controller' => 'admins', 'action' => 'editprofile'));
	Router::connect('/admin/newsletter', array('controller' => 'admins', 'action' => 'newsletter'));
	Router::connect('/admin/logout', array('controller' => 'admins', 'action' => 'logout'));
	Router::connect('/admin/listdriver', array('controller' => 'admins', 'action' => 'listdriver'));
	Router::connect('/admin/assigndriver', array('controller' => 'admins', 'action' => 'assigndriver'));
	Router::connect('/admin/prepay', array('controller' => 'admins', 'action' => 'prepay'));
	Router::connect('/admin/assignorder', array('controller' => 'admins', 'action' => 'assignorder'));
	
	
	Router::connect('/login/*', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/signin', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout/*', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/signup', array('controller' => 'users', 'action' => 'signup'));
	Router::connect('/dashboard', array('controller' => 'users', 'action' => 'dashboard'));
	Router::connect('/forgotpassword', array('controller' => 'users', 'action' => 'forgotpassword'));
	Router::connect('/changepassword', array('controller' => 'users', 'action' => 'changepassword'));
	Router::connect('/profile/*', array('controller' => 'users', 'action' => 'viewprofile'));
	Router::connect('/requestToAddUser/*', array('controller' => 'users', 'action' => 'requesteduser'));
	Router::connect('/viewUserRequest/*', array('controller' => 'users', 'action' => 'admin_viewUserRequest'));
	
	
	Router::connect('/editprofile/*', array('controller' => 'userdetails', 'action' => 'edit_profile'));
	Router::connect('/profilepic/*', array('controller' => 'userdetails', 'action' => 'edit_profile_photo'));
	Router::connect('/account/*', array('controller' => 'userdetails', 'action' => 'edit_profile_account'));
	Router::connect('/deleteaccount/*', array('controller' => 'userdetails', 'action' => 'edit_profile_dangerzone'));
		
	Router::connect('/deleteprofile/*', array('controller' => 'users', 'action' => 'deleteprofile'));
	Router::connect('/sendmessage/*', array('controller' => 'messages', 'action' => 'send'));
	Router::connect('/compose/*', array('controller' => 'messages', 'action' => 'composemessage'));
	Router::connect('/inbox/*', array('controller' => 'messages', 'action' => 'inbox'));
	Router::connect('/sent-message/*', array('controller' => 'messages', 'action' => 'sentmessage'));
	Router::connect('/trash/*', array('controller' => 'messages', 'action' => 'trashmessage'));
	Router::connect('/movetrash/*', array('controller' => 'messages', 'action' => 'movetrash'));
	Router::connect('/removemessage/*', array('controller' => 'messages', 'action' => 'removemessage'));
	Router::connect('/message/*', array('controller' => 'messages', 'action' => 'viewmessages'));
	Router::connect('/ipnhandler/*', array('controller' => 'courses', 'action' => 'ipnhandler'));
	Router::connect('/ipnhandler', array('controller' => 'courses', 'action' => 'ipnhandler'));
	Router::connect('/ipnhandler.php', array('controller' => 'courses', 'action' => 'ipnhandler'));
	//Router::connect('/mywishlist', array('controller' => 'courses', 'action' => 'relatedcourses','w'));
	//Router::connect('/wishlist/*', array('controller' => 'courses', 'action' => 'userrelatedcourses'));
	Router::connect('/st/*', array('controller' => 'pages', 'action' => 'staticpages'));
	Router::connect('/site-map', array('controller' => 'pages', 'action' => 'sitemap'));
	Router::connect('/contact-us', array('controller' => 'pages', 'action' => 'contactus'));
	Router::connect('/support', array('controller' => 'pages', 'action' => 'support'));
	Router::connect('/add', array('controller' => 'users', 'action' => 'requesteduser'));
		
	Router::connect('/doctors/*', array('controller' => 'users', 'action' => 'search','1'));
	Router::connect('/labs/*', array('controller' => 'users', 'action' => 'search','2'));
	Router::connect('/members/*', array('controller' => 'users', 'action' => 'search','3'));
	
	Router::connect('/add-lab', array('controller' => 'users', 'action' => 'requesteduser','2'));
	Router::connect('/favourite-labs/*', array('controller' => 'users', 'action' => 'favourite_lab'));
	
	Router::connect('/add-doctor', array('controller' => 'users', 'action' => 'requesteduser','1'));
	
	Router::connect('/signup-as-lab', array('controller' => 'users', 'action' => 'signup','2'));
	Router::connect('/signup-as-doctor', array('controller' => 'users', 'action' => 'signup','1'));
   //Router::connect('/signup-as-driver', array('controller' => 'users', 'action' => 'signup','3'));
	Router::connect('/add-members', array('controller' => 'users', 'action' => 'requesteduser','3'));
	
	Router::connect('/send-email', array('controller' => 'users', 'action' => 'send_emailforapproval'));
	Router::connect('/admin/approval_request', array('controller' => 'users', 'action' => 'index','request','admin'=>true));
        Router::connect('/admin/upload', array('controller' => 'users', 'action' => 'upload', 'admin'=>true));
	
	Router::connect('/place-order', array('controller' => 'orders', 'action' => 'order'));
	
	Router::connect('/new-order/*', array('controller' => 'orders', 'action' => 'new_order'));
	
	Router::connect('/reorder/*', array('controller' => 'orders', 'action' => 'reorder'));
	Router::connect('/preview-orders', array('controller' => 'orders', 'action' => 'orderForDoctor'));	
	Router::connect('/archive-orders', array('controller' => 'orders', 'action' => 'orderForDoctor',1));	
	Router::connect('/view-orders', array('controller' => 'orders', 'action' => 'accept_order'));
	Router::connect('/archive-orders-labs', array('controller' => 'orders', 'action' => 'accept_order',1));
	Router::connect('/track-order', array('controller' => 'orders', 'action' => 'track_order'));	

	Router::connect('/preview-orders-driver', array('controller' => 'orders', 'action' => 'driverorders')); 
	Router::connect('/preview-orders-staff', array('controller' => 'orders', 'action' => 'orderForStaff')); 
	Router::connect('/view-order-staff/*', array('controller' => 'orders', 'action' => 'viewOrderStaff')); 
	
	Router::connect('/view-orders-lab/*', array('controller' => 'orders', 'action' => 'viewOrderForLab'));
	Router::connect('/pending-payment', array('controller' => 'orders', 'action' => 'payment_history'));
	Router::connect('/archive-payments', array('controller' => 'orders', 'action' => 'payment_history',1));
	Router::connect('/make-payment', array('controller' => 'orders', 'action' => 'payment'));
	Router::connect('/make-payment-paypal/*', array('controller' => 'orders', 'action' => 'paywithPaypal'));
	Router::connect('/Reviews/*', array('controller' => 'users', 'action' => 'show_reviews'));
	Router::connect('/Seldoctor/*', array('controller' => 'userdetails', 'action' => 'seldoctor'));
	Router::connect('/Changecard/*', array('controller' => 'users', 'action' => 'changecarddetail'));
	
	
	
	
	
	
	
/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
