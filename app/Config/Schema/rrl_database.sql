-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2015 at 03:16 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rrl`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `passwordstatus` tinyint(1) NOT NULL,
  `remembertoken` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `status`, `passwordstatus`, `remembertoken`, `created`, `modified`) VALUES
(1, 'shivamsharma@zapbuild.com', '9a2ce73bde64af02061df81ec8b6c0cb', 1, 0, '', NULL, '2013-08-05 14:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `backupdbs`
--

CREATE TABLE IF NOT EXISTS `backupdbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `breadcrumbs`
--

CREATE TABLE IF NOT EXISTS `breadcrumbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `keyval` varchar(255) DEFAULT NULL,
  `keycontroller` varchar(255) DEFAULT NULL,
  `keyaction` varchar(255) DEFAULT NULL,
  `keylink` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cmsemails`
--

CREATE TABLE IF NOT EXISTS `cmsemails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailfrom` varchar(255) DEFAULT NULL,
  `mailsubject` varchar(255) DEFAULT NULL,
  `mailcontent` longtext,
  `status` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `cmspages`
--

CREATE TABLE IF NOT EXISTS `cmspages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` longtext NOT NULL,
  `metatitle` varchar(255) DEFAULT NULL,
  `seourl` varchar(255) DEFAULT NULL,
  `metadesc` text,
  `metakeyword` text,
  `status` tinyint(1) NOT NULL,
  `showinfooter` tinyint(1) NOT NULL,
  `showinleft` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) DEFAULT NULL,
  `default_header` varchar(100) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `reciever_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text,
  `messagestatus` enum('Unread','Read','Trash') NOT NULL DEFAULT 'Unread',
  `userdelstatus` enum('View','Delete') NOT NULL DEFAULT 'View',
  `recvdelstatus` enum('View','Delete') NOT NULL DEFAULT 'View',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reciever_id` (`reciever_id`),
  KEY `messagestatus` (`messagestatus`),
  KEY `recvdelstatus` (`recvdelstatus`),
  KEY `userdelstatus` (`userdelstatus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `paymentref` varchar(255) DEFAULT NULL,
  `paymentnote` varchar(255) NOT NULL,
  `invoice` varchar(255) NOT NULL,
  `paymentstatus` tinyint(1) DEFAULT NULL,
  `payment` float DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE IF NOT EXISTS `userdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `paypalaccount` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `about` text NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `biography` text,
  `notification` varchar(255) DEFAULT NULL,
  `privacy` varchar(255) DEFAULT NULL,
  `webLink` varchar(255) DEFAULT NULL,
  `fbLink` varchar(255) DEFAULT NULL,
  `twitterLink` varchar(255) DEFAULT NULL,
  `gplusLink` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `state_id` (`state_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remembertoken` varchar(255) NOT NULL,
  `status` enum('-1','0','1') NOT NULL COMMENT '-1 is for Under Review, 0 is for Inactive, 1 is for active',
  `user_type` enum('1','2','3') NOT NULL COMMENT '1 is for Doctors, 2 is for Labs, 3 is for Staff Members',
  `passwordstatus` varchar(255) NOT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;





--
-- Query to insert data into country table
-- sukhwinder
-- 5th FEb 2015

INSERT INTO `rrl`.`countries` (`id`, `name`, `code`, `status`, `created`, `modified`) VALUES (NULL, 'United States', 'US', '1', '2015-02-05 00:00:00', '2015-02-05 00:00:00');

--
-- Query to insert data into state table
-- sukhwinder
-- 5th FEb 2015


INSERT INTO `states` (`id`, `country_id`, `name`, `code`, `status`, `created`, `modified`) VALUES
(1, 1, 'Alaska', 'AL', 1, '2014-12-22 00:00:00', '2014-12-22 00:00:00'),
(2, 1, 'Albama', 'ALB', 1, '2014-12-22 00:00:00', '2014-12-22 00:00:00');





--
-- Query to create new table addusers.
-- Nishi Kant Sharma
-- 16th FEb 2015


CREATE TABLE IF NOT EXISTS `addusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




--
-- Query to add date to cmsemails table.
-- Nishi Kant Sharma
-- 17th FEb 2015

INSERT INTO `cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES
(1, 'noreply@royalroad.com', 'Registration  Request', '<p>\n	Dear <strong>{USER}</strong>,</p>\n<p>\n	Your request to register with Royal Road has been sent to Admin for review, We will get back to you soon.</p>\n\n', 1, '2013-04-29 10:54:50', '2013-11-28 07:44:46'),
(2, 'noreply@royalroad.com', 'Forgot Password', '<p>\n	Dear <strong>{USER}</strong>,</p>\n<p>\n	<br />\n	It happens to the best of us. We all forget our password sometimes.</p>\n<p>\n	So here&#39;s a new password for you: {PASSWORD}</p>\n<p>\n	You can use this password from now onwards.</p>\n<p>\n	If you didn&#39;t request this, please ignore this email.</p>\n', 1, '2013-05-06 11:31:14', '2013-11-28 07:41:03'),
(3, 'noreply@rrl.com', 'Activate Account', '<p> Dear <strong>{USER}</strong>,\n\n</p> <p> Your account with Royal Road has been activated by Admin, You can now login to your account.</p>', 1, '2015-02-02 00:00:00', '2015-02-02 00:00:00'),
(4, 'noreply@royalroad.com', 'Registration  Request From User', '<p>\n	Dear <strong>{USER}</strong>,</p>\n<p>\n	Registration request from user to get approved.</p>', 1, '2015-02-02 00:00:00', '2015-02-03 00:00:00'),
(5, 'noreply@royalroad.com', 'Registration For Staff Member', '<p>\n	Dear <strong>{USER}</strong>,</p>\n<p>\n	Your account has been successfully created. Now you can login to Royal Road by using following credentials:\n<br/>\n<br/>\n<br/>\n        Username : <strong>{USERNAME}</strong><br/>\n        Password : <strong>{PASSWORD}</strong>\n</p>\n', 1, '2015-02-03 00:00:00', '2015-02-03 00:00:00'),
(6, 'noreply@royalroad.com', 'Reject Account', '<p> Dear <strong>{USER}</strong>,\n\n</p> <p> Your account with Royal Road has been rejected by Admin, Sorry you can not login to your account.</p>\n', 1, '2015-02-10 00:00:00', '2015-02-10 00:00:00'),
(7, 'noreply@royalroad.com', 'Add User Request', '<p>\r\n	Dear <strong>Admin</strong>,</p>\r\n<p>{SENDER} wants to add the user with following credentials:\r\n<br/>\r\n<br/>\r\n<br/>\r\n        <strong>Username :</strong> {USERNAME}<br/>\r\n        <strong>Email :</strong> {USEREMAIL}<br/>\r\n        <strong>User type : </strong> {USERTYPE}<br/>\r\n        <strong>Phone : </strong>{PHONE}<br/>\r\n        <strong>Address : </strong>{ADDRESS}<br/>\r\n        <strong>Description : </strong>{DES}<br/>\r\n</p>\r\n', 1, '2015-02-16 00:00:00', '2015-02-16 00:00:00');






--
-- Query to create user_ratings to save user rating data.
-- sukhwinder
-- 17th FEb 2015


CREATE TABLE IF NOT EXISTS `user_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rater_id` int(11) NOT NULL,
  `rating` int(6) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Query to create change the datatype of adduser table.
-- sukhwinder
-- 18th FEb 2015


ALTER TABLE `addusers` CHANGE `phone` `phone` VARCHAR( 20 ) NOT NULL ;

--
-- Query to add company_name column in users table.
-- Nishi Kant Sharma
-- 19th FEb 2015

ALTER TABLE `users` ADD `company_name` VARCHAR( 200 ) NOT NULL AFTER `password` ;

--
-- Query to update the cmsemail table for reject or accept user profile.
-- Nishi Kant Sharma
-- 19th FEb 2015

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{USER}</strong>, </p> <p> Your profile with Royal Road has been rejected by Admin, Sorry you can not login to your account.</p> ' WHERE `cmsemails`.`id` =6;

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{USER}</strong>, </p> <p> Your profile with Royal Road has been activated by Admin, You can now access your account.</p>' WHERE `cmsemails`.`id` =3;

--
-- Query to insert new cmsemail data for approve profile request.
-- Nishi Kant Sharma
-- 19th FEb 2015

INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('8', 'noreply@royalroad.com', 'Request to approve profile', '<p>Dear <strong>Admin</strong>,</p>
<p>
	{NAME} wants to approve the profile.</p>
<p>
	Please {LINK} to view user profile.</p>

', '1', '2015-02-19 00:00:00', '2015-02-19 00:00:00');

--
-- Query to update new cmsemail data for approve profile request.
-- Nishi Kant Sharma
-- 19th FEb 2015

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p>Dear <strong>Admin</strong>,</p> <p> {NAME} wants to approve the profile.</p> <p> Please {LINK} to view the complete profile.</p> ' WHERE `cmsemails`.`id` =8;

--
-- Query to update user table.
-- sukhwinder
-- 19th FEb 2015

ALTER TABLE `users` ADD `profile_status` TINYINT( 1 ) NOT NULL AFTER `status` ;


--
-- Query to insert data into cmspages table.
-- sukhwinder
-- 20th FEb 2015

INSERT INTO `cmspages` (`id`, `name`, `content`, `metatitle`, `seourl`, `metadesc`, `metakeyword`, `status`, `showinfooter`, `showinleft`, `created`, `modified`) VALUES
(1, 'About Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'About Us', 'about-us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 0, 0, '2015-02-20 11:41:52', '2015-02-20 11:41:52'),
(2, 'Our Team', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Our Team', 'our-team', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 0, 0, '2015-02-20 11:55:38', '2015-02-20 11:55:38');



--
-- Query to create new table doctor_lab.
-- Nishi Kant Sharma
-- 3rd march 2015


CREATE TABLE IF NOT EXISTS `doctor_labs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- added by shivam for order placing
--
truncate table orders;
drop table orders;


-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_id` int(11) DEFAULT NULL,
  `payment` float DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_delivery_charges`
--

CREATE TABLE IF NOT EXISTS `order_delivery_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `staff_member_id` int(11) DEFAULT NULL,
  `pickup_place` varchar(200) DEFAULT NULL,
  `delivery_place` varchar(200) DEFAULT NULL,
  `deliver_charge` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `details` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_requests`
--

CREATE TABLE IF NOT EXISTS `order_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `payment` float DEFAULT NULL,
  `payment_reference` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `userdetails` ADD `address` VARCHAR( 250 ) NOT NULL AFTER `phone` ;

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p>Dear <strong>{USER}</strong>,</p> <p> Your registration with Royal Road Logistics has been successful.</p> <p> Please {LINK} to activate your account.If you face any problem in clicking the link just copy below link and paste it in your browser to activate your account:</p> <p>{LINKURL}</p>',`status` = '1' WHERE `cmsemails`.`id` =1;

UPDATE `rrl`.`cmsemails` SET `mailsubject` = 'Registration Confirmation ' WHERE `cmsemails`.`id` =1;

ALTER TABLE `users` CHANGE `status` `status` TINYINT( 2 ) NOT NULL COMMENT '-1 is for Under Review, 0 is for Inactive, 1 is for active';

ALTER TABLE `order_details` ADD `order_id` INT NOT NULL AFTER `id` ;

-- --------------------------------------------------------

--
-- Created by Nishi Kant Sharma for order email
--

INSERT INTO `rrl`.`cmsemails` (
`id` ,
`mailfrom` ,
`mailsubject` ,
`mailcontent` ,
`status` ,
`created` ,
`modified`
)
VALUES (
'9', 'noreply@royalroad.com', 'New Order Request', '<p> Dear <strong>{USER}</strong>,</p> <p> Doctor {DOCNAME} has given the order with following details: <br/> <br/> <br/> Order id : <strong>{ORDERID}</strong><br/> Order Description : <strong>{ORDERDESC}</strong> </p> ', '1', '2015-03-04 00:00:00', '2015-03-04 00:00:00'
);
-- --------------------------------------------------------

--
-- Created by Nishi Kant Sharma for accept/reject order email
--

INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('10', 'noreply@royalroad.com', 'Order Accepted', '<p> Dear <strong>{USER}</strong>,</p> <p> Lab {LABNAME} has accepted the order with following details: <br/> <br/> <br/>
 Order id : <strong>{ORDERID}</strong><br/> Order Description : <strong>{ORDERDESC}</strong> </p> ', '1', '2015-03-04 00:00:00', '2015-03-04 00:00:00'), ('11', 'noreply@royalroad.com', 'Order Rejected', '<p> Dear <strong>{USER}</strong>,</p> <p> Lab {LABNAME} has rejected the order with following details: <br/> <br/> <br/>
 Order id : <strong>{ORDERID}</strong><br/> Order Description : <strong>{ORDERDESC}</strong> </p> ', '1', '2015-03-04 00:00:00', '2015-03-04 00:00:00');


-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma to insert record in statuses table.


REATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `status_type_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `title`, `status_type_id`, `status`, `created`, `modified`) VALUES
(1, 'Waiting', 1, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(2, 'Accept', 2, 0, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(3, 'Reject', 2, 0, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(4, 'In Progress', 2, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(5, 'Completed, waiting for payment', 2, 1, NULL, NULL),
(6, 'Paid', 1, 0, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(7, 'Ready to Dispatch', 2, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(8, 'Dispatched', 3, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(9, 'Delivered', 3, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(10, 'Confirmed', 1, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(11, 'Reorder', 1, 1, '2015-03-04 00:00:00', '2015-03-04 00:00:00'),
(12, 'cancelled ', 1, 1, '2015-03-05 00:00:00', '2015-03-05 00:00:00');

-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma to insert record in cmsemail table.



INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('12', 'noreply@royalroad.com', 'Order ready for dispatched', '<p> Dear <strong>Admin</strong>,</p> <p> Lab {LABNAME} has completed the order with following details: <br/> <br/> <br/>
 Order id : <strong>{ORDERID}</strong><br/> Order Description : <strong>{ORDERDESC}</strong> </p> ', '1', '2015-03-10 00:00:00', '2015-03-10 00:00:00');
 
-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma to update record in cmsemail table.

 
UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>Admin</strong>,</p> <p> Lab {LABNAME} has completed the order with following details: <br/> <br/> Order id : <strong>{ORDERID}</strong> <br/> Order Description : <strong>{ORDERDESC}</strong> </p> <br/> <br/> <br/> <strong>Lab Details:</strong><br/><br/> Order id : <strong>{LABNAME}</strong><br/> Order id : <strong>{LABEMAIL}</strong><br/> Order id : <strong>{LABPHONE}</strong><br/> Order id : <strong>{LABADDRESS}</strong>' WHERE `cmsemails`.`id` =12;
-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma to update record in cmsemail table.
UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>Admin</strong>,</p> <p> Lab {LABNAME} has completed the order with following details: <br/> <br/> Order id : <strong>{ORDERID}</strong> <br/> Order Description : <strong>{ORDERDESC}</strong> </p> <strong>Lab Details:</strong><br/><br/> Lab name : <strong>{LABNAME}</strong><br/> Lab email : <strong>{LABEMAIL}</strong><br/> Lab phone : <strong>{LABPHONE}</strong><br/> Lab address : <strong>{LABADDRESS}</strong>' WHERE `cmsemails`.`id` =12;

-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma for order payment.

CREATE TABLE IF NOT EXISTS `order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `payer` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `payment` varchar(100) NOT NULL,
  `payment_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------
--
-- Created by Nishi Kant Sharma for order attachment table.

ALTER TABLE `order_payments` ADD `created` DATETIME NOT NULL AFTER `payment_status` ,
ADD `modified` DATETIME NOT NULL AFTER `created` ;

CREATE TABLE IF NOT EXISTS `order_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------
--
-- Upadate new order email message for cmsemail table.

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{USER}</strong>,</p> <p> Doctor {DOCNAME} has given the order with following details: <br/> <br/> <br/> Order id : <strong>{ORDERID}</strong><br/> For Order Detail , Please {LINK} </p> ' WHERE `cmsemails`.`id` =9;
UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{USER}</strong>,</p> <p> Lab {LABNAME} has accepted the order with following details: <br/> <br/> <br/> Order id : <strong>{ORDERID}</strong><br/> For Order Detail, Please {LINKURL} </p> ' WHERE `cmsemails`.`id` =10;
UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>Admin</strong>,</p> <p> Lab {LABNAME} has completed the order with following details: <br/> <br/> Order id : <strong>{ORDERID}</strong> <br/> For Order Detail , Please {LINKURL} </p> <strong>Lab Details:</strong><br/><br/> Lab name : <strong>{LABNAME}</strong><br/> Lab email : <strong>{LABEMAIL}</strong><br/> Lab phone : <strong>{LABPHONE}</strong><br/> Lab address : <strong>{LABADDRESS}</strong>' WHERE `cmsemails`.`id` =12;
UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{USER}</strong>,</p> <p> Lab {LABNAME} has rejected the order with following details: <br/> <br/> <br/> Order id : <strong>{ORDERID}</strong><br/> For Order Detail, Please {LINKURL} </p> ' WHERE `cmsemails`.`id` =11;

-- --------------------------------------------------------
--
-- Upadate userdetails table to add zipcode, latitude and longitude.

ALTER TABLE `userdetails` ADD `zipcode` int( 10 ) NOT NULL AFTER `phone` ;
ALTER TABLE `userdetails` ADD `latitude` float NOT NULL AFTER `zipcode` ;
ALTER TABLE `userdetails` ADD `longitude` float NOT NULL AFTER `latitude` ;

-- --------------------------------------------------------
--
-- Add new entry for states table.

INSERT INTO `rrl`.`statuses` (`id`, `title`, `status_type_id`, `status`, `created`, `modified`) VALUES (NULL, 'Completed', '1', '1', '2015-03-17 00:00:00', '2015-03-17 00:00:00');

-- --------------------------------------------------------
--
-- add content for the email sent to the admin on the order completed for cmsemails table.


INSERT INTO `rrl`.`cmsemails` (
`id` ,
`mailfrom` ,
`mailsubject` ,
`mailcontent` ,
`status` ,
`created` ,
`modified`
)
VALUES (
'13', 'noreply@royalroad.com', 'Order Payment Request', '<p> Dear <strong>Admin</strong>,</p> <p> Doctor {DOCNAME} has been marked the order completed and you can pay the lab ${PAYMENT} for the order with following details: <br/> <br/> Order id : <strong>{ORDERID}</strong> <br/> For Order Detail , Please {LINKURL} </p> <strong>Lab Details:</strong><br/><br/> Lab name : <strong>{DOCNAME}</strong><br/> Lab email : <strong>{DOCEMAIL}</strong><br/> Lab phone : <strong>{DOCPHONE}</strong><br/> Lab address : <strong>{DOCADDRESS}</strong>', '1', '2015-03-18 00:00:00', '2015-03-18 00:00:00'
);

-- --------------------------------------------------------
--
-- add content for the email sent to the doctor for payment reminder.
INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('14', 'noreply@royalroad.com', 'Order Payment Reminder', '<p> Dear <strong>{DOCNAME}</strong>,
</p> <p> 
You have completed the order with reference id {ORDERID} . <br/>
Please pay <strong>${TOTALPAYMENT}</strong> for the order to the RRL as soon as possible.
</p>', '1', '2015-03-18 00:00:00', '2015-03-18 00:00:00');

-- --------------------------------------------------------
--
-- update cmsemail for approval request.

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p>Dear <strong>Admin</strong>,</p> <p>User {NAME} with email {EMAIL} wants to approve the profile.</p> <p> Please signup to admin panel to approve the user profile.</p> ' WHERE `cmsemails`.`id` =8;

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p>Dear <strong>Admin</strong>,</p> <p>User <strong>{NAME}</strong> with email <strong>{EMAIL} </strong>wants to approve the profile.</p> <p> Please signup to admin panel to approve the user profile.</p> ' WHERE `cmsemails`.`id` =8;

-- --------------------------------------------------------
--
-- insert into cmsemail for reorder.

INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('15', 'noreply@royalroad.com', 'Reorder Request', '<p> Dear <strong>{USER}</strong>,
</p> <p> 
Doctor <strong>{DOCNAME}</strong> has been reorder the product with Order reference id <strong>{PREID}</strong> and New order has been placed with reference id <strong>{NEWID}</strong>
</p>', '1', '2015-03-23 00:00:00', '2015-03-23 00:00:00');

INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('16', 'noreply@royalroad.com', 'Request for order changes', '<p> Dear <strong>Admin</strong>, </p> <br/>
<p> Lab {LABNAME} has given the following request to change in the Order with reference id {ORDERID} </p><br/>
<p>{CHANGES}</p>', '1', '2015-04-13 00:00:00', '2015-04-13 00:00:00');

UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>Admin</strong>, </p> <p> Lab {LABNAME} has given the following request to change in the Order with reference id {ORDERID} </p> <p>{CHANGES}</p>' WHERE `cmsemails`.`id` =16;


UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{DOCNAME}</strong>, </p> <p> Lab {LABNAME} has given the following request to change in the Order with reference id {ORDERID} </p> <p>{CHANGES}</p>' WHERE `cmsemails`.`id` =16;



-- --------------------------------------------------------
--
-- added by nishi on 28th april 2015.


ALTER TABLE `userdetails` ADD `serviceTags` VARCHAR( 255 ) NOT NULL AFTER `webLink` ;
ALTER TABLE `order_details` ADD `patient_name` VARCHAR( 255 ) NOT NULL AFTER `order_id` ;




CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `status`, `created`, `modified`) VALUES
(1, 'abc', 1, NULL, NULL),
(2, 'xyz', 1, NULL, NULL);


INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('17', 'noreply@royalroad.com', 'Registration with Royal Road Logistics', '<p> Dear <strong>{EMAIL}</strong>, </p>
<p> Your account has been created on Royal Road Logistics with following credentials: </p></br></br>
<p>Username = {USERNAME}</p>
<p>Password = {PASSWORD}</p>', '1', '2015-04-30 00:00:00', '2015-04-30 00:00:00');



UPDATE `rrl`.`cmsemails` SET `mailcontent` = '<p> Dear <strong>{EMAIL}</strong>, </p> <p> Your account has been created on Royal Road Logistics with following credentials: </p> <p>Username = {USERNAME}</p> <p>Password = {PASSWORD}</p> <p> For Login , Please {LINKURL}</p>' WHERE `cmsemails`.`id` =17;



ALTER TABLE `user_ratings` ADD `review` TEXT NOT NULL AFTER `rating` ;
ALTER TABLE `user_ratings` ADD `order_id` INT( 11 ) NOT NULL AFTER `rater_id` ;
ALTER TABLE `user_ratings` ADD `pricerating` INT( 11 ) NOT NULL AFTER `rating` ;


INSERT INTO `rrl`.`cmsemails` (`id`, `mailfrom`, `mailsubject`, `mailcontent`, `status`, `created`, `modified`) VALUES ('18', 'noreply@royalroad.com', 'order delivered', '<p> Dear <strong>Admin</strong>,</p> <p> Staff member {STAFFMEMBER} has delivered the order with following details: <br/> <br/> Order id : <strong>{ORDERID}</strong> <br/>  For Order Detail , Please {LINKURL} </p> ', '1', '2015-05-05 00:00:00', '2015-05-05 00:00:00');
