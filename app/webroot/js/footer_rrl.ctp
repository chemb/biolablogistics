<!-- Footer Strart -->
<footer>
    <p class="logo-footer">
     <a href="<?php echo $this->Html->url("/"); ?>"><?php echo $this->Html->image("logo-2.png", array("alt" => "Biolab", "style" => "margin:0px 20px 0px 0px;height:53px;width:223px;")); ?></a>
    
    </p>
    <p class="copyright"> &copy; biolab | 1875 Connecticut Ave. NW, 10th Floor, Washington, DC 20009 | 202.400.5170 | <a href="mailto:joinus@biolablogistics.com"> joinus@biolablogistics.com</a> </p>
</footer>

<!-- Footer End -->

<!-- jQuery library (served from Google) -->
<?php echo $this->Html->script(array('jquery', 'bootstrap.min', 'jquery.bxslider.min', 'jquery.validate', 'functionality', 'bootstrap-select', 'bootstrap-filestyle','bootstrap-datepicker.min')); 
echo $this->Html->css('bootstrap-datepicker.min');
?>
<?php if ($this->params['action'] == 'viewprofile') { ?>
    <?php echo $this->Html->script(array('jquery.raty.min')); ?>
    <script>
        $(document).ready(function() {
            $.fn.raty.defaults.path = '<?php echo SITE_LINK; ?>js/rating';
            $('.response-time').raty({score: 0});
        });
    </script>
<?php } ?>
<?php if ($this->params['action'] == 'edit_profile') { ?>
    <script>
		//alert("here");
		$('#UserdetailStateId,#UserdetailCountryId').selectpicker('refresh');
    </script>
<?php } ?>
<?php if ($this->params['action'] == 'signup') { ?>
    <script>
		//alert("here");
		$('#UserdetailStateId').selectpicker('refresh');
    </script>
<?php } ?>
<?php if ($this->params['action'] == 'order' || $this->params['action'] == 'viewOrder'|| $this->params['action'] == 'new_order') { 
	//echo $this->Html->script("bootstrap-datepicker.min");
	
	 ?>
    <script>
		$('#OrderDetail1DateofRx').datepicker({
			format: "mm-dd-yyyy",
			startDate: "current"
		});
		$('#OrderDetail2RequestedReturnDate').datepicker({
			format: "mm-dd-yyyy",
			startDate: "current"
		});
		
		
		
		
    </script>
<?php } ?>
<script type="text/javascript">
	$('#datepicker').datepicker({
			format: "mm-dd-yyyy",
			startDate: "current"
		});
	
  </script>
<script>

    $('.bxslider').bxSlider({
        auto: true
    });
    $('.user-box').click(function(e) {
        $('.user-dropdown').slideToggle('1000');
        
        e.stopPropagation();

    });
   
    $('body').on('click',function(){

   $('.user-dropdown').slideUp('1000');
   

});
    
    $(":file").filestyle({buttonText: "Find file"});
    $(":file").filestyle('clear');
    //$(":file").filestyle('destroy');

</script>

<?php echo $this->Html->script("jquery.form.js"); ?>
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-box margin-none">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Any Query</h2>

                </div>
                <?php //echo $this->Session->flash(); ?>
                <label id="contact_cont" class="contact_form_err hide" ></label>
                <?php echo $this->Form->create('Page', array('class' => "contact_us_frm", "id" => "ContactForm")); ?>
                <div class="modal-body">
                    <?php
                    echo $this->Form->input('first_name', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'First Name'));
                    echo $this->Form->input('email', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Email'));
                    echo $this->Form->input('phone', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Phone'));
                    ?>	
                    <span class="left_bx cntct_select"> 
                        <?php echo $this->Form->input('type', array('class' => "selectpicker", 'label' => false, 'div' => true, "options" => array("" => "Select query type", "General Inquiry" => "General Inquiry", "Partnerships" => "Partnerships", "Press" => "Press", "Business Development" => "Business Development", "Report a Bug" => "Report a Bug"))); ?>
                    </span>		
                    <?php echo $this->Form->input('message', array('rows' => '4', 'cols' => '50', 'label' => false, 'div' => array('class' => 'left_bx cntct_txtarea input'), 'class' => "left", 'placeholder' => 'Message')); ?>
                </div>
                <div class="modal-footer text-center">

                    <div class="cancel-btn-outer">
                        <button type="button" class="cancel-btn" data-dismiss="modal">Close</button> 
                    </div> &nbsp;                  
                    <div class="login-btn-outer">
                        <?php echo $this->Form->submit(__('Submit', true), array('class' => 'login-btn')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- popup for staff member detail -->
<div id="myModalForMember" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-box margin-none">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Enter Delivery Charges</h2>
                </div>
                <?php //echo $this->Session->flash(); ?>
                <label id="contact_cont2" class="contact_form_err hide" ></label>
                <?php echo $this->Form->create('OrderPayment', array('class' => "contact_us_frm", "id" => "MemberDetailForm")); ?>
                <div class="modal-body">
                    <?php
                   // echo $this->Form->input('pickup_place', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Pickup place'));
                   //echo $this->Form->input('delivery_place', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Delivery place'));                    
                    echo $this->Form->input('deliver_charge', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Delivery charge'));     
                    echo $this->Form->hidden('order_id');                     
                    echo $this->Form->hidden('status_val');                  
                    echo $this->Form->hidden('order_status_id');                     
                    echo $this->Form->hidden('selected_val');                 
                                        
                                	
                   ?>	
                </div>
                <div class="modal-footer text-center">

                    <div class="cancel-btn-outer">
                        <button type="button" class="cancel-btn" data-dismiss="modal">Close</button> 
                    </div> &nbsp;                  
                    <div class="login-btn-outer">
                        <?php echo $this->Form->submit(__('Submit', true), array('class' => 'login-btn', 'id'=>'deliveryChargesBtn')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>


<!-- popup for Lab if status slected as Ready to Dispatch -->
<div id="myModalForLab" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-box margin-none">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Enter Order Charges</h2>
                </div>
                <?php //echo $this->Session->flash(); ?>
                <label id="contact_cont3" class="contact_form_err hide" ></label>
                <?php echo $this->Form->create('OrderPayment', array('class' => "contact_us_frm", "id" => "LabDetailForm")); ?>
                <div class="modal-body">
                    <?php
                     if ( isset($order_data['Order']['budget']) && !empty($order_data['Order']['budget']) ) {
						echo $this->Form->input('payment_amount', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Payment Amount',"value"=>$order_data['Order']['budget'],"readonly"=>true));     
					} else {
						echo $this->Form->input('payment_amount', array("maxlength" => "50", 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Payment Amount'));     
					}
                    echo $this->Form->hidden('order_id1');                     
                    echo $this->Form->hidden('status_val1');                  
                    echo $this->Form->hidden('order_status_id1');                     
                    echo $this->Form->hidden('selected_val1');                
                                	
                   ?>	
                </div>
                <div class="modal-footer text-center">

                    <div class="cancel-btn-outer">
                        <button type="button" class="cancel-btn" data-dismiss="modal">Close</button> 
                    </div> &nbsp;                  
                    <div class="login-btn-outer">
                        <?php echo $this->Form->submit(__('Submit', true), array('class' => 'login-btn', 'id'=>'paymentChargesBtn')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>


<!-- popup for Lab if lab needs any change for order -->
<div id="myModalForLabUser" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-box margin-none">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Enter your Changes </h2>
                </div>
                <?php //echo $this->Session->flash(); ?>
                <label id="contact_cont4" class="contact_form_err hide" ></label>
                <?php echo $this->Form->create('LabRequest', array('class' => "contact_us_frm", "id" => "DetailFormForLab")); ?>
                <div class="modal-body">
                <?php                           
                    echo $this->Form->input('query', array('type' => 'textarea', 'class' => "form-control", 'label' => false, 'div' => array('class' => 'left_bx contact_fld input'), 'placeholder' => 'Please enter your changes here.')); 
                     echo $this->Form->hidden('order_id2'); 
                     echo $this->Form->hidden('doc_email'); 
                     echo $this->Form->hidden('doc_name'); 
                ?>	
                </div>
                <div class="modal-footer text-center">

                    <div class="cancel-btn-outer">
                        <button type="button" class="cancel-btn" data-dismiss="modal">Close</button> 
                    </div> &nbsp;                  
                    <div class="login-btn-outer">
                        <?php echo $this->Form->submit(__('Submit', true), array('class' => 'login-btn', 'id'=>'paymentChargesBtn')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>



<!-- Upload csv data  -->


<div id="myModalCSV" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-box margin-none">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Upload Csv file</h2>
                </div>
                <?php //echo $this->Session->flash(); ?>
                <label id="contact_cont4" class="contact_form_err hide" ></label>
                <?php echo $this->Form->create('Uploadcsv',array('url' => '/users/uploadcsv','class' => "contact_us_frm", "id" => "Uploadcsv","enctype" => "multipart/form-data")); ?>
                <div class="modal-body">					
				<?php echo $this->Form->input('file', array("type" => "file", "class" => "TxtFld upload_default", "label" => false, "div" => true, "id" => "CsvFile",'placeholder'=>'Upload csv')); ?>
				</div>
                <div class="modal-footer text-center">

                    <div class="cancel-btn-outer">
                        <button type="button" class="cancel-btn" data-dismiss="modal">Close</button> 
                    </div> &nbsp;                  
                    <div class="login-btn-outer">
                        <?php echo $this->Form->submit(__('Upload Csv', true), array('class' => 'login-btn', 'id'=>'paymentChargesBtn')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

