var stateSelectTmpl = "<option value=''>Select State</option>";
var stateTmpl = "<option class='state_class' value='{KEY}'>{VAL}</option>";
var status_val = order_id = order_status_id = selected_val = '';

var turnarounds = '<div class="col-sm-4  margin-btm form-group"><input type="text" id="UserdetailTurnarounds{key1}Name" maxlength="50" class="form-control" placeholder="Procedure" name="data[Userdetail][turnarounds][{key2}][name]"></div><div class="col-sm-4 form-group"><input type="text" id="UserdetailTurnarounds{key3}Value" maxlength="60" class="form-control-num" placeholder="Timings" name="data[Userdetail][turnarounds][{key4}][value]"></div><div class="col-sm-4 form-group"><input type="text" id="UserdetailTurnarounds{key5}Price" maxlength="60" class="form-control-num" placeholder="Price" name="data[Userdetail][turnarounds][{key6}][price]"></div>';


$(document).ready(function () {
	$.validator.addMethod("letterssonly", function (value, element) {
		return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
	}, "Only alphabet characters are allowed.");
	/* end here */

	/* code to check only letter and allow some special charcters*/
	$.validator.addMethod("letterssconly", function (value, element) {
		return this.optional(element) || /^[a-zA-Z\&\,\.\-\0-9\s]*$/.test(value);
	}, "Please enter valid value.");
	/* end here */
	
    $('#sort_rating').change(function () {		
		if(($('#UserSearchval').val() == '') && ($(this).val() == 'Distance')){			
			$('#sort_distance_msg').show();						
			$('#sort_rating').val('All');
			return false;
		}
		else {
			$('#sort_distance_msg').hide();
		}
		$('#UserSearchForm').submit();
	});
   
	 $('#UserSearchForm').on('submit', function(e){		
		if(($('#UserSearchval').val() == '') && ($('#sort_rating').val() == 'Distance')){
			$('#sort_distance_msg').show();	
			return false;	
		} else {
			$('#sort_distance_msg').hide();
		}		
	});

	$('#sort_rating2').change(function () {
		$('#UserfavFavouriteLabForm').submit();
	});
	

   $(document).on("click", "#Upload_csv", function () {	
	   //$('#Uploadcsv').trigger('reset');
	   $('#myModalCSV').modal('show');
	
   });
   
	$(document).on("click", ".download_file", function () {
		var id = $(this).attr("data-attr").split("_");
		id = id[1];
		var link = SITE_LINK + "orders/downlodfiles/" + id;
		location.href = link;
	});

	$(document).on('click', '.remove_attach', function () {
		var y = confirm('Are you sure you want to remove this file?');
		if (y) {
			var id = $(this).attr("data-attrr").split("_");
			var tmp_name = id[3].split("/");
			var obj = $(this);
			$.ajax({
				type: "POST",
				url: BASE_URL + "orders/remove_attachment/" + id[1] + "/" + id[2] + "/" + tmp_name[4],
				success: function (resp) {
					data = $.parseJSON(resp);
					if (!data.error) {
						obj.parent("div").remove();
						$('#err_msg').html('');
						$('.alert-danger').removeClass('show');
						$('.alert-danger').addClass('hide');
						$('#succ_msg').html(data.message);
						$('.alert-success').addClass('show');
						$('.alert-success').removeClass('hide');
						$('.alert-success').fadeOut(5000, function () {
							$('.alert-success').removeClass('show');
							$('.alert-success').addClass('hide');
						});

					} else if (data.error) {
						$('#succ_msg').html('');
						$('.alert-success').removeClass('show');
						$('.alert-success').addClass('hide');
						$('#err_msg').html(data.message);
						$('.alert-danger').addClass('show');
						$('.alert-danger').removeClass('hide');
						$('.alert-danger').fadeOut(5000, function () {
							$('.alert-danger').removeClass('show');
							$('.alert-danger').addClass('hide');
						});
					}
				}
			});
		} else {
			return false;
		}
	});

	//$("#LabformViewOrderForLabForm :input").attr("disabled", true); /* we ccomment this line */
	$('#LabformStatus').removeAttr('disabled');
	$('#sendemailforDoctor').removeAttr('disabled');

	$(document).on("submit", "#UserSendEmailforapprovalForm", function (e) {
		if (!confirm("Are you sure, you want to send a request to approve your account?")) {
			return false;
		}
	});

	$(document).on("submit", "#UserdetailEditProfileDangerzoneForm", function (e) {
		if (!confirm("Are you sure, you want to delete your account?\n There will be no rollback once you will delete your account.")) {
			return false;
		}
	});

	setTimeout(function () {
		$("#flashMessage").hide();
	}, 9000);
	$('.error-message').css('color', '#ff0000');
	$('.error-message').css('font-size', '13px');

	$("#UserLoginForm").validate({
		rules: {
			'data[User][username]': {
				required: true,
				email: true
			},
			'data[User][password]': {
				required: true,
			}
		},
		messages: {
			'data[User][username]': {
				required: 'Please enter email.',
				email: 'Please enter valid email.'
			},
			'data[User][password]': {
				required: 'Please enter password.',
			}
		}
	});

	/* Created by Nishi Kant Sharma for Page Signup Form validations.   */

	$("#ContactForm").validate({
		rules: {
			'data[Page][first_name]': {
				required: true
			},
			'data[Page][email]': {
				required: true,
				email: true
			},
			'data[Page][phone]': {
				required: true,
				number: true,
				minlength: 10,
				maxlength: 10
			},
			'data[Page][type]': {
				required: true
			},
			'data[Page][message]': {
				required: true
			}
		},
		messages: {
			'data[Page][first_name]': {
				required: 'Please enter first name.',
			},
			'data[Page][email]': {
				required: 'Please enter email.',
				email: 'Please enter correct email.'
			},
			'data[Page][phone]': {
				required: 'Please enter phone number.',
				number: 'Phone number must contain digits only.',
			},
			'data[Page][type]': {
				required: 'Please select query type.'

			},
			'data[Page][message]': {
				required: 'Please enter message.'
			}

		},
		submitHandler: function (f) {
			saveContactusData();
		}
	});

	/* Created by Nishi Kant Sharma for Staff member details Form validations.   */

	$("#MemberDetailForm").validate({
		rules: {
			'data[OrderPayment][deliver_charge]': {
				required: true,
				number: true
			}
		},
		messages: {
			'data[OrderPayment][deliver_charge]': {
				required: 'Please enter delivery charges.',
				number: 'Delivery charges must contain digits only.'
			}

		},
		submitHandler: function (f) {
			saveMemberData();
		}
	});


	/* Created by Nishi Kant Sharma for Lab payment amount validations.   */

	$("#LabDetailForm").validate({
		rules: {
			'data[OrderPayment][payment_amount]': {
				required: true,
				number: true
			}

		},
		messages: {
			'data[OrderPayment][payment_amount]': {
				required: 'Please enter order charges.',
				number: 'Order charges must contain digits only.'
			}

		},
		submitHandler: function (f) {
			saveLabData();
		}
	});
	
	
	$("#DetailFormForLab").validate({
		rules: {
			'data[LabRequest][query]': {
				required: true
			}

		},
		messages: {
			'data[LabRequest][query]': {
				required: 'Please enter your changes.',
			}

		},
		submitHandler: function (f) {
			requestforChange();
		}
	});
	


	/* Created by Nishi Kant Sharma for order form validations.   */

	$("#OrderNewOrderForm").validate({
		rules: {
			'data[OrderDetail][18][details]': {
				required: true
			},
			'data[OrderDetail][19][quantity]' : {
				required: true,
				number:true,
				range:[1,100]
			},
			'data[OrderDetail][20][specification]' :{
				required :true
			}

		},
		messages: {
			'data[OrderDetail][18][details]': {
				required: 'Please enter order details.',
			},
			'data[OrderDetail][19][quantity]' : {
				required: 'Please enter quantity.',
				number: 'Order quantity must contain numbers only.',
				range : 'Please enter correct quantity number.'
			},
			'data[OrderDetail][20][specification]' : {
				required : 'Please enter order specification.'
				
			}
		}
	});
	
	$("#OrderOrderForm").validate({
		rules: {
			'data[OrderDetail][18][details]': {
				required: true
			},
			'data[OrderDetail][20][quantity]' : {
				required: true,
				number:true,
				range:[1,100]
			},
			'data[OrderDetail][19][specification]' :{
				required :true
			}

		},
		messages: {
			'data[OrderDetail][18][details]': {
				required: 'Please enter order details.',
			},
			'data[OrderDetail][20][quantity]' : {
				required: 'Please enter quantity.',
				number: 'Order quantity must contain numbers only.',
				range : 'Please enter correct quantity number.'
			},
			'data[OrderDetail][19][specification]' : {
				required : 'Please enter order specification.'
				
			}
		}
	});


	/* Created by Nishi Kant Sharma for signup validations.   */

	$("#UserSignupForm").validate({
		rules: {
			'data[User][username]': {
				required: true,
				email: true,
				remote: {
					url: BASE_URL + "users/uniqueuser",
					type: "post"
				}
			},
			'data[User][password]': {
				required: true,
				minlength: 6,
				maxlength: 15
			},
			'data[User][confirmpassword]': {
				required: true,
				equalTo: "#UserPassword"
			},
			'data[Userdetail][first_name]': {
				required: true,
				letterssonly: true

			},
			'data[Userdetail][last_name]': {
				required: true,
				letterssonly: true
			},
			'data[Userdetail][address]': {
				required: true,
			},
			'data[Userdetail][city]': {
				required: true,
			},
			'data[Userdetail][state_id]': {
				required: true,
			},
			'data[Userdetail][zipcode]': {
				required: true
			},
			'data[User][company_name]': {
				required: true
			},
			'data[Userdetail][phone]': {
				required: true,
				number: true,
				minlength: 10,
				maxlength: 15
			},'data[Userdetail][serviceTags]': {
				required: true
			}
		},
		messages: {
			'data[User][username]': {
				required: 'Please enter email.',
				email: 'Please enter valid email.',
				remote: 'Email already exists'
			},
			'data[User][password]': {
				required: 'Please enter password.',
				minlength: 'Password should be at-least 6 characters.',
				maxlength: 'Password should be less than 15 characters.'
			},
			'data[User][confirmpassword]': {
				required: 'Please enter confirm password.',
				equalTo: 'Password and confirm password not matched.'
			},
			'data[Userdetail][first_name]': {
				required: 'Please enter name.'
			},
			'data[Userdetail][last_name]': {
				required: 'Please enter last name.',
			},
			'data[User][company_name]': {
				required: 'Please enter company name.'
			},
			'data[Userdetail][address]': {
				required: "Please enter address.",
			},
			'data[Userdetail][city]': {
				required: "Please eneter city.",
			},
			'data[Userdetail][state_id]': {
				required: "Please select state.",
			},
			'data[Userdetail][zipcode]': {
				required: "Please enter zipcode."
			},
			'data[Userdetail][phone]': {
				required: "Please enter phone number.",
				number: "Please enter valid phone number.",
				minlength: "Please enter valid phone number.",
				maxlength: "Please enter valid phone number."
			},'data[Userdetail][serviceTags]': {
				required: "Please select specialization.",
			}
			
		}
	});

	$("#UserForgotpasswordForm").validate({
		rules: {
			'data[User][username]': {
				required: true,
				email: true
			}
		},
		messages: {
			'data[User][username]': {
				required: 'Please enter email.',
				email: 'Please enter valid email.',
			}
		}
	});


	$("#UserEditProfileAccountForm").validate({
		rules: {
			'data[User][currentpassword]': {
				required: true
			},
			'data[User][password]': {
				required: true,
				minlength: 6,
				maxlength: 15
			},
			'data[User][confirmpassword]': {
				required: true,
				equalTo: "#UserPassword"
			}
		},
		messages: {
			'data[User][currentpassword]': {
				required: 'Please enter current password.',
			},
			'data[User][password]': {
				required: 'Please enter new password.',
				minlength: 'Password must be 6 characters long.',
				maxlength: 'Password can not be more than 15 characters long.'
			},
			'data[User][confirmpassword]': {
				required: 'Please re-type new password.',
				equalTo: 'New and confirm password not matched.'
			}
		}
	});


	$("#AdduserRequesteduserForm").validate({
		rules: {
			'data[Adduser][name]': {
				required: true,
				letterssonly: true,
				minlength: 2

			},
			'data[Adduser][email]': {
				required: true,
				validEmail: true,
				remote: {
					url: BASE_URL + "users/uniquemember",
					type: "post"
				}
			},
			'data[Adduser][phone]': {
				required: true,
				number: true,
				minlength: 10,
				maxlength: 10
			},
			'data[Adduser][address]': {
				required: true
			},
			'data[Adduser][description]': {
				required: true
			},
			'data[Adduser][user_type]': {
				required: true
			}
		},
		messages: {
			'data[Adduser][name]': {
				required: 'Please enter name.'
			},
			'data[Adduser][email]': {
				required: 'Please enter email.',
				email: 'Please enter valid email.',
				remote: 'Email already exists'

			},
			'data[Adduser][phone]': {
				required: 'Please enter phone number.',
				number: 'Phone number must contain digits only.',
			},
			'data[Adduser][address]': {
				required: 'Please enter address.'
			},
			'data[Adduser][description]': {
				required: 'Please enter description.'
			},
			'data[Adduser][user_type]': {
				required: 'Please select user type.'
			},
		},
		submitHandler: function ()
		{
			$("#fade-whole").show();
			$("#AdduserRequesteduserForm").submit();

		}
	});

	/*  VALIDATIONS FOR EDIT USER PROFILE FORM  */


	$("#UserdetailEditProfileForm").validate({
		rules: {
			'data[Userdetail][first_name]': {
				required: true,
				letterssonly: true,
				minlength: 2,
				maxlength: 20

			},
			'data[Userdetail][last_name]': {
				required: true,
				letterssonly: true,
				minlength: 2,
				maxlength: 20
			},
			'data[Userdetail][phone]': {
				required: true,
				number: true,
				minlength: 10,
				maxlength: 15
			},'data[Userdetail][serviceTags]': {
				required: true
			},
			'data[Userdetail][designation]': {
				required: true,
				minlength: 2
			},
			'data[Userdetail][address]': {
				required: true,
				minlength: 2,
				maxlength: 200
			},
			'data[Userdetail][country_id]': {
				required: true
			},
			'data[Userdetail][state_id]': {
				required: true
			},
			'data[Userdetail][city]': {
				required: true,
				minlength: 2
			},
			'data[Userdetail][zipcode]': {
				required: true
			},
			'data[User][company_name]': {
				required: true
			},
		},
		messages: {
			'data[Userdetail][first_name]': {
				required: 'Please enter first name.'
			},
			'data[Userdetail][last_name]': {
				required: 'Please enter last name.',
			},
			'data[Userdetail][phone]': {
				required: 'Please enter phone number.',
				number: 'Phone number must contain digits only.',
				minlength: 'Please enter atleast 10 digits.',
				maxlength: 'Please enter only 10 digits.'
			},'data[Userdetail][serviceTags]': {
				required: "Please select a specialization."
			},
			'data[Userdetail][designation]': {
				required: 'Please enter designation.',
			},
			'data[Userdetail][address]': {
				required: 'Please enter address.',
			},
			'data[Userdetail][country_id]': {
				required: 'Please select your country.',
			},
			'data[Userdetail][state_id]': {
				required: 'Please select your state.',
			},
			'data[Userdetail][city]': {
				required: 'Please enter your city.',
			},
			'data[Userdetail][zipcode]': {
				required: 'Please enter your zipcode.'
			},
			'data[User][company_name]': {
				required: 'Please enter company name.'
			},
		}
	});

	$("#UserdetailEditProfilePhotoForm").validate({
		rules: {
			'data[Userdetail][image]': {
				required: true,
				accept: 'jpeg|jpg|png|gif'
			}
		},
		messages: {
			'data[Userdetail][image]': {
				required: "Please select a picture to upload.",
				accept: 'Please select valid file to upload.'
			}
		}
	});
	
	
   $("#Uploadcsv").validate({
		rules: {
			'data[Uploadcsv][file]': {
				required: true,
				accept: 'csv'
			}
		},
		messages: {
			'data[Uploadcsv][file]': {
				required: "Please select a csv file to upload.",
				accept: 'Please select only csv file.'
			}
		}
	});
	



	/* code to open form for save and edit user rating*/

	$('.rating-btn').on('click', function (event) {
		event.preventDefault();
		var id = $(this).attr("id");
		var ratingId = $('#ratingid').val();
		var response_rating = jQuery("#response-time" + id + " > [name='score']").val();
		var dataString = 'id=' + id + '&response_rating=' + response_rating + '&ratingId=' + ratingId;
		$.ajax({
			type: "POST",
			data: dataString,
			url: BASE_URL + "/users/addRating/",
			success: function (data) {
				jQuery('#response-box' + id).html('<div class="response-message" style="color:green;">Rating has been saved.</div>');
				setTimeout(function () {
					location.reload();
				}, 1000);
			},
			error: function (err, req) {
				jQuery('#response-box' + id).html('<div class="response-message" style="color:red;">Please select atleast one star for rating.</div>');
			}
		});
	});

	/* end of code*/

	/* code to open form for user rating*/

	$('.user_rating').on('click', function () {
		var id = $(this).attr("id").split("_");
		$('.addRating').fadeOut();
		$('#add_rating_' + id[1]).fadeIn(5000);
		$('#edit_rating_' + id[1]).fadeIn(5000);
	});

	/* end of code*/


	$('#UserPassword,#UserConfirmpassword').keydown(function (e) {
		if (e.ctrlKey && (e.keyCode == 88 || e.keyCode == 67 || e.keyCode == 86)) {
			return false;
		}
	});

	$('#UserPassword,UserConfirmpassword').on("cut copy paste", function (e) {
		e.preventDefault();
	});

	setTimeout("jQuery('.error-message').hide();", 5000);

	$(document).on('click', '.open_contact_popup', function () {
		$('#ContactForm').trigger('reset');
		$('#contact_cont').removeClass("show");
	}
	); 
	
	$(document).on('click', '#sendemailforDoctor', function () {
		var attr = $('#ord_id').html().split(':');		
		var doc_username = $('#LabformDocEmail').val();	
		var doc_name = $('#LabformDocName').val();	
		$('#LabRequestDocEmail').val(doc_username);
		$('#LabRequestDocName').val(doc_name);
		$('#LabRequestOrderId2').val(attr[1]);		
		$('#myModalForLabUser').modal('show'); 
	}); 
	

	$('.add_lab').click(function () {
		var obj = $(this);
		var lab_id = obj.attr('id').split("_");
		var type = obj.attr('data-attr');
		$.ajax({
			type: "POST",
			url: BASE_URL + "userdetails/save_doc_lab/" + lab_id[1] + "/" + type,
			success: function (resp) {
				data = $.parseJSON(resp);
				if (!data.error) {
					if (type == 'add') {
						obj.attr("data-attr", 'remove');
						obj.html('Remove from Favorite');
						$('#addlab_' + lab_id[1]).attr('title', 'Remove from Favorite');

					} else if (type == 'remove') {
						obj.attr("data-attr", 'add');
						obj.html('Add to Favorite');
						$('#addlab_' + lab_id[1]).attr('title', 'Add to Favorite');

					}
					$('#err_msg').html('');
					$('.alert-danger').removeClass('show');
					$('.alert-danger').addClass('hide');
					$('#succ_msg').html(data.message);
					$('.alert-success').addClass('show');
					$('.alert-success').removeClass('hide');
					$('.alert-success').fadeOut(5000, function () {
						$('.alert-success').removeClass('show');
						$('.alert-success').addClass('hide');
					});
				} else if (data.error) {
					$('#succ_msg').html('');
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');
					$('#err_msg').html(data.message);
					$('.alert-danger').addClass('show');
					$('.alert-danger').removeClass('hide');
					$('.alert-danger').fadeOut(5000, function () {
						$('.alert-danger').removeClass('show');
						$('.alert-danger').addClass('hide');
					});
				}
			},
			error: function (err) {
				$('#err_msg').html('An error occured.Please try later.');
				$('.alert-success').addClass('show');
				$('.alert-danger').show();
				$('.alert-success').removeClass('hide');
				$('.alert-success').fadeOut(50000, function () {
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');
				});
			}
		});
	});
	/*$('#chek_msg').hide();
	 $("#OrderOrderForm").on('submit',function(){
	 var cnt = $(".checkbox-group-lab:checked").length;
	 var cnt1 = $(".checkbox-group-lab").length;
	 if(cnt1 < 1) {
	 $("html, body").animate({ scrollTop: "100px" });
	 return false;
	 } else if(cnt<1) {
	 $(".checkbox-group-lab").focus();
	 $('#chek_msg').show();
	 return false;
	 } else {
	 $('#chek_msg').hide();
	 return true;
	 }
	 });
	 
	 /*$('#deliveryChargesBtn').click(function(){
	 $('#myModalForMember').modal('hide');
	 location.reload();
	 });
	 $('#paymentChargesBtn').click(function(){
	 $('#myModalForLab').modal('hide');
	 location.reload();
	 }); */

	//$("#track_order > li").last().addClass("lastOrder");
	
	$("a.bottom").tooltip();
	
	
	
	$(".ratestar").click(function(e){
		var id = $(this).attr("id").split("_");
		$(".ratestar").attr("src",DEFAULT_LINK+"img/rating-star-1.png");
		$("#rating_val").val(id[1]);
		id = id[1];
		$(".ratestar").each(function(e){
			if(id > 0) {
				$("#rate_"+id).attr("src",DEFAULT_LINK+"img/rating-star.png");
			}
			id--;
		});
		
	});
	
	$("img.rateprice").click(function(e){
		var id = $(this).attr("id").split("_");
		$(".rateprice").attr("src",DEFAULT_LINK+"img/price-black.png");
		$("#pricerating_val").val(id[1]);
		id = id[1];
		$(".rateprice").each(function(e){
			if(id > 0) {
				$("#pricerate_"+id).attr("src",DEFAULT_LINK+"img/price-rated.png");
			}
			id--;
		});
		
	});
	
	$("#userRatingViewprofileForm,#userRatingShowReviewsForm").validate({
		rules: {
			'data[userRating][rating]': {
				required: true
			},
			'data[userRating][review]' : {
				required: true
			},
			'data[userRating][pricerating]': {
				required: true
			}
		},
		messages: {
			'data[userRating][rating]': {
				required: 'Please select atleast one star.'
			},
			'data[userRating][review]' : {
				required: 'Please enter your review.'				
			},
			'data[userRating][pricerating]':{
				required: 'Please enter price rating.'				
			}
		}
	});
	
	
	$(document).on("click",".add_turnarounds",function(e){
		var val = $("#UserdetailTurnCount").val();
		var t = turnarounds;
		t = t.replace('{key1}',val);
		t = t.replace('{key2}',val);
		t = t.replace('{key3}',val);
		t = t.replace('{key4}',val);
		t = t.replace('{key5}',val);
		t = t.replace('{key6}',val);
		$("#UserdetailTurnCount").val(((val*1)+1));
		$("#turnarounds_container").append(t);
		 $(".form-control-num").keypress(function (e) {
   //alert("ok");
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
   
		
	});
	
	$(document).on("click","#rate_it",function(e){
		$("#userRatingId").val('');
		$("#userRatingReview").html('');
		for ( var i = 1; i <= 5 ; i++ ) {
			$("#rate_"+i).attr("src",BASE_URL+"img/rating-star-1.png")
		}
		for ( var i = 1; i <= 4 ; i++ ) {
			$("#pricerate_"+i).attr("src",BASE_URL+"img/price-black.png")
		}
		$("#review-profile").modal("show");
	});
	
	$(document).on('click',".edit_rating",function(e){
		var id = $(this).attr("val");
		var reviews = $("#review_"+id).val();
		var ratings = $("#rating_"+id).val();
		var priceratings = $("#pricerating_"+id).val();
		$("#userRatingId").val(id);
		$("#userRatingReview").html(reviews);
		$("#rating_val").val(ratings);
		for ( var i = 1; i <= ratings ; i++ ) {
			$("#rate_"+i).attr("src",BASE_URL+"img/rating-star.png")
		}
		$("#pricerating_val").val(priceratings);
		for ( var i = 1; i <= priceratings ; i++ ) {
			$("#pricerate_"+i).attr("src",BASE_URL+"img/price-rated.png")
		}
		$("#review-profile").modal("show");
	});

});



function saveContactusData() {
	$("#fade-whole").show();
	var data = $('#ContactForm').serialize();
	$("#ContactForm").ajaxSubmit({
		type: "POST",
		url: SITE_LINK + "contact-us",
		data: data,
		success: function (data) {
			$('#ContactForm').trigger('reset');
			var obj = $.parseJSON(data);			
			if (obj.error) {
				$('#contact_cont').removeClass("success_message");
			} else {
				$('#contact_cont').addClass("success_message");
			}
			$('#contact_cont').html(obj.message);
			$('#contact_cont').addClass("show");
			$("#fade-whole").hide();
			setTimeout(function(){
				$('#myModal').modal('hide');
             },5000)
             
		},
		error: function (f) {
			$("#fade-whole").hide();
			return false;
		}
	});
}

function refreshTime() {
	$('.from_time').selectpicker('refresh');
	$('.to_time').selectpicker('refresh');
}

function getState(val) {
	$(".state_class").remove();
	$('#UserdetailStateId').html('');
	$('#UserdetailStateId').selectpicker('refresh');
	if (val.length != 0) {
		$.ajax({
			type: "POST",
			url: BASE_URL + "userdetails/get_states",
			data: {'country_id': val},
		}).done(function (data) {
			data = $.parseJSON(data);
			t = stateSelectTmpl;
			$("#UserdetailStateId").append(t);
			$.each(data, function (k, v) {
				var t = stateTmpl;
				t = t.replace('{KEY}', k);
				t = t.replace('{VAL}', v);
				$("#UserdetailStateId").append(t);
			});
			$('#UserdetailStateId').selectpicker('refresh');
		});
	}
}

$.validator.addMethod("validEmail", function (value, element)
{
	if (value == '')
		return true;
	var temp1;
	temp1 = true;
	var ind = value.indexOf('@');
	var str2 = value.substr(ind + 1);
	var str3 = str2.substr(0, str2.indexOf('.'));
	if (str3.lastIndexOf('-') == (str3.length - 1) || (str3.indexOf('-') != str3.lastIndexOf('-')))
		return false;
	var str1 = value.substr(0, ind);
	if ((str1.lastIndexOf('_') == (str1.length - 1)) || (str1.lastIndexOf('.') == (str1.length - 1)) || (str1.lastIndexOf('-') == (str1.length - 1)))
		return false;
	str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
	temp1 = str.test(value);
	return temp1;
}, "Please enter valid email.");

function update_status(status_val, order_id, order_status_id) {
	//alert(BASE_URL);
	//alert(status_val+'--'+order_id+'--'+order_status_id); 
	if(status_val.length == 0){
		//alert(status_val+'--'+order_id+'--'+order_status_id);
			$('#succ_msg').html('');
			$('.alert-success').removeClass('show');
			$('.alert-success').addClass('hide');
			$('#err_msg').html('Please select a valid status type');
			$('.alert-danger').addClass('show');
			$('.alert-danger').removeClass('hide');
			$('.alert-danger').fadeOut(5000, function () {
			$('.alert-danger').removeClass('show');
			$('.alert-danger').addClass('hide');
			});
			return false;		
	}
	var selected_val = $("#OrderStatus option:selected").text();	
	if (selected_val == 'Select Status') {

	} else {
		
		//var x = confirm("Are you sure you want to change the status? Once you changed the status, you can not select the same for the next time.");
	var x = con_pop("Are you sure you want to change the status? Once you changed the status, you can not select the same for the next time.");
	function con_pop(mssg){
	$('#constring').html(mssg);
	$('#myModalcon').modal('show');	
}
	$('.cancel-btn').click(function(){
	var val1=$(this).html();
	
	
		//if (x) {
		if(val1=='Ok'){
			if (status_val == 11) {				
				var link = SITE_LINK + "reorder/" + order_id;
				location.href = link;
				return false;
			}
			status_val = status_val;
			order_id = order_id;
			order_status_id = order_status_id;
			selected_val = selected_val;
			if (status_val == 8) {
				$("#OrderPaymentOrderId").val(order_id);
				$("#OrderPaymentStatusVal").val(status_val);
				$("#OrderPaymentOrderStatusId").val(order_status_id);
				$("#OrderPaymentSelectedVal").val(selected_val);
				$('#myModalForMember').modal('show');
			} else if (status_val == 7) {
				$("#OrderPaymentOrderId1").val(order_id);
				$("#OrderPaymentStatusVal1").val(status_val);
				$("#OrderPaymentOrderStatusId1").val(order_status_id);
				$("#OrderPaymentSelectedVal1").val(selected_val);
				abc(status_val, order_id, order_status_id, selected_val);
                //window.location.href = window.location.href;
				//$('#myModalForLab').modal('show');
				return false;
			} else {
				
				$("#fade-whole").show();
				abc(status_val, order_id, order_status_id, selected_val);
			}
		} else {
			return false;
		}
		});
	}
}


function abc(status_val, order_id, order_status_id, selected_val) {
	//alert(status_val+'---'+order_id+'----'+order_status_id+'----'+selected_val);
	//return false;
	if(status_val=='2'||status_val=='12')
	{
		var url=BASE_URL + "orders/orderstatus2";
	}
	else
	{
		var url=BASE_URL + "orders/update_status";
	}
	//alert(url);
	
	$.ajax({
		type: "POST",
		url: url,
		data: {'status_id': status_val, 'order_id': order_id, 'order_status_id': order_status_id, 'selected_text': selected_val},
		success: function (resp) {

         	
			if(status_val=='12')  
			{
				window.location=BASE_URL+'preview-orders';
			}
            if(status_val=='2'||status_val=='3'||status_val=='7')  
			{
				window.location=BASE_URL+'view-orders';
			}
            if(status_val=='8'||status_val=='9')  
			{
				window.location=BASE_URL+'preview-orders-staff';
			}				
			
			$("#fade-whole").hide();
			data = $.parseJSON(resp);			
			if (!data.error) {				
				$('#statusID').html(selected_val);
				$('#succ_msg').html(data.message);
				$('.alert-success').addClass('show');
				$('.alert-success').removeClass('hide');
				$('.alert-success').fadeOut(4000, function () {
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');
					
				});

			} else if (data.error) {				
				$('#succ_msg').html('');
				$('.alert-success').removeClass('show');
				$('.alert-success').addClass('hide');
				$('#err_msg').html(data.message);
				$('.alert-danger').addClass('show');
				$('.alert-danger').removeClass('hide');
				$('.alert-danger').fadeOut(5000, function () {
					$('.alert-danger').removeClass('show');
					$('.alert-danger').addClass('hide');
				});
			}
		}
	});

}

/* Save staff member data  */


/*$(document).on('click','#orderBtn',function(){	
var order_id = $('#OrderOrderId').val();
alert(order_id);

	$("#OrderReorderForm").ajaxSubmit({
		type: "POST",
		url: BASE_URL + "orders/update_status",
		data: { 'status_id': '11', 'order_id': order_id,'selected_text': 'Reorder' },
		success: function (data) {	
			alert(data);		
		}
	});
	return false;
});
*/

function saveMemberData() {
	$("#myModalForMember").hide();
	$("#fade-whole").show();
	var data = $('#MemberDetailForm').serialize();
	$("#MemberDetailForm").ajaxSubmit({
		type: "POST",
		url: SITE_LINK + "orders/save_member_detail",
		data: data,
		success: function (data) {
			$('#MemberDetailForm').trigger('reset');
			var obj = $.parseJSON(data);
			order_id = $("#OrderPaymentOrderId").val();
			status_val = $("#OrderPaymentStatusVal").val();
			order_status_id = $("#OrderPaymentOrderStatusId").val();
			selected_val = $("#OrderPaymentSelectedVal").val();
			if (obj.error) {
				$('#contact_cont2').removeClass("success_message");
			} else {
				abc(status_val, order_id, order_status_id, selected_val);
				$('#contact_cont2').addClass("success_message");
			}
			$('#contact_cont2').html(obj.message);
			$('#contact_cont2').addClass("show2");
			//$("#fade-whole").hide();
		},
		error: function (f) {
			$("#fade-whole").hide();
			return false;
		}
	});
}

/* Save Lab order charges data  */

function saveLabData() {
	$("#myModalForLab").hide();
	$("#fade-whole").show();
	
	var data = $('#LabDetailForm').serialize();
	$("#LabDetailForm").ajaxSubmit({
		type: "POST",
		url: SITE_LINK + "orders/save_lab_detail",
		data: data,
		success: function (data) {			
			$('#LabDetailForm').trigger('reset');
			var obj = $.parseJSON(data);
			order_id = $("#OrderPaymentOrderId1").val();
			status_val = $("#OrderPaymentStatusVal1").val();
			order_status_id = $("#OrderPaymentOrderStatusId1").val();
			selected_val = $("#OrderPaymentSelectedVal1").val();
			if (obj.error) {
				$('#contact_cont3').removeClass("success_message");
			} else {
				abc(status_val, order_id, order_status_id, selected_val);
				$('#contact_cont3').addClass("success_message");
			}
			$('#contact_cont3').html(obj.message);
			$('#contact_cont3').addClass("show2");
			//$("#fade-whole").hide();
		},
		error: function (f) {
			$("#fade-whole").hide();
			return false;
		}
	});
}

function requestforChange(){		
    $("#myModalForLabUser").hide();
	$("#fade-whole").show();
	var data = $('#DetailFormForLab').serialize();
		$("#DetailFormForLab").ajaxSubmit({
		type: "POST",
		url: SITE_LINK + "orders/orderChangeRequest",
		data: data,
		success: function (resp) {								
			$("#fade-whole").hide();
			data = $.parseJSON(resp);
			if (!data.error) {			
				$('#succ_msg').html(data.message);
				$('.alert-success').addClass('show');
				$('.alert-success').removeClass('hide');
				$('.alert-success').fadeOut(4000, function () {
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');	
					window.location.href = window.location.href;				
				});
			} else if (data.error) {
				$('#succ_msg').html('');
				$('.alert-success').removeClass('show');
				$('.alert-success').addClass('hide');
				$('#err_msg').html(data.message);
				$('.alert-danger').addClass('show');
				$('.alert-danger').removeClass('hide');
				$('.alert-danger').fadeOut(5000, function () {
					$('.alert-danger').removeClass('show');
					$('.alert-danger').addClass('hide');
				});
			}
		}	
	});
}

function save_rating(){
	var data = $('#OrderReviewViewOrderForm').serialize();	
	$("#OrderReviewViewOrderForm").ajaxSubmit({
		type: "POST",
		url: SITE_LINK + "orders/save_rating",
		data: data,
		success: function (resp) {
			data = $.parseJSON(resp);					
			if (!data.error) {			
				$('#succ_msg').html(data.message);
				$('.alert-success').addClass('show');
				$('.alert-success').removeClass('hide');
				$('.alert-success').fadeOut(4000, function () {
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');	
					window.location.href = window.location.href;				
				});
			  } else if (data.error) {
				$('#succ_msg').html('');
				$('.alert-success').removeClass('show');
				$('.alert-success').addClass('hide');
				$('#err_msg').html(data.message);
				$('.alert-danger').addClass('show');
				$('.alert-danger').removeClass('hide');
				$('.alert-danger').fadeOut(5000, function () {
					$('.alert-danger').removeClass('show');
					$('.alert-danger').addClass('hide');
					window.location.href = window.location.href;
				});
			}			
		}
	});
	
}
