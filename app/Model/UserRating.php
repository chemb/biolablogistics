<?php
App::uses('AppModel', 'Model');
/**
 * UserRating Model
 *
 * @property User $User
 */
class UserRating extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Userdetail' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Userdetail.user_id = UserRating.user_id',
			'fields' => '',
			'order' => '',
			'type'=>'Inner'
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => false,
			'conditions' => 'User.id = UserRating.user_id',
			'fields' => '',
			'order' => '',
			'type'=>'Inner'
		)
	);
}
