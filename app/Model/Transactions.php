<?php
App::uses('AppModel', 'Model');
/**
 * OrderDetail Model
 *
 * @property Order $Order
 */
class Transaction extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $name = 'Transaction';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'OrderPayment' => array(
			'className' => 'OrderPayment',
			'foreignKey' => 'order_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		
	);
}
