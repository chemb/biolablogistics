<?php
App::uses('AppModel', 'Model');
/**
 * OrderRequest Model
 *
 * @property Order $Order
 */
class OrderRequest extends AppModel {

    var $name = 'OrderRequest';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'type'=>'Inner',
		),
		'Lab' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Lab.user_id = OrderRequest.lab_id',
			'fields' => '',
			'order' => '',
			'type'=>'Inner',
		)
	);
	
	function beforesave($options = array()){
		parent::beforesave();
		if(empty($this->data[$this->name]['lab_id'])){
			unset($this->data[$this->name]);
		}
	}
}
