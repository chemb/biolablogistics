<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Bookmark $Bookmark
 * @property Testimonial $Testimonial
 * @property Userdetail $Userdetail
 */
class OrderPayment extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * hasMany associations
 *
 * @var array
 */
	
	var $name = 'OrderPayment';

	public $hasOne = array(
		'Transactions' => array(
			'className' => 'transaction',
			'foreignKey' => 'order_payment_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		
	);
		public $belongsTo = array(
		'Doctor' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Doctor.user_id = OrderPayment.payer',
			'fields' => '',
			'order' => ''
		),'Lab' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Lab.user_id = OrderPayment.receiver',
			'fields' => '',
			'order' => ''
		)
	); 

	


}
