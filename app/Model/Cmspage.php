<?php
App::uses('AppModel', 'Model');
/**
 * Cmspage Model
 *
 */
class Cmspage extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
	var $validate	=	array(
		"name"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page name.'
			),
			"isUnique"=>array(
				"rule"=>"isUnique",
				"message"=>'Page name already exists.'
			)
		),
		"content"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page content.'
			)
		),
		"metatitle"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page meta title.'
			)
		),
		"seourl"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page seo url.'
			),
			"isUnique"=>array(
				"rule"=>"isUnique",
				"message"=>'Seo url already exists.'
			)
		),
		"metadesc"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page meta description.'
			)
		),
		"metakeyword"=>array(
			"notempty"=>array(
				"rule"=>"notempty",
				"message"=>'Please enter page meta keywords.' 
			)
		)
	);
}
