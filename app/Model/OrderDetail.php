<?php
App::uses('AppModel', 'Model');
/**
 * OrderDetail Model
 *
 * @property Order $Order
 */
class OrderDetail extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $name = 'OrderDetail';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Lab' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Lab.user_id = Order.lab_id',
			'fields' => '',
			'order' => ''
		),
		'Doctor' => array(
			'className' => 'Userdetail',
			'foreignKey' => false,
			'conditions' => 'Doctor.user_id = Order.doctor_id',
			'fields' => '',
			'order' => ''
		)
	);
}
