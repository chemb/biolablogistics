<?php
App::uses('AppModel', 'Model');
/**
 * OrderDetail Model
 *
 * @property Order $Order
 */
class Favorite extends AppModel {
    public $primaryKey = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	//var $name = 'LabTransaction';

/**
 * belongsTo associations
 *
 * @var array
 */

public $belongsTo = array(
                'User1' => array(
                        'className' => 'User',
                        'foreignKey' => 'doctor_id'
                )
        );

public $actsAs = array('Containable');

}
?>