<?php
App::uses('AppModel', 'Model');
/**
 * Userdetail Model
 *
 * @property User $User
 */
class Userdetail extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	var $name = 'Userdetail';
	
	public $virtualFields = array(
        'doctorService' => 'CONCAT(Userdetail.first_name, "(", Userdetail.serviceTags, ")")',
		'full_name' => 'CONCAT(Userdetail.first_name, " ", Userdetail.last_name)',
	);

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['doctorService'] = sprintf(
			'CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias
		);
		$this->virtualFields['full_name'] = sprintf(
			'CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias
		);
	}
	public $hasOne = array(
		'UserRating' =>array(
			'className'	=> 'UserRating',
			'foreignKey' => 'rater_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''	
		)
	);
 
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'type'=>'Inner'
		),
		'Country'=>array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => 'Country.name',
			'order' => ''
		),
		'State'=>array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => array('State.name','State.code'),
			'order' => ''
		)
	);
	var $validate = array(
		'first_name'=>array(
			'rule'=>'notempty',
			'message'=>'Please enter first name'
		),
		'last_name'=>array(
			'rule'=>'notempty',
			'message'=>'Please enter last name'
		),
		'country_id'=>array(
			'rule'=>'notempty',
			'message'=>'Please select country'
		),
		'state_id'=>array(
			'rule'=>'notempty',
			'message'=>'Please enter state'
		),
		'city'=>array(
			'rule'=>'notempty',
			'message'=>'Please enter city'
		),
		'image' => array(
			'rule'    => array('extension', array('gif', 'jpeg', 'png', 'jpg')),
			'message' => 'Please upload a valid image format ie. jpg,png,jpeg,gif',
			'allowEmpty'=>true
		),
		'cardnumber' => array(
			'cc'=>array(
				'rule' => array('cc', array('visa', 'maestro'), false, null),
				'message' => 'The credit card number you supplied was invalid.'
			),
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter card number'
			)
		),
		'cardtype'=>array(
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter card type'
			)
		),
		'month'=>array(
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter month'
			),
			'checkdate'=>array(
				'rule'=>'checkdate',
				'message'=>'Please enter valid date'
			)
		),
		'email'=>array(
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter email'
			),
			'email'=>array(
				'rule'=>'email',
				'message'=>'Please enter valid email'
			)
		),
		'paypalaccount'=>array(
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter paypal email'
			),
			'email'=>array(
				'rule'=>'email',
				'message'=>'Please enter valid paypal email'
			)
		),
		'zipcode'=>array(
			'rule'=>'notempty',
			'message'=>'Please enter zipcode'
		)
	);
	function checkdate() {
		$curdate = date("m");
		$curyear = (date("Y"))*1;
		$month = ($this->data['Userdetail']['month'])*1;
		$curmonth = (date("m"))*1;
		$year = ($this->data['Userdetail']['year'])*1;
		if($year < $curyear) {
			return false;
		}
		if($year == $curyear && $month < $curmonth){
			return false;
		}
		else {
			return true;
		}
	}
	
	function beforesave($options = array()){
		parent::beforesave();
		foreach ( $this->data[$this->name] as $key=>$val ) {
			$this->data[$this->name][$key] = strip_tags($val,"<p>,<br>,<label>");
		}
	}
}
