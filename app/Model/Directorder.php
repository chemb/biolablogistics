<?php
App::uses('AppModel', 'Model');
/**
 * OrderDetail Model
 *
 * @property Order $Order
 */
class Directorder extends AppModel {
    public $primaryKey = 'oid';


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	//var $name = 'LabTransaction';

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $actsAs = array(
    		'Containable'
    );

}
?>