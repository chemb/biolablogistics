<?php 
App::uses('AppModel', 'Model');

class Adduser extends AppModel {
	
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'type'=>''
		)
	);	
	var $validate = array(
		'email'=>array(
			'notempty'=>array(
				'rule'=>'notempty',
				'message'=>'Please enter email.'
			),
			'email'=>array(
				'rule'=>'email',
				'message'=>'Please enter valid email.'
			),
			'isUnique'=>array(
				'rule'=>'isUnique',
				'message'=>'This email is already registered.'
			)
		)
		);
}
?>
