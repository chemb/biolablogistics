<?php
App::uses('AppModel', 'Model');
/**
 * DoctorLab Model
 *
 * @property DoctorLab $DoctorLab
 */
class DoctorLab extends AppModel {
	var $name = 'DoctorLab';
	
	public $belongsTo = array(
		'Userdetail' => array(
			'className' => 'Userdetail',
			'foreignKey' => 'lab_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'type'=>''
		)		
		
	);
}?>
