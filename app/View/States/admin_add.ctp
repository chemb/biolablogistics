<?php echo $this->element("admins/common",array("place"=>'Search by state name or country name',"flag"=>false,"pageheader"=>'',"buttontitle"=>'Add State',"listflag"=>false)); ?>
<div class="states form">
<?php echo $this->Form->create('State');?>
	<fieldset>
 		<legend><?php echo __('Add State'); ?></legend>
	<?php
		echo $this->Form->input('country_id');
		echo $this->Form->input('name',array("label"=>"State Name"));
		echo $this->Form->input('code');
		echo $this->Form->input('status',array("label"=>"Active","type"=>"checkbox"));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
<a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
</div>

