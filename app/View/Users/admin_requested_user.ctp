<style>
#checkstatus
{
	display:none;
}
</style>
<div class="users index">
	<?php echo $this->Form->create("Adduser",array("div"=>false)); ?>
	
	<?php echo $this->element("admins/common",array("place"=>'Search by name , email',"flag"=>false,"pageheader"=>"Requested Users","buttontitle"=>'no',"listflag"=>true,"action"=>'admin_add')); ?>
	<h2><?php //  echo __('Requested User'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<!--th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th -->
			<th class="leftalign"><?php echo $this->Paginator->sort('Name'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Email'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('phone'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Usertype'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('address'); ?></th>			
			<th class="leftalign"><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php 
	if(!empty($newUser)){	
		foreach ($newUser as $user): 		
				?>
					<tr>				
						<td class="leftalign"><?php echo (!empty($user['Adduser']['name'])) ? $user['Adduser']['name'] :'N-A';?>&nbsp;</td>
						<td class="leftalign"><?php echo (!empty($user['Adduser']['email'])) ? $user['Adduser']['email'] :'N-A'; ?>&nbsp;</td>
						<td class="leftalign"><?php echo (!empty($user['Adduser']['phone'])) ? $user['Adduser']['phone'] :'N-A'; ?>&nbsp;</td>
						<td class="leftalign"><?php if(!empty($user['Adduser']['user_type'])) {
						if($user['Adduser']['user_type'] == 1){ echo 'Doctor'; } else { echo 'Lab'; }} else { echo 'N-A'; }   ?>&nbsp;</td>
						<td class="leftalign"><?php echo (!empty($user['Adduser']['address'])) ? $user['Adduser']['address'] :'N-A'; ?>&nbsp;</td>
						<td class="leftalign"><?php echo (!empty($user['Adduser']['created'])) ? $user['Adduser']['created'] :'N-A'; ?>&nbsp;</td>
						<td class="actions">					
							<?php echo $this->Html->link(__('View'), array('action' => 'view_userrequest', $user['Adduser']['id']), null); ?>	
						</td>
					</tr>
        <?php endforeach; } else
                          {  ?>
	                        <td class="leftalign" colspan='7' style="text-align:center; color:red;">No Record found</td>	
	              <?php   }  ?>

	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->Form->end(); ?>
