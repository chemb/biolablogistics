<div class="categories form">
<?php echo $this->Form->create('User',array('novalidate'=>true,"name"=>"user_admin_edit_form")); ?>
    <fieldset>
        <legend><?php echo __('Edit User'); ?></legend>
	<?php

		echo $this->Form->input('Userdetail.first_name');
		echo $this->Form->input('Userdetail.last_name');
		echo $this->Form->input('username', array('readonly' => 'readonly', 'label' =>'Email'));
		echo $this->Form->input('status',array("type"=>"checkbox"));
		echo $this->Form->input('Userdetail.privacy',array("type"=>"checkbox","label"=>"Privacy Check"));
		echo $this->Form->input('id', array('type' => 'hidden')); 	
		echo $this->Form->input('Userdetail.id', array('type' => 'hidden','value'=>$this->data['Userdetail']['id'])); 
	?>
    </fieldset>
    <a class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
<?php echo $this->Form->submit(); ?>
  
 <?php echo $this->Form->end(); ?>
</div>
