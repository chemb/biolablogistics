<div class="users view">
	
   <h2><?php  echo __('User'); ?></h2>
   <dl>
      <dt><?php echo __('Profile Image'); ?></dt>
      <dd>
         <?php echo (!empty($user['Userdetail']['image'])?$this->Html->image($user['Userdetail']['image']):''); ?>
         &nbsp;
      </dd>
      <dt><?php echo __('First Name'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['first_name']) ? 'N-A' : $user['Userdetail']['first_name']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Last Name'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['last_name']) ? 'N-A' : $user['Userdetail']['last_name']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Email'); ?></dt>
      <dd>
         <?php echo empty($user['User']['username']) ? 'N-A' : $user['User']['username']; ?> 
         &nbsp;
      </dd>
      <dt><?php echo __('User Type'); ?></dt>
      <dd><?php if(!empty($user['User']['user_type']))
         {
         	if($user['User']['user_type'] == 1) {  echo "Doctor"; }
         	elseif($user['User']['user_type'] == 2) {  echo "Lab"; }
         	else { echo "Staff Member"; }         	
         } else { echo "N-A"; } ?>			
         &nbsp;
      </dd>
     <dt><?php echo __('Phone'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['phone']) ? 'N-A' : $user['Userdetail']['phone']; ?>			
         &nbsp;
      </dd>
      <dt><?php echo __('Address'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['address']) ? 'N-A' : $user['Userdetail']['address']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Country'); ?></dt>
      <dd>
         <?php echo empty($user['Country']['name']) ? 'N-A' : $user['Country']['name']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('State'); ?></dt>
      <dd>
         <?php echo empty($user['State']['name']) ? 'N-A' : $user['State']['name']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('City'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['city']) ? 'N-A' : $user['Userdetail']['city']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('About'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['about']) ? 'N-A' : $user['Userdetail']['about']; ?>
         &nbsp;
      </dd>
     <dt><?php echo __('Biography'); ?></dt>
      <dd>
         <?php echo empty($user['Userdetail']['biography']) ? 'N-A' : $user['Userdetail']['biography']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Created On'); ?></dt>
      <dd>
         <?php echo empty($user['User']['created']) ? 'N-A' : $user['User']['created']; ?>			
         &nbsp;
      </dd>
      <?php if ($user['User']['profile_status']==0) { ?>
		<dt><?php echo __('Status'); ?></dt>
		<dd>		 		
		<?php echo $this->Html->link(__('Approve Profile'), array('action' => 'userstatus', $user['User']['id'] , 1), null, __('Are you sure you want to approve user profile %s?', $user['Userdetail']['first_name'])); 
		echo str_repeat('&nbsp;', 5); 
//		echo $this->Html->link(__('Disapprove Profile'), array('action' => 'userstatus', $user['User']['id'], 0), null, __('Are you sure you want to reject user profile %s?', $user['Userdetail']['first_name'])); 
		}

		?>
		</dd>
      <dt><?php if ($user['User']['status'] != -1) { 
		echo $this->Html->link(__("Edit"),array("action"=>"edit", $user['User']['id']),array('class' => 'btn btn-grey')); 
	    }  ?><a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a></dt>
   </dl>
</div>

