<?php //pr($reviews); ?>
<div class="new-order">
<h2><?php echo __('Reviews :'); ?></h2>
<div class="white-bg">
<ul>	
<?php
foreach ($reviews as $review) { ?>	
	<?php echo $this->Form->hidden("review".$review['UserRating']['id'],array("id"=>"review_".$review['UserRating']['id'],"value"=>$review['UserRating']['review'])); ?>
	<?php echo $this->Form->hidden("rating".$review['UserRating']['id'],array("id"=>"rating_".$review['UserRating']['id'],"value"=>$review['UserRating']['rating'])); ?>
	<?php echo $this->Form->hidden("pricerating".$review['UserRating']['id'],array("id"=>"pricerating_".$review['UserRating']['id'],"value"=>$review['UserRating']['pricerating'])); ?>
	<li>
		<p class="review-by"><?php echo ucwords($review['UserRating']['rater_name']); ?>
		<?php  if ( $this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.id") == $review['UserRating']['user_id'] ) { ?>
			<a href="javascript:void(0);" val="<?php echo $review['UserRating']['id']; ?>" class="edit_rating"><span aria-hidden="true" class="glyphicon glyphicon-pencil"></span></a>
		<?php } ?>
		<span class="pull-right"><?php echo date("Y-m-d g:ia",strtotime($review['UserRating']['created'])); ?></span></p>
		<p><?php echo h($review['UserRating']['review']); ?> </p>
		<p class="rating-pri"><span class="pull-left">Rating : </span><?php echo $this->element("ratingstars",array("rating"=>$review['UserRating']['rating'])); ?>  <span class="pull-left">&nbsp;</span>  <span class="pull-left">Price Rating : </span><?php echo $this->element("pricerating",array("rating"=>$review['UserRating']['pricerating'])); ?> </p>
		<div class="clear"></div>
	</li>
<?php } ?>	
</ul>
</div>
<?php echo $this->Element("pagination"); ?>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ReviewProfile" aria-hidden="false" id="review-profile">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ">
			<?php echo $this->Form->create("userRating"); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Write Review</h4>
				</div>
				<div class="modal-body">
					<?php echo $this->Form->hidden("id"); ?>
					<div class="row">
						<div class="col-sm-5">
							<div class="row">
								<div class="col-sm-6 text-left">Rating</div>
								<div class="col-sm-6 text-left">
									<ul>
										<li>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_1")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_2")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_3")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_4")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_5")); ?>
											<?php echo $this->Form->hidden("rating",array("value"=>"","id"=>"rating_val")); ?>							
											<span id="rating_err_msg" style="color:red; display:none;"></span>
										</li>
										<li></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6 text-left">Price rating</div>
								<div class="col-sm-6 text-left">
									<ul>
										<li>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_1")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_2")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_3")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_4")); ?>
											<?php echo $this->Form->hidden("pricerating",array("value"=>"","id"=>"pricerating_val")); ?>							
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-7 text-left"><?php echo $this->Form->input("review",array("type"=>"textarea","div"=>false,"label"=>false,"placeholder"=>"Write Review")); ?></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="login-btn-outer">
						<?php echo $this->Form->submit("Rate", array("class"=>'all-btn login-btn')); ?>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
