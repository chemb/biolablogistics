<?php //pr($reviews); die; ?>
<div class="row">
	<div class="col-lg-12">
        <div class="profile-box">
        <!-- User Image -->
        <div class="user-pro-img">
			<?php 
			if ( $user['User']['user_type'] == 1 ) { 
				$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/docprofile.jpg");
			} else if ( $user['User']['user_type'] == 2 ) { 
				$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/labprofile.jpg");
			} else {
				$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no-img.png");
			}
			$profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, MediumProfileImagePrefix);
			echo $this->Html->image($profileImgThumb1,array("alt"=>($user['Userdetail']['first_name'].' '.$user['Userdetail']['last_name']),"width"=>"192px","height"=>"198"));
			?>
		</div>
		<!-- User Image -->
		<!-- User Detail -->
        <div class="user-pro-txt">
            <div class="pull-right">
			<?php if($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.id") == $user['User']['id']) { ?>
				<span class="profile-edit-btn"><a href="<?php echo $this->Html->url("/account"); ?>" title="Edit Profile" class="btn btn-primary">Edit Profile</a></span>
			<?php } ?>
		</div>
		<?php $rater_list = array();
			$rater_list[] = $user['UserRating']['rater_id']; ?>
		<h2><?php echo h($user['User']['company_name']); ?></h2>
		<br>
        <div class="row">
			 <div class="col-md-6 profile-cont"> 
		<?php  if($user['User']['user_type'] == 2) { ?>		 
       <p>
		   <?php //if(!empty($user['Userdetail']['rating'])){
				echo $this->element("ratingstars",array("rating"=>$user['Userdetail']['rating']));
				echo "<br/>";
				echo $this->element("pricerating",array("rating"=>$user['Userdetail']['pricerating']));
		} ?> 
		<?php if ( $this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type") == 1 && $user['User']['user_type'] == 2 ) { ?>
				<br/>
				<a href="javascript:void(0);" id="rate_it">Write a Review</a>
		   <?php } ?>
		<?php if(!empty($review_count)) { ?>
		<a href="#reviews"><?php echo '<br/>('.$review_count. ') Reviews'; ?><?php } ?> </a>
		
			
		<?php  if($this->params['pass'][0] != $curruser && $this->Session->read("Auth.User.id")){
				if (!in_array($user['User']['id'],$rater_list)){  ?>
					<!--<a href="javascript:void(0)" class="user_rating btn btn-sm btn-warning" id="userRating_<?php echo $user['User']['id'];?>"><span aria-hidden="true" class="glyphicon glyphicon-star-empty"></span> Cick here for rating</a> -->
					</p>
					<div id="response-box<?php echo $user['User']['id'];?>"></div>
					<div style="display:none" class="addRating" id="add_rating_<?php echo $user['User']['id']; ?>">
						<?php echo $this->Form->create('UserRating', array('id' => 'rating'.$user['User']['id'])); ?>
							<div id="response-time<?php echo $user['User']['id']?>" class="response-time"></div>
							<span id="rating_error<?php echo $user['User']['id']; ?>"></span>
							<?php echo $this->Form->Submit(__d('labels','Submit'),array('label'=>false,'div'=>false,'class'=>'rating-btn btn btn-primary', 'id'=>$user['User']['id'])); ?>
						<?php echo $this->Form->end(); ?>
					</div>
				<?php } else { ?>
					<!--<p><a href="javascript:void(0)" class="user_rating btn btn-sm btn-warning" id="userRating_<?php echo $user['User']['id'];?>"><span aria-hidden="true" class="glyphicon glyphicon-star-empty"></span> Edit rating</a></p>-->
					<div id="response-box<?php echo $user['User']['id'];?>"></div>
					<div style="display:none" class="addRating" id="edit_rating_<?php echo $user['User']['id']; ?>">
						<?php echo $this->Form->create('UserRating', array('id' => 'Editrating'.$user['User']['id'])); 
							echo $this->Form->hidden('id', array('value' => $user['UserRating']['id'], 'id'=>'ratingid'));?>
							<div id="response-time<?php echo $user['User']['id']?>" class="response-time"></div>
							<span id="rating_error<?php echo $user['User']['id']; ?>"></span>
							<?php echo $this->Form->Submit(__d('labels','Submit'),array('label'=>false,'div'=>false,'class'=>'rating-btn btn btn-primary', 'id'=>$user['User']['id'])); ?>
						<?php echo $this->Form->end(); ?>
					</div>
				<?php } 
			}  ?>
			
			
		
            <p> <span aria-hidden="true" class="glyphicon glyphicon-earphone"></span>&nbsp;
		<?php echo (empty($user['Userdetail']['phone'])?'**********':(empty($user['Userdetail']['privacy'])?$user['Userdetail']['phone']:'************')) ; ?>
            </p>
            <p><span aria-hidden="true" class="glyphicon glyphicon-send"></span>&nbsp; <?php echo (empty($user['User']['username'])?'**********':(empty($user['Userdetail']['privacy'])?$user['User']['username']:'************')) ; ?></p>
             <p><span aria-hidden="true" class="glyphicon glyphicon-link"></span>&nbsp; <?php echo empty($user['Userdetail']['webLink']) ? 'N-A' : $user['Userdetail']['webLink']; ?></p>
            </div>
            <div class="col-md-6"> 
				<p><span class="glyphicon glyphicon-ok"> </span> &nbsp; Specialization: <?php echo h($user['Userdetail']['serviceTags']); ?></p>
				 <span aria-hidden="true" class="glyphicon glyphicon-map-marker"></span>&nbsp;
               <div class="location">
			<?php echo empty($user['Userdetail']['address']) ? '' : $user['Userdetail']['address']." ,"; ?>
			<?php echo empty($user['Userdetail']['city']) ? '' : $user['Userdetail']['city']; ?>
			<?php echo empty($user['State']['name']) ? '' : $user['State']['name']; ?>
			<?php echo empty($user['Userdetail']['zipcode']) ? '' : $user['Userdetail']['zipcode']; ?>
			<?php echo empty($user['Country']['name']) ? '' : $user['Country']['name']; ?><br>
			<?php if ( $this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type") == 1 && $this->Session->read("Auth.User.profile_status") == 1 && $user['User']['user_type'] == 2 ) { ?>
				<div class="pull-left">
						<div class="view-btn-outer" style="margin: 10px 15px 0px -22px;">	
							<a class="view-btn" href=<?php echo SITE_LINK."orders/order/".$user['User']['id']; ?> >Place Order</a>
						</div>
						<div class="view-btn-outer">	
							<?php if (empty($user['Userdetail']['is_member'])) { ?>													
								<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Add to Favourite" data-attr="add" >Add to Favourite</a> 
							<?php } else { ?>
								<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Remove from Favourite" data-attr="remove">Remove from Favourite</a>
							<?php } ?>
						</div>
				</div>
			<?php } ?>
	</div></div>
	</div>		
                <div class="clear"></div>
</div>
        </div></div>
<?php if ( $user['User']['user_type'] == 2 ) { ?>
    <div class="col-lg-12">
        <div class="profile-box-text">
			<div class="row">
				<div class="col-sm-6">
            <h3>Turnarounds</h3>
            <table class="table">
				<thead>
				<tr>
					<th>Procedures</th>
					<th>Timings</th>
				</tr>
				</thead>
			<?php 
            if(!empty($user['Userdetail']['turnarounds'])){
				$turnarounds = unserialize($user['Userdetail']['turnarounds']);
				foreach ( $turnarounds as $turnkey => $turnval ) {
				?>
				<tr>
					<td><?php echo $turnval['name']; ?></td>
					<td> <?php echo $turnval['value']; ?> </td>
				</tr>
				<?php }
			} ?>
			</table>
            </div>
            <div class="col-sm-6">
            <h3>Timings</h3>
            <table class="table">
				<thead>
				<tr>
					<th>Day</th>
					<th>Timings</th>
				</tr>
				</thead>
            <?php 
            if(!empty($user['Userdetail']['timings'])){
				$timings = unserialize($user['Userdetail']['timings']);
				foreach ( $timings as $timingkey => $timingval ) {
				?>
					<tr>
						<td>
							<?php echo $weekArray[$timingkey]; ?>
						</td>
						<td>
							<?php 
							if ( !empty($timingval['from']) ) {
								echo $timingval['from'].' - '.$timingval['to']."<br/>";
							} else {
								echo "Closed";
							}
							?>
						</td>
					
					</tr>
			<?php	}
				
			} ?>
			</table>
			</div>
			</div>
		
    
    
    
<h3 id="reviews">Reviews</h3>
<div class="white-bg">
<ul>	
<?php
foreach ($reviews as $review) { //pr($review); die; ?>	
	<?php echo $this->Form->hidden("review".$review['UserRating']['id'],array("id"=>"review_".$review['UserRating']['id'],"value"=>$review['UserRating']['review'])); ?>
	<?php echo $this->Form->hidden("rating".$review['UserRating']['id'],array("id"=>"rating_".$review['UserRating']['id'],"value"=>$review['UserRating']['rating'])); ?>
	<?php echo $this->Form->hidden("pricerating".$review['UserRating']['id'],array("id"=>"pricerating_".$review['UserRating']['id'],"value"=>$review['UserRating']['pricerating'])); ?>
	<li>
		<p class="review-by">
			<?php echo $this->Html->link(ucwords($review['User']['company_name']), array("controller" => "profile", "action" => $review['UserRating']['user_id'], $this->Common->makeurl(($review['User']['company_name']))), array("title"=>"View Profile","target"=>"_blank")); ?>
		<?php  if ( $this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.id") == $review['UserRating']['user_id'] ) { ?>
			<a href="javascript:void(0);" val="<?php echo $review['UserRating']['id']; ?>" class="edit_rating"><span aria-hidden="true" class="glyphicon glyphicon-pencil"></span></a>
		<?php } ?>
		<span class="pull-right"><?php echo date("Y-m-d g:i A",strtotime($review['UserRating']['created'])); ?></span>
		<br/>
		<span ><?php echo ucwords($review['Userdetail']['city'].', '.$state[$review['Userdetail']['state_id']]); ?></span>
		</p>
		<p><?php echo h($review['UserRating']['review']); ?> </p>
		<p class="rating-pri"><span class="pull-left">Rating : </span><?php echo $this->element("ratingstars",array("rating"=>$review['UserRating']['rating'])); ?>  <span class="pull-left">&nbsp;</span>  <span class="pull-left">Price Rating : </span><?php echo $this->element("pricerating",array("rating"=>$review['UserRating']['pricerating'])); ?> </p>
		<div class="clear"></div>
	</li>
<?php } ?>	
</ul>
</div>
</div>
  <?php } ?>  
    
    
   </div> 
    
    
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ReviewProfile" aria-hidden="false" id="review-profile">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ">
			<?php echo $this->Form->create("userRating"); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Write Review</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-5">
							<div class="row">
								<div class="col-sm-6 text-left">Rating</div>
								<div class="col-sm-6 text-left">
									<ul>
										<li>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_1")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_2")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_3")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_4")); ?>
											<?php echo $this->Html->image("/img/rating-star-1.png",array("class"=>"ratestar","id"=>"rate_5")); ?>
											<?php echo $this->Form->hidden("rating",array("value"=>"","id"=>"rating_val")); ?>							
											<span id="rating_err_msg" style="color:red; display:none;"></span>
											<?php echo $this->Form->hidden("id"); ?>
										</li>
										<li></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6 text-left">Price rating</div>
								<div class="col-sm-6 text-left">
									<ul>
										<li>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_1")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_2")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_3")); ?>
											<?php echo $this->Html->image("/img/price-black.png",array("class"=>"rateprice","id"=>"pricerate_4")); ?>
											<?php echo $this->Form->hidden("pricerating",array("value"=>"","id"=>"pricerating_val")); ?>							
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-7 text-left"><?php echo $this->Form->input("review",array("type"=>"textarea","div"=>false,"label"=>false,"placeholder"=>"Write Review")); ?></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="login-btn-outer">
						<?php echo $this->Form->submit("Rate", array("class"=>'all-btn login-btn')); ?>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

