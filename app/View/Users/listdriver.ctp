<div class="users index">
	<?php echo $this->Form->create("User",array("div"=>false)); ?>
	<?php echo $this->element("admins/common",array("place"=>'Search by email',"flag"=>false,"pageheader"=>$heading,"buttontitle"=>'no',"listflag"=>true,"action"=>'admin_add')); ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
			<th class="leftalign">Name</th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Email'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('User Type'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Status'); ?></th>
			<th class="leftalign">Privacy Check</th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Profile Status'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('Created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	if(!empty($users))
	{
	 foreach ($users as $user): ?>
	 <tr>
		
		<td><?php echo $this->Form->input("id.".$user['User']['id'],array("class"=>'chk',"value"=>$user['User']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?><?php echo $this->Form->input("status.".$user['User']['id'],array("type"=>'hidden',"value"=>($user['User']['status'] == 1?0:1))); ?></td>
		<td class="leftalign"><?php echo substr($user['Userdetail']['first_name'].' '.$user['Userdetail']['last_name'],0,40); ?>&nbsp;</td>
		<td class="leftalign"><?php echo substr($user['User']['username'],0,40); ?>&nbsp;</td>
		<td class="leftalign"><?php echo ($user['User']['user_type'] == 1)?'Doctor':(($user['User']['user_type'] == 2)?'Lab':'Staff member'); ?>&nbsp;</td>
		<td class="leftalign"><?php echo ($user['User']['status'] == -1)?'New User':(($user['User']['status'] == 1)?'Active':'Inactive'); ?>&nbsp;</td>
		<td class="leftalign"><?php echo (empty($user['Userdetail']['privacy'])?'Off':"On"); ?>&nbsp;</td>
		<td class="leftalign"><?php echo ($user['User']['profile_status'] == 0)?'Not approved':'Approved'; ?>&nbsp;</td>
		
		<td class="leftalign"><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete user %s?', $user['User']['username'])); ?>
			<?php echo $this->Html->link(__("Edit"),array("action"=>"edit", $user['User']['id']));  ?> 
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']), null); ?>	
		</td>
	</tr>
<?php endforeach; 
    } else 
    { ?>
		<td colspan='8' class="leftalign" style='color:red; text-align:center;'>No record found</td>
		
 <?php   }   ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->Form->end(); ?>
