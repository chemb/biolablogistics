<div class="categories form">
<?php echo $this->Form->create('User',array('novalidate'=>true)); ?>
    <fieldset>
        <legend><?php echo __('Add Driver'); ?></legend>
	<?php

		echo $this->Form->input('Userdetail.first_name');
		echo $this->Form->input('Userdetail.last_name');
		echo $this->Form->input('Userdetail.address');
		echo $this->Form->input('Userdetail.city');
		echo $this->Form->input('Userdetail.zipcode');
		echo $this->Form->input('Userdetail.phone');
	    echo $this->Form->input('Userdetail.state_id', array('options' => $states2,'class' => 'State selectpicker', "label" => false,"div" => true, 'empty' => 'Select State')); 
		echo $this->Form->input('username', array('label' =>'Email','value'=>''));
		echo $this->Form->input('password', array("maxlength" => "15", "type" => "password", 'label' => 'Password','value'=>'')); 
       
		echo $this->Form->input('user_type',array('class'=>"left ",'label'=>false,'div'=>false,'placeholder'=>'User Type','options' => array('' => 'User type','4' => 'Driver'))); 
		
	//	echo $this->Form->input('');
	
	?>
    </fieldset>
    <a class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
<?php echo $this->Form->submit(); ?>
  
 <?php echo $this->Form->end(); ?>
  
</div>
