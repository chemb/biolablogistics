<div class="login-box">
   
        <h2>Sign In </h2>
        <p class="text-center">On-demand deliveries for your lab.</p>
        <?php echo $this->Form->create('User'); ?>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->input("username", array("div" => true, "type" => "text", "label" => false, "placeholder" => "Email", "class" => "form-control")); ?>
        <?php echo $this->Form->input('password', array("label" => 'Password', "type" => 'password', "div" => true, "label" => false, "placeholder" => "Password", "class" => "form-control")); ?>
        <div class="clear"></div>
        <div class="keep-me"><?php echo $this->Form->input('remember_me', array('type' => 'checkbox', 'checked' => false, 'label' => 'Keep me logged in', 'div' => false)); ?></div>
        <div class="forgot-password"><a href="<?php echo $this->Html->url("/forgotpassword"); ?>" title="Forgot  your password?">Forgot your password?</a>
        </div>
        <div class="clear">&nbsp;</div>
        <div class="text-center">
            <div class="login-btn-outer">
            <?php echo $this->Form->submit("Sign In", array("class" => "login-btn", "div" => false, "label" => false)) ?>
            </div>
        </div>
        <p class="sign-up">Don’t have an account yet? <a href="<?php echo $this->Html->url("/signup"); ?>" title="Sign Up">Signup</a></p>
        
   
    <?php echo $this->Form->end(); ?>
</div> 

