   
    <?php if ($user_type == -1) { ?>
      
        <div class="login-box signin-box">
            <h2><?php echo ($user_type == 1) ? 'Signup as a Doctor' : (($user_type == 2) ? 'Signup as a Lab' : 'Create an Account'); ?></h2>
              <p>&nbsp;</p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="boxes-bg">
                        <a href="<?php echo $this->Html->url("/signup-as-doctor"); ?>" class="associate-doctors" title="Sign up as a Doctor">Sign up as a Doctor</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="boxes-bg">
                        <a href="<?php echo $this->Html->url("/signup-as-lab"); ?>" class="associate-labs" title="Sign up as a Lab">Sign up as a Lab</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
<div class="login-box">	
     <h2><?php echo ($user_type == 1) ? 'Signup as a Doctor' : (($user_type == 2) ? 'Signup as a Lab' : 'Create an Account'); ?></h2>
              <p>&nbsp;</p>
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create('User', array("enctype" => "multipart/form-data")); ?>
        <?php echo $this->Form->input('Userdetail.first_name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Name', "class" => "form-control")); ?>
        <?php echo $this->Form->input('username', array("maxlength" => "150", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Email', "class" => "form-control")); ?>
        <?php echo $this->Form->input('password', array("maxlength" => "15", "type" => "password", 'class' => "left ", 'label' => false, 'div' => true, 'placeholder' => 'Password', "class" => "form-control")); ?>	
        <?php echo $this->Form->input('confirmpassword', array("maxlength" => "15", "type" => "password", 'class' => "left ", 'label' => false, 'div' => true, 'placeholder' => 'Confirm password', "class" => "form-control")); ?>
        <?php echo $this->Form->input('company_name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Company name', "class" => "form-control")); ?>	
        <?php echo $this->Form->input('Userdetail.address', array("placeholder" => "Address", "div" => true, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
		<?php echo $this->Form->input('Userdetail.city', array("placeholder" => "City", "class" => "form-control", "div" => true, "label" => false, "type" => "text", "maxlength" => 60)); ?>
        <?php echo $this->Form->input('Userdetail.state_id', array('options' => $state,'class' => 'State selectpicker', "label" => false,"div" => true, 'empty' => 'Select State')); ?>
        <?php echo $this->Form->input('Userdetail.zipcode', array("placeholder" => "Zipcode", "div" => true, "label" => false, "class" => "form-control", "type" => "text", "maxlength" => 6));
				?> 
		
        <div class="clear-fix"></div>
        <?php //code for popup ?>


        <a href="#myModal" class="left open_contact_popup" data-toggle="modal">Any Doubts? Ask Us</a>


        <div class="text-center">
            <div class="login-btn-outer">
                <?php echo $this->Form->submit(__('Signup', true), array('class' => 'login-btn')); ?>
            </div>
        </div>
        <p class="sign-up">Already a user? <a href="<?php echo $this->Html->url("/login"); ?>" title="Log in">Sign in</a></p>

        <?php echo $this->Form->end(); ?>
   
</div>
 <?php } ?>


