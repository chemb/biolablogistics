<div class="lab-list">
    <?php echo $this->Form->create("Userfav", array("div" => false, 'type' => 'get')); ?>	
     <div class="add-btn-outer pull-right">
				
				<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/labs') ?>" title="View all Labs">View all Labs</a>
			</div>
		<h2 class="col-sm-3"><?php echo 'Favorite Labs'; ?></h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
				<?php echo $this->Form->input("searchval", array("type" => 'text', "placeholder" => 'Search by any header below', 'class' => 'form-control', "div" => false, "label" => false, "value" => (isset($set_val['searchval']) && !empty($set_val['searchval'])) ? $set_val['searchval'] : '')); ?>
				<?php echo $this->Form->submit("Search", array("label" => false, "div" => false, "class" => 'invite-button nomargin submitsearch search-btn', "id" => 'submitbtn', "attr" => 'search')); ?>
			</div>
		</div><style>
	.sorting-select{width:148px;}
	@media screen and (max-width:1024px){
	 .sorting-select{margin-left:70px;}
	}
</style>
		<div class="col-sm-2">
			<div class="sorting-select">
				<?php echo $this->Form->input('sort_rating', array("options" => array("All" => "All", "Rating" => "Rating"), "empty" => "Specialization", 'class' => "selectpicker", 'id' => 'sort_rating2', "value" => (isset($set_val['sort_rating']) && !empty($set_val['sort_rating'])) ? $set_val['sort_rating'] : '', 'label' => false, 'div' => false)); ?></div>
			</div>
			<div class="clear"></div>
<?php echo $this->Html->script('sorttable');?>
		
<style>
table.sortable thead {
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: "\25B4\25BE" 
}
</style>

			<div class="responsive-table">
				<table id="result_table" class="sortable table table-striped">
				<thead><tr>
						<th class="st">Name</th>
						<th class="st">Rating</th>
						<th class="st">Price</th>
						<th class="st"><span style="margin-left:16px">Distance</span></th>
						<th>&nbsp</th>
					</tr></thead>
				
				<tbody>
					<?php if (!empty($users)) {
						foreach ($users as $user): 
						 $labid=$user['User']['id'].'<br>';
						      $doctoid=$this->Session->read('Auth.User.id');?>
							<tr> 
								<td>
									<div class="listing-user-img">
										<?php //$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no-img.png");
										$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/labprofile.jpg");
										$profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, MediumProfileImagePrefix);
										echo $this->Html->image($profileImgThumb1, array("alt" => ($user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name']), "width" => "70px", "height" => "70"));?>
									</div>
									<div class="listing-user-txt">
									<h3><?php echo substr($user['User']['company_name'], 0, 40); ?></h3>
									<p style="  font-size: larger;"><?php echo substr($user['Userdetail']['serviceTags'], 0, 40); ?></p>
										<?php // echo substr($user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name'], 0, 40); ?>
										<?php echo substr($user['Userdetail']['address'], 0, 40); ?><br>
										<p style="text-align:none !important;"><?php echo (isset($user['Userdetail']['city']) ? $user['Userdetail']['city'] : '');echo ","; 
										 echo (isset($user['User']['StateName']) ? $user['User']['StateName'] : ''); echo " ";
										 echo (isset($user['Userdetail']['zipcode']) ? $user['Userdetail']['zipcode'] : ''); ?></p>
										 <?php //echo (isset($user['Userdetail']['phone']) ? $user['Userdetail']['phone'] : ''); 
										 echo (empty("(".substr($user['Userdetail']['phone'], 0, 3).") ".substr($user['Userdetail']['phone'], 3, 3)."-".substr($user['Userdetail']['phone'],6))?'**********':(empty($user['Userdetail']['privacy'])?"(".substr($user['Userdetail']['phone'], 0, 3).") ".substr($user['Userdetail']['phone'], 3, 3)."-".substr($user['Userdetail']['phone'],6):'************')) ;
										 ?>
										
										<?php // echo (isset($user['Userdetail']['State']['name']) ? $user['Userdetail']['State']['name'] : 'N-A'); ?>
										<?php // echo (isset($user['Userdetail']['State']['code']) ? $user['Userdetail']['State']['code'] : 'N-A'); ?>
									</div>
								</td>
								<td><?php echo $this->element("ratingstars", array("rating" => $user['User']['rating'])); ?></td>
								<td><?php echo $this->element("pricerating", array("rating" => $user['User']['pricerating'])); ?></td>
								<td style="padding: 7px 0px 0px;text-align:center;">
									<?php if(isset($shwDistance)) { //echo $user['User']['distance'].' mi.';
										echo $this->requestAction('App/getdistance/'. $labid);
									 }  ?>
								</td>
								<td> 
									<div class="pull-right" style="  width: 378px;">
										
											<div class="view-btn-outer">	
												<?php echo $this->Html->link("Place Order", array("controller" => "orders", "action" =>'order',$user['Userdetail']['user_id']), array("class" => "view-btn")); ?>
											</div>
											&nbsp;
											<div class="view-btn-outer">
											<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Remove from Favorite" data-attr="remove" style="width:150px;"><span style="margin-left:-8px;">Remove from Favorite</span></a>
											</div>
											
										 &nbsp;
										<div class="view-btn-outer">	
											<?php echo $this->Html->link("View Profile", array("controller" => "profile", "action" => $user['User']['id'], $this->Common->makeurl(($user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name']))), array("class" => "view-btn")); ?>
										</div>	
									</div>
								</td>
							</tr> 
				<?php endforeach;
			} else {?> 
                <td colspan="3" style="color:red;" class="leftalign">No record found</td>  
            <?php } ?>
	</tbody>
        </table>
    </div>
   <?php echo $this->Element("pagination"); ?>
<?php echo $this->Form->end(); ?>
</div>

