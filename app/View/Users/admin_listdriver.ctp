<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />
<h2>Driver list</h2>
<div class="users index">
	<table class="table table-striped table-bordered" id="example">
	<thead>
	<tr>
			
			<th class="leftalign">Name</th>
			<th class="leftalign">City</th>
			<th class="leftalign">Phone</th>
			<th class="leftalign">Zipcode</th>
			
			<th class="leftalign">Address</th>
	
			<th class="leftalign">Created</th>
			<th class="actions"><?php echo __('Actions');?></th>
			
	</tr>
	</thead>
	<body>
	<?php 
	if(!empty($pro))
	{
	 foreach ($pro as $pros): ?>
	 <?php  //print_r($pros); ?>
	 <tr>
		
		
		<td class="leftalign"><?php echo $pros['Userdetail']['first_name'].' '.$pros['Userdetail']['last_name']; ?>&nbsp;</td>
		<td class="leftalign"><?php echo $pros['Userdetail']['city']; ?>&nbsp;</td>
		<td class="leftalign"><?php echo $pros['Userdetail']['phone']; ?>&nbsp;</td>
		<td class="leftalign"><?php echo $pros['Userdetail']['zipcode']; ?>&nbsp;</td>
		<td class="leftalign"><?php echo $pros['Userdetail']['address']; ?>&nbsp;</td>
	
		<td class="leftalign"><?php echo $pros['Userdetail']['created']; ?>&nbsp;</td>
		<td class="actions">
					<?php echo $this->Html->link(__('Edit'), array('action' => 'driveredit', $pros['User']['id'])); ?>
					<?php echo $this->Html->link(__('Delete'), array('action' => 'driverdelete', $pros['User']['id']), null, __('Are you sure you want to delete # %s?', $pros['User']['id'])); ?>
				</td>
		
	</tr>
<?php endforeach; 
    } else 
    { ?>
		<td colspan='8' class="leftalign" style='color:red; text-align:center;'>No record found</td>
		
 <?php   }   ?>
 </body>
	</table>
	

</div>

<script>
$(document).ready(function() {
    $('#example').DataTable({pageLength: 100});
    $("#serMemtb").attr("placeholder", "Type a Location");
} );
</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}

</style>
