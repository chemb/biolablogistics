<div class="lab-list">
	<?php echo $this->Session->Flash(); ?>
    <?php echo $this->Form->create("User", array("div" => false, 'type' => 'get',"enctype" => "multipart/form-data")); ?>	
		<?php if ($this->Session->read('Auth.User.id')) {
			$add_type_name = ($this->Session->read('Auth.User.user_type') == 1 ? 'Lab' : (($this->Session->read('Auth.User.user_type') == 2) ? 'Doctor' : 'Member'));?>
			<div class="add-btn-outer pull-right">				
				<?php $url = (($add_type_name == 'Lab') ? "/add-lab" : (($add_type_name == 'Doctor') ? "/add-doctor" : "/add-members")); ?>
				<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url($url) ?>" title="Suggest a<?php echo $add_type_name; ?>">Suggest a <?php echo $add_type_name; ?></a>
			</div>
		<?php }   ?>
		<h2 class="col-sm-3"><?php echo 'View'.' '.$add_type; ?></h2>
		
		<div class="col-sm-4"> 
			<div class="searchbox_ld">
				
				<?php echo $this->Form->input("searchval", array("type" => 'text', "placeholder" => 'Search by specialization, city, state, zip or name', 'class' => 'form-control', "div" => false, "label" => false, "value" => (isset($set_val['searchval']) && !empty($set_val['searchval'])) ? $set_val['searchval'] : '')); ?>
				<?php echo $this->Form->submit("Search", array("label" => false, "div" => false, "class" => 'invite-button nomargin submitsearch search-btn', "id" => 'submitbtn', "attr" => 'search')); 
				?>
				 
				
			</div><span id="sort_distance_msg" style="display:none;">Please enter zip-code.</span>
		</div>		
		<div class="col-sm-2">
			<div class="sorting-select">
				
				<?php if($this->Session->read('Auth.User.user_type') == 3) { ?>				  
			  				
				<?php echo $this->Form->input('sort_rating', array("options" => array("Labs" => "Labs", "Doctors" => "Doctors"), "empty" => "Search By", 'class' => "selectpicker", 'id' => 'sort_rating', "value" => (isset($set_val['sort_rating']) && !empty($set_val['sort_rating'])) ? $set_val['sort_rating'] : '', 'label' => false, 'div' => false)); ?>
				<div class="clear"></div>
				
				<?php } else if($add_type == 'Labs'){ ?>
					<?php echo $this->Form->input('sort_rating', array("options" => array("All" => "All","City" => "City","State" => "State","Zip"=>"Zip","Name"=>"Name","Distance"=>"Distance","Rating" => "Rating"), "empty" => "Sort By", 'class' => "selectpicker", 'id' => 'sort_rating', "value" => (isset($set_val['sort_rating']) && !empty($set_val['sort_rating'])) ? $set_val['sort_rating'] : '', 'label' => false, 'div' => false)); 
					} else if($add_type == 'Doctors') { ?>
					<?php echo $this->Form->input('sort_rating', array("options" => array("All" => "All","City" => "City","State" => "State","Zip"=>"Zip","Name"=>"Name","Distance"=>"Distance"), "empty" => "Sort By", 'class' => "selectpicker", 'id' => 'sort_rating', "value" => (isset($set_val['sort_rating']) && !empty($set_val['sort_rating'])) ? $set_val['sort_rating'] : '', 'label' => false, 'div' => false)); 
				 } ?>
				</div>
			</div>
			<div class="clear"></div>
			<?php if($this->Session->read('Auth.User.user_type') == 3) { ?>
			<span id="Upload_csv" class="pull-right upload-csv">Upload Csv</span>
			<?php } ?>
			
			  
			<div class="clear"></div>
			<div class="responsive-table">
				<table class="table table-striped">
					<tr>
						<th style="text-align: center;">Name</th>
						<th>
							<?php if(isset($users[0]['User']['user_type']) && $users[0]['User']['user_type'] == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Rating&sort=".(($sort == 'asc')?'desc':'asc'); ?>">Rating</a>
							<?php } ?>
							</th>
						<th>
							<?php if(isset($users[0]['User']['user_type']) && $users[0]['User']['user_type'] == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Pricerating&sort=".(($sort == 'asc')?'desc':'asc'); ?>">Price</a>
							<?php } ?>
						</th>
						<th>
							<?php if(isset($users[0]['User']['user_type']) && $users[0]['User']['user_type'] == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Distance&sort=".(($sort == 'asc')?'desc':'asc'); ?>">Distance</a>
							<?php } ?>
						</th>
						<th>&nbsp</th>
					</tr>
					<?php if (!empty($users)) { //pr($users); 
						
						foreach ($users as $user): ?> 					
							<tr> 
								<td width="30%">
									<div class="listing-user-img">
										<?php 
										if ( $user['User']['user_type'] == 1 ) { 
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/docprofile.jpg");
										} else if ( $user['User']['user_type'] == 2 ) { 
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/labprofile.jpg");
										} else {
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no_img.png");
										}
										
										
										//$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no-img.png");
										$profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, MediumProfileImagePrefix);
										echo $this->Html->image($profileImgThumb1, array("alt" => ($user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name']), "width" => "70px", "height" => "70"));?>
									</div>
									<div class="listing-user-txt">
										<h3><?php echo substr($user['User']['company_name'], 0, 40); ?></h3>
										<?php echo substr($user['Userdetail']['address'], 0, 100); ?><br>
										<?php echo (isset($user['Userdetail']['city']) ? $user['Userdetail']['city'] : ''); ?>
										<?php echo (isset($user['User']['StateName']) ? $user['User']['StateName'] : ''); ?>
										<?php echo (isset($user['Userdetail']['zipcode']) ? $user['Userdetail']['zipcode'] : ''); ?><br/>
										<?php echo (isset($user['User']['CountryName']) ? $user['User']['CountryName'] : ''); ?>						
										
									</div>
								</td>
								<td><?php if($user['User']['user_type'] == 2) {
									 echo $this->element("ratingstars", array("rating" => $user['User']['rating'])); 
									 } ?>
								</td>
								<td><?php if($user['User']['user_type'] == 2) {
									 echo $this->element("pricerating", array("rating" => $user['User']['pricerating'])); 
									 } ?>
								</td>
								<td style="padding: 7px 0px 0px;">
									<?php if(isset($shwDistance) && $add_type == 'Labs') { echo $user['User']['distance'].' Miles.'; }  ?>
								</td>
								<td> 
									<div class="pull-right">
										<?php if (($this->Session->read('Auth.User.user_type') == 1) && ($add_type == 'Labs')) { ?>
											<div class="view-btn-outer">	
												<?php echo $this->Html->link("Place Order", array("controller" => "orders", "action" =>'order',$user['User']['id']), array("class" => "view-btn")); ?>
											</div>&nbsp;
											<div class="view-btn-outer">	
												<?php if (empty($user['User']['is_member'])) { ?>													
													<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Add to Favourite" data-attr="add" >Add to Favourite</a> 
												<?php } else { ?>
													<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Remove from Favourite" data-attr="remove">Remove from Favourite</a>
												<?php } ?>
											</div>
										<?php } ?> &nbsp;
										<div class="view-btn-outer">	
											<?php echo $this->Html->link("View Profile", array("controller" => "profile", "action" => $user['User']['id'], $this->Common->makeurl(($user['User']['company_name']))), array("class" => "view-btn","title"=>"View Profile")); ?>
										</div>	
									</div>
								</td>
							</tr> 
				<?php endforeach;
			} else {?> 
                <td colspan="3" style="color:red;" class="leftalign">No record found</td>  
            <?php } ?>
        </table>
    </div>
   <?php echo $this->Element("pagination"); ?>   		
<?php echo $this->Form->end(); ?>
</div>


