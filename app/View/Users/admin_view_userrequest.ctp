<div class="users view">

   <h2><?php  echo __('Requested User'); ?></h2>
   <dl>

      <dt><?php echo __('Name'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['name']) ? 'N-A' : $user['Adduser']['name']; ?>
         &nbsp;
      </dd>

      <dt><?php echo __('Email'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['email']) ? 'N-A' : $user['Adduser']['email']; ?> 
         &nbsp;
      </dd>
      <dt><?php echo __('User Type'); ?></dt>
      <dd><?php if(!empty($user['Adduser']['user_type']))
         {
         	if($user['Adduser']['user_type'] == 1) {  echo "Doctor"; }         	
         	else { echo "Lab"; }
         	
         } else { echo "N-A"; } ?>			
         &nbsp;
      </dd>
     <dt><?php echo __('Phone'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['phone']) ? 'N-A' : $user['Adduser']['phone']; ?>			
         &nbsp;
      </dd>
      <dt><?php echo __('Address'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['address']) ? 'N-A' : $user['Adduser']['address']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Description'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['description']) ? 'N-A' : $user['Adduser']['description']; ?>
         &nbsp;
      </dd>
      <dt><?php echo __('Created date'); ?></dt>
      <dd>
         <?php echo empty($user['Adduser']['created']) ? 'N-A' : $user['Adduser']['created']; ?>
         &nbsp;
      </dd>
      
      
      <dt><?php
		echo $this->Html->link(__("Add"),array("action"=>"addstaffmember"),array('class' => 'btn btn-grey')); 
	     ?><a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a></dt>
   </dl>
</div>

