<?php echo $this->Html->script("jquery"); ?>
<script type="text/javascript" src="/js/jquery.creditCardValidator.js"></script>

<script>
function cardtype(){
		  $('.log').attr('style','display: block');
	  }

$(function() {
        $('#cardnumber').validateCreditCard(function(result) {

			
		
            $('.log').html('Card Type : '+(result.card_type == null ? 'Checking...' : result.card_type.name));
              
               $('#paymentstatus').val(result.valid);
	
        });
		//console.log(result);
		
    });
function valid(){
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var re16digit = /^\d{16}$/;
var cvc=/^[0-9]{3,4}$/;
//var exp2=document.getElementById('exp-month').value+'/'+document.getElementById('exp-year').value;
//document.getElementById('exp').value=exp2;
//alert(document.getElementById('exp').value);

  if (!re16digit.test(document.getElementById('cardnumber').value)) {
        alert("Please enter your 16 digit credit card numbers");
        return false;
    }
    else if(document.getElementById('paymentstatus').value=='false')
    {
		
	 alert("Please Enter correct Card number");
        return false;	
	}
    
    else if(!cvc.test(document.getElementById('cvc').value))
    {
	 alert("Please enter correct CVV number");
        return false;	
	}
	else if(document.getElementById('CarddetailExmonth').value==''||document.getElementById('CarddetailExyear').value=='')
    {
	 alert("Please enter expiry date");
        return false;	
	}
return true;
}

</script>


<div class="row">
    <?php echo $this->Session->flash(); ?>
    <div class="clear">&nbsp;</div>
    <?php echo $this->element('userdetailsleft'); ?>
    <section class="col-sm-8 right-panel">
        <h2>Change Card Detail<br />
            <span>&nbsp;</span>
        </h2>
 <?php
       echo $this->Form->create('User', array("enctype" => "multipart/form-data", 'onsubmit'=>'return valid();', 'autocomplete'=>'off'));
 
 echo $this->Form->input('Carddetail.cardnumber', array("placeholder" => "Card number", "div" => true, "label" => false, "class" => "form-control", "type" => "text", "minlength" =>16,"maxlength" =>16, 'onkeyup' => 'return cardtype()','id'=>'cardnumber','autocomplete'=>false));			
?>
<span id="detail" class="log"></span>
<div class="row">
	<div class="col-md-4">
	<?php
echo $this->Form->input('Carddetail.cvv', array("placeholder" => "CVV", "div" => true, "label" => false, "class" => "form-control", "type" => "text",'id'=>'cvc',"maxlength" =>4,'autocomplete'=>false));
		?>
		</div>
		<div class="col-md-4">
	<?php	
	echo $this->Form->input('Carddetail.exmonth', array("options"=>$months,"div" => true, "label" => false, "class" => "selectpicker","empty"=>"Select Month")); ?>
	</div>
	<div class="col-md-4">
	<?php
	echo $this->Form->input('Carddetail.exyear', array("options"=>$years,"div" => true, "label" => false, "class" => "selectpicker","empty"=>"Select Year"));		
	?>	
	
	</div>
	</div>
	<?php echo $this->Form->input("paymentstatus",array("type"=>"hidden", 'div'=>false,'label'=>false ,'id'=>'paymentstatus' )); ?>
		<div class="text-center">
            <div class="login-btn-outer">
                <?php echo $this->Form->submit(__('Change', true), array('class' => 'login-btn')); ?>
            </div>
        </div>
		
        <?php echo $this->Form->end(); ?>
        
        
</section>		
</div>

<style>
.log{
	display:none;
  color: darkgreen;	
}
.btn-group{
margin-top:15px;
}
</style>
