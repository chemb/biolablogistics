<style>
    .show-errors {
        display: none;
        margin-top: -10px;
    }
    
    fieldset {
       padding-right: 11px; 
    }
    
    .no-scroll {
        border: 1px solid #909090;
        border-bottom: 0;
        overflow-x: hidden;
        overflow-y: hidden;        
    }
    
    .for-scroll {
        height: auto;
        max-height: 300px;
        border: 1px solid #909090;
        border-top: 0;
        overflow-x: auto;
        overflow-y: auto;
    }
    
    
    table.errors {
        margin: 0;
        border: 0;
    }

    table.errors tr th {
        background: none;
        border: 1px solid #ddd;        
    }
    
    table.errors tr td {
        border: 1px solid #ddd;
        height: 15px;
    }
</style>
<div class="categories form">
    <div class="show-errors well">   
        <?php if (isset($errors)) : ?>
        <div class="no-scroll" >            
            <table class="table errors" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 129px; min-width: 129px; max-width: 129px;"><?= __('Company Name') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('First Name') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Last Name') ?></th>
                        <th style="width: 204px; min-width: 204px; max-width: 204px;"><?= __('Email') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Password') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Service Tag') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Phone') ?></th>
                        <th style="width: 204px; min-width: 204px; max-width: 204px;"><?= __('Address') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('City') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('State Id') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Zip Code') ?></th>
                        <th style="width: 104px; min-width: 104px; max-width: 104px;"><?= __('Licence') ?></th>
                        <th style="width: 79px; min-width: 79px; max-width: 79px;"><?= __('User Type') ?></th>
                        <th style="width: 15px; min-width: 15px; max-width: 15px;"></th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="for-scroll">
            <table class="table errors" cellspacing="0">
                <tbody>
                    <?php foreach ($errors as $each) : ?>
                    <tr>
                        <td style="width: 125px; min-width: 125px; max-width: 125px;"><?= $each[0] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[1] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[2] ?></td>
                        <td style="width: 200px; min-width: 200px; max-width: 200px;"><?= $each[3] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[4] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[5] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[6] ?></td>
                        <td style="width: 200px; min-width: 200px; max-width: 200px;"><?= $each[7] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[8] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[9] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[10] ?></td>
                        <td style="width: 100px; min-width: 100px; max-width: 100px;"><?= $each[11] ?></td>
                        <td style="width: 75px; min-width: 75px; max-width: 75px;"><?= $each[12] ?></td>
                    </tr>                    
                    <?php endforeach; ?>
                </tbody>
            </table>              
        </div>      
        <?php endif; ?>
    </div>
    <?php echo $this->Form->create('User', ['prefix'=>'admin', 'action'=>'upload', 'enctype'=>'multipart/form-data']); ?>
    <fieldset>
        <legend><?php echo __('Upload Customers CSV'); ?></legend>  
        <?php echo $this->Form->input('CsvFile', ['label'=>false, 'type'=>'file', 'id'=>'file-input', 'style'=>'width: auto;']); ?>
        <?php echo $this->Form->submit('Upload', ['id'=>'upload']); ?>
    </fieldset>
    <?php echo $this->Form->end(); ?>
</div>
<script>
    var previousLeft = 0;
    $(document).ready(function() {
        $('#upload').parent('div').css({'padding':'0'});
        $('.show-error').click(function () {
            $('.show-errors').toggle('fold');
        });
        
        $('.for-scroll').scroll(function() {
            var currentLeft =  $(this).scrollLeft();
            if (previousLeft !== currentLeft) {
                $('.no-scroll').scrollLeft(currentLeft);
                previousLeft = currentLeft;
                return false;
            }            
        });
        
        $('table.errors tr th').each(function(i) {
            var len = $('table.errors tr th').length;
            if (i === len-2) {
                $(this).css({'border-right':'0'});                
            } else if (i === len-1) {
                $(this).css({'border-left':'0'});                
            }
        });
    });
</script>