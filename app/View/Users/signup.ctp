<?php echo $this->Html->script("jquery"); ?>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.creditCardValidator.js"></script>
<script>
function cardtype(){
		  $('.log').attr('style','display: block');
	  }

$(function() {
        $('#cardnumber').validateCreditCard(function(result) {

			
		
            $('.log').html('Card Type : '+(result.card_type == null ? 'Checking...' : result.card_type.name));
              
               $('#paymentstatus').val(result.valid);
	
        });
		//console.log(result);
		
    });
function valid(){
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var re16digit = /^\d{16}$/;
var cvc=/^[0-9]{3,4}$/;
//var exp2=document.getElementById('exp-month').value+'/'+document.getElementById('exp-year').value;
//document.getElementById('exp').value=exp2;
//alert(document.getElementById('exp').value);

  if (!re16digit.test(document.getElementById('cardnumber').value)) {
        alert("Please enter your 16 digit credit card numbers");
        return false;
    }
    else if(document.getElementById('paymentstatus').value=='false')
    {
		
	 alert("Please Enter correct Card number");
        return false;	
	}
    
    else if(!cvc.test(document.getElementById('cvc').value))
    {
	 alert("Please enter correct CVV number");
        return false;	
	}
	else if(document.getElementById('CarddetailExmonth').value==''||document.getElementById('CarddetailExyear').value=='')
    {
	 alert("Please enter expiry date");
        return false;	
	}
return true;
}

</script>


    <?php if ($user_type == -1) {  ?>
      
        <div class="login-box signin-box">
            <h2><?php echo ($user_type == 1) ? 'Signup as a Doctor' : (($user_type == 2) ? 'Signup as a Lab' : 'Create an Account'); ?></h2>
              <p>&nbsp;</p>
            <div class="row">
                <div class="col-lg-6" >
                    <div class="boxes-bg">
                        <a href="<?php echo $this->Html->url("/signup-as-doctor"); ?>" class="associate-doctors" title="Sign up as a Doctor">Sign up as a Doctor</a>
                        <p>On-demand deliveries for your lab.</p>
                    </div>
                </div>

                <div class="col-lg-6" >
                    <div class="boxes-bg">
                        <a href="<?php echo $this->Html->url("/signup-as-lab"); ?>" class="associate-labs" title="Sign up as a Lab">Sign up as a Lab</a>
                        <p>On-demand deliveries for your lab.</p>
                    </div>
                </div>
                
                
            </div>
        </div>
    <?php } else {   ?>
<div class="login-box">	
     <h2>
     <?php if($user_type==1){echo 'Signup as a Doctor';} else if($user_type==2) {echo 'Signup as Lab'; } else if($user_type==3) { echo 'Signup as Driver';} else {echo 'Create an Account';}   ?>
     </h2>
              <p>&nbsp;</p>
        <p class="text-center">On-demand deliveries for your lab.</p>
        <?php echo $this->Session->flash(); ?>
        <?php 
        if($user_type==1){
         echo $this->Form->create('User', array("enctype" => "multipart/form-data")); 
        }else{
        echo $this->Form->create('User', array("enctype" => "multipart/form-data", 'onsubmit'=>'return valid();'));
        }
        ?>
         <?php echo $this->Form->input('company_name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Company name', "class" => "form-control")); ?>	
        <?php echo $this->Form->input('Userdetail.first_name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'First Name', "class" => "form-control")); ?>
        <?php echo $this->Form->input('Userdetail.last_name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Last Name', "class" => "form-control")); ?>
        <?php echo $this->Form->input('username', array("maxlength" => "150", 'class' => "left", 'label' => false, 'div' => true, 'placeholder' => 'Email', "class" => "form-control")); ?>
        <?php echo $this->Form->input('password', array("maxlength" => "15", "type" => "password", 'class' => "left ", 'label' => false, 'div' => true, 'placeholder' => 'Password', "class" => "form-control")); ?>	
        <?php echo $this->Form->input('confirmpassword', array("maxlength" => "15", "type" => "password", 'class' => "left ", 'label' => false, 'div' => true, 'placeholder' => 'Confirm password', "class" => "form-control")); ?>
      
        <?php if($user_type==1|| $user_type==2){echo $this->Form->input('Userdetail.serviceTags', array("options"=>$services,"div" => true, "label" => false, "class" => "selectpicker","empty"=>"Select Specialization"));} ?>
         
        <?php echo $this->Form->input('Userdetail.phone', array("placeholder" => "Phone", "class" => "form-control", "div" => true, "label" => false, "type" => "text", "maxlength" => 15)); ?>
		<?php echo $this->Form->input('Userdetail.address',array('class'=>'form-control placepicker',"type" => "textarea", "maxlength" => 500,"rows" =>4,'label'=>false, 'placeholder' => 'Address',)); ?>
		<?php //echo $this->Form->input('Userdetail.address', array("placeholder" => "Address", "div" => true, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
		<?php echo $this->Form->input('Userdetail.city', array("placeholder" => "Apt / Suite", "class" => "form-control city", "div" => true, "label" => false, "type" => "text", "maxlength" => 60)); ?>
        <?php //echo $this->Form->input('Userdetail.state_id', array('options' => $state,'class' => 'State selectpicker', "label" => false,"div" => true, 'empty' => 'Select State')); ?>
        <?php //echo $this->Form->input('Userdetail.zipcode', array("placeholder" => "Zipcode", "div" => true, "label" => false, "class" => "form-control zip", "type" => "text", "maxlength" => 6));
			
				?> 
		<input type="hidden" name="data[Userdetail][latitude]" id="UserdetailLatitude" class="lat" value="">	
		<input type="hidden" name="data[Userdetail][longitude]" id="UserdetailLongitude" class="lng" value="">
		 <?php if($user_type==1){ echo $this->Form->input('Userdetail.licence', array("placeholder" => "Licence Number", "div" => true, "label" => false, "class" => "form-control", "type" => "text", "maxlength" => 6));
				} else
				{ ?>
<h3>Payment Details</h3>
	<div class="add-box">
		<?php
	echo $this->Form->input('Carddetail.cardnumber', array("placeholder" => "Card number", "div" => true, "label" => false, "class" => "form-control", "type" => "text", "minlength" =>16,"maxlength" =>16, 'onkeyup' => 'return cardtype()','id'=>'cardnumber','autocomplete'=>false));			
	?>
	<span id="detail" class="log"></span>
	<div class="row">
	<div class="col-md-4">
	<?php
	echo $this->Form->input('Carddetail.cvv', array("placeholder" => "CVV", "div" => true, "label" => false, "class" => "form-control", "type" => "text",'id'=>'cvc',"maxlength" =>4,'autocomplete'=>false));
		?>
		</div>
		<div class="col-md-4">
		<?php	
	echo $this->Form->input('Carddetail.exmonth', array("options"=>$months,"div" => true, "label" => false, "class" => "selectpicker","empty"=>"Select Month")); ?>
	</div>
	<div class="col-md-4">
	<?php
	echo $this->Form->input('Carddetail.exyear', array("options"=>$years,"div" => true, "label" => false, "class" => "selectpicker","empty"=>"Select Year"));		
	?>	
	</div>
	</div>
	</div>
	<?php echo $this->Form->input("paymentstatus",array("type"=>"hidden", 'div'=>false,'label'=>false ,'id'=>'paymentstatus' )); ?>
		
		<?php		
				
				}?> 
        <div class="clear-fix"></div>
        <?php //code for popup ?>


        <a href="#myModal" class="left open_contact_popup" data-toggle="modal">Any Doubts? Ask Us</a>


        <div class="text-center">
            <div class="login-btn-outer">
                <?php echo $this->Form->submit(__('Signup', true), array('class' => 'login-btn')); ?>
            </div>
        </div>
        <p class="sign-up">Already a user? <a href="<?php echo $this->Html->url("/login"); ?>" title="Log in">Sign in</a></p>

        <?php echo $this->Form->end(); ?>
   
</div>
 <?php } ?>
 
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="<?php echo $this->webroot; ?>assets/jquery.placepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(".placepicker").placepicker();

$('.placepicker').change(function(e){
var add=$('.placepicker').val();
//alert(add);
 $.ajax({ url:"http://maps.googleapis.com/maps/api/geocode/json?address="+add+"&sensor=true",
         success: function(data){
       console.log(JSON.stringify(data));
                      /*for (var component in data.results[0]['address_components']) {
                         for (var i in data.results[0]['address_components'][component]['types']) {
                            if (data.results[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
                                state = data.results[0]['address_components'][component]['long_name'];
                                $('.city').val(data.results[0]['address_components'][4]['long_name']);
								if(data.results[0]['address_components'][5]['long_name'])
                                $('.zip').val(data.results[0]['address_components'][5]['long_name']);
                            }
                        }                     
                    }   */
					
            //alert(data.results[0].formatted_address);
			        for (var ii = 0; ii < data.results[0].address_components.length; ii++){
						var street_number = route = street = city = state = zipcode = country = formatted_address = '';
						var types = data.results[0].address_components[ii].types.join(",");
						if (types == "street_number"){
						  street_number = data.results[0].address_components[ii].long_name;
						}
						if (types == "route" || types == "point_of_interest,establishment"){
						  route = data.results[0].address_components[ii].long_name;
						}
						if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political"){
						  city = (city == '' || types == "locality,political") ? data.results[0].address_components[ii].long_name : city;
						 // $('.city').val(city);
						}
						if (types == "administrative_area_level_1,political"){
						  state = data.results[0].address_components[ii].short_name;
						}
						if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
						  zipcode = data.results[0].address_components[ii].long_name;
						  //$('.zip').val(zipcode);
						  //alert(zipcode);
						}
						if (types == "country,political"){
						  country = data.results[0].address_components[ii].long_name;
						}
					}
					
					var lat = data.results[0].geometry.location.lat;
					$('.lat').val(lat);
					var lng = data.results[0].geometry.location.lng;
					$('.lng').val(lng);
         }
   });
});
</script>
<style>
.log{
	display:none;
  color: darkgreen;	
}
.add-box{
border: 2px solid black;
padding: 10px;
}
</style>




