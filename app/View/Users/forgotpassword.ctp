
<script type="text/javascript">
	$(document).ready(function(){
		setInterval('$("#flashMessage").hide();',2000);
	});
</script>
<div class="login-box">
	<h2>Forgot your password </h2><br>
	<?php echo $this->Form->create("User");
		  echo $this->Session->flash();
		  echo $this->Form->input("User.username",array("type"=>"text","div"=>'usrnme_eml_bx',"label"=>false,"placeholder"=>"Email","class"=>"chng_pswd_fld form-control"));
	?>
	<div class="text-center">
		<div class="login-btn-outer"><?php echo $this->Form->Submit("Reset password",array('label'=>false,'div'=>true,'class'=>'reset-pass login-btn')); ?></div>
	</div>
	<div class="clear-fix"></div>
	<?php echo $this->Form->end(); ?>
	<div class="back-login text-center">Back to <a href="<?php echo $this->Html->url("/login"); ?>" title="login">Sign in</a></div>
</div>
