<?php //error_reporting(0); ?>
<script type="text/javascript">
	
	function doSearch() {
    var searchText = document.getElementById('searchTerm').value.toLowerCase();;
    var targetTable = document.getElementById('dataTable');
    var targetTableColCount;
            
    //Loop through table rows
    for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
        var rowData = '';

        //Get column count from header row
        if (rowIndex == 0) {
           targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
           continue; //do not execute further code for header row.
        }
                
        //Process data rows. (rowIndex >= 1)
        for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
            rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent.toLowerCase();;
        }

        //If search term is not found in row data
        //then hide the row, else show
        if (rowData.indexOf(searchText) == -1)
            targetTable.rows.item(rowIndex).style.display = 'none';
        else
            targetTable.rows.item(rowIndex).style.display = 'table-row';
    }
}
	</script>
        <style>
            .listing-user-txt p{width: 100%;}
        </style>
<div class="lab-list"> <?php //pr($services); ?>
	<?php echo $this->Session->Flash(); ?>
    <?php echo $this->Form->create("User", array("div" => false, 'type' => 'get',"enctype" => "multipart/form-data")); ?>	
		<?php if ($this->Session->read('Auth.User.id')) {
			$add_type_name = ($this->Session->read('Auth.User.user_type') == 1 ? 'Lab' : (($this->Session->read('Auth.User.user_type') == 2) ? 'Doctor' : 'Member'));?>
			<div class="add-btn-outer pull-right">				
				<?php $url = (($add_type_name == 'Lab') ? "/add-lab" : (($add_type_name == 'Doctor') ? "/add-doctor" : "/add-members")); ?>
				<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url($url) ?>" title="Suggest a<?php echo $add_type_name; ?>">Suggest a <?php echo $add_type_name; ?></a>
			</div>
		<?php }   ?>
		<h2 class="col-sm-3"><?php echo 'View'.' '.$add_type; ?></h2>
		
		<div class="col-sm-4"> 
			<div class="searchbox_ld">
				
				<?php echo $this->Form->input("searchval", array("type" => 'text', "placeholder" => 'Search by any header below', 'id'=>'searchTerm','class' => 'form-control search_box','onkeyup'=>"doSearch()", "div" => false, "label" => false, "value" => (isset($set_val['searchval']) && !empty($set_val['searchval'])) ? $set_val['searchval'] : '')); ?>
				<?php echo $this->Form->submit("Search", array("label" => false, "div" => false, "class" => 'invite-button nomargin submitsearch search-btn', "id" => 'submitbtn', "attr" => 'search')); 
				?>
				 
				
			</div><span id="sort_distance_msg" style="display:none;">Please enter zip-code.</span>
		</div>		
		<div class="col-sm-1">

<style>
#notificationContainer {
	left: -75px;
}
	.sorting-select{width:148px;}
	@media screen and (max-width:1024px){
	 .sorting-select{margin-left:80px;}
	}
@media (min-width: 768px){
.col-sm-3 {width: 15% !important;}
}
</style>
			<div class="sorting-select">
				
				<?php if($this->Session->read('Auth.User.user_type') == 3) { ?>				  
			  				
				<?php echo $this->Form->input('sort_rating', array("options" => array("Labs" => "Labs", "Doctors" => "Doctors"), "empty" => "Specialization", 'class' => "selectpicker", 'id' => 'sort_rating', "value" => (isset($set_val['sort_rating']) && !empty($set_val['sort_rating'])) ? $set_val['sort_rating'] : '', 'label' => false, 'div' => false)); ?>
				<div class="clear"></div>
				
				<?php } else { ?>
					<?php echo $this->Form->input('search_specialization', array("options" => $services, "empty" => "Specialization ", 'class' => "selectpicker", 'id' => 'sort_rating', "value" => (isset($set_val['search_specialization']) && !empty($set_val['search_specialization'])) ? $set_val['search_specialization'] : '', 'label' => false, 'div' => false)); 
				} ?>
				</div>
			</div>
			<div class="clear"></div>
<style>
.upcv:hover{
  color: #09978B;
  background: #09978B !important;
 }
</style>
			<?php if($this->Session->read('Auth.User.user_type') == 3) { ?>
			<span id="Upload_csv" class="pull-right upload-csv upcv" style="border: 1px solid #000 !important;padding: 5px;">Upload Csv</span>
			<?php } ?>
			
	<?php echo $this->Html->script('sorttable');?>
		
<style>
table.sortable thead {
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
/*#sorttable_sortfwdind{display: none;}*/
</style>

<style>
    .sorting {
    background-image: url("images/sort_both.png");
    background-repeat: no-repeat;
}
</style>
			<div class="clear"></div>
                        <div class="responsive-table">
				<table id="dataTable" class="sortable table table-striped">
					<thead>
						<tr>
                                                    <th <?php //if ($this->Session->read('Auth.User.user_type') == 2){ echo 'class="st"'; } ?>>
                                                        <?php if($this->Session->read('Auth.User.user_type') == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Name&sort=".(($sort == 'asc')?'desc':'asc'); ?>" style="<?php if($this->Session->read('Auth.User.user_type') == 3) {echo 'color:#1F2427';}else{echo 'color:#FFFFFF';}?>">Name</a>
                					<?php } ?>
							</th>
						<th <?php //if ($this->Session->read('Auth.User.user_type') == 1){ echo 'class="st"'; } ?>>
							<?php if($this->Session->read('Auth.User.user_type') == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Rating&sort=".(($sort == 'asc')?'desc':'asc'); ?>" style="<?php if($this->Session->read('Auth.User.user_type') == 3) {echo 'color:#1F2427';}else{echo 'color:#FFFFFF';}?>">Rating</a>
							<?php } ?>
							</th>
						<th <?php //if ($this->Session->read('Auth.User.user_type') == 1){ echo 'class="st"'; } ?>>
							<?php if($this->Session->read('Auth.User.user_type') == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Pricerating&sort=".(($sort == 'asc')?'desc':'asc'); ?>" style="<?php if($this->Session->read('Auth.User.user_type') == 3) {echo 'color:#1F2427';}else{echo 'color:#FFFFFF';}?>">Price</a>
							<?php } ?>
						</th>
						<th <?php //if ($this->Session->read('Auth.User.user_type') == 1){ echo 'class="st"'; } ?>>
							<?php if($this->Session->read('Auth.User.user_type') == 2) { ?>
							<a href="<?php echo $this->here."?searchval=".$searchval."&sort_rating=Distance&sort=".(($sort == 'asc')?'desc':'asc'); ?>" style="margin-left: 11px;<?php if($this->Session->read('Auth.User.user_type') == 3) {echo 'color:#1F2427';}else{echo 'color:#FFFFFF';}?>">Distance</a>
							<?php } ?>
						</th>
						<th> &nbsp; </th>
					</tr>
					</thead>

					 <tbody>
					<?php if (!empty($users)) { 
						
						
						foreach ($users as $user): 
						      $favid=$user['User']['id'].'<br>';
						      $userId=$this->Session->read('Auth.User.id');
						    
						     
						//print_r($user);?> 					
							<tr> 
								<td width="50%">
									<div class="listing-user-img">
										<?php 
										if ( $user['User']['user_type'] == 1 ) { 
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/docprofile.jpg");
										} else if ( $user['User']['user_type'] == 2 ) { 
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/labprofile.jpg");
										} else {
											$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no_img.png");
										}
										
										
										//$profileImgPathThumb1 = (($user['Userdetail']['image'] && file_exists(WWW_ROOT . $user['Userdetail']['image'])) ? $user['Userdetail']['image'] : "/img/no-img.png");
										$profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, MediumProfileImagePrefix);
										echo $this->Html->image($profileImgThumb1, array("alt" => ($user['Userdetail']['first_name'] . ' ' . $user['Userdetail']['last_name']), "width" => "70px", "height" => "70"));?>
									</div>
									<div class="listing-user-txt">
                                                                            <h5><strong><?php echo substr($user['User']['company_name'], 0, 40); ?></strong></h5>
										<p style="  font-size: larger;"><?php echo substr($user['Userdetail']['serviceTags'], 0, 40); ?></p>
										<p style="text-align:none !important;"><?php echo substr($user['Userdetail']['address'], 0, 100); ?><br>
                                                                                <?php
                                                                                if (!empty($user['Userdetail']['city'])) {
                                                                                    echo $user['Userdetail']['city'];
                                                                                }
                                                                                if (!empty($user['User']['StateName'])) {
                                                                                    if (!empty($user['Userdetail']['city'])) {
                                                                                        echo ', '.$user['User']['StateName'];
                                                                                    } else {
                                                                                        echo $user['User']['StateName'];
                                                                                    }
                                                                                }
                                                                                if (!empty($user['Userdetail']['zipcode'])) {
                                                                                    if (!empty($user['User']['StateName'])) {
                                                                                        echo ' '.$user['Userdetail']['zipcode'];
                                                                                    } else {
                                                                                        echo $user['Userdetail']['zipcode'];
                                                                                    }                            
                                                                                }
                                                                                ?>
										</p>
										<?php //echo (isset($user['Userdetail']['phone']) ? $user['Userdetail']['phone'] : ''); 
										echo (empty("(".substr($user['Userdetail']['phone'], 0, 3).") ".substr($user['Userdetail']['phone'], 3, 3)."-".substr($user['Userdetail']['phone'],6))?'**********':(empty($user['Userdetail']['privacy'])?"(".substr($user['Userdetail']['phone'], 0, 3).") ".substr($user['Userdetail']['phone'], 3, 3)."-".substr($user['Userdetail']['phone'],6):'************')) ;
										?>						
										
									</div>
								</td>
								<td style="vertical-align: middle;"><?php if($this->Session->read('Auth.User.user_type') == 2) {
									 echo $this->element("ratingstars", array("rating" => $user['User']['rating'])); 
									 } ?>
								</td>
								<td style="vertical-align: middle;"><?php if($this->Session->read('Auth.User.user_type') == 2) {
									 echo $this->element("pricerating", array("rating" => $user['User']['pricerating'])); 
									 } ?>
								</td>
								<td style="padding: 7px 0px 0px; text-align:center; min-width: 120px;">
									<?php if(isset($shwDistance) && $add_type == 'Labs') {  //echo $user['User']['distance'].' mi.'; 
										    print_r($this->requestAction('App/getdistance/'. $favid));
										}elseif(isset($shwDistance) && $add_type == 'Doctors'){
											print_r($this->requestAction('App/getdistance/'. $favid));
										}  ?>
								</td>
								
								
								<td> 
                                                                    <div class="pull-right;">
										<?php if (($this->Session->read('Auth.User.user_type') == 1) && ($add_type == 'Labs')) { ?>
											<div class="view-btn-outer">	
												<?php echo $this->Html->link("Place Order", array("controller" => "orders", "action" =>'order',$user['User']['id']), array("class" => "view-btn")); ?>
											</div> &nbsp;
											<div class="view-btn-outer">	
												<?php if (empty($user['User']['is_member'])) { ?>													
													<a class="view-btn add_lab view-btn-fix" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Add to Favourite" data-attr="add">Add to Favorite</a> 
												<?php } else { ?>
													<a class="view-btn" id="addlab_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Remove from Favourite" data-attr="remove">Remove from Favorite</a>
												<?php } ?>
											</div>
										<?php }elseif(($this->Session->read('Auth.User.user_type') == 2)){ ?> 
										     <div class="view-btn-outer">	
												<?php if(in_array($user['User']['id'], $favDoctors)){ ?>
                                                    <a class="view-btn make_favorite" id="favorite_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Remove from Favourite" data-attr="remove">Remove from Favorite</a>												
													 
												<?php } else { ?>
													<a class="view-btn make_favorite view-btn-fix" id="favorite_<?php echo $user['User']['id']; ?>" href="javascript:void(0)" title="Add to Favourite" data-attr="add">Add to Favorite</a>
												<?php } ?>
											</div>
										<?php }?>&nbsp;
										
										<div class="view-btn-outer">	
											<?php echo $this->Html->link("View Profile", array("controller" => "profile", "action" => $user['User']['id'], $this->Common->makeurl(($user['User']['company_name']))), array("class" => "view-btn","title"=>"View Profile")); ?>
										</div>	
									</div>
								</td>
							</tr> 
				<?php endforeach;
			} else {?> 
                <td colspan="3" style="color:red;" class="leftalign">No record found</td>  
            <?php } ?>
            <input type="hidden" name="labId" id="labId" value="<?php echo $labId?>">
	 </tbody>
        </table>
    </div>
   <?php echo $this->Element("pagination"); ?>   		
<?php echo $this->Form->end(); ?>
</div>
<script>
$('.make_favorite').click(function() {
	    var obj = $(this);
		var doc_id = obj.attr('id').split("_");
		var type = obj.attr('data-attr');
        var lab_id = $('#labId').val();
		
            $.ajax({
                type: "POST",
                url: '<?php echo $this->Html->url(array('action' => 'makeFavorite', 'controller' => 'users')); ?>', 
                data: {'lab_id': lab_id, 'doctor_id': doc_id[1], 'type': type}, //--> send id of checked checkbox on other page
                success: function (resp) {
				data = $.parseJSON(resp);
				if (!data.error) {
					if (type == 'add') {
						obj.attr("data-attr", 'remove');
						obj.html('Remove from Favorite');
						$('#favorite_' + doc_id[1]).attr('title', 'Remove from Favorite');

					} else if (type == 'remove') {
						obj.attr("data-attr", 'add');
						obj.html('Add to Favorite');
						$('#favorite_' + doc_id[1]).attr('title', 'Add to Favorite');

					}
					$('#err_msg').html('');
					$('.alert-danger').removeClass('show');
					$('.alert-danger').addClass('hide');
					$('#succ_msg').html(data.message);
					$('.alert-success').addClass('show');
					$('.alert-success').removeClass('hide');
					$('.alert-success').fadeOut(5000, function () {
						$('.alert-success').removeClass('show');
						$('.alert-success').addClass('hide');
					});
				} else if (data.error) {
					$('#succ_msg').html('');
					$('.alert-success').removeClass('show');
					$('.alert-success').addClass('hide');
					$('#err_msg').html(data.message);
					$('.alert-danger').addClass('show');
					$('.alert-danger').removeClass('hide');
					$('.alert-danger').fadeOut(5000, function () {
						$('.alert-danger').removeClass('show');
						$('.alert-danger').addClass('hide');
					});
				}
				},
				error: function (err) {
					$('#err_msg').html('An error occured.Please try later.');
					$('.alert-success').addClass('show');
					$('.alert-danger').show();
					$('.alert-success').removeClass('hide');
					$('.alert-success').fadeOut(50000, function () {
						$('.alert-success').removeClass('show');
						$('.alert-success').addClass('hide');
					});
				}
            });
      });

    $(document).ready(function() {
        $('#dataTable > thead > tr > th').each(function(i) {
            if (i !== $('#dataTable > thead > tr > th').length - 1) {
                $(this).addClass('st');
            }
        });        
    });
</script>