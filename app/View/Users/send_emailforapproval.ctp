<div class="row">
	<?php echo $this->Session->flash(); ?>
	<div class="clear-fix">&nbsp;</div>
	<?php echo $this->element('userdetailsleft');?>
	<section class="col-sm-8 right-panel">
		<h2>Send Email for Profile Approval</h2>
		<p>On-demand deliveries for your lab.</p><br>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $this->Form->create('User'); ?>
                <div class="text-center">
					<div class="login-btn-outer">
						<?php echo $this->Form->Submit("Send Email",array('label'=>false,'div'=>false,'class'=>'login-btn send_approval_email')); ?>	
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
			</div>
		</div>		
	</section>	
</div>


