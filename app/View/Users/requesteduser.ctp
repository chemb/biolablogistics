<div class="login-box">	
    <h2> <?php echo $this->Session->read("Auth.User.user_type") == 1 ? 'Suggest a Lab' : ($this->Session->read("Auth.User.user_type") == 2 ? 'Suggest a Doctor' : 'Suggest a Member') ?></h2>
    <p class="text-center">On-demand deliveries for your lab.</p>
    <span class="contact_us_heading"><?php echo $title_for_layout; ?></span>
    <?php echo $this->Session->flash(); ?>
    <?php
    echo $this->Form->create('Adduser', array('class' => "contact_us_frm"));
    echo $this->Form->input('name', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'class' => 'form-control', 'placeholder' => 'Name'));
    echo $this->Form->input('email', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'class' => 'form-control', 'placeholder' => 'Email'));
    echo $this->Form->input('phone', array("maxlength" => "50", 'class' => "left", 'label' => false, 'div' => true, 'class' => 'form-control', 'placeholder' => 'Phone', 'type' => 'text'));


    echo $this->Form->input('address', array('rows' => '4', 'cols' => '50', 'label' => false, 'div' => true, 'class' => 'form-control', 'class' => "left", 'placeholder' => 'Address'));

    echo $this->Form->input('description', array('rows' => '4', 'cols' => '50', 'label' => false, 'div' => true, 'class' => 'form-control', 'class' => "left", 'placeholder' => 'Description'));
    ?>


    <?php if ($this->Session->read("Auth.User.user_type") == 3) { ?>
        <?php echo $this->Form->input('user_type', array('class' => "selectpicker", 'label' => false, 'div' => array('class' => 'input  cntct_txtarea'), 'placeholder' => 'User Type', 'options' => array('' => 'User type', '1' => 'Doctor', '2' => 'Lab'))); ?>
    <?php } ?>


    <div class="clear-fix"></div>
    <div class="text-center">
        <div class="login-btn-outer">
            <?php echo $this->Form->submit(__('Submit', true), array('class' => 'login-btn')); ?>
        </div> &nbsp;
        <?php echo $this->Form->end(); ?>
        <div class="cancel-btn-outer">
            <?php $redirt = ($this->Session->read("Auth.User.user_type") == 1) ? '/labs' : (($this->Session->read("Auth.User.user_type") == 2) ? '/doctors' : '/members'); ?>
            <a href="<?php echo $this->Html->url($redirt); ?>" id="cntct_sbmt_cancl" class= "cancel-btn">Cancel</a>
        </div>
    </div>
</div>
