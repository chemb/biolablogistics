<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @version		: 1.0
 * @created by	: shivam sharma
 */
class UsersController extends AppController {
	
	
	public $userstatus = '';

	/*
	 * @function name	: beforefilter
	 * @purpose			: index page for site user
	 * @arguments		: none
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10rd Jan 2013
	 * @description		: NA
	 */

	function beforefilter() {
		if ($this->params['action'] == 'login') {
			if ($this->Cookie->read('Auth.User') && !$this->Session->read("Auth.User.id")) {
				$this->login();
			}
		}
		if ($this->params['action'] == 'logout') {
			$this->logout();
		}
		parent::beforefilter();
		$this->Auth->allow("login", "logout", "signup", "uniqueuser", "newpassword", "requestaccount", "forgotpassword", "getindustry", "getstates", "confirmregisteration", "Captcha", "choosetype", "viewprofile", "loginwith", "search","show_reviews");
		$admin = $this->Session->read("admin");
		$this->set('curruser', $this->Auth->user('id'));

		$this->checklogin();
		//pr($this->params);
	}

	/* end of function */


	/*
	 * @function name	: index
	 * @purpose			: index page for site user
	 * @arguments		: none
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10rd Jan 2013
	 * @description		: NA
	 */

	function index() {
		$this->layout = "frontend";
		$user = $this->Session->read("Auth.User.id");
		$this->set("index", "1");
		if (empty($user) && $this->Cookie->read('Auth.User')) {
			$this->__login();
		}
	}

	/* end of function */



	/*
	 * @function name	: login
	 * @purpose			: to show login page for users
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10th Jan 2013
	 * @description		: NA
	 */

	function login($url = NULL) {
		$this->layout = "frontend_new";
		$this->set("title_for_layout", "User Login");
		if ($this->Session->read('Auth.User.id')) {
			$this->response->disableCache();
			$this->redirect("/");
		}
		if ((isset($this->data) && !empty($this->data))) {
			$this->__login();
		} elseif ($this->Cookie->read('Auth.User')) {
			$user = $this->Session->read("Auth.User.id");
			if (empty($user)) {
				$this->__login();
			}
		}
	}

	/* end of function */


	/*
	 * @function name	: __login
	 * @purpose			: to check user authentication
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10rd Jan 2013
	 * @description		: it is a private function
	 */

	private function __login($url = NULL) {
		if (isset($this->data) && !empty($this->data)) {
			if ($this->Auth->login()) {
				$this->loginedUserInfo = $this->Auth->user();				
				if (!empty($this->loginedUserInfo) && $this->loginedUserInfo['status'] == -1 || $this->loginedUserInfo['status'] == 0) {
					$this->Session->setFlash("Your account is not activated yet.");
					$this->Auth->logout();
					$this->redirect(array("action" => "login"));
					exit;
				}
			} else {
				$this->Session->setFlash("Invalid Email or Password.");
				$this->Auth->logout();
				$this->redirect(array("action" => "login"));
				exit;
			}
		}

		/* code to perform remeber me functinality " remember_login " */
		//condition to check if remember me checkbox is checked or not, if checked a cookie Auth.Member will be written
		if (isset($this->data['User']['remember_me']) && !empty($this->data['User']['remember_me']) && !empty($this->loginedUserInfo)) {
			$cookie = array();
			$cookie['remembertoken'] = $this->encryptpass($this->data['User']['username']) . "^" . base64_encode($this->data['User']['password']);
			$data['User']['remembertoken'] = $this->encryptpass($this->data['User']['username']);
			$this->User->create();
			$this->User->id = $this->loginedUserInfo['id'];
			$this->User->save($data);
			$this->Cookie->write('Auth.User', $cookie, false, '+2 weeks');
		}
		//condition to check if cookie Auth.Member is set or not if set then automatically logged in
		if (empty($this->data)) {
			$cookie = $this->Cookie->read('Auth.User');
			if (!is_null($cookie)) {
				$cookie = explode("^", $cookie['remembertoken']);
				$this->User->recursive = 0;
				$user = $this->User->find("first", array("conditions" => array("User.remembertoken" => $cookie[0], "User.status" => 1), "fields" => array("User.username", "User.password")));
				$user['User']['password'] = base64_decode($cookie[1]);
				unset($user['User']['id']);
				$this->data = $user;

				if ($this->Auth->login()) {
					$this->loginedUserInfo = $this->Auth->user();
					//  Clear auth message, just in case we use it.
					$this->Session->delete('Message.auth');
				}
				//die("here");
			} else {
				$this->Auth->logout();
			}
		}
		/* end of code */
		if($this->loginedUserInfo['user_type'] == 1){
			$this->redirect("/preview-orders");
		} else if($this->loginedUserInfo['user_type'] == 2){
				$this->redirect("/view-orders");
		} else if($this->loginedUserInfo['user_type'] == 3){
			$this->redirect("/preview-orders-staff");
			
		}
		
		
	}

	/* end of function */

	function dashboard() {
		$this->layout = false;
		if ($this->Session->read("Auth.User.loginfrom") == 'Twitter') {
			$this->render("twitterredirect");
		} else {
			$this->set("title_for_layout", "Dashboard");
			$this->render("dashboard");
		}
	}

	function choosetype() {
		if (isset($this->data) && !empty($this->data)) {
			$this->User->create();
			$this->User->id = $this->Session->read("Auth.User.id");
			if ($this->User->save($this->data)) {
				$this->Session->write("Auth.User.type", $this->data['User']['type']);
				$this->redirect("/dashboard");
			}
		}
	}

	/*
	 * @function name	: logout
	 * @purpose			: to logout from user panel
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10rd Jan 2013
	 * @description		: NA
	 */

	function logout() {
		$this->Auth->logout();
		if ( $this->Cookie->read("Auth.User") ) {
			$this->Cookie->destroy();
		}
		$this->Session->delete("Auth");
		$this->Session->delete("Profile");
		$this->response->disableCache();
		$this->redirect("/");
	}

	/* end of function */


	/*
	 * @function name	: signup
	 * @purpose			: to register new user account
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 3rd May 2013
	 * @modified by		: Nishi Kant Sharma
	 * @modified on		: 2nd feb 2015
	 * @description		: This function is responsible to create new user account.
	 */

	function signup($user_type = -1) {
		$this->layout = "frontend_new";
		$accountstatus = '';
		$this->set("title_for_layout", "User Signup");
		$this->set(compact("user_type"));
		$this->loadModel('State');
		$this->State->recursive = -1;
		$this->set("state", $this->State->find("list", array("conditions" => array("State.status" => 1), "fields" => array("State.id", "State.name"))));
		if ($this->Session->read('Auth.User.id')) {
			$this->response->disableCache();
			$this->redirect("/");
		}
		$this->loadModel("Service");
		$services = $this->Service->find("list",array("conditions"=>array("status"=>1),"fields"=>array("name","name")));
		$services['Other'] = "Other";
		$this->set("services",$services);
		
		if (isset($this->data) && !empty($this->data)) {
			$this->loadModel("Userdetail");
			$this->User->set($this->data);
			$this->Userdetail->set($this->data);
			$errflag = 0;
			if ($this->User->validates()) {
				$errflag += 1;
			}
			if ($this->Userdetail->validates()) {
				$errflag += 1;
			}
			if ($errflag == 2) {
				$user = $this->data;
				$address = $this->getaddress($this->data['Userdetail']['zipcode']);
				$user['Userdetail']['latitude'] = $address['lat']; 
				$user['Userdetail']['longitude'] = $address['long'];
				$user['User']['password'] = $this->Auth->password($user['User']['password']);
				$user['User']['passwordstatus'] = $passwordstatus = $this->encryptpass($user['User']['username']);
				$user['User']['status'] = 0;
				$user['User']['user_type'] = $user_type;
				if ($this->User->save($user, array('validate' => false))) {
					$userdetails = $this->data;
					$userdetails['Userdetail']['user_id'] = $this->User->getLastInsertId();
					$this->Userdetail->save($userdetails);
					/* code to send email confirmation for signup */
					$user = $userdetails['Userdetail']['first_name'];
					$confirmlink = "<a href=" . SITE_LINK . "users/confirmregisteration/" . $passwordstatus . ">Click Here</a>";
					$linkurl = SITE_LINK . "users/confirmregisteration/" . $passwordstatus;
					$this->getmaildata(1);
					$this->mailBody = str_replace("{USER}", $user, $this->mailBody);
					$this->mailBody = str_replace("{LINK}", $confirmlink, $this->mailBody);
					$this->mailBody = str_replace("{LINKURL}", $linkurl, $this->mailBody);
					$this->sendmail($this->data['User']['username']);
					/* code to send email confirmation for signup */
					$this->Session->setFlash("Your registration with Bio Lab has been successfully done and a verification link has been sent to your registered email.", 'default', array("class" => "success_message"));
					$redirt = (($user_type == 1) ? "/signup-as-doctor" : "/signup-as-lab");
					$this->redirect($redirt);
				}
			}
		}
	}

	/* end of function */

	/*
	 * @function name	: search
	 * @purpose			: Display listing for doctors and labs
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 18th feb 2015
	 * @description		: NA
	 */
//mahi

	function search($user_type = null) {
		if ( !$this->Session->read("Auth.User.id") ) {
			$this->Session->setFlash("You need to login before view labs or doctors listing.");
			$this->redirect("/login");
		}
		$searchval = '';
		if ( isset($this->params->query['searchval']) && !empty($this->params->query['searchval'])) {
			$searchval = $this->params->query['searchval'];
		}
		$this->set("searchval",$searchval);
		
		$this->layout = "frontend_new";
		//$order = "User.created desc";
		$order = "";
		$flag = false;
		$sort = 'asc';
		if (isset($this->request->query['sort']) && !empty($this->request->query['sort'])) {
			$sort = $this->request->query['sort'];
		}
		$this->set("sort",$sort);
		
		if ($user_type != 3) {
			$this->conditions = array("User.user_type" => $user_type, "User.profile_status" => 1, "User.status" => 1);
		} else {
			$this->conditions = array("User.user_type" => array(1, 2), "User.profile_status" => 1, "User.status" => 1);
		}
		/* code to perform search functionality */	
		
		if (isset($this->request->query) && !empty($this->request->query)) {  // Read data from query string.
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {				
				$address = $this->getaddress($this->request->query['searchval'], true);				
				if (!empty($address)) {
					$this->conditions = array_merge($this->conditions, array("OR" => array("Userdetail.first_name like" => "%" . $this->request->query['searchval'] . "%", "User.company_name like" => "%" . $this->request->query['searchval'] . "%","Userdetail.city like" => "%" . $this->request->query['searchval'],"User.StateName like" => "%" . $this->request->query['searchval']."%","Userdetail.serviceTags like" => "%" . $this->request->query['searchval'] . "%","Userdetail.zipcode like" => "%" . $this->request->query['searchval'] . "%")));
				}
			}
			if (isset($this->request->query['search_specialization']) && !empty($this->request->query['search_specialization'])) {
				$order = "User.created ".$sort;
				$this->conditions = array_merge($this->conditions, array('Userdetail.serviceTags' => $this->request->query['search_specialization']));
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Labs') {
				$order = "User.created ".$sort;
				$this->conditions = array_merge($this->conditions, array('User.user_type' => 2));
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Doctors') {
				$order = "User.created ".$sort;
				$this->conditions = array_merge($this->conditions, array('User.user_type' => 1));
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Rating') {
				$order = "User.rating ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Pricerating') {
				$order = "User.pricerating ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'City') {
				$order = "Userdetail.city ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'State') {
				$order = "User.StateName ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Zip') {
				$order = "Userdetail.zipcode ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Name') {
				$order = "User.company_name ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Distance') {				
				$order = "User.distance ".$sort;
			}
			if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'All') {
				$order = "User.created ".$sort;
			} elseif ($flag) {
				
			}
			$this->set("set_val", $this->request->query);
		}
		
		if ( !$flag && $this->Session->read("Auth.User.id") ) {
			$address = $this->getaddress($this->Session->read("Auth.User.Userdetail.zipcode"), true);	
			if (!empty($address)) {
				$lat = $address['lat'];
				//$this->Session->write("latit", $lat);
				
				$lon = $address['long'];
				if ( empty($order )) {
					$order = "User.distance desc";
				}
				$flag = true;
			}
		}

		/* end of code to perform search functionality */
		$add_type = ($user_type == 1) ? "Doctors" : (($user_type == 2) ? "Labs" : "Members");
		$fields = array("User.id","User.user_type",'User.company_name', "User.rating", "Userdetail.first_name", "Userdetail.first_name", "Userdetail.last_name", "Userdetail.city", "Userdetail.serviceTags", "Userdetail.address","Userdetail.zipcode", "Userdetail.image", "User.profile_status", "Userdetail.state_id", "Userdetail.phone", "Userdetail.country_id","User.StateName","User.CountryName","User.pricerating");
		$this->User->recursive = 0;
		$this->User->virtualFields = array(
			"rating" => "select AVG(rating) as average from user_ratings where rater_id = User.id",
			"StateName" => "select concat(state.name,', ',state.code) from states state INNER JOIN userdetails Userdetail on state.id = Userdetail.state_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
			"CountryName" => "select country.name from countries country INNER JOIN userdetails Userdetail on country.id = Userdetail.country_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
			"pricerating" => "select AVG(pricerating) as averageprice from user_ratings where rater_id = User.id",
		);
		
		if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type") == 1 && $user_type == 2) {
			$this->User->virtualFields = array_merge($this->User->virtualFields, array("is_member" => "SELECT count(*) FROM `doctor_labs` dl INNER JOIN users u WHERE dl.lab_id = u.id and u.user_type = 2 and dl.doctor_id = " . $this->Session->read("Auth.User.id") . " and dl.lab_id = User.id"));
			array_push($fields, 'User.is_member');
		}
		if ($flag) {			
			$this->User->virtualFields = array_merge($this->User->virtualFields, array("distance" => 'ROUND(69.0 * DEGREES(ACOS(COS(RADIANS(' . $lat . '))			
                 * COS(RADIANS(latitude))
                 * COS(RADIANS(' . $lon . ') - RADIANS(longitude))
                 + SIN(RADIANS(' . $lat . '))
                 * SIN(RADIANS(latitude)))),1)'));                
			array_push($fields, 'User.distance');
			$shwDistance = 'yes';
			$this->set(compact("shwDistance"));			
		}
	    if (empty($this->request->query['sort_rating']) && in_array('User.distance',$fields)) {
			//$order = "User.distance asc";		
		}
		$this->User->unBindModel(array('hasMany' => array('Adduser')));
		$this->paginate = array("fields" => $fields, "order" => $order);
		$this->set('users', $this->paginate($this->conditions));
		$this->set(compact("add_type"));
		$this->loadModel("Service");
		$services = $this->Service->find("list",array("conditions"=>array("status"=>1),"fields"=>array("name","name")));
		$services[] = "All";
		$services['Other'] = "Other";
		$this->set("services",$services);
		$this->render('view_lab_doctor');
		
		
	}

	/* end of function */

	/*
	 * @function name	: favourite_lab
	 * @purpose			: Display listing of favourite labs for doctor
	 * @arguments		: NA
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 25th March 2015
	 * @description		: NA
	 */
//mahi
	function favourite_lab() {
		$this->layout = "frontend_new";
		$this->loadModel('doctorLab');
		$flag = false;
		if ( !$flag && $this->Session->read("Auth.User.id") ) {
			$address = $this->getaddress($this->Session->read("Auth.User.Userdetail.zipcode"), true);	
			if (!empty($address)) {
				$lat = $address['lat'];
				//$this->Session->write("latit", $lat);
				
				$lon = $address['long'];
				if ( empty($order )) {
					$order = "User.distance desc";
				}
				$flag = true;
			}
		}
		
		
		$labs = $this->doctorLab->find('list', array('conditions' => array('doctorLab.doctor_id' => $this->Session->read('Auth.User.id')), 'fields' => array('doctorLab.lab_id')));
		$fields = array("User.id", "Userdetail.first_name",'User.company_name', "User.rating", "Userdetail.user_id", "Userdetail.zipcode", "Userdetail.first_name", "Userdetail.last_name", "Userdetail.serviceTags", "Userdetail.city", "Userdetail.address", "Userdetail.image", "User.profile_status", "Userdetail.state_id", "Userdetail.serviceTags", "Userdetail.phone", "Userdetail.country_id","User.StateName","User.CountryName", "User.pricerating");
		$this->User->virtualFields = array(
		"rating" => "select AVG(rating) as average from user_ratings where rater_id = User.id",
		"StateName" => "select concat(state.name,', ',state.code) from states state INNER JOIN userdetails Userdetail on state.id = Userdetail.state_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
			"CountryName" => "select country.name from countries country INNER JOIN userdetails Userdetail on country.id = Userdetail.country_id INNER JOIN users Users ON Users.id = Userdetail.user_id where Users.id = User.id",
		"pricerating" => "select AVG(pricerating) as averageprice from user_ratings where rater_id = User.id",
		);
		if ($flag) {			
			 $this->User->virtualFields = array_merge($this->User->virtualFields, array("distance" => 'ROUND(69.0 * DEGREES(ACOS(COS(RADIANS(' . $lat . '))			
                 * COS(RADIANS(latitude))
                 * COS(RADIANS(' . $lon . ') - RADIANS(longitude))
                 + SIN(RADIANS(' . $lat . '))
                 * SIN(RADIANS(latitude)))),1)'));                
			array_push($fields, 'User.distance');
			$shwDistance = 'yes';
			$this->set(compact("shwDistance"));			
		}
		
		$this->paginate = array("fields" => $fields);
		$this->conditions = array('User.id' => $labs);
		if (isset($this->request->query) && !empty($this->request->query)) {  // Read data from query string.
			if (isset($this->request->query['searchval']) && !empty($this->request->query['searchval'])) {
				$this->conditions = array_merge($this->conditions, array("OR" => array("Userdetail.first_name like" => "%" . $this->request->query['searchval'] . "%", "User.company_name like" => "%" . $this->request->query['searchval'] . "%", "Userdetail.zipcode like" => "%" . $this->request->query['searchval'] . "%")));
			} else if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'Rating') {
				$this->paginate = array_merge($this->conditions, array("order" => "User.rating desc"));
			} else if (isset($this->request->query['sort_rating']) && $this->request->query['sort_rating'] == 'All') {
				$this->paginate = array_merge($this->conditions, array("order" => "User.rating asc"));
			} else {
				$this->paginate = array_merge($this->conditions, array("order" => "User.created desc"));
			}
			$this->set("set_val", $this->request->query);
		}
		$this->set('users', $this->paginate($this->conditions));
	}

	/* end of function */

	function confirmregisteration($id = null) {
		$this->layout = "frontend_new";
		//$this->layout = "frontend";
		$this->User->recursive = -1;
		if (!empty($id)) {
			$user = $this->User->find("first", array("conditions" => array("User.passwordstatus" => $id)));
		}
		if (empty($user)) {
			$this->Session->setFlash("Invalid User.");
		} elseif ($user['User']['status'] == 1) {
			$this->Session->setFlash("Account is already activated.");
		} else {
			$this->User->create();
			$this->User->id = $user['User']['id'];
			$updateuser['User']['status'] = 1;
			$updateuser['User']['passwordstatus'] = 0;
			$this->User->save($updateuser);
			$this->Session->setFlash("Congrats, your account is activated.", 'default', array("class" => "success_message"));
		}
		$this->redirect(array("action" => "login"));
	}

	function captcha() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if (!isset($this->Captcha)) { //if Component was not loaded throug $components array()
			$this->Captcha = $this->Components->load('Captcha', array(
				'width' => 150,
				'height' => 50,
				'theme' => 'default', //possible values : default, random ; No value means 'default'
			)); //load it
		}
		$this->Captcha->create();
	}

	/*
	 * @function name	: newpassword
	 * @purpose			: to set password after registration of user
	 * @arguments		: token will be given with aet password url.
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 15rd Jan 2013
	 * @description		: This function is responsible set new password for newly registered user. User can set password only once.
	 */

	function newpassword($token) {
		//$this->layout = "frontend";
		$this->layout = "frontend_new";
		$this->User->recursive = 0;
		$user = $this->User->find("first", array("conditions" => array("User.remembertoken" => $token)));
		if (isset($this->data) && !empty($this->data)) {
			$this->User->create();
			$this->User->id = $this->data['User']['id'];
			$data['User']['password'] = $this->Auth->password($this->data['User']['password'], $this->data['User']['username']);
			$data['User']['confirmpassword'] = $this->Auth->password($this->data['User']['confirmpassword'], $this->data['User']['username']);
			$data['User']['status'] = 1;
			if ($this->User->save($data)) {
				$this->Cookie->destroy();
				$this->Session->destroy();
				$this->Session->setFlash("Your account is successfully confirmed.", 'default', array("class" => "success_message"));
				$this->redirect("/login");
			}
		} elseif (!empty($user)) {
			if (!empty($user['User']['password'])) {
				$this->Session->setFlash("You have already set your password.");
				$this->redirect("/login");
			} else {
				$this->data = $user;
			}
		} else {
			$this->Session->setFlash("Invalid link.");
			$this->redirect("/login");
		}
	}

	/* end of function */


	/*
	 * @function name	: admin_index
	 * @purpose			: to show listing of users for admin of site
	 * @arguments		: Following arguments have been passed:
	 * status	: status of users listed like 'Apprroved','Rejected'
	 * @return			: none
	 * @created by		: shivam sharma
	 * @created on		: 10rd Jan 2013
	 * @description		: NA
	 */

	public function admin_index($type = NULL) {
		$this->bulkactions();
		$heading = $type == 'request' ? 'Approve Users' : 'Users';
		/* code to perform search functionality */
		if (isset($this->data) && !empty($this->data['User']['searchval'])) {
			$this->Session->write('searchval', $this->data['User']['searchval']);
			$this->conditions = array("OR" => array("User.username like" => "%" . $this->data['User']['searchval'] . "%"));
		}

		if (isset($this->params['named']['page'])) {

			if ($this->Session->read('searchval')) {
				$this->conditions = array("OR" => array("User.username like" => "%" . $this->Session->read('searchval') . "%"));
				$this->data['User']['searchval'] = $this->Session->read('searchval');
			}
		} elseif (empty($this->conditions)) {
			$this->Session->delete('searchval');
		}
		if (!empty($type) && $type == 'request') {
			$this->conditions = array_merge($this->conditions, array("User.profile_status" => 0));
		} else {
			$this->conditions = array_merge($this->conditions, array("User.profile_status" => 1));
		}
		$this->set(compact("heading"));
		/* end of code to perform search functionality */
		$this->paginate = array("order" => "User.created desc");
		$this->User->recursive = 0;
		$this->set('users', $this->paginate($this->conditions));
	}
	
	public function driver_index($type = NULL) {
		$this->bulkactions();
		$heading = $type == 'request' ? 'Approve Users' : 'Users';
		/* code to perform search functionality */
		if (isset($this->data) && !empty($this->data['User']['searchval'])) {
			$this->Session->write('searchval', $this->data['User']['searchval']);
			$this->conditions = array("OR" => array("User.username like" => "%" . $this->data['User']['searchval'] . "%"));
		}

		if (isset($this->params['named']['page'])) {

			if ($this->Session->read('searchval')) {
				$this->conditions = array("OR" => array("User.username like" => "%" . $this->Session->read('searchval') . "%"));
				$this->data['User']['searchval'] = $this->Session->read('searchval');
			}
		} elseif (empty($this->conditions)) {
			$this->Session->delete('searchval');
		}
		if (!empty($type) && $type == 'request') {
			$this->conditions = array_merge($this->conditions, array("User.profile_status" => 0));
		} else {
			$this->conditions = array_merge($this->conditions, array("User.profile_status" => 1));
		}
		$this->set(compact("heading"));
		/* end of code to perform search functionality */
		$this->paginate = array("order" => "User.created desc");
		$this->User->recursive = 0;
		$this->set('users', $this->paginate($this->conditions));
	}


	/* end of function */


	/*
	 * @function name	: admin_addstaffmember
	 * @purpose			: Add staff member, Doctor,Labs from admin panel .	
	 * @return			: none
	 * @created by		: Nishi Kant Sharma
	 * @created on		: 3rd feb 2015
	 * @description		: Here admin can add staff members,Doctor,Labs from admin panel and send mail, once account get created.
	 */

	
}
