<div class="categories form">
<?php echo $this->Form->create('User',array('novalidate'=>true)); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
	<?php

		echo $this->Form->input('Userdetail.first_name');
		echo $this->Form->input('Userdetail.last_name');
		echo $this->Form->input('username', array('label' =>'Email'));
		echo $this->Form->input('user_type',array('class'=>"left ",'label'=>false,'div'=>false,'placeholder'=>'User Type','options' => array('' => 'User type','1' => 'Doctor', '2' => 'Lab','3' => 'Staff Member'))); 
		
	//	echo $this->Form->input('');
	
	?>
    </fieldset>
    <a class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
<?php echo $this->Form->submit(); ?>
  
 <?php echo $this->Form->end(); ?>
  
</div>
