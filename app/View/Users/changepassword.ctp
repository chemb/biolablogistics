<div class="login-box">
		<h2>Change Password</h2>
		<?php echo $this->Session->flash(); ?>
		<div class="changepassword_bx">
		<?php echo $this->Form->create("User"); ?>
			<?php echo $this->Form->input("currentpassword",array("type"=>'password',"class"=>'form-control','label'=>false,'div'=>'chng_pswd_flds','placeholder'=>'Current Password')); ?>
			<?php echo $this->Form->input("password",array("type"=>'password',"class"=>'form-control','label'=>false,'div'=>'chng_pswd_flds','placeholder'=>'New Password')); ?>
			<?php echo $this->Form->input("confirmpassword",array("type"=>'password',"class"=>'form-control','label'=>false,'div'=>'chng_pswd_flds','placeholder'=>'Confirm Password')); ?>
			<?php echo $this->Form->input("id",array("type"=>'hidden','label'=>false,"value"=>$this->Session->read("Auth.User.id"))); ?>
			 <div class="clear">&nbsp;</div>
        <div class="text-center">
            <div class="login-btn-outer">
				<?php echo $this->Form->Submit("Save",array('label'=>false,'div'=>false,'class'=>'savebtn login-btn')); ?>
                </div>
        </div>	
		<?php echo $this->Form->end(); ?>
		</div>	
		</div>
	
