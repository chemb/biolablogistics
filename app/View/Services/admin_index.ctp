<div class="users index">
	<?php echo $this->Form->create("Service",array("div"=>false)); ?>
	<?php echo $this->element("admins/common",array("place"=>'Search by name',"flag"=>false,"pageheader"=>$heading,"buttontitle"=>'Add service',"listflag"=>true,"action"=>'admin_add')); ?>	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="leftalign"><?php echo $this->Paginator->sort('status'); ?></th>				
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	if(!empty($services))
	{
	 foreach ($services as $service): ?>
	 <tr>		
		<td><?php echo $this->Form->input("id.".$service['Service']['id'],array("class"=>'chk',"value"=>$service['Service']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?><?php echo $this->Form->input("status.".$service['Service']['id'],array("type"=>'hidden',"value"=>($service['Service']['status'] == 1?0:1))); ?></td>
		<td class="leftalign"><?php echo substr($service['Service']['name'],0,50);  ?>&nbsp;</td>
		<td class="leftalign"><?php echo $service['Service']['status'] == 1 ? 'Active':'Inactive';  ?>&nbsp;</td>
		
		
	
		<td class="actions">
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $service['Service']['id']), null, __('Are you sure you want to delete service %s?', $service['Service']['name'])); ?>
			<?php echo $this->Html->link(__("Edit"),array("action"=>"edit", $service['Service']['id']));  ?> 
				
				
		</td>
	</tr>
<?php endforeach; 
    } else 
    { ?>
		<td colspan='8' class="leftalign" style='color:red; text-align:center;'>No record found</td>
		
 <?php   }   ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->Form->end(); ?>
