<div class="categories form">
<?php echo $this->Form->create('Service',array('novalidate'=>true,"name"=>"service_admin_edit_form")); ?>
    <fieldset>
        <legend><?php echo __('Edit Service'); ?></legend>
	<?php

		echo $this->Form->input('Service.name');		
		echo $this->Form->input('Service.status',array('class'=>"left ",'label'=>false,'div'=>false,'placeholder'=>'Service Type','options' => array('' => 'Service type','1' => 'Active', '0' => 'Inactive')));		 	
		echo $this->Form->input('Service.id', array('type' => 'hidden','value'=>$this->data['Service']['id'])); 
	?>
    </fieldset>
    <a class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
<?php echo $this->Form->submit(); ?>
  
 <?php echo $this->Form->end(); ?>
</div>
