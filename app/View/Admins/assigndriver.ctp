<?php 

?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
	//var dateToday = new Date(); 
	$(function() {
    $( "#datepicker" ).datepicker({ minDate:0});
    formatDate:'m/d/Y'
  });
  $(function() {
    $( "#datepicker2" ).datepicker({ minDate:0});
    formatDate:'m/d/Y'
  });
	
	
	
	function callajax(res){
		//alert(res);
	$.ajax({
		url: "<?php echo Router::url(array('controller'=>'admins','action'=>'labname'));?>",
		
		type: "POST",
		data:'res='+res ,
		success:function(data){
			//alert(data);
			var a = JSON.parse(data);
			//alert(JSON.stringify(data));
			document.getElementById("labname").value=a.company_name;
			document.getElementById("labid").value=a.id;
			document.getElementById("docid").value=a.doc_id;
			document.getElementById("docname").value=a.doc_name;
			//document.getElementById("labdoctor").value=a.labdoctor;
			document.getElementById("labdoctorname").value=a.labdoctor;
			//alert(document.getElementById("labdoctor").value);
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
}
function callajax2(res){
		//alert(res);
	$.ajax({
		url: "<?php echo Router::url(array('controller'=>'admins','action'=>'labname'));?>",
		
		type: "POST",
		data:'res='+res ,
		success:function(data){
			//alert(data);
			var a = JSON.parse(data);
			//alert(JSON.stringify(data));
			document.getElementById("labname2").value=a.company_name;
			document.getElementById("labid2").value=a.id;
			document.getElementById("docid2").value=a.doc_id;
			document.getElementById("docname2").value=a.doc_name;
			//document.getElementById("labdoctor2").value=a.labdoctor;
			//document.getElementById("labdoctor2").value=a.labdoctor;
			document.getElementById("labdoctorname2").value=a.labdoctor;
			//alert(document.getElementById("labdoctor").value);
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
}
	
</script>
<h2>Assign Driver</h2>
<br>

<?php  
$option = array('1'=>'Pickup from lab','2'=>'Pickup from Doctor');
echo $this->Form->input('select_action', array(
    'options' => array($option),
    'empty' => 'Select Action',
    'onchange'=> 'changediv(this.value);',
    'style'=>'height:40px; border-radius:5px; color:#b2312c; opacity:9;'
    
));

?>
<br>
<div id="pickup" style="display:none;">
<?php  
echo $this->Form->create('', array('action' => 'assigndriver' ,'onSubmit' => 'return validate();')); 
 
echo $this->Form->input('select_action',array('type'=>'hidden','value'=>'1'));
$name = array();

foreach($order_data as $order)
{
	$last_st = $this->requestAction('admins/getstatus/'.$order);
if($last_st != 'Remake Submitted'){
$name[$order] = $this->requestAction('admins/getorderid/'.$order);
}
}

echo $this->Form->input('order_id', array(
    'options' => array($name),
    'empty' => 'Select Order',
    'label'=>'Select Order',
    'onchange'=> 'callajax(this.value);',
    'style'=>'height:40px; border-radius:5px; color:#b2312c; opacity:9;'
    
   
));
echo '<br>';
echo $this->Form->input('lab_name',array('type'=>'text','id'=>'labname','disabled'=>'disabled'));
echo '<br>';
echo $this->Form->input('doc_name',array('type'=>'text','id'=>'docname','disabled'=>'disabled','label'=>'Doctor Name'));
echo '<br>';
//echo $this->Form->input('lab_doctor',array('type'=>'text','id'=>'labdoctor','disabled'=>'disabled','label'=>"Lab's Doctor Name"));
echo '<br>';
echo $this->Form->input('lab_id',array('type'=>'hidden','id'=>'labid'));
echo $this->Form->input('doc_id',array('type'=>'hidden','id'=>'docid'));
echo $this->Form->input('labdoctorname',array('type'=>'hidden','id'=>'labdoctorname'));

$d_name = array();
foreach($driverlist as $driver)
{
	//echo '<pre>'; 
//print_r($driver);
//echo '</pre>';
$d_name[$driver['User']['id']] = $driver['Userdetail']['first_name'];

}

echo $this->Form->input('driver_id', array(
    'options' => array($d_name),
    'empty' => 'Select Driver',
    'label'=>'Select Driver',
    'style'=>'height:40px; border-radius:5px; color:#b2312c; opacity:9;'
    
   
));
echo '<br>';
 echo $this->Form->input('date', array("type" => "text",'id'=> 'datepicker', 'class' => false, 'label' => 'Select pickup date')); 
 echo $this->Form->submit();
echo $this->Form->end();

?>
</div>

<!--------------------------------  Drop     --------------------------------------------------------->


<div id="drop" style="display:none;">
<?php  
echo $this->Form->create('', array('action' => 'assigndriver','onSubmit' => 'return validate2();')); 
 
echo $this->Form->input('select_action',array('type'=>'hidden','value'=>'2'));
$name2 = array();
//echo $this->Form->create('admin', array('action' => 'assigndriver')); 
foreach($order_data as $order)
{
	$last_st = $this->requestAction('admins/getstatus/'.$order);
if($last_st == 'Remake Submitted'){
  $name2[$order]= $this->requestAction('admins/getorderid/'.$order);
 }

}
echo $this->Form->input('order_id', array(
    'options' => array($name2),
    'empty' => 'Select Order',
    'label'=>'Select Order',
    'onchange'=> 'callajax2(this.value);',
    'style'=>'height:40px; border-radius:5px; color:#b2312c; opacity:9;',
    'id' =>'AdminOrderId2'
   
));

echo '<br>';
echo $this->Form->input('lab_name',array('type'=>'text','id'=>'labname2','disabled'=>'disabled'));
echo '<br>';
echo $this->Form->input('doc_name',array('type'=>'text','id'=>'docname2','disabled'=>'disabled','label'=>'Doctor Name'));
echo '<br>';
//echo $this->Form->input('lab_doctor2',array('type'=>'text','id'=>'labdoctor2','disabled'=>'disabled','label'=>"Lab's Doctor Name"));
echo '<br>';
echo $this->Form->input('lab_id',array('type'=>'hidden','id'=>'labid2'));
echo $this->Form->input('doc_id',array('type'=>'hidden','id'=>'docid2'));
echo $this->Form->input('labdoctorname2',array('type'=>'hidden','id'=>'labdoctorname2'));
$d_name = array();
foreach($driverlist as $driver)
{
	//echo '<pre>'; 
//print_r($driver);
//echo '</pre>';
$d_name[$driver['User']['id']] = $driver['Userdetail']['first_name'];

}

echo $this->Form->input('driver_id', array(
    'options' => array($d_name),
    'empty' => 'Select Driver',
    'label'=>'Select Driver',
    'style'=>'height:40px; border-radius:5px; color:#b2312c; opacity:9;',
    'id' => 'AdminDriverId2'
   
));
echo '<br>';
 echo $this->Form->input('date', array("type" => "text",'id'=> 'datepicker2', 'class' => false, 'label' => 'Select pickup date')); 
 echo $this->Form->submit();
echo $this->Form->end();
 
 ?>
</div>


<script type="text/javascript">
function validate()
{
//alert("ok");
if(document.getElementById('AdminOrderId').value=='')
{
alert("Please select Order.");
return false;
}
else if(document.getElementById('AdminDriverId').value=='')
{
alert("Please select driver.");
return false;
}
else if(document.getElementById('datepicker').value=='')
{
alert("Please select date.");
return false;
}
else
{
return true;
}
}

function validate2()
{
//alert("ok");
if(document.getElementById('AdminOrderId2').value=='')
{
alert("Please select Order.");
return false;
}
else if(document.getElementById('AdminDriverId2').value=='')
{
alert("Please select driver.");
return false;
}
else if(document.getElementById('datepicker2').value=='')
{
alert("Please select date.");
return false;
}
else
{
return true;
}

}	
	
	
function changediv(selectbox)
{
//var  = document.getElementById('change_type').value;
if(selectbox=='1'){
	document.getElementById('pickup').style.display="block";
	document.getElementById('drop').style.display="none";	
}
else if(selectbox=='2'){
	document.getElementById('pickup').style.display="none";
	document.getElementById('drop').style.display="block";	
}
else
{
document.getElementById('pickup').style.display="none";
document.getElementById('drop').style.display="none";	
}
}
</script>
<style>
input{
	width:30%;
	
}
select {
	width:32%;
}

</style>
