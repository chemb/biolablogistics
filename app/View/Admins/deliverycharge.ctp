<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />




<h2>Lab Delivery Charges</h2>
<table class="table table-striped table-bordered" id="example">
<thead>
<th>Cust Id</th>
<th>Lab Name</th>
<th>Delivery Charge</th>
<th>Mile Cost</th>
</thead>
<tbody>
<?php
$i =0;
foreach($lab_data as $lab_data1)
{
	 $company_name[$lab_data1['User']['company_name']] = $lab_data1['User']['company_name'];
?>
<tr>
<td><?php echo $lab_data1['User']['id']; ?></td>
<td><?php echo $company_name[$lab_data1['User']['company_name']]; ?></td>
<td><?php 
$l_id = $lab_data1['User']['id'];
$l_data1 = $this->requestAction("admins/getdelivery/".$l_id); 

if($l_data1 == ''){
echo '$5';
}else{
echo '$'.$l_data1['Deliverycharge']['delivery_amt'];
}
?></td>
<td><?php 
if($l_data1 == ''){
echo '$0.50/mile';
}else{
echo '$'.$l_data1['Deliverycharge']['mile_amt'].'/mile';
}
 ?></td>
</tr>
<?php     
}
?>
</tbody>
</table>
<br/>
<div style="margin-bottom: 83px;">
<?php //print_r($lab_data); 
$company_name = array();
echo $this->Form->create('admin', array('action' => 'deliverycharge', 'onsubmit'=>'return valid();')); 
foreach($lab_data as $lab_data1)
{
	 $company_name[$lab_data1['User']['id']] = $lab_data1['User']['company_name'];

     
}

echo $this->Form->input('labname', array('options' => array($company_name), 'class'=>'form-control','empty' => 'Select labs','label'=>'Labs Name','type'=>'select', 'multiple' => true), array('style'=>'width:400px;'));

echo "<br>";
echo $this->Form->input('delivery_amt',array('type' => 'text', 'class'=>'form-control', 'style'=>'width: 30%;','label'=>'Set Delivery Charge','onkeypress'=>'return numbersonly(this,event)')); 
echo $this->Form->input('mile_amt',array('type' => 'text', 'class'=>'form-control', 'style'=>'width: 30%;','label'=>'Set Mile Cost','onkeypress'=>'return validateNumber(event)'));
echo $this->Form->submit();
echo $this->Form->end();
?>
</div>
<script>
function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode === 8 || event.keyCode === 46
        || event.keyCode === 37 || event.keyCode === 39) {
        return true;
    }
else if(key == 46){
return true;
}
    else if ( key < 48 || key > 57) {
        return false;
    }
    else return true;
};



function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function valid(){
var lab =document.getElementById('adminLabname').value;
if(lab == ''){
alert('Please select the lab');
return false;
}
}

$(document).ready(function() {
    $('#example').DataTable({pageLength: 100});
    $("#serMemtb").attr("placeholder", "Type a Location");
} );
</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}

.submit input{
    width: 150px;
}
select[multiple=multiple] {
    width: 32%;
}
</style>
