<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />


<h2>Lab Prepayment</h2>
<table class="table table-striped table-bordered" id="example">
<thead>
<th>Cust Id</th>
<th>Lab Name</th>
<th>Prepay Amount</th>
</thead>
<tbody>
<?php
$i =0;
//print_r($lab_data);
foreach($lab_data as $lab_data1)
{
	 $company_name[$lab_data1['User']['company_name']] = $lab_data1['User']['company_name'];
?>
<tr>
<td><?php echo $lab_data1['Userdetail']['user_id']; ?></td>
<td><?php echo $company_name[$lab_data1['User']['company_name']]; ?></td>
<td><?php 
$l_id = $lab_data1['User']['id'];

$l_data1 = $this->requestAction("admins/getprepay/".$l_id); 

if($l_data1 == ''){
echo '0';
}else{
echo $l_data1.'%';
}

?></td>

</tr>
<?php     
}
?>
</tbody>
</table>
<br/>
<?php //print_r($lab_data); 
$company_name = array();
echo $this->Form->create('admin', array('action' => 'prepay')); 
foreach($lab_data as $lab_data1)
{
	 $company_name[$lab_data1['User']['id']] = $lab_data1['User']['company_name'];

     
}

echo $this->Form->input('labname', array('options' => array($company_name),'empty' => 'Select labs','label'=>'Labs Name','type'=>'select', 'multiple' => true,'width'=>'45%'), array('style'=>'width:400px;'));
echo "<br>";
echo $this->Form->input('amt',array('type' => 'number','label'=>'Set Prepay Amount')); 

echo $this->Form->submit();
echo $this->Form->end();
?>

<script>
$(document).ready(function() {
    $('#example').DataTable({pageLength: 100});
    $("#serMemtb").attr("placeholder", "Type a Location");
} );
</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}
.submit{
float: right !important;
margin-top: -52px !important;
}
.submit input{
    width: 150px;
    float:right;
}
</style>
