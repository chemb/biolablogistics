<?php 
error_reporting(0);
 ?>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />

<h2>Pickup Orders</h2>
<table class="table table-striped table-bordered" id="example">
	<thead>
	<tr>
	<th>Order ID</th>
	<th>From</th>
	<th>To</th>
	<th>Driver Name</th>
	<th>Status</th>
	<th>Pickup Date</th>
	
	</tr>
	</thead>
	<tbody>
<?php   
	//print_r($orders);
foreach($orders as $dri){
	//print_r($dri);
	$last_st = $this->requestAction('admins/getstatus/'.$dri['Driver']['order_id']);

?>
<tr>
<td>
<?php 
echo $this->requestAction('admins/getorderid/'.$dri['Driver']['order_id']);
?>
</td>
<td>
<?php //from as lab_id
$doc_detail = $this->requestAction('admins/companydetail/'.$dri['Driver']['lab_id']);
if($doc_detail['User']['user_type'] == 1){
echo '<b>Doctor Name : </b>'.$doc_detail['Userdetail']['first_name'].'<br>';
}else{echo '<b>Lab Name : </b>'.$doc_detail['User']['company_name'].'<br>';}
$state = $this->requestAction('admins/getstate/'.$doc_detail['Userdetail']['state_id']);
	$st = $state[0]['State']['name'].', '.$state[0]['State']['code'].' ';
echo $doc_detail['Userdetail']['address'].'<br>'.$doc_detail['Userdetail']['city'].', '.$st.''.$doc_detail['Userdetail']['zipcode'];


?>
</td>
<td>
<?php //to as doc_id
$doc_detail = $this->requestAction('admins/companydetail/'.$dri['Driver']['doc_id']);
if($doc_detail['User']['user_type'] == 1){
echo '<b>Doctor Name : </b>'.$doc_detail['Userdetail']['first_name'].'<br>';
}else {echo '<b>Lab Name : </b>'.$doc_detail['User']['company_name'].'<br>';}
$state2 = $this->requestAction('admins/getstate/'.$doc_detail['Userdetail']['state_id']);
	$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
echo $doc_detail['Userdetail']['address'].'<br>'.$doc_detail['Userdetail']['city'].', '.$st2.''.$doc_detail['Userdetail']['zipcode'];
?>
</td>
<td>
<?php 
$doc_detail = $this->requestAction('admins/companydetail/'.$dri['Driver']['driver_id']);
echo $doc_detail['Userdetail']['first_name'];
 ?>
 </td>
<td><?php echo $dri['Driver']['status'] ==0 ? 'Pickup' : 'Drop'; ?></td>
<td><?php echo date('m/d/y',strtotime($dri['Driver']['order_date'])); ?></td>

</tr>
<?php  } ?>
</tbody>
</table>
<script>
$(document).ready(function() {
    $('#example').DataTable({
        pageLength: 100
    });
   
} );
</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}

</style>
