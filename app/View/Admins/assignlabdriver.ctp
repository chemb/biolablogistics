<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<?php echo $this->Html->script(array("bootstrap.min")); ?>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />

<style>
    .modal-open {
        overflow: hidden;
    }
    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;        
    }     
    .modal {
        text-align: center;
        padding: 0!important;
    }    
    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px; /* Adjusts for spacing */
    }    
    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }    
    .modal.fade .modal-dialog {
        -webkit-transition: -webkit-transform .3s ease-out;
             -o-transition:      -o-transform .3s ease-out;
                transition:         transform .3s ease-out;
        -webkit-transform: translate(0, -25%);
            -ms-transform: translate(0, -25%);
             -o-transform: translate(0, -25%);
                transform: translate(0, -25%);
    }
    .modal.in .modal-dialog {
        -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
             -o-transform: translate(0, 0);
                transform: translate(0, 0);
    }
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
    }
    .modal-dialog {
        position: relative;
        width: auto;
        margin: 10px;
    }
    .modal-content {
        position: relative;
        background-color: #fff;
        -webkit-background-clip: padding-box;
                background-clip: padding-box;
        border: 1px solid #999;
        border: 1px solid rgba(0, 0, 0, .2);
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
                box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
    }
    .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-color: #000;
    }
    .modal-backdrop.fade {
        filter: alpha(opacity=0);
        opacity: 0;
    }
    .modal-backdrop.in {
        filter: alpha(opacity=50);
        opacity: .5;
    }
    .modal-header {
        min-height: 16.42857143px;
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
    }
    .modal-header .close {
        margin-top: -2px;
    }
    .modal-title {
        margin: 0;
        line-height: 1.42857143;
    }
    .modal-body {
        position: relative;
        padding: 15px;
    }
    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }
    .modal-footer .btn + .btn {
        margin-bottom: 0;
        margin-left: 5px;
    }
    .modal-footer .btn-group .btn + .btn {
        margin-left: -1px;
    }
    .modal-footer .btn-block + .btn-block {
        margin-left: 0;
    }
    .modal-scrollbar-measure {
          position: absolute;
        top: -9999px;
        width: 50px;
        height: 50px;
        overflow: scroll;
    }
    @media (min-width: 768px) {
        .modal-dialog {
          width: 600px;
          margin: 30px auto;
        }
        .modal-content {
          -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
                  box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
        }
        .modal-sm {
          width: 300px;
        }
    }
    @media (min-width: 992px) {
        .modal-lg {
          width: 900px;
        }
    }
    .modal-footer:before,
    .modal-footer:after {
        display: table;
        content: " ";
    }
    .modal-footer:after {
        clear: both;
    }
</style>

<style>
table.dataTable tbody td {   padding: 8px 5px !important;}
.dataTable {
  font-size: 11px;
}
input[type=submit]{
    padding: 0px 3px;
        font-size: 12px;
}
select {
font-size: 10px;
padding: 1px;
}
#header {
width: 103%;
}
h2.user-name {
margin-right: 70px;
}
#footer {
 width: 104%;
}

</style>
<script>
function hide(){
$('.hided').css({'display': 'block'});
}
</script>
<div class="search-box">
<?php if($archive == 1){ ?>
    <a href="<?php echo $this->Html->url('/admins/assignlabdriver') ?>" style="text-decoration: none;"><button>All Orders</button></a>
<?php }else { ?>
<a href="<?php echo $this->Html->url('/admins/assignlabdriver/1') ?>" style="text-decoration: none;"><button>Archived Orders</button></a>
<?php } ?>
</div>
<?php if($archive == 1){ ?>
<h2>Archive Orders</h2>
<?php }else { ?>
<h2>Lab Orders</h2>
<?php } ?>
<?php echo $this->Session->flash(); ?>
<!-- <input type="button" style="width: 100px;padding: 3px;font-size: 10px;" value="Payments" id="payment" onclick="hide();"> -->
<table class="table table-striped table-bordered" id="example">
	<thead>
	<tr>
	
	<th><?php echo $this->Form->input("Directorder.check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
	<th>&nbsp;</th>
	<th>Order ID</th>
	<th>Date</th>
	<th>Lab</th>
	<th>From</th>
	<th>To</th>
	<?php if($archive != 1) {?>
	<th class="hided">Pay</th> <?php }?>
	<th>Dropoff</th>
	<th>Pickup</th>
	<th>Price</th>
	<th>Priority</th>
	<!-- <th>Recepient</th> -->
	<th>Distance</th>
	<th>Payment Status</th>
	<th>Driver</th>
	<th>Action</th>
	</tr>
	</thead>
	<tbody>
	
	<tr>
	<?php 
	$option = array();
	foreach($driverlist as $driver){
	$option[$driver['User']['id']] = $driver['Userdetail']['first_name'];
	}
	if(!empty($orders)){
	foreach($orders as $order){
	 ?>
<td><?php echo $this->Form->input("id.".$order['Directorder']['oid'],array("class"=>'chk',"value"=>$order['Directorder']['oid'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?></td>	 
<td align="center">
<?php if($order['Directorder']['order_status'] != 'Delivered' && $order['Directorder']['aprice'] == ''){ ?>
<input type="button" style="width: 98%;padding: 3px;font-size: 10px;" value="Edit" id="id" onclick="pop1(<?php echo $order['Directorder']['oid']; ?>);">
<?php } ?>
  <?php echo $this->Html->link(__('Delete'), array('action' => 'order_delete', $order['Directorder']['oid']), ["class" => "button"], __('Are you sure you want to delete # %s?', $order['Directorder']['oid'])); ?>
</td>

<td align="center"><?php echo '#'.$order['Directorder']['oid'];?></td>
<td align="center"><?php echo $order['Directorder']['pick_date'];?></td>
<td align="center"><?php 
$lname = $this->requestAction('admins/companyname/'.$order['Directorder']['lab_id']); 
echo $lname;

echo $this->Form->input("lname",array("id"=>"lname_".$order['Directorder']['oid'], "type"=>"hidden",'div'=>false,'label'=>false,'value'=>$lname)); 
?></td>
<td align="center"><?php 
$state1 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['code'].' ';
			echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip'];
 ?></td>
<td align="center"><?php 
$state2 = $this->requestAction('App/getstate/'.$order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['code'].' ';
			echo $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'];
 ?></td>



<?php if($order['Directorder']['aprice'] == ''){ ?>
<input type="hidden" value="<?php echo $order['Directorder']['payment_status']; ?>" id="pay_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['priority']; ?>" id="priority_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['dropoff_zip']; ?>" id="dropzip_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['pickup_zip']; ?>" id="pickzip_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['pickup']; ?>" id="pickup_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['dropoff']; ?>" id="dropoff_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['price']; ?>" id="price_<?php echo $order['Directorder']['oid']; ?>" >
<input type="hidden" value="<?php echo $order['Directorder']['lab_id']; ?>" id="labid_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $total_p[$order['Directorder']['oid']]; ?>" id="popprice_<?php echo $order['Directorder']['oid']; ?>">

<?php if($archive != 1) {?><td class="hided" align="center">
<?php  
foreach($tr_data as $key => $value){
if($key == $order['Directorder']['oid'] && $order['Directorder']['payment_status'] != '1'){
$carddata = $this->requestAction('admins/carddata/'.$order['Directorder']['lab_id']);
echo '<form action="'.$url.'" method="post" onsubmit="return popshow('.$order['Directorder']['oid'].');">';
echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][first_name]",'div'=>false,'label'=>false,'id'=>'fname','value'=>$carddata['Carddetail']['fname']));
echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][last_name]",'div'=>false,'label'=>false,'id'=>'lname','value'=>$carddata['Carddetail']['lname']));
echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][email]",'div'=>false,'label'=>false,'id'=>'email','value'=>$carddata['Carddetail']['email']));
echo $this->Form->input("",array("type"=>"hidden", 'div'=>false,'label'=>false,'name'=>'transaction[credit_card][number]' ,'id'=>'cardnumber','value'=>$carddata['Carddetail']['cardnumber'] ));
echo $this->Form->input("cvc",array("value"=>$carddata['Carddetail']['cvv'],"type"=>"hidden","name"=>"transaction[credit_card][cvv]",'div'=>false,'label'=>false,'id'=>'cvc'));
echo $this->Form->input("exp-month",array("type"=>"hidden","name"=>"transaction[credit_card][expiration_date]","value"=>$carddata['Carddetail']['exmonth'].'/'.$carddata['Carddetail']['exyear'],'label'=>false));
echo $this->Form->input("tr_data",array("type"=>"hidden","name"=>"tr_data",'div'=>false,'label'=>false,'value'=>$value));
echo $this->Form->input("",array("type"=>"hidden","name"=>"uid","id"=>"uid","value"=>$order['Directorder']['lab_id'],'div'=>false,'label'=>false));
echo '<input type="submit" value="Pay">';
echo $this->Form->end();
}
}
?>


</td><?php }?>

<td align="center"><?php echo $order['Directorder']['pickup']; ?></td>
<td align="center"><?php echo $order['Directorder']['dropoff']; ?></td>
<td align="center"><?php echo '$'.$order['Directorder']['price']; ?></td>
<?php } else { $aprip = number_format((($order['Directorder']['price']*2.9/100)+$order['Directorder']['price']+0.30),2); ?>
<input type="hidden" value="<?php echo $order['Directorder']['payment_status']; ?>" id="pay_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['priority']; ?>" id="priority_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['dropoff_zip']; ?>" id="dropzip_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['pickup_zip']; ?>" id="pickzip_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['apickup']; ?>" id="pickup_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['adropoff']; ?>" id="dropoff_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $order['Directorder']['aprice']; ?>" id="price_<?php echo $order['Directorder']['oid']; ?>" >
<input type="hidden" value="<?php echo $aprip; ?>" id="price2_<?php echo $order['Directorder']['oid']; ?>" >
<input type="hidden" value="<?php echo $order['Directorder']['lab_id']; ?>" id="labid_<?php echo $order['Directorder']['oid']; ?>">
<input type="hidden" value="<?php echo $total_p[$order['Directorder']['oid']]; ?>" id="popprice_<?php echo $order['Directorder']['oid']; ?>">
<?php if($archive != 1) {?><td class="hided" align="center">
<?php  

foreach($tr_data as $key => $value){

if($key == $order['Directorder']['oid'] && $order['Directorder']['astatus'] != '1'){

 
$carddata = $this->requestAction('admins/carddata/'.$order['Directorder']['lab_id']);

echo '<form action="'.$url.'" method="post" onsubmit="return popshow('.$order['Directorder']['oid'].');">';

echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][first_name]",'div'=>false,'label'=>false,'id'=>'fname','value'=>$carddata['Carddetail']['fname']));

echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][last_name]",'div'=>false,'label'=>false,'id'=>'lname','value'=>$carddata['Carddetail']['lname']));

echo $this->Form->input("",array("type"=>"hidden","name"=>"transaction[customer][email]",'div'=>false,'label'=>false,'id'=>'email','value'=>$carddata['Carddetail']['email']));

echo $this->Form->input("",array("type"=>"hidden", 'div'=>false,'label'=>false,'name'=>'transaction[credit_card][number]' ,'id'=>'cardnumber','value'=>$carddata['Carddetail']['cardnumber'] ));

echo $this->Form->input("cvc",array("value"=>$carddata['Carddetail']['cvv'],"type"=>"hidden","name"=>"transaction[credit_card][cvv]",'div'=>false,'label'=>false,'id'=>'cvc'));

echo $this->Form->input("exp-month",array("type"=>"hidden","name"=>"transaction[credit_card][expiration_date]","value"=>$carddata['Carddetail']['exmonth'].'/'.$carddata['Carddetail']['exyear'],'label'=>false));

echo $this->Form->input("tr_data",array("type"=>"hidden","name"=>"tr_data",'div'=>false,'label'=>false,'value'=>$value));


echo $this->Form->input("",array("type"=>"hidden","name"=>"uid","id"=>"uid","value"=>$order['Directorder']['lab_id'],'div'=>false,'label'=>false));

echo '<input type="submit" value="Pay">';
echo $this->Form->end();
}
}
?>
</td> <?php } ?>

<td align="center"><?php echo $order['Directorder']['apickup']; ?></td>
<td align="center"><?php echo $order['Directorder']['adropoff']; ?></td>
<td align="center"><?php echo '$'.$order['Directorder']['aprice']; ?></td>
<?php } ?>
<td align="center"><?php echo $order['Directorder']['priority']; ?></td>
<td><?php
			$distance = $this->requestAction('admins/getzipdistance/'.$order['Directorder']['pickup_zip'].'/'.$order['Directorder']['dropoff_zip']);
			 echo $distance; 
			 ?></td>
<!-- <td align="center"><?php echo $order['Directorder']['recipient']; ?></td> -->
<td align="center"><?php echo ($order['Directorder']['payment_status'] == 0)? 'Pending':'Payment Done'; ?></td>
<td align="center"><?php
if($order['Directorder']['driver_id'] == '0'){
 echo $this->Form->input('driver', array(
         'options' => array($option),
         'empty' => 'Select Driver',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
          'id' =>'driver_'.$order['Directorder']['oid']
      ));
      }
      else{
      
$driv = $this->requestAction('admins/companydetail/'.$order['Directorder']['driver_id']);
echo $driv['Userdetail']['first_name'];
      }
 ?></td>
 <td align="center"><?php
 if($order['Directorder']['driver_id'] == '0'){
 $select = array('Pickup'=>'Assign Driver'); 
 echo $this->Form->input('action', array(
         'options' => array($select),
         'empty' => 'Select action',
         'myatt' => $order['Directorder']['oid'],
         'onchange' => 'update("'.$order['Directorder']['oid'].'")',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
          'id' =>'action_'.$order['Directorder']['oid']
      )); }
      /*
     elseif($order['Directorder']['order_status'] == 'Order Submitted'){
      $select = array('Pickup'=>'Pickup'); 
 echo $this->Form->input('action', array(
         'options' => array($select),
         'empty' => 'Select action',
         'myatt' => $order['Directorder']['oid'],
         'onchange' => 'update_st("'.$order['Directorder']['oid'].'")',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
          'id' =>'action_'.$order['Directorder']['oid']
      ));
      }
  elseif($order['Directorder']['order_status'] == 'Pickup'){
  $select = array('Delivered'=>'Delivered'); 
  echo $this->Form->input('action', array(
         'options' => array($select),
         'empty' => 'Select action',
         'myatt' => $order['Directorder']['oid'],
         'onchange' => 'update_st("'.$order['Directorder']['oid'].'")',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
          'id' =>'action_'.$order['Directorder']['oid']
      ));
  }*/
  else{
  echo 'Driver Assigned';
  }
 ?></td>
	</tr>
	<?php } } else{ ?>
<td colspan="11" style="color:red;">No Order Found</td>
<?php  } ?>
</tbody>
</table>
<div id="custom-modal" class="modal fade bs-example-modal-sm">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body" style="padding:50px 0 50px 0;">
                <p style="text-align:center;" id="alert-text"></p>
            </div>
        </div>
    </div>
</div>

<script>
function update(orderid){
driver = $('#driver_'+orderid).val();
action = $('#action_'+orderid).val();
pays = $('#pay_'+orderid).val();

if(pays != 1){
price = $('#price_'+orderid).val();
pickup = $('#pickup_'+orderid).val();
dropoff = $('#dropoff_'+orderid).val();

if(driver == ''){
alert('Please select the driver');
location.reload();
return false;

}
if(price == ''){
    price = 10;
    alert('Please enter price field');
    location.reload();
    return false;
}
if(pickup == ''){
alert('Please enter pickup field');
location.reload();
return false;

}
if(dropoff == ''){
alert('Please enter dropoff field');
location.reload();
return false;

}

$.ajax({
		url: "<?php echo Router::url(array('controller'=>'admins','action'=>'updateassignlabdriver'));?>",
		type: "POST",
		data: {'action': action , 'driver': driver , 'orderid':orderid , 'price': price , 'pickup': pickup , 'dropoff':dropoff, 'pays':pays},
		success:function(data){
                    if (driver !== 0) {
                        $('#alert-text').html('Driver assigned successfully.');
                        $('#custom-modal').modal();                       
                    } else {
                        alert(data);
                    }
                    location.reload();
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
}
else{
if(driver == ''){
alert('Please select the driver');
location.reload();
return false;

}
$.ajax({
		url: "<?php echo Router::url(array('controller'=>'admins','action'=>'updateassignlabdriver'));?>",
		type: "POST",
		data: {'action': action , 'driver': driver , 'orderid':orderid, 'pays':pays},
		success:function(data){
              alert(data);
    location.reload();
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });


}
}
function update_st(orderid){
action = $('#action_'+orderid).val();
$.ajax({
		url: "<?php echo Router::url(array('controller'=>'admins','action'=>'updateassignlabdriver'));?>",
		type: "POST",
		data: {'action': action, 'orderid':orderid},
		success:function(data){
              alert(data);
    location.reload();
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
}

$(document).ready(function() {
    $('#example').DataTable({
        pageLength: 100
    });
    $("#serMemtb").attr("placeholder", "Type a Location");
} );

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


function pop1(val1){
price = $('#price_'+val1).val();
pickup = $('#pickup_'+val1).val();
dropoff = $('#dropoff_'+val1).val();
priority = $('#priority_'+val1).val();
dropzip = $('#dropzip_'+val1).val();
pickzip = $('#pickzip_'+val1).val();
labid = $('#labid_'+val1).val();


$('#dropzip').val(dropzip);
$('#pickzip').val(pickzip);
$('#labid').val(labid);


$('#orderid').val(val1);
$('#apickup').val(pickup);
$('#adropoff').val(dropoff);
$('#tempdrop').val(dropoff);
$('#temppick').val(pickup);
$('#priority').val(priority);

$( "#dialog" ).dialog();
}

function popshow(orderid){
lname = $('#lname_'+orderid).val();
popprice = $('#popprice_'+orderid).val();
ppay = $('#pay_'+orderid).val();
ppri = $('#price2_'+orderid).val();
if(ppay == 1){
var x = confirm(lname+" lab will be charged $"+popprice+". Already Paid $"+ppri);
}else {
var x = confirm(lname+" lab will be charged $"+popprice);
}
if(x){
return true;
}else{
return false;
}
}


</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}
#dialog{
display:none;
  
}
#dialog input.text { margin-bottom:12px; width:95%; padding: .4em; }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



 
<div id="dialog" title="Update Details">
  <table>
  <form action="/admins/updatedirectorder/" method="post" >
  <td>Dropoff</td><td><span>
  <input style="width: 80%;" type="text" name="apickup" id="apickup" placeholder="Pickup">
  <input type="hidden" name="priority" id="priority">
  <input type="hidden" name="labid" id="labid">
  <input type="hidden" name="orderid" id="orderid">
  <input type="hidden" name="pickzip" id="pickzip">
  <input type="hidden" name="dropzip" id="dropzip">
  <input type="hidden" name="temppick" id="temppick">
  <input type="hidden" name="tempdrop" id="tempdrop">
  </span></td>
   <td>Pickup</td><td>
  <span>
  <input style="width: 80%;" type="text" name="adropoff" id="adropoff" placeholder="Dropoff">
  </span>
  </td>
  <td>&nbsp;</td>
  <td>
  <input type="submit"></td>
  </form>
  </table>
</div>
 
