<!DOCTYPE HTML>
<html>
<head>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->meta('icon'); ?>
	<meta name="title" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>
		<?php echo $title_for_layout; ?>
		<?php if ($this->params['controller'] == 'pages' && $this->params['action'] == 'index') { ?>
			<?php echo ""; ?>
		<?php } else { ?>
			<?php echo("Royal Road"); ?>
		<?php } ?>
	</title>
	<?php echo $this->Html->script(array('jquery.1.8.2','html5')); ?>
	<?php echo $this->Html->css(array("style"),NULL,array("media"=>"screen")); ?>  
	
<!--[if IE 9]>  
<?php echo $this->Html->css(array("ie8","style"),NULL,array("media"=>"screen")); ?>  
<![endif]--> 
<!--[if IE 9]>  
<?php //echo $this->Html->script(array('html5')); ?>
<![endif]--> 
<script type="text/javascript">
		var SITE_LINK = "<?php echo SITE_LINK; ?>";
		var BASE_URL = "<?php echo SITE_LINK; ?>";
		var DEFAULT_LINK = "<?php echo $this->webroot; ?>";
</script>

</head>
<body>
	<?php echo $this->element("header"); ?>
	<div id="wrapper">
		<?php if(isset($mobile) && $this->Session->read("mobile") &&  $this->Session->read("mobile") == 1) { ?>
			<!--<div class="mobile">Working on new version to be compatible with mobile devices.</div>-->
			<script>
				alert("Current site of Royal Roads is not Fully compatible with mobile devices. A few of the features might not work properly on the Mobile Devices.\n We are working on a newer version to be compatible with mobile devices.");
			</script>
		<?php } ?>
		<?php echo $content_for_layout; ?>
		
	</div>
	<?php echo $this->element("frontfooter"); ?>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
