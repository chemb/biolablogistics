<!DOCTYPE HTML>
<html>
    <head>
        
        <!--title>Royal Road Logistics</title-->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <?php // echo $this->Html->meta('favicon.ico','/favicon.ico',array('type' => 'icon')); ?>
        <?php echo $this->Html->meta('icon'); ?>
        <title>
			<?php echo $title_for_layout; ?>
			<?php if ($this->params['controller'] != 'pages') { ?>
			<?php echo(TITLE_EXT); ?>
			<?php } ?>
		</title>
        <!-- Bootstrap Core CSS -->
        <?php echo $this->Html->css(array("bootstrap.min","common")); ?>
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300normal,300italic,400normal,400italic,600normal,600italic,700normal,700italic,800normal,800italic;subset=all">
        <!--[if lte IE 8]><script src="js/html5.js"></script><![endif]-->
        <script type="text/javascript">
			var SITE_LINK = "<?php echo SITE_LINK; ?>";
			var BASE_URL = "<?php echo SITE_LINK; ?>";
			var DEFAULT_LINK = "<?php echo $this->webroot; ?>";
		</script>
    </head>
    <body>
		<div id="fade-whole"></div>
        <div class="main-wrapper">
            <?php echo $this->element("header_rrl"); ?>
			<div class="wrapper">
				<?php echo $content_for_layout; ?>
            </div>
            <div class="clear"></div>
        </div>
        <?php echo $this->element("footer_rrl"); ?>
        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
