<?php echo $this->Session->flash(); ?>
<!-- Banner Start-->
<div class="bx-wrapper">
	<div class="bx-viewport">
		<ul class="bxslider">
			<li><img src="img/banner-img01.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img02.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img03.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
		</ul>
	</div>
</div>
<!-- Banner Start-->
<!-- Three boxes Start-->
<div class="row">
	<div class="col-lg-4">
		<div class="boxes-bg">
			<a href="<?php echo $this->Html->url('/doctors'); ?>" class="associate-doctors" title="Associate Doctors">Associate Doctors</a>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="boxes-bg">
			<?php if (!$this->Session->read("Auth.User.id")) { ?>
				<a href="<?php echo $this->Html->url('/signin')?>" class="track-order" title="Track Your Order">Track Your Order</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
			<?php } else { ?>
				<a href="<?php echo $this->Html->url('/track-order')?>" class="track-order" title="Track Your Order">Track Your Order</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
			<?php } ?>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="boxes-bg">
			<a href="<?php echo $this->Html->url('/labs'); ?>" class="associate-labs" title="Associate Labs">Associate Labs</a>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac congue nunc, eu gravida diam.</p>
		</div>
	</div>
</div>
<!-- Three boxes Start-->

<div class="clear"></div>
