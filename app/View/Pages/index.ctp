<?php echo $this->Session->flash(); ?>
<link rel="stylesheet" type="text/css" href="assets/customstyle.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
<script>
var START_DATE = new Date("October 10, 2012 22:30:00"); // put in the starting date here

var INTERVAL = 900; // in seconds ( 15 min = 60x15 = 900)
var INCREMENT = 1; // thhe number increase per tick

var START_VALUE = 35001; // initial value when it's the start date
var count = 0;

$(document).ready(function() {
 var msInterval = INTERVAL * 1000;
 var now = new Date();
 count = parseInt((now - START_DATE)/msInterval) * INCREMENT - 103002;
 document.getElementById('counter').innerHTML = count;

 window.setInterval( function(){
    count += INCREMENT; 
    document.getElementById('counter').innerHTML = count;
 }, msInterval);
    
});
    

</script>

<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700,600);
.counter {

    font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;
	font-size: 100px;
	height: 162px;


}
</style>
<script src="assets/jequiry1.11.js"></script>
    <script src="assets/carousel.js"></script>
<!-- Banner Start-->
<div class="bx-wrapper">
	<div class="bx-viewport">
		<ul class="bxslider">
			<li><img src="img/banner-img01.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img02.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img03.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
		</ul>
	</div>
</div>
<!-- Banner Start-->
<!-- end video banner -->
  <div class="servicebox" style="background:url(http://biolablogistics.com/img/bg/concrete_seamless.png) repeat;">
   <div class="headr2">
  <h2>6-REASONS TO GET STARTED NOW</h2>
   <hr class="style-two">
</div>
    <ul>
      <li> <span class="doctor"><img src="assets/images/signup.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">$5,000 Signup Bonus</strong></h4>
        <p style="color:#000;">Start working with biolab and immediately receive $5,000 towards your business expenses!</p>
      </li>
      <li> <span class="track"><img src="assets/images/car.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">$5 Delivery</strong></h4>
        <p style="color:#000;">Increase your sales by expanding your delivery zone, without worrying about increasing overhead and payroll.</p>
      </li>
      <li> <span class="lab"><img src="assets/images/user.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">5 New Customers</strong></h4>
        <p style="color:#000;">Join biolab's extensive network of doctors, nurses and other medical prefessionals, and immediately gain 5 new customers.</p>
      </li>
      <div class="clearfix"></div>
    </ul>
    <br/> <br/>
    <ul>
      
      <li> <span class="track2"><img src="assets/images/paid.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">Paid Immediately</strong></h4>
        <p style="color:#000;">Tired of waiting 30, 45 and even 60-days to receive payment from vendors and clients? With biolab you are paid within 24-hrs for your products and services.</p>
      </li>
      <li> <span class="lab2"><img src="assets/images/minutes.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">27-Minutes</strong></h4>
        <p style="color:#000;">That is the average time it takes for your goods to get to their location.</p>
      </li>
      <li> <span class="doctor2"><img src="assets/images/flow.png" width="125" alt=""/></span>
        <h4><strong style="color:#25a5eb;">Follow Everything</strong></h4>
        <p style="color:#000;">Yes, everything. How many patients you served last year, types of procedures you did most, all of your prescriptions, and of course who owes you what.</p>
      </li>
      <div class="clearfix"></div>
    </ul>
  </div>
  <!-- end 3box area -->
  <div style="height:20px;"></div>
  <div class="graph">
  <div class="headr">
  <h2>COST OF ONE DELIVERY</h2>
   <hr class="style-two">
  <h4>Compare us to our competitors and see the difference! </h4>
</div>
  <table id="q-graph">
<thead>

</thead>
<tbody>
<tr class="qtr" id="q1">
<th scope="row"><strong>@5mi</strong></th>
<td class="fedex bar" style="height: 40px;"><p>$16</p></td>
<td class="uber bar" style="height: 50px;"><p>$21</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
<tr class="qtr" id="q2">
<th scope="row"><strong>@20mi</strong></th>
<td class="fedex bar" style="height: 92px;"><p>$38</p></td>
<td class="uber bar" style="height: 158px;"><p>$66</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
<tr class="qtr" id="q3">
<th scope="row"><strong>@45mi</strong></th>
<td class="fedex bar" style="height: 144px;"><p>$60</p></td>
<td class="uber bar" style="height: 338px;"><p>$141</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
</tbody>
</table>
<div id="ticks">
<div class="tick" style="height: 52px;"><p>$125</p></div>
<div class="tick" style="height: 52px;"><p>$100</p></div>
<div class="tick" style="height: 52px;"><p>$75</p></div>
<div class="tick" style="height: 52px;"><p>$50</p></div>
<div class="tick" style="height: 52px;"><p>$25</p></div>
<div class="tick" style="height: 52px;"><p>$0</p></div>
</div>
<div class="instraction">
<ul>
<li>FedEx</li>
<li>UberRush</li>
<li>biolab</li>
</ul>
</div>
  </div>
<!-- end graph -->
<div style="height:20px;"></div>
<div class="ordrtime">
 <div class="headr"><br>
  <h2>GET YOUR ITEMS IN MINUTES</h2>
   <hr class="style-two">
</div>
<div class="order-time-container">
      <div class="order-time-part1">
        The average order takes
      </div>
      <div class="order-time-counter">
        <div class="characters">
          <span class="character">
            <span>2</span>
          </span>
          <span class="character">
            <span>7</span>
          </span>
        </div>
      </div>
      <div class="order-time-part2">
        minutes to be delivered.
      </div>
      <div class="cleafix"></div>
    </div>
    <div class="order-time-container">
 <div class="headr2"><br/><br/>
  <h2>NUMBER OF DELIVERIES MADE IN 2016</h2>
   <hr class="style-two">
</div>
<div class="order-time-container2">
      <div class="order-time-counter2">
        <div class="character">
          <center><div id="counter" style="color: #fff; line-height: 162px; font-size: 102px; font-family: 'Courier New', Courier, 'Lucida Sans Typewriter', 'Lucida Typewriter', monospace; line-height:200px; letter-spacing: 35px; padding-left:40px; padding-top:5px; background:url(http://biolablogistics.com/img/counter-bgnew.png) no-repeat center;"></div></center>
        </div>
      </div>
    </div>

    </div>
    </div>
<!-------------------end Order Time -------->

    <div style="height:20px;"></div>
<!-------------------end Order Time 2-------->
 <div class="clientslogo">
  <div class="headr">
  <h2>YOU MAY HAVE SEEN US...</h2>
  <hr class="style-two">
  <h4>What's the buzz all about? Ask these guys! </h4>
</div>
     
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td bgcolor="#FFFFFF" height="30">&nbsp;</td>
</tr>
<tr>
  <td bgcolor="#ffffff" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td width="20">&nbsp;</td>
          <td align="center" class="client"><table align="center" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tbody>
                        <tr>
                          <td align="center" height="160" style="border-right:1px solid #e4e4e4;" valign="middle" width="255"><a href="http://www.cityam.com/" target="_blank"><img src="http://biolablogistics.com/img/client_icon1.jpg" width="180" height="60" alt=""/></a></td>
                          <td align="center" style="border-right:1px solid #e4e4e4;" valign="middle" width="255"><a href="http://techcrunch.com/" target="_blank"><img src="http://biolablogistics.com/img/client_icon2.jpg" width="180" height="60" alt=""/></a></td>
                          <td align="center" valign="middle" width="255"><a href="http://www.telegraph.co.uk/" target="_blank"><img src="http://biolablogistics.com/img/client_icon3.jpg" width="180" height="60" alt=""/></a></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
                <tr>
                  <td height="1" style="border-bottom:1px solid #e4e4e4"></td>
                </tr>
                <tr>
                  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tbody>
                        <tr>
                          <td align="center" height="160" style="border-right:1px solid #e4e4e4;" valign="middle" width="255"><a href="http://www.bloomberg.com/" target="_blank"><img src="http://biolablogistics.com/img/client_icon4.jpg" width="180" height="60" alt=""/></a></td>
                          <td align="center" style="border-right:1px solid #e4e4e4;" valign="middle" width="255"><a href="http://www.inc.com/" target="_blank"><img src="http://biolablogistics.com/img/client_icon5.jpg" width="180" height="60" alt=""/></a></td>
                          <td align="center" valign="middle" width="255"><a href="http://www.cnn.com/" target="_blank"><img src="http://biolablogistics.com/img/client_icon6.jpg" width="180" height="60" alt=""/></a></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
              </tbody>
            </table></td>
          <td width="20">&nbsp;</td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td bgcolor="#FFFFFF" height="30">&nbsp;</td>
</tr>
</table>

    
    
    
  </div>
  <div style="height:20px;"></div>
  <!-- end clients logo -->
  <div class="testimonials" style="height:610px;">
  <div class="headr">
  <h2>WHAT ARE BUSINESSES SAYING ABOUT US?</h2>
  <hr class="style-two">
  <h4 style="color:#FFF;">It's a great feeling when people tell you that you have made a difference! </h4>
</div>
    <div class="carousel-reviews broun-block">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">$5, really</a>
							    
						        <p>Seriously, how do you do it? $5 and my delivery zone has now expanded to Richmond, VA and Annapolis, MD? Yeah, I can get behind that!</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<a title="" href="#">John Sohn</a>
								<i>CEO & President, Korea Lab</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">Small startup</a>
							    
        						<p>They are a small team. I have actually met every single person at some point or another, but don't let that fool you though. These guys are changing the way products are delivered.</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
						        <a title="" href="#">Elsa Montree</a>
								<i>Executive Vice President, Big 1 Pharmacy</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Sales growing</a>
								
    							 <p>We had hit a plateau. In order to grow, we needed to expand our delivery area, but didn't want to increase spending on payroll, vehicles, insurance and tons of other expenses. Along came biolab, and now we can expand without the overhead.</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<a title="" href="#">Chris Ranyn</a>
								<i>Chief Technology Officer, Spectrum Health</i>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">Huge network</a>
							    
						         <p>They have a large corporate network. Almost every doctor we worked with, was on biolab's radar. Which really helps you make a connection.</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<a title="" href="#">Anna Ludlow</a>
								<i>Director of Sales, Genome Medical Supplies</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">$5,700</a>
							    
        						<p>We began working with biolab last summer, and everything was going well. Then one day I get a call telling us we hadn't used our $5k towards business expenses. No need to say, I was shocked.</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
						        <a title="" href="#">Chris Trege</a>
								<i>Owner, Trege Dental Labs</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Paid right away</a>
								
    							<p>In the past we have always had a problem receiving our payments on time from vendors. We are lucky if we receive payment within 45-60 days. Now, biolab pays us as soon as the hospital confirms they have received the case. Which most of the time is within 30-minutes. Why would any lab wait 60-days to get paid is beyond me.</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<a title="" href="#">Siri Patel</a>
								<i>Chief Financial Officer, Tier 1 Pharm</i>
							</div>
						</div>
                    </div>                    
                </div>
                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                    <span class="leftarrow"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                    <span class="rightarrow"></span>
                </a>
            </div>    
</div>
  </div>
  <!-- end Testimonial --> 
 <!-- Three boxes Start
<script>
	var unix = Math.round(+new Date()/1000);
	document.cookie=unix;
function PostMainChartValues()
{
	var unix = Math.round(+new Date()/1000);
	var unix=String(unix);
	 //alert(unix);
	var num=unix.slice(-5);
	var arrayelement = new Array();
	var arrayelement=num.split('');
	var finalstring='';
	for (i=0;i<arrayelement.length;i++)
{
   finalstring=finalstring+'<span>'+arrayelement[i]+'</span>';
}
  //alert(finalstring);
     //$('#test').html(finalstring);
  document.getElementById("test").innerHTML=finalstring;
 
}


$(document).ready(function(){
	//alert("ok");
	$.ajax({
		url: "http://biolablogistics.com/Pages/counter",
		type: "POST",
		success:function(response){
			//alert(JSON.stringify(response));
              //$('#tabble').html('');
              //$('#tabble').append(data);
            },
         error:function(response){
			// alert(JSON.stringify(response));
			 //alert('Waiting for data...');
		 }
        });
	
});



	/* var i = 1000;
    var funcNameHere = function(){
        //alert(i++);
    };
    // This block will be executed 100 times.
    setInterval(funcNameHere, 7000);
    funcNameHere();*/
	
	 

 
window.setInterval(function() {
    PostMainChartValues();
}, 5000);
</script>

<div class="counter">
<p id="innercounter">
	<span style="font-size:32px;" id='test'></span><br><br>
<span id="delivery">Number of deliveries made in <?php echo $date=date('Y').' '; ?></span>


<?php //$arr2 = str_split($count, 1);  foreach($arr2 as $val){ echo "<span>".$val."</span>"; } //echo "<span>".$count."</span>"; ?>
</p>
</div>
-->

<div class="clear"></div>