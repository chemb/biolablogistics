<?php echo $this->Session->flash(); ?>
<link rel="stylesheet" type="text/css" href="assets/customstyle.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
<script>
var START_DATE = new Date("October 10, 2012 22:30:00"); // put in the starting date here

var INTERVAL = 900; // in seconds ( 15 min = 60x15 = 900)
var INCREMENT = 1; // thhe number increase per tick

var START_VALUE = 35001; // initial value when it's the start date
var count = 0;

$(document).ready(function() {
 var msInterval = INTERVAL * 1000;
 var now = new Date();
 count = parseInt((now - START_DATE)/msInterval) * INCREMENT - 103002;
 document.getElementById('counter').innerHTML = count;

 window.setInterval( function(){
    count += INCREMENT; 
    document.getElementById('counter').innerHTML = count;
 }, msInterval);
    
});
    

</script>

<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700,600);
.counter {

    font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;
	font-size: 100px;
	height: 162px;


}
</style>
<script src="assets/jequiry1.11.js"></script>
    <script src="assets/carousel.js"></script>
<!-- Banner Start-->
<div class="bx-wrapper">
	<div class="bx-viewport">
		<ul class="bxslider">
			<li><img src="img/banner-img01.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img02.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img03.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
		</ul>
	</div>
</div>
<!-- Banner Start-->
<!-- end video banner -->
  <div class="servicebox">
    <ul>
      <li> <span class="doctor"><img src="assets/images/stethoscopeblue.png" width="51" alt=""/></span>
        <h4><strong>Associate Doctors</strong></h4>
        <p>If you are a public hospital or a private practice, and are on the receiving end of services, please click here.</p>
      </li>
      <li> <span class="track"><img src="assets/images/binocularsred.png" width="80" alt=""/></span>
        <h4><strong>Track Your Order</strong></h4>
        <p>Already a member? Click here and<br> find out where your order<br> is currently.</p>
      </li>
      <li> <span class="lab"><img src="assets/images/microscopegreen.png" width="51" alt=""/></span>
        <h4><strong>Associate Labs</strong></h4>
        <p>If you are a laboratory that provides <br>services to healthcare institutions, <br> please click here.</p>
      </li>
      <div class="clearfix"></div>
    </ul>
  </div>
  <!-- end 3box area -->
  <div style="height:20px;"></div>
  <div class="graph">
  <div class="headr">
  <h2>COST OF ONE DELIVERY</h2>
   <hr class="style-two">
  <h4>What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
  <table id="q-graph">
<thead>

</thead>
<tbody>
<tr class="qtr" id="q1">
<th scope="row">@5mi</th>
<td class="fedex bar" style="height: 40px;"><p>$16</p></td>
<td class="uber bar" style="height: 50px;"><p>$21</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
<tr class="qtr" id="q2">
<th scope="row">@20mi</th>
<td class="fedex bar" style="height: 92px;"><p>$38</p></td>
<td class="uber bar" style="height: 158px;"><p>$66</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
<tr class="qtr" id="q3">
<th scope="row">@45mi</th>
<td class="fedex bar" style="height: 144px;"><p>$60</p></td>
<td class="uber bar" style="height: 338px;"><p>$141</p></td>
<td class="biolab bar" style="height: 12px;"><p>$5</p></td>
</tr>
</tbody>
</table>
<div id="ticks">
<div class="tick" style="height: 59px;"><p>$125</p></div>
<div class="tick" style="height: 59px;"><p>$100</p></div>
<div class="tick" style="height: 59px;"><p>$75</p></div>
<div class="tick" style="height: 59px;"><p>$50</p></div>
<div class="tick" style="height: 59px;"><p>$25</p></div>
<div class="tick" style="height: 59px;"><p>$0</p></div>
</div>
<div class="instraction">
<ul>
<li>FedEx</li>
<li>UberRush</li>
<li>biolab</li>
</ul>
</div>
  </div>
<!-- end graph -->
<div style="height:20px;"></div>
<div class="ordrtime">
 <div class="headr"><br>
  <h2>Get your items in minutes</h2>
   <hr class="style-two">
</div>
<div class="order-time-container">
      <div class="order-time-part1">
        The average order takes
      </div>
      <div class="order-time-counter">
        <div class="characters">
          <span class="character">
            <span>3</span>
          </span>
          <span class="character">
            <span>7</span>
          </span>
        </div>
      </div>
      <div class="order-time-part2">
        minutes to be delivered.
      </div>
      <div class="cleafix"></div>
    </div>
    <div class="order-time-container">
 <div class="headr2"><br/><br/>
  <h2>Number of deliveries made in 2016</h2>
   <hr class="style-two">
</div>
<div class="order-time-container2">
      <div class="order-time-counter2">
        <div class="character">
          <center><div id="counter" style="color: #fff; line-height: 162px; font-size: 102px; font-family: 'Courier New', Courier, 'Lucida Sans Typewriter', 'Lucida Typewriter', monospace; line-height:200px; letter-spacing: 35px; padding-left:40px; padding-top:5px; background:url(http://biolablogistics.com/img/counter-bgnew.png) no-repeat center;"></div></center>
        </div>
      </div>
    </div>

    </div>
    </div>
<!-------------------end Order Time -------->

    <div style="height:20px;"></div>
<!-------------------end Order Time 2-------->
 <div class="clientslogo">
  <div class="headr">
  <h2>What these awesome people have to say!</h2>
  <hr class="style-two">
  <h4>What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
  <ul class="row1">
        <li>
          <img src="assets/images/client_icon01.jpg" width="180" height="63" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon02.jpg" width="180" height="60" alt=""/>
        </li>
        <li>
         <img src="assets/images/client_icon03.jpg" width="180" height="50" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon04.jpg" width="180" height="54" alt=""/>
        </li>
        </ul>
  <ul class="row2">
        <li>
          <img src="assets/images/client_icon05.jpg" width="180" height="96" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon06.jpg" width="180" height="16" alt=""/>
        </li>
    </ul>
  </div>
  <div style="height:20px;"></div>
  <!-- end clients logo -->
  <div class="testimonials">
  <div class="headr">
  <h2>What these awesome people have to say!</h2>
  <hr class="style-two">
  <h4 style="color:#FFF;">What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
    <div class="carousel-reviews broun-block">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">Hercules</a>
							    
						        <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">The Purge: Anarchy</a>
							    
        						<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Planes: Fire & Rescue</a>
								
    							 <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<a title="" href="#">Rannynm</a>
								<i>Indonesia</i>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">Hercules</a>
							    
						         <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">The Purge: Anarchy</a>
							    
        						<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Planes: Fire & Rescue</a>
								
    							<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<a title="" href="#">Rannynm</a>
								<i>Indonesia</i>
							</div>
						</div>
                    </div>                    
                </div>
                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                    <span class="leftarrow"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                    <span class="rightarrow"></span>
                </a>
            </div>    
</div>
  </div>
  <!-- end Testimonial --> 
 <!-- Three boxes Start
<script>
	var unix = Math.round(+new Date()/1000);
	document.cookie=unix;
function PostMainChartValues()
{
	var unix = Math.round(+new Date()/1000);
	var unix=String(unix);
	 //alert(unix);
	var num=unix.slice(-5);
	var arrayelement = new Array();
	var arrayelement=num.split('');
	var finalstring='';
	for (i=0;i<arrayelement.length;i++)
{
   finalstring=finalstring+'<span>'+arrayelement[i]+'</span>';
}
  //alert(finalstring);
     //$('#test').html(finalstring);
  document.getElementById("test").innerHTML=finalstring;
 
}


$(document).ready(function(){
	//alert("ok");
	$.ajax({
		url: "http://biolablogistics.com/Pages/counter",
		type: "POST",
		success:function(response){
			//alert(JSON.stringify(response));
              //$('#tabble').html('');
              //$('#tabble').append(data);
            },
         error:function(response){
			// alert(JSON.stringify(response));
			 //alert('Waiting for data...');
		 }
        });
	
});



	/* var i = 1000;
    var funcNameHere = function(){
        //alert(i++);
    };
    // This block will be executed 100 times.
    setInterval(funcNameHere, 7000);
    funcNameHere();*/
	
	 

 
window.setInterval(function() {
    PostMainChartValues();
}, 5000);
</script>

<div class="counter">
<p id="innercounter">
	<span style="font-size:32px;" id='test'></span><br><br>
<span id="delivery">Number of deliveries made in <?php echo $date=date('Y').' '; ?></span>


<?php //$arr2 = str_split($count, 1);  foreach($arr2 as $val){ echo "<span>".$val."</span>"; } //echo "<span>".$count."</span>"; ?>
</p>
</div>
-->

<div class="clear"></div>