<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<script>
$(document).ready(function() {
    $(window).scroll(function() {
        if($(this).scrollTop() > 150){
            $('#goTop').stop().animate({
                top: '500px'    
                }, 500);
        }
        else{
            $('#goTop').stop().animate({
               top: '-100px'    
            }, 500);
        }
    });
    $('#goTop').click(function() {
        $('html, body').stop().animate({
           scrollTop: 0
        }, 500, function() {
           $('#goTop').stop().animate({
               top: '-100px'    
           }, 500);
        });
    });
    
    });
    

</script>
<div class="static-pages">
		<aside class="left-bar1">
			<?php //pr($this->params);?>
			<ul>
				<?php foreach($pagesName as $page): ?>
					<li class="<?php if($this->params['pass'][0] == $page['Cmspages']['seourl']) echo 'selected';?>"><a href="<?php echo $this->Html->url("/st/".$page['Cmspages']['seourl']); ?>" title="Terms of use"><?php echo $page['Cmspages']['name'];?></a></li>
				<?php endforeach;?>
			</ul>
		</aside>
	<section class="right-panel-1">
		<!--h2><?php echo $pageContent['Cmspages']['name'];?></h2-->
		<?php echo $pageContent['Cmspages']['content'];?>
	</section>
		
</div>
<a id="goTop" href="#"><i style="font-style: normal !important;"  class="fa fa-chevron-circle-up fa-3x"></i></a>

<style>
ol, ul {
    list-style: inherit;
}
p {
margin:0 0 6px;
}
#goTop{
    background:transparent;
    padding:5px;
    position:fixed;
    top:-100px;
    right:10px;
    box-shadow: 0 0 23px;
}
</style>
