<?php echo $this->Session->flash(); ?>
<link rel="stylesheet" type="text/css" href="assets/customstyle.css" />
<script src="assets/jequiry1.11.js"></script>
    <script src="assets/carousel.js"></script>
<!-- Banner Start-->
<div class="bx-wrapper">
	<div class="bx-viewport">
		<ul class="bxslider">
			<li><img src="img/banner-img01.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img02.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
			<li><img src="img/banner-img03.jpg" alt="Bio Lab Logistics" width="1000" height="412" /></li>
		</ul>
	</div>
</div>
<!-- Banner Start-->
<!-- end video banner -->
  <div class="servicebox">
    <ul>
      <li> <span class="doctor"><img src="assets/images/stethoscopeblue.png" width="51" alt=""/></span>
        <h4>Associate Doctors</h4>
        <p>If you are a public hospital or a private practice, and are on the receiving end of services, please click here.</p>
      </li>
      <li> <span class="track"><img src="assets/images/binocularsred.png" width="80" alt=""/></span>
        <h4>Track Your Order</h4>
        <p>Already a member? Click here and<br> find out where your order<br> is currently.</p>
      </li>
      <li> <span class="lab"><img src="assets/images/microscopegreen.png" width="51" alt=""/></span>
        <h4>Associate Labs</h4>
        <p>If you are a laboratory that provides services to healthcare institutions, please click here.</p>
      </li>
      <div class="clearfix"></div>
    </ul>
  </div>
  <!-- end 3box area -->
  <div class="testimonials">
  <div class="testmonial_headr">
  <h2>What these awesome people have to say!</h2>
  <h4>What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
    <div class="carousel-reviews broun-block">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">Hercules</a>
							    
						        <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img13.png">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">The Purge: Anarchy</a>
							    
        						<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img14.png">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Planes: Fire & Rescue</a>
								
    							 <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img15.png">
								<a title="" href="#">Rannynm</a>
								<i>Indonesia</i>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a title="" href="#">Hercules</a>
							    
						         <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img13.png">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-4 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
						        <a title="" href="#">The Purge: Anarchy</a>
							    
        						<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img14.png">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
							<div class="block-text rel zmin">
								<a title="" href="#">Planes: Fire & Rescue</a>
								
    							<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? ...</p>
								<ins class="ab zmin sprite sprite-i-triangle block"></ins>
							</div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img15.png">
								<a title="" href="#">Rannynm</a>
								<i>Indonesia</i>
							</div>
						</div>
                    </div>                    
                </div>
                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                    <span class="leftarrow"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                    <span class="rightarrow"></span>
                </a>
            </div>    
</div>
  </div>
  <!-- end Testimonial --> 
  <div class="clientslogo">
  <div class="clientslogo_headr">
  <h2>What these awesome people have to say!</h2>
  <h4>What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
  <ul class="row1">
        <li>
          <img src="assets/images/client_icon01.jpg" width="180" height="63" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon02.jpg" width="180" height="60" alt=""/>
        </li>
        <li>
         <img src="assets/images/client_icon03.jpg" width="180" height="50" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon04.jpg" width="180" height="54" alt=""/>
        </li>
        </ul>
        <ul class="row2">
        <li>
          <img src="assets/images/client_icon05.jpg" width="180" height="96" alt=""/>
        </li>
        <li>
          <img src="assets/images/client_icon06.jpg" width="180" height="16" alt=""/>
        </li>
    </ul>
  </div>
  <!-- end clients logo -->
  <div class="graph">
  <div class="graph_headr">
  <h2>Biolab Graphical Statics</h2>
  <h4>What we love about our customers is that they are as passionate about great food as we are.<br>
See what some of them have to say about us. </h4>
</div>
  <ul class="chart">
                                                  <li class="axis">
                                                    <div class="lebel">$ 5</div>
                                                    <div class="lebel">$ 4</div>
                                                    <div class="lebel">$ 3</div>
                                                    <div class="lebel">$ 2</div>
                                                    <div class="lebel">$ 1</div>
                                                  </li>
                                                  <li class="bar teal" style="height: 80%;" title="$4">
                                                    <div class="skill">Leading Competitors</div>
                                                  </li>
                                                  <li class="bar salmon" style="height: 40%;" title="$2">
                                                    <div class="skill">Harry's</div>
                                                  </li>
                                                 
                                                </ul>
  </div>
<!-- end graph -->
<script>
	var unix = Math.round(+new Date()/1000);
	document.cookie=unix;
function PostMainChartValues()
{
	var unix = Math.round(+new Date()/1000);
	var unix=String(unix);
	 //alert(unix);
	var num=unix.slice(-5);
	var arrayelement = new Array();
	var arrayelement=num.split('');
	var finalstring='';
	for (i=0;i<arrayelement.length;i++)
{
   finalstring=finalstring+'<span>'+arrayelement[i]+'</span>';
}
  //alert(finalstring);
     //$('#test').html(finalstring);
  document.getElementById("test").innerHTML=finalstring;
 
}


$(document).ready(function(){
	//alert("ok");
	$.ajax({
		url: "http://biolablogistics.com/Pages/counter",
		type: "POST",
		success:function(response){
			//alert(JSON.stringify(response));
              //$('#tabble').html('');
              //$('#tabble').append(data);
            },
         error:function(response){
			// alert(JSON.stringify(response));
			 //alert('Waiting for data...');
		 }
        });
	
});



	/* var i = 1000;
    var funcNameHere = function(){
        //alert(i++);
    };
    // This block will be executed 100 times.
    setInterval(funcNameHere, 7000);
    funcNameHere();*/
	
	 

 
window.setInterval(function() {
    PostMainChartValues();
}, 5000);
</script>
<!-- Three boxes Start-->
<div class="counter">
<p id="innercounter">
	<span style="font-size:32px;" id='test'></span><br><br>
<span id="delivery">Number of deliveries made in <?php echo $date=date('Y').' '; ?></span>


<?php //$arr2 = str_split($count, 1);  foreach($arr2 as $val){ echo "<span>".$val."</span>"; } //echo "<span>".$count."</span>"; ?>
</p>
</div>


<div class="clear"></div>
