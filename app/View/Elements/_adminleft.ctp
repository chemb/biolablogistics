<style> #accordion ul { display: block !important;} </style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".unlinkit").click(function(e){
			alert("The page is under Construction!");
			e.preventDefault();
		});
	});
</script>
<div id="adminMenu" class="ddsmoothmenu actions">
	<a class="side-bar" onclick="hidepanel();" id="btn" href="javascript:void(0);" title="Click to hide panel" >Click to hide panel</a>
	<ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Dashboard</a></li>
		</ul>
		<ul class="sublist-menu <?php echo ((($this->params['controller']=='admins' || $this->params['controller']=='backupdbs') && ($this->params['action'] =='changepassword' || $this->params['action'] =='admin_index'))?'hide1':'hide'); ?>"style="display: block;">
			<li><?php echo $this->Html->link("Change Password","/admins/changepassword"); ?></li>
			<li><?php echo $this->Html->link("Database Backups","/admin/backupdbs"); ?></li>
			<li><?php //echo $this->Html->link("Configuration","/admin/configurations"); ?></li>
			<li><?php //echo $this->Html->link("Newsletter","/admin/newsletter"); ?></li>
			<li><?php //echo $this->Html->link("Transactions","/admin/orders"); ?></li>
		</ul>
		<?php $admin = $this->Session->read("admin");?>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Users</a></li>
		</ul>
		<ul class="sublist-menu <?php echo ((($this->params['controller']=='users' || $this->params['controller']=='admins') && ($this->params['action']=='admin_index' || $this->params['action']=='admin_addstaffmember' || $this->params['action']=='admin_requestedUser' || $this->params['action']=='approval_request' || $this->params['action']=='admin_upload'))?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Users', true), '/admin/users/index');?></li>
			<li><?php echo $this->Html->link(__('Add User', true), '/admin/users/addstaffmember');?></li>
			<li><?php echo $this->Html->link(__('Requested Users', true), '/admin/users/requestedUser'); ?></li> 
			<li><?php echo $this->Html->link(__('Approval Request', true), '/admin/approval_request'); ?></li> 
			<!--<li><?php echo $this->Html->link(__('Banner images', true), '/admin/users/bannerImage'); ?></li> -->
                        <li><?php echo $this->Html->link(__('Upload Users', true), '/admin/users/upload'); ?></li>
		</ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Orders</a></li>
		</ul>
		<ul class="sublist-menu <?php echo ((($this->params['controller']=='orders' || $this->params['controller']=='admins') && ($this->params['action'] =='admin_index' || $this->params['action'] =='prepay' || $this->params['action'] =='deliverycharge'))?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Orders', true), '/admin/orders/index');?></li>
			<li><?php echo $this->Html->link(__('Add Prepayment', true), '/admin/prepay');?></li>
			<li><?php echo $this->Html->link(__('Delivery Charges', true), '/admins/deliverycharge');?></li>
			<!--li><?php echo $this->Html->link(__('Doctors not paid payment', true), '/admin/orders/');?></li>
			<li><?php echo $this->Html->link(__('Lab waiting for payment', true), '/admin/orders/');?></li-->

		</ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Payment History</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='orders' && ($this->params['action']=='admin_payments' || $this->params['action']=='admin_mark_paid' || $this->params['action']=='admin_transaction' || $this->params['action']=='admin_transactionlab' || $this->params['action']=='admin_view' ) )?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Payments to Labs', true), '/admin/orders/payments/2');?></li>
			<li><?php echo $this->Html->link(__('Payments from Doctors', true), '/admin/orders/payments/1');?></li>
			<li><?php echo $this->Html->link(__('Payments from Labs', true), '/admin/orders/labpayments');?></li>
		</ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Services</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='services')?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Manage services', true), '/admin/services/index');?></li>
		</ul>	
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Email Templates</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='cmsemails')?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Email Templates', true), '/admin/cmsemails/index');?></li>
		</ul>	
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Static Pages</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='cmspages')?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Static Pages', true), '/admin/cmspages/index');?></li>
		</ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Country Management</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='countries')?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Countries', true), '/admin/countries/index');?></li>
		</ul>
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">State Management</a></li>
		</ul>
		<ul class="sublist-menu <?php echo (($this->params['controller']=='states')?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('States', true), '/admin/states/index');?></li>
		</ul>
		
		<ul class="admintoggel">
			<li><a href="javascript:void(0);" class="Admins">Driver Management</a></li>
		</ul>
		<ul class="sublist-menu <?php echo ((($this->params['controller']=='users' || $this->params['controller']=='admins') && ($this->params['action']=='admin_adddriver' || $this->params['action']=='admin_listdriver' || $this->params['action']=='assigndriver' || $this->params['action']=='assignlabdriver' || $this->params['action']=='assignorder'))?'hide1':'hide'); ?>">
			<li><?php echo $this->Html->link(__('Add Drivers', true), '/admin/users/adddriver');?></li>
			<li><?php echo $this->Html->link(__('Drivers List', true), '/admin/users/listdriver');?></li>
			<li><?php echo $this->Html->link(__('Assign Driver', true), '/admin/assigndriver');?></li>
			<li><?php echo $this->Html->link(__('Lab Orders', true), '/admins/assignlabdriver');?></li>
			<li><?php echo $this->Html->link(__('Pickup Orders', true), '/admin/assignorder');?></li>
		</ul>
		<br style="clear: left" />
	</ul>
</div>
	
