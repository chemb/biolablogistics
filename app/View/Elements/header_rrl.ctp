<!-- Header Section Start-->
<?php echo $this->Html->script('jquery.min'); 
echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
?>
<!--[if gte IE 8]>
    <link rel="stylesheet" type="text/css" href="css/all-ie-only.css">
    <![endif]-->
<script>
function redirectdoctor(id,uid)
{
//alert(id);
//alert(uid);
var status_id = document.getElementById('status_id').value;

//alert(status_id);
$.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'notification'));?>",
		
		type: "POST",
		data:'status_id='+status_id+'&id='+id+'&uid='+uid ,
		success:function(xhr, status, error){
             //alert(xhr+'--'+status+'--'+error);
              window.location="/orders/viewOrder/"+id;
            },
         error:function(data){
			// alert(xhr+'--'+status+'--'+error);
			 window.location="/orders/viewOrder/"+id;
		 }
        });

}
function redirectlab(id,uid)
{
//alert(id);
//alert(uid);
var status_id = document.getElementById('status_id').value;

//alert(status_id);
$.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'notification'));?>",
		
		type: "POST",
		data:'status_id='+status_id+'&id='+id+'&uid='+uid ,
		success:function(xhr, status, error){
             //alert(xhr+'--'+status+'--'+error);
              window.location="/view-orders-lab/"+id;
            },
         error:function(data){
			// alert(xhr+'--'+status+'--'+error);
			window.location="/view-orders-lab/"+id;
		 }
        });


//alert(id);

}



function redirectstaff(id,uid)
{
//alert(id);
//alert(uid);
var status_id = document.getElementById('status_id').value;

//alert(status_id);
$.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'notification'));?>",
		
		type: "POST",
		data:'status_id='+status_id+'&id='+id+'&uid='+uid ,
		success:function(xhr, status, error){
             //alert(xhr+'--'+status+'--'+error);
              window.location="/view-order-staff/"+id;
            },
         error:function(data){
			// alert(xhr+'--'+status+'--'+error);
			 window.location="/view-order-staff/"+id;
		 }
        });
//alert(id);

}
function redirectall(uid)
{
window.location="/orders/allnotification/"+uid;
}
</script>
<div class="alert alert-success text-center hide" role="alert">
  <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
  <!--<label id="succ_msg"><strong>Warning!</strong> Better check yourself, you're not looking too good.</label> -->
</div>
<div class="alert alert-danger text-center hide" role="alert">
  <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
 <!-- <label id="err_msg"><strong>Warning!</strong> Better check yourself, you're not looking too good.</label> -->
</div>
<header>
    <h1 class="logo col-sm-4">
        <a class="navbar-brand" href="<?php echo $this->Html->url("/"); ?>" ><?php echo $this->Html->image("logo.png", array("alt" => "Bio Lab Logistics", "style" => "margin:20px 20px 0px 0px;")); ?></a>
    </h1>
    <div class="login-section col-sm-3">
		<?php if (!$this->Session->read("Auth.User.id")) { ?>
			<a class="sign-up" href="<?php echo $this->Html->url("/signup"); ?>" >
                <span class="sign-icon"></span>   
                <strong>Sign Up</strong>   
            </a>
			<a class="sign-in" href="<?php echo $this->Html->url("/signin"); ?>" >
                <span class="sign-icon"></span>   
                <strong>Sign In</strong>   
            </a>

        <?php } else { ?>
        
        
        <div class="mystyle">
        <li id="notification_li">
<span id="notification_count">
<?php
if ($this->Session->read("Auth.User.user_type") == 1){
$lab_id = $this->Session->read("Auth.User.id");
$count = $this->requestAction('App/notificationcount/'.$lab_id);
echo count($count);
}
elseif ($this->Session->read("Auth.User.user_type") == 2){
$lab_id = $this->Session->read("Auth.User.id");
$count = $this->requestAction('App/notificationcount/'.$lab_id);
echo count($count);
}
elseif ($this->Session->read("Auth.User.user_type") == 3){
$lab_id = $this->Session->read("Auth.User.id");
$count = $this->requestAction('App/notificationcount/'.$lab_id);
echo count($count);
}
?>
</span>
<?php if($this->Session->read("Auth.User.user_type") != 4){ ?>
<a href="#" myval="<?php echo $this->Session->read("Auth.User.id")?>" id="notificationLink">Notifications</a>
<?php } ?>
<div id="notificationContainer">
<div id="notificationTitle">Notifications</div>
<div id="notificationsBody" class="notifications">
<?php
if ($this->Session->read("Auth.User.user_type") == 1){
$lab_id = $this->Session->read("Auth.User.id");
$data = $this->requestAction('App/notificationdata/'.$lab_id);



for($i=0;$i<count($count);$i++){
?>
<table style="text-align:left;" class="table table-striped">
<tr><td style="padding:10px;font: none;font-size: 11px;width:111px;"><?php 
$or_id = $this->requestAction('App/getorderid/'.$data['norder_id'][$i]);
echo $this->Html->link($or_id,array(),array('onclick'=>"redirectdoctor('".$data['norder_id'][$i]."','".$this->Session->read('Auth.User.id')."')"));
 ?></td><td style="padding:10px;font: none;font-size: 11px;width:111px;">
 <input type="hidden" id="status_id" value="<?php echo $data['noti_data'][$i][0]['Notificationtitle']['id']; ?>">
 
 <?php echo $data['noti_data'][$i][0]['Notificationtitle']['name']; ?></td><td style="padding:10px;font: none;  font-size: 11px;"><?php echo $data['noti_data'][$i][0]['Notificationtitle']['detail']; ?></td></tr>
</table>
<?php

}
}
elseif ($this->Session->read("Auth.User.user_type") == 2){
$lab_id = $this->Session->read("Auth.User.id");
$data = $this->requestAction('App/notificationdata/'.$lab_id);



for($i=0;$i<count($count);$i++){
?>
<table style="text-align:left;" class="table table-striped">
<tr><td style="padding:10px;font: none;  font-size: 11px;width:111px;"><?php 
$or_id = $this->requestAction('App/getorderid/'.$data['norder_id'][$i]);
echo $this->Html->link($or_id,array(),array('onclick'=>"redirectlab('".$data['norder_id'][$i]."','".$this->Session->read('Auth.User.id')."')"));
?></td><td style="padding:10px;font: none;  font-size: 11px;width:111px;">
<input type="hidden" id="status_id" value="<?php echo $data['noti_data'][$i][0]['Notificationtitle']['id']; ?>">

<?php echo $data['noti_data'][$i][0]['Notificationtitle']['name']; ?></td><td style="padding:10px;font: none;  font-size: 11px;"><?php echo $data['noti_data'][$i][0]['Notificationtitle']['detail']; ?></td></tr>
</table>
<?php

}
}
elseif ($this->Session->read("Auth.User.user_type") == 3){
$lab_id = $this->Session->read("Auth.User.id");
$data = $this->requestAction('App/notificationdata/'.$lab_id);



for($i=0;$i<count($count);$i++){
?>
<table style="text-align:left;" class="table table-striped">
<tr><td style="padding:10px;  font-size: 11px;font: none;width:111px;"><?php 
$or_id = $this->requestAction('App/getorderid/'.$data['norder_id'][$i]);
echo $this->Html->link($or_id, array(),array('onclick'=>"redirectstaff('".$data['norder_id'][$i]."','".$this->Session->read('Auth.User.id')."')"));
?></td><td style="padding:10px;  font-size: 11px;font: none;width:111px;">
<input type="hidden" id="status_id" value="<?php echo $data['noti_data'][$i][0]['Notificationtitle']['id']; ?>">

<?php echo $data['noti_data'][$i][0]['Notificationtitle']['name']; ?></td><td style="padding:10px;  font-size: 11px;font: none;"><?php echo $data['noti_data'][$i][0]['Notificationtitle']['detail']; ?></td></tr>
</table>
<?php

}
}

?>


</div>
<div id="notificationFooter">
<?php 
echo $this->Html->link('See All', array(),array('onclick'=>"redirectall('".$this->Session->read('Auth.User.id')."')"));
?>
</div>
</div>

</li>
</div>	
			<div class="user-box ">
                <span class="user-img" style="background:#fff;">
					 <?php 
					 if ( $this->Session->read("Auth.User.user_type") == 1 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/docprofile.jpg");
					} else if ( $this->Session->read("Auth.User.user_type") == 2 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/labprofile.jpg");
					} else {
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/no-img.png");
					}
                    $profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, ThumbImageProfilePrefix);
                    echo $this->Html->image($profileImgThumb1, array("alt" => $this->Session->read("Auth.User.Userdetail.first_name") . ' ' . $this->Session->read("Auth.User.Userdetail.last_name"), "width"=>"44", "height"=>"30" ,"style"=>"height:30px; margin-top:7px;" ));?>
                </span>
                <span class="txt"> 
                
                    <?php 
                    if($this->session->read('Auth.User.user_type') != 4) { 
                    echo ucfirst($this->Session->read("Auth.User.Userdetail.first_name"))." ".ucfirst($this->Session->read("Auth.User.Userdetail.last_name"));
                    }else{
                    echo ucfirst($this->Session->read("Auth.User.Userdetail.first_name"));
                    }
                    ?>
                </span>
                <div class="user-dropdown" style="display:none;">
                    <ul>      
                       <li>
                       <?php if(($this->session->read('Auth.User.user_type') != 4) && ($this->session->read('Auth.User.profile_status') == '1')) { ?>
                       <?php echo $this->Html->link(" Profile",array("controller"=>"profile","action"=>$this->Session->read("Auth.User.Userdetail.user_id"),$this->Common->makeurl($this->Session->read("Auth.User.company_name"))));?></li>
                       <?php } ?>
						<?php if($this->session->read('Auth.User.user_type') == 1) { 
						if($this->session->read('Auth.User.profile_status') == 1){
						?>								
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Labs')), array('plugin' => false,'controller' => 'users', 'action' => 'search','2')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Favorite Labs')), array('plugin' => false,'controller' => 'users', 'action' => 'favourite_lab')); ?>
							</li>
							
							<!--	<li>
									<?php //echo $this->Html->link(__(__d('labels', 'Place an Order')), array('plugin' => false, 'controller' =>'orders', 'action' =>'order')); ?>
								</li> -->
								<li>
									<?php echo $this->Html->link(__(__d('labels', 'Rx Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'orderForDoctor')); ?>
								</li>
								<li>
								<?php echo $this->Html->link(__(__d('labels','Delivery')), array('plugin' => false,'controller' =>'orders', 'action' =>'directorderlist')) ?>
							</li>
								<!--<li>
									<?php echo $this->Html->link(__(__d('labels','Pending Payment')), array('plugin' => false,'controller' =>'orders', 'action' =>'payment_history')) ?>
								</li>-->
								<!--<li>
									<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order')) ?>
								</li>-->
							<?php
					}	} elseif($this->session->read('Auth.User.user_type') == 2) {  
						
						if($this->session->read('Auth.User.profile_status') == 1){
						?>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Doctors')), array('plugin' => false,'controller' => 'users', 'action' => 'search','1')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Rx Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'accept_order')); ?>
							</li>
						<!--	<li>
								<?php echo $this->Html->link(__(__d('labels','Pending Payment')), array('plugin' => false,'controller' =>'orders', 'action' =>'payment_history')) ?>
							</li> -->
							<li>
								<?php echo $this->Html->link(__(__d('labels','Delivery')), array('plugin' => false,'controller' =>'orders', 'action' =>'directorderlist')) ?>
							</li>
							<!-- <li>
								<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order')) ?>
							</li> -->
						<?php } } elseif($this->session->read('Auth.User.user_type') == 3) { ?>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Members')), array('plugin' => false, 'controller' => 'users', 'action' => 'search','3')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Rx Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'orderForStaff')); ?>
							</li>
							<li>
									<?php echo $this->Html->link(__(__d('labels','Delivery')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'stafflaborder')) ?>
								</li>
							
							<!-- <li>
									<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order')) ?>
								</li> -->
						<?php }else {?>
						<!-- <li>
						<?php echo $this->Html->link(__(__d('labels', 'Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'driverorders')); ?>
						</li> -->
						<?php } ?>
						<li>
							<?php echo $this->Html->link(__(__d('labels', 'Logout')), array('plugin' => false, 'controller' => 'users', 'action' => 'logout')); ?>
						</li>
                    </ul>
                </div>
            </div>
		<?php } ?>
	</div>
    <!-- Brand and toggle get grouped for better mobile display -->

    <nav class="navbar nav-bar">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
<?php //print_r($pages); ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav custom-nav">

				<li class="<?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'index')?'active':''; ?>">			
				<!--	<a href="<?php echo $this->Html->url("/"); ?>" >Home</a> -->
				</li>
				<?php if(isset($pages) && !empty($pages)) { foreach ($pages as $page):  
				if(!empty($page['Cmspages']['seourl'])) { ?>
					<li class="<?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'staticpages' && $this->params['pass'][0] == $page['Cmspages']['seourl'] )?'active':''; ?>"><a href="<?php echo $this->Html->url("/st/" . $page['Cmspages']['seourl']); ?>" ><?php echo $page['Cmspages']['name']; ?></a></li>
				<?php }
				endforeach;  } ?>
				<li>
					<a href="#myModal" class="left open_contact_popup" data-toggle="modal" >Contact Us</a>
					
				</li>
			</ul>
		</div>                             
	</nav>
	<div class="clear"></div>
</header>

<script type="text/javascript" >
$(document).ready(function()
{
$("#notificationLink").click(function()
{
$("#notificationContainer").fadeToggle(300);
$("#notification_count").fadeOut("slow");
return false;
});

//Document Click
$(document).click(function()
{
$("#notificationContainer").hide();
});
//Popup Click
$("#notificationContainer").click(function()
{
return false
});

});




$('#notificationLink').click(function(evt) {
      var res= $("#notificationLink").attr("myval");
      //alert(res);
      $.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'anotification'));?>",
		
		type: "POST",
		data:'uid='+res ,
		success:function(data){
             //alert(xhr);alert(status);alert(error);
            },
         error:function(data){
			
		 }
        });
      
      
      
        });
</script>
<style>





#notification_li{position:relative; list-style:none;}
#notificationContainer {
background-color: #fff;
/* border: 1px solid rgba(100, 100, 100, .4); */
-webkit-box-shadow: 0 10px 10px rgba(0, 0, 0, .25);
overflow: visible;
position: absolute;
top: 30px;
/*margin-left: -170px;*/
/* width: 400px; */
z-index: 99;
display: none;
}
#notificationContainer:before {
content: '';
display: block;
position: absolute;
width: 0;
height: 0;
color: transparent;
border: 10px solid black;
border-color: transparent transparent #e9eaed;
margin-top: -20px;
margin-left: 188px;
}
#notificationTitle {
z-index: 1000;
font-weight: bold;
padding: 8px;
font-size: 13px;
background-color: #e9eaed;
width: 384px;
border-bottom: 1px solid #dddddd;
}
#notificationsBody {
/* padding: 33px 0px 0px 0px !important; */
 max-height:300px; 
 	overflow: scroll;
}
#notificationFooter {
background-color: #e9eaed;
text-align: center;
font-weight: bold;
padding: 8px;
font-size: 12px;
border-top: 1px solid #dddddd;
}
#notification_count {
padding: 3px 7px 3px 7px;
background: #cc0000;
color: #ffffff;
font-weight: bold;
margin-left: 77px;
border-radius: 9px;
position: absolute;
margin-top: -13px;
font-size: 11px;
}
.mystyle{
    position: relative;
    left: -250px;
    top: 60px;
}

@media all and (max-width:480px){
.mystyle {position: relative;left: 0px;top:10px;}
.navbar-brand {margin-left: 25px;}
#notificationContainer {left: -15px;width:350px;}
#notificationTitle {width: 348px;}
}
@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {  
  .boxes-bg p {
  height: 70px;
  } 
  #notification_count { margin-left:209px;}
}
@media screen and (min-width: 760px) and (max-width: 1300px) { 
#notificationContainer {   
 margin-left: -25%;
}
}
</style>

<!-- Header Section End -->
