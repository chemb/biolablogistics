<?php
$style = 'margin-left: 20px !important; margin-right: 20px !important; background-color: #dff0d8; border-color: #d6e9c6; color: #3c763d; opacity: 1; border-radius: 4px; margin: 10px 10px 0 10px; padding: 15px; height: 20px;';
$error = '';
if (isset($params['error']) && $params['error']) :
    $style = 'margin-left: 20px !important; margin-right: 20px !important; background-color: #f2dede; border-color: #ebccd1; color: #a94442; opacity: 1; border-radius: 4px; margin: 10px 10px 0 10px; padding: 15px; height: 20px;';
    $error = ' [<a class="show-error" style="text-decoration: none; font-weight: bold; color: #a94442;" href="javascript:void(0)">' . __('Show errors') . '</a>]';
endif;
$message = $message . $error;
?>
<div class="custom-alert" style="<?= $style ?>">
    <?php if (isset($params['close']) && $params['close']) : ?>
    <a id="alert-close" style="color: #000; float: right; font-size: 21px; font-weight: 700; opacity: 0.2; text-shadow: 0 1px 0 #fff; text-decoration: none; margin-top: -3px;" href="javascript:void(0)">×</a>
    <?php endif; ?>
    <?= $message ?>
</div>
<script>
    $(document).ready(function() {
        $('#alert-close').click(function() {
            $('.custom-alert').hide();
            if ($('.show-errors').is(':visible')) {
                $('.show-errors').toggle('fold');
            }           
            window.location.href = '<?php echo $this->Html->url(array('prefix'=>'admin', 'controller'=>'users','action'=>'upload'),true); ?>';
        });
    });
</script>