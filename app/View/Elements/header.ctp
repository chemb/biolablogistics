<header>
<div class="header">
<div class="logo"><a href="<?php echo $this->Html->url("/"); ?>" title="Bio Lab">Bio Lab</a></div>
<div class="right_header">
<?php if($this->Session->read("Auth.User.id")) { ?>
<div class="login-reg">
	<a href="<?php echo $this->Html->url("/logout"); ?>" id="signup" class="btn2 login" >Logout</a>
</div>
<?php } else { ?>
<div class="login-reg">
	<a href="<?php echo $this->Html->url("/signup"); ?>" id="signup" class="btn2 login" >Signup</a>/<a href="<?php echo $this->Html->url("/login"); ?>" id="signin" class="btn2 login">Login</a>
</div>
<?php } ?>
</div>

</header>
