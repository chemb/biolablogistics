<?php $rating1 = number_format($rating,"2",".","")*18; ?>
<?php if (number_format($rating,"2",".","") <= 1) {
$str = "Eek! Methinks not.";
} else if (number_format($rating,"2",".","") <= 2 &&  number_format($rating,"2",".","") > 1 ) { 
$str = "Meh. I’ve experienced better..";
} else if (number_format($rating,"2",".","") <= 3 &&  number_format($rating,"2",".","") > 2 ) { 
$str = "A-OK.";
} else if (number_format($rating,"2",".","") <= 4 &&  number_format($rating,"2",".","") > 3 ) { 
$str = "Yay! I’m a fan.";
}else if (number_format($rating,"2",".","") <= 5 &&  number_format($rating,"2",".","") > 4 ) { 
$str = "Woohoo! As good as it gets!";
}
?>
<a href="javascript:void(0);" value="<?php echo empty($rating)?'No ratings available.':$str; ?>">
<span class="grey_star">
<span class="gold_star" style="width:<?php echo $rating1; ?>px;"></span>
</span>
</a>
<style>
a[value]:hover:after {
  content: attr(value);
  padding: 4px 8px;
    position: absolute;
  color:#000;
    border:2px solid #fff !important; 
    background:#EF7D01;
 margin-top:25px;
  white-space: nowrap;
  z-index: 999px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -moz-box-shadow: 0px 0px 2px #222;
  -webkit-box-shadow: 0px 0px 2px #222;
  box-shadow: 0px 0px 2px #222;
 
}
</style>


