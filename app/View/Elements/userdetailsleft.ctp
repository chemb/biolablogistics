<aside class="col-sm-4">
<div class="left-bar">
	<div class="profile-img">
		
			<?php 
			// use thumb path from helper
			if ( $this->Session->read("Auth.User.user_type") == 1 ) { 
				$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/docprofile.jpg");
			} else if ( $this->Session->read("Auth.User.user_type") == 2 ) { 
				$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/labprofile.jpg");
			} else {
				$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/no-img.png");
			}
			$profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, MediumProfileImagePrefix);
			echo $this->Html->image($profileImgThumb1,array("alt"=>$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"),"width"=>"248px","height"=>"198px"));
			
			?>
		<p class="user-name" ><?php echo (!empty($this->data['Userdetail']['first_name'])?ucwords($this->data['Userdetail']['first_name']):'').' '.(!empty($this->data['Userdetail']['last_name'])?ucwords($this->data['Userdetail']['last_name']):'');?></p>
		<?php echo (!empty($this->data['Userdetail']['heading'])?$this->data['Userdetail']['heading']:'');?>
	</div>	
</div>

<div class="clear-fix">&nbsp;</div>
<div class="left-bar nav-links">	
	<ul>
			<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_account') echo  'selected'?>"><a href="<?php echo $this->Html->url("/account"); ?>" title="Account Settings">Account Settings</a></li>
			<!--<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_photo') echo  'selected'?>"><a href="<?php echo $this->Html->url("/profilepic"); ?>" title="Edit Profile Photo">Edit Profile Photo</a></li>-->
			<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile') echo  'selected'?>"><a href="<?php echo $this->Html->url("/editprofile"); ?>" title="Profile">Profile</a></li>
			<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_dangerzone') echo  'selected'?>"><a href="<?php echo $this->Html->url("/deleteaccount"); ?>" title="Delete Account">Delete Account</a></li>			 
			<li class="<?php if($this->params['controller'] == 'users' && $this->params['action'] == 'send_emailforapproval') echo  'selected'?>"><a href="<?php echo $this->Html->url("/send-email"); ?>" title="Send Approval Email">Send Approval Email</a></li>
			<li class="<?php if($this->params['controller'] == 'users' && $this->params['action'] == 'changecarddetail') echo  'selected'?>">
			
<?php if($this->Session->read("Auth.User.user_type") == 2){ ?>
<a href="<?php echo $this->Html->url("/Changecard"); ?>" title="Edit Payment Info">Edit Payment Info</a>
<?php } ?>
			<!-- <a href="<?php echo $this->Html->url("userdetails/Seldoctor"); ?>" title="Send Approval Email">Select doctors</a> -->
			</li>
		    
		    
		    
		    
		    
		<!--
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile') echo  'selected'?>"><a href="<?php echo $this->Html->url("/editprofile"); ?>" title="Profile">Profile</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_photo') echo  'selected'?>"><a href="<?php echo $this->Html->url("/profilepic"); ?>" title="Photo">Photo</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_account') echo  'selected'?>"><a href="<?php echo $this->Html->url("/account"); ?>" title="Account Settings">Account Settings</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'premium_instructor') echo  'selected'?>"><a href="<?php echo $this->Html->url("/paypal-account"); ?>" title="Account">Paypal Account</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_privacy') echo  'selected'?>"><a href="<?php echo $this->Html->url("/privacy"); ?>" title="Privacy Settings">Privacy Settings</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_notification') echo  'selected'?>"><a href="<?php echo $this->Html->url("/notifications"); ?>" title="Notifications">Notifications</a></li>
		<li class="<?php if($this->params['controller'] == 'userdetails' && $this->params['action'] == 'edit_profile_dangerzone') echo  'selected'?>"><a href="<?php echo $this->Html->url("/deleteaccount"); ?>" title="Delete Account">Delete Account</a></li>  --> 
	</ul>
	</div>
</aside>
