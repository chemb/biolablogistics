<!-- Header Section Start-->
<div class="alert alert-success text-center hide" role="alert">
  <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
  <label id="succ_msg"><strong>Warning!</strong> Better check yourself, you're not looking too good.</label>
</div>
<div class="alert alert-danger text-center hide" role="alert">
  <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
  <label id="err_msg"><strong>Warning!</strong> Better check yourself, you're not looking too good.</label>
</div>
<header>
    <h1 class="logo col-sm-4">
        <a class="navbar-brand" href="<?php echo $this->Html->url("/"); ?>" title="Bio Lab Logistics"><?php echo $this->Html->image("logo.png", array("alt" => "Bio Lab Logistics", "width" => "253", "height" => "83")); ?></a>
    </h1>
    <div class="login-section col-sm-3">
		<?php if (!$this->Session->read("Auth.User.id")) { ?>
			<a class="sign-up" href="<?php echo $this->Html->url("/signup"); ?>" title="Sign Up">
                <span class="sign-icon"></span>   
                <strong>Sign Up</strong>   
            </a>
			<a class="sign-in" href="<?php echo $this->Html->url("/signin"); ?>" title="Sign In">
                <span class="sign-icon"></span>   
                <strong>Sign In</strong>   
            </a>

        <?php } else { ?>
			<div class="user-box ">
                <span class="user-img">
					 <?php 
					 if ( $this->Session->read("Auth.User.user_type") == 1 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/docprofile.jpg");
					} else if ( $this->Session->read("Auth.User.user_type") == 2 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/labprofile.jpg");
					} else {
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/no-img.png");
					}
                    $profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, ThumbImageProfilePrefix);
                    echo $this->Html->image($profileImgThumb1, array("alt" => $this->Session->read("Auth.User.Userdetail.first_name") . ' ' . $this->Session->read("Auth.User.Userdetail.last_name"), "width"=>"44", "height"=>"44" ));?>
                </span>
                <span class="txt"> 
                    <?php echo ucfirst($this->Session->read("Auth.User.Userdetail.first_name")).' '.ucfirst($this->Session->read("Auth.User.Userdetail.last_name"));?>
                </span>
                <div class="user-dropdown" style="display:none;">
                    <ul>      
                       <li><?php echo $this->Html->link("View Profile",array("controller"=>"profile","action"=>$this->Session->read("Auth.User.Userdetail.user_id"),$this->Common->makeurl($this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name"))),array("title"=>"View Profile"));?></li>
						<?php if($this->session->read('Auth.User.user_type') == 1) { ?>								
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Labs')), array('plugin' => false,'controller' => 'users', 'action' => 'search','2'),array('title' => 'View Labs')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Favourite Labs')), array('plugin' => false,'controller' => 'users', 'action' => 'favourite_lab'),array('title' => 'Favourite Labs')); ?>
							</li>
							
							<!--	<li>
									<?php //echo $this->Html->link(__(__d('labels', 'Place an Order')), array('plugin' => false, 'controller' =>'orders', 'action' =>'order'),array('title'=>'Place an Order')); ?>
								</li> -->
								<li>
									<?php echo $this->Html->link(__(__d('labels', 'View Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'orderForDoctor'),array('title'=>'View Orders')); ?>
								</li>
								<li>
									<?php echo $this->Html->link(__(__d('labels','Pending Payment')), array('plugin' => false,'controller' =>'orders', 'action' =>'payment_history'),array('title'=>'Pending Payment')) ?>
								</li>
								<li>
									<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order'),array('title'=>'Track Order')) ?>
								</li>
							<?php
						} elseif($this->session->read('Auth.User.user_type') == 2) {  ?>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Doctors')), array('plugin' => false,'controller' => 'users', 'action' => 'search','1'),array('title'=>'View Doctors')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'Orders Requests')), array('plugin' => false, 'controller' => 'orders', 'action' => 'accept_order'), array('title'=>'Orders Requests')); ?>
							</li>
							<li>
									<?php echo $this->Html->link(__(__d('labels','Pending Payment')), array('plugin' => false,'controller' =>'orders', 'action' =>'payment_history'),array('title'=>'Pending Payment')) ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order'),array('title'=>'Track Order')) ?>
							</li>
						<?php } else { ?>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Members')), array('plugin' => false, 'controller' => 'users', 'action' => 'search','3'),array('title'=>'View Members')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__(__d('labels', 'View Orders')), array('plugin' => false, 'controller' => 'orders', 'action' => 'orderForStaff'), array('title'=>'View Orders')); ?>
							</li>
							<li>
									<?php echo $this->Html->link(__(__d('labels','Track Order')), array('plugin' =>false, 'controller' =>'orders', 'action' =>'track_order'),array('title'=>'Track Order')) ?>
								</li>
						<?php } ?>
						<li>
							<?php echo $this->Html->link(__(__d('labels', 'Logout')), array('plugin' => false, 'controller' => 'users', 'action' => 'logout'),array('title'=>'Logout')); ?>
						</li>
                    </ul>
                </div>
            </div>
		<?php } ?>
	</div>
    <!-- Brand and toggle get grouped for better mobile display -->

    <nav class="navbar nav-bar">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav custom-nav">
				<li class="<?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'index')?'active':''; ?>">			
					<a href="<?php echo $this->Html->url("/"); ?>" title="Home">Home</a>
				</li>
				<?php if(isset($pages) && !empty($pages)) { foreach ($pages as $page):  
				if(!empty($page['Cmspages']['seourl'])) { ?>
					<li class="<?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'staticpages' && $this->params['pass'][0] == $page['Cmspages']['seourl'] )?'active':''; ?>"><a href="<?php echo $this->Html->url("/st/" . $page['Cmspages']['seourl']); ?>" title="<?php echo __d('labels', $page['Cmspages']['name']); ?>"><?php echo $page['Cmspages']['name']; ?></a></li>
				<?php }
				endforeach;  } ?>
				<li>
					<a href="#myModal" class="left open_contact_popup" data-toggle="modal" title="Contact Us">Contact Us</a>
					
				</li>
			</ul>
		</div>                             
	</nav>
	<div class="clear"></div>
</header>
<!-- Header Section End -->
