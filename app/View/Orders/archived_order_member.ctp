<div class="new-order">
	<?php echo $this->Form->create("User",array("div"=>false,'type' => 'get')); ?>	
		<h2 class="col-sm-2"><?php echo 'View Orders'; ?></h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by order id, status','class'=>'form-control',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
			</div>
		</div>
		<div class="add-btn-outer pull-right">		
		<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/preview-orders-staff') ?>" title="All orders">All orders</a>
		</div>
		
		<!--<div class="col-sm-3">
            <div class="sorting-select">
				<?php echo $this->Form->input('sort_rating',array("options"=>array("All"=>"All","Rating"=>"Rating"),"empty"=>"Sort By",'class'=>"selectpicker",'id'=>'sort_rating',"value"=>(isset($set_val['sort_rating']) && !empty($set_val['sort_rating']))?$set_val['sort_rating']:'','label'=>false,'div'=>false)); ?>
			</div>
		</div> -->
		<div class="clear"></div>
		<div class="responsive-table">
			<table class="table table-striped">
				<tr>
					<th>Order ID</th>
					<th>Status</th>
					<th>Created</th> 
					<th>&nbsp;</th> 
				</tr>
				<?php  if(!empty($order_data))  {
					foreach ($order_data as $order){ pr($order);
					if($order['Status']['id'] == 12 || $order['Status']['id'] == 13) {		
						?>
						<tr> 
							<td><?php echo $order['Order']['id']; ?></td>
							<td><?php echo $order['Status']['title']; ?></td>
							<td><?php echo $order['Order']['created']; ?></td>
							<td>
								<div class="black-btn-outer pull-right">	
									<a href="<?php echo $this->Html->url('/view-order-staff/'.$order['Order']['id']);  ?>" class="black-btn" >View Order</a>
								</div>
							</td>
						</tr> 
					<?php }  } 
				} else {  ?> 
					<td colspan="4" style="color:red;" class="leftalign">No record found</td>  
				<?php }?>
			</table>
		</div>
	<?php echo $this->Element("pagination"); ?>
	<?php echo $this->Form->end(); ?>
</div>
   

