<?php error_reporting(0);  ?><div class="users index">
	<?php echo $this->Form->create("Payment",array("div"=>false)); ?>
	<?php $heading = $user == 1 ? 'Payments from doctors' : 'Payments to labs'; ?>
	<?php echo $this->element("admins/common",array("place"=>'Search by Payment status',"flag"=>false,"pageheader"=>$heading,"buttontitle"=>'no',"listflag"=>true,"action"=>"no","selflag"=>false)); ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		   <?php if($user == 1) { ?>
					
			<th class="leftalign"><?php echo $this->Paginator->sort('id','Order ID'); ?></th>
			<th class="leftalign"><?php echo "Doctor  name"; ?></th>
			<th class="leftalign"><?php echo 'Payment to biolab'; ?></th>
		
			<th class="leftalign"><?php echo 'Pay by Doctor'; ?></th>
			<th class="leftalign"><?php echo 'Payment status'; ?></th>			
			<th class="leftalign"><?php echo 'Transaction history'; ?></th>	
			<!-- <th class="leftalign">Method</th>
			<th class="leftalign">Cheque Number</th>			-->
			<!--th class="actions"><?php echo __('Actions'); ?></th-->
			<?php } else { ?>
						
			<th class="leftalign"><?php echo $this->Paginator->sort('id','Order ID'); ?></th>
			<th class="leftalign"><?php echo 'Order Cost'; ?></th>
			<th class="leftalign"><?php echo 'Prepay Amount'; ?></th>		
				<th class="leftalign"><?php echo 'Delivery charges'; ?></th>
			<th class="leftalign"><?php echo 'Payment status'; ?></th>
			<th class="leftalign"><?php echo __('Actions'); ?></th>	
			
			<?php } ?>	
	</tr>
	<?php //echo "<pre>";print_r($payments); echo "</pre>" ;
	if(!empty($payments))
	{$i=0;
	 foreach ($payments as $key=>$payment): ?>
	 
	 <tr>
		 <?php if($user == 1) { ?>
			<td><a href="<?php 
			$or_id= $this->requestAction('admins/getorderid/'.$payment['OrderPayment']['order_id']); 
			
			echo $this->Html->url("/admin/orders/view/".$payment['OrderPayment']['order_id']) ?>"><?php echo $or_id; ?></a></td>
			<td><?php echo $payment['Doctor']['first_name'].' '. $payment['Doctor']['last_name']; ?> </td>
			<td><?php echo "$".number_format($payment['OrderPayment']['order_payment'],2,'.',','); ?></td>
<!--			<td><?php echo "$".number_format($payment['OrderPayment']['payment'],2,'.',','); ?></td>  -->
			<td><?php echo "$".number_format($payment['OrderPayment']['order_payment'],2,'.',','); ?></td>
			<td><?php echo $this->requestAction('admins/getpaymentstatus/'.$payment['OrderPayment']['order_id']); ?></td>
			<?php if($payment['OrderPayment']['order_status'] != 11) { ?>
			<?php if($payment['OrderPayment']['payment_status'] ==1) { ?>
			<td><a href="<?php echo $this->Html->url("/admin/orders/transaction/".$payment['OrderPayment']['id']."/".$payment['OrderPayment']['order_id']) ?>">View detail</a></td>
			
			<?php } else { ?> <td> N-A</td> <?php  } } else { echo "<td>Reorder</td>"; } ?>
			
			<!-- <td><?php if($payment['Transactions']['payment_method']!='')
			{
			 echo $payment['Transactions']['payment_method']; 
			 }
			 else
			 {
			 echo "N-A";
			 }
			 ?></td>
		<td><?php  if($payment['Transactions']['chequenumber']!=''){echo $payment['Transactions']['chequenumber'];} else{ echo "N-A"; } ?></td> -->
		<?php } else { ?>			
			<td><a href="<?php 
			$or_id= $this->requestAction('admins/getorderid/'.$payment['OrderPayment']['order_id']); 
			
			echo $this->Html->url("/admin/orders/view/".$payment['OrderPayment']['order_id']) ?>"><?php echo $or_id; ?></a></td>
			<td><?php echo "$".number_format($payment['OrderPayment']['payment'],2,'.',','); ?></td>
			<td><?php $prepay = $this->requestAction('admins/getprepay/'.$payment['OrderPayment']['receiver']);  
			echo '$'.$payment['OrderPayment']['payment']*$prepay/100;
			?></td>
			
			<td><?php 
			$pri2= $this->requestAction('admins/getprize/'.$payment['OrderPayment']['order_id']); 
			if(array_key_exists("1",$pri2)){
			echo '$'.$pri2[1]['OrderPayment']['payment'];
			}else{ echo 'N-A';}
			?></td>
								
			<td id="status_<?php echo $payment['OrderPayment']['order_id']; ?>"><?php echo $payment['OrderPayment']['adminpayment'] == 0 ? 'Pending':'Completed'; ?></td>	
			
			<td>
				<?php if($payment['OrderPayment']['order_status'] != 11) { ?>
				<?php if(empty($payment['OrderPayment']['adminpayment'])) {?>
				<a href="<?php echo $this->Html->url("mark_paid/".$payment['OrderPayment']['id']); ?>">Mark as Paid</a>
				<?php }  else { ?>
				<a href="<?php echo $this->Html->url("/admin/orders/transactionlab/".$payment['OrderPayment']['id']."/".$payment['OrderPayment']['order_id']) ?>">View detail</a>	
				<?php } } else { echo "Reorder"; } ?>
				</td>
				
				
		<?php } ?>	
		
	</tr>
<?php  $i++; endforeach; 
    } else 
    { ?>
		<td colspan='8' class="leftalign" style='color:red; text-align:center;'>No record found</td>
		
 <?php   }   ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->Form->end(); ?>

