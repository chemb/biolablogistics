<?php 
error_reporting(0);
?>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<style>
.select label{ display:none;}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}

table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
.add-btn{ margin-top: 15px;
  margin-right: 260px;}
</style>

<script type="text/javascript">


function callajax(res){
	$.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'mahitable'));?>",
		
		type: "POST",
		data:'res='+res ,
		success:function(data){
              $('#tabble').html('');
              $('#tabble').append(data);
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
}


/* Formatting function for row details - modify as you need */


function doSearch() {
    var searchText = document.getElementById('searchTerm').value;
    var targetTable = document.getElementById('dataTable');
    var targetTableColCount;
            
    //Loop through table rows
    for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
        var rowData = '';

        //Get column count from header row
        if (rowIndex == 0) {
           targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
           continue; //do not execute further code for header row.
        }
                
        //Process data rows. (rowIndex >= 1)
        for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
            rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent;
        }

        //If search term is not found in row data
        //then hide the row, else show
        if (rowData.indexOf(searchText) == -1)
            targetTable.rows.item(rowIndex).style.display = 'none';
        else
            targetTable.rows.item(rowIndex).style.display = 'table-row';
    }
}
function changestatus(status_val,order_id)
	{
		if(status_val=='8')
		{
			//alert("ok");
					$("#OrderPaymentOrderId").val(order_id);
				$("#OrderPaymentStatusVal").val(status_val);
				$("#OrderPaymentOrderStatusId").val(order_status_id);
				$("#OrderPaymentSelectedVal").val(selected_val);
				$('#myModalForMember').modal('show');
		}
		
		//alert(status_val+'--'+order_id);return false;
		$.ajax({
		type: "POST",
		url: BASE_URL + "orders/update_status",
		data: {'status_id': status_val, 'order_id': order_id, 'selected_text': 'cancel'},
		success: function (resp) {	
			location.reload();
			 
		}
	});
	}
	
	function update(payid)
	{
		alert(payid);
	$.ajax({
		type: "POST",
		url: "orders/update_check_status",
		data: 'payment_id='+payid,
		success: function (resp) {
			//alert(resp);
			//return false;	
			location.reload();
			 
		}
	});	
		
	}
	
</script>


<div class="new-order">
	<?php 
	echo $this->Html->script('sorttable');
	echo $this->Form->create("User",array("div"=>false,'type' => 'get')); ?>	
		<h2 class="col-sm-2"><?php echo 'View Orders'; ?></h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by any header','class'=>'form-control search_box','onkeyup'=>"doSearch();",'id'=>'searchTerm',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
			
			
			
		
				
				
			</div>
			
		</div>
		<div class="pull-right">
			<?php echo $this->Html->link('Advanced Search',
            '/orders/advancesearch',
            array('id'=>'submitbtn2','class'=>'add-btn')
        );?> 
		
		</div>
		<!--<div class="add-btn-outer pull-right">		
		<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/archived_orders-member') ?>" title="Archive old orders">Archive old orders</a>
		</div>-->
		
		<!--<div class="col-sm-3">
            <div class="sorting-select">
				<?php echo $this->Form->input('sort_rating',array("options"=>array("All"=>"All","Rating"=>"Rating"),"empty"=>"Sort By",'class'=>"selectpicker",'id'=>'sort_rating',"value"=>(isset($set_val['sort_rating']) && !empty($set_val['sort_rating']))?$set_val['sort_rating']:'','label'=>false,'div'=>false)); ?>
			</div>
		</div> -->
		<div class="clear"></div>
		<script>
		$(document).ready(
		function(){
			$(".class_toggle").click(function(){
			    $(this).parent().next().toggle();
				
					
					if($(this).parent().next().css('display') == 'none')
					{if($(this).hasClass("minus")){$(this).removeClass("minus");$(this).addClass("plus");}}
					else{
						if($(this).hasClass("plus")){$(this).removeClass("plus");$(this).addClass("minus");}
					}
				
			});
			
		}
		
		);
		</script>
		<div class="responsive-table">
			<table id="dataTable" class="table table-striped sortable">
				<thead>
				<tr>
				   <th style="width:19px;"></th>
					<th class="st">Order Id <?php //echo $this->Paginator->sort('id',"Order Id"); ?></th>
					<!--<th class="st">Patient</th>-->
					<th class="st">Lab</th>
					<th class="st">Doctor</th>
					<th class="st">Status</th>
					<th class="st">Payment Status</th>
					<th class="st">Price</th>
					<!--<th style="" class="st">Created</th>--> 
					<!--<th class="st">Driver</th>-->
					<th class="st"><span style="margin-left:11px;">Action</span></th>
				</tr>
				</thead>
                            <tbody>
				<?php  if(!empty($order_data)) {  $i=1;
				
					foreach ($order_data as $order){  
						
					$ordee = $order['Order']['id'];
	              $status =$this->requestAction('App/getstatus/'.$order['Order']['id']);
	              $paystatus =$this->requestAction('App/getorderpayment/'.$order['Order']['id']);
	              $orderdata=$this->requestAction('App/getorderdata/'.$order['Order']['id']);
	          
	             //$transaction=$this->requestAction('App/gettransaction/'.$order['OrderPayment']['id']);
	            // echo "<pre>"; print_r($orderdata); echo "</pre>";
	               // echo $status;
					if(($order['Status']['id'] == 7) || ($order['Status']['id'] == 8) || ($order['Status']['id'] == 9)) { ?>
						<tr class="<?php if($i%2==0){ echo "even"; } else {echo "odd"; } ?>"> 
						<td style="width:19px;" class="class_toggle plus"></td>
							<td class="details-control" ><?php $ordee_n = $order['Order']['order_manualid']; 
							
							?>
							<a href="<?php echo $this->Html->url('/view-order-staff/'.$order['Order']['id']); ?>" ><?php echo $ordee_n; ?></a>
							</td>
							
							
							<!--<td class="details-control"><?php $r = unserialize($order['OrderDetail']['details']); echo $r[0]['patientName']; ?></td>-->
							<td class="details-control" ><?php //
							$lab_id = $order['Order']['lab_id'];
							 $lab = $this->requestAction('App/companyname/'.$lab_id);
							echo $this->Html->link($lab , array("controller" => "profile", "action" =>$lab_id , $this->Common->makeurl($lab))); 
							 ?></td>
							 <td class="details-control">
							 <?php  
							 $doc_id = $order['Order']['doctor_id'];
							 $doc_name = $r[148]['doctorname'];
							 echo $this->Html->link($doc_name , array("controller" => "profile", "action" =>$doc_id , $this->Common->makeurl($doc_name))); 
							 ?>
							 
							 
							 </td>
							 
							 
							<td class="details-control"><?php 
echo '<a href="#" style="cursor:pointer;color:black;" data-toggle="modal" onclick = "callajax('.$ordee.');"class="open-mahiModal" data-target="#mahiModal" >';
echo $order['Status']['title']; 
echo '</a>';
?></td>
    
       <td><?php if($paystatus['Transactions']['chequenumber']!='' && $paystatus['Transactions']['payment_method']=='By Cheque' && $orderdata['Order']['check_recieved']=='1') { echo $orderdetail= $this->requestAction('App/getpaymentstatus/'.$ordee);  } 
       else if($paystatus['Transactions']['chequenumber']=='' && $paystatus['Transactions']['payment_method']=='Braintree'){
		   echo $orderdetail= $this->requestAction('App/getpaymentstatus/'.$ordee);
		   }
		   else if($paystatus['Transactions']['chequenumber']!='' && $paystatus['Transactions']['payment_method']=='By Cheque' && $orderdata['Order']['check_recieved']=='0')
		   {
			  echo "Paid by Check"; 
			   }
		   else
		   {
			 echo "Pending";
		   }
       
       
       ?> </td>
							<td class="details-control">
							<?php 

								$pre = $this->requestAction('App/getprepay/'.$lab_id);
								if($pre !=0){
								$pre_show = '<br>'."Prepay Amt: $".$order['Order']['budget']*$pre/100;
							}
							
							
							
							
							echo "Order Cost: $".$order['Order']['budget'];
							echo $pre_show;
							$pri2 = $this->requestAction('App/getprize/'.$ordee);
							if($pri2[1]['OrderPayment']['payment']){
							echo "<br>Delivery : $".$pri2[1]['OrderPayment']['payment'];
							}
							?>
							
							</td>
							
							<!--<td class="details-control"><?php //$date = explode(' ',$order['Order']['created']); 
							//echo $date[0];
							?></td>-->
							<!--<td class="details-control"><?php //$driver_detail = $this->requestAction('App/getdriver/'.$lab_id); 
							echo $driver_detail['Userdetail']['first_name'];
							?></td>-->
							<td class="details-control">
							<?php if($status=='Order Submitted'||$status=='Lab Rejected'||$status=='Lab Accepted'||
							$status=='In Progress'|| $status=='Remake Submitted'||$status=='Cancel'||$status=='Cancel'||$status=='Order Completed') {?>
								<div class="black-btn-outer pull-right">	
									<a href="<?php echo $this->Html->url('/view-order-staff/'.$order['Order']['id']);  ?>" class="black-btn" >View Order</a>
								</div>
							<?php }
							if($status=='Ready to Dispatch')
							{
								$option = array('8'=>'Biolab Pickup');
								echo $this->Form->input('select_action', array(
                                'options' => array($option),
                                'empty' => 'Select Action',
                                'onchange'=> "update_status(this.value,".$order['Order']['id'].")",
                                'style'=>'height:30px; border-radius:2px; color:#b2312c; opacity:9;'
    
));
							}
							if($status=='Biolab Pickup')
							{
								$option = array('9'=>'Biolab Delivered');
								echo $this->Form->input('select_action', array(
                                'options' => array($option),
                                'empty' => 'Select Action',
                                'onchange'=> "update_status(this.value,".$order['Order']['id'].")",
                                'style'=>'height:30px; border-radius:2px; color:#b2312c; opacity:9;'
    
));
							}
							
							if( $paystatus['Transactions']['chequenumber']!='' &&$paystatus['Transactions']['payment_method']=='By Cheque' && $orderdata['Order']['check_recieved']=='0')
							{
								$option = array('14'=>'Check Recieved');
								echo $this->Form->input('select_action', array(
                                'options' => array($option),
                                'empty' => 'Select Action',
                                'onchange'=> 'update("'.$paystatus['OrderPayment']['order_id'].'")',
                                'style'=>'height:30px; border-radius:2px; color:#b2312c; opacity:9;'
    
));
							}
							
							if($orderdata['Order']['check_recieved']=='1' && $paystatus['Transactions']['payment_method']=='By Cheque' && $paystatus['Transactions']['chequenumber']!='')
							{
								echo "Check Recieved";
							}
							if($paystatus['Transactions']['chequenumber']=='' && $paystatus['Transactions']['payment_method']=='Braintree' && $orderdata['Order']['check_recieved']=='0')
							{
								echo "Paid By Braintree";
							}
							
							
							
					
							
							?>
							</td>
						</tr>

<tr class="detail_data" style="display:none;">
	
	<td colspan="7"><?php  $r = unserialize($order['OrderDetail']['details']); echo "<b>Patient Name :</b> ".$r[0]['patientName']; ?><br>
	<?php $date = explode(' ',$order['Order']['created']); 
							echo "<b>Created Date :</b> ".$date[0];
							?><br>
							<?php $driver_detail = $this->requestAction('App/getdriver/'.$ordee);
							if($driver_detail != ''){ 
							echo "<b>Driver Name :</b> ".$driver_detail;
						}
						else{
							echo '<div style="color:red">Driver not assigned.</div>';
							}
							?><br><b>Pickup from : </b><?php $lab2 = $this->requestAction('App/companydetail/'.$lab_id);
							$state = $this->requestAction('App/getstate/'.$lab2['Userdetail']['state_id']);
							$st = $state[0]['State']['name'].', '.$state[0]['State']['code'].'-';
							echo $lab2['Userdetail']['address'].', '.$lab2['Userdetail']['city'].', '.$st.''.$lab2['Userdetail']['zipcode'];
							
							 ?><br> <b>Drop to : </b><?php $doc2 = $this->requestAction('App/companydetail/'.$doc_id);
							 $state1 = $this->requestAction('App/getstate/'.$doc2['Userdetail']['state_id']);
							$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].'-';
							  
							 echo $doc2['Userdetail']['address'].', '.$doc2['Userdetail']['city'].', '.$st1.''.$doc2['Userdetail']['zipcode'];
							 echo '<br>';
							 /*if($order['Order']['priority'] == 1){ echo '<b>Priority :</b> Morning';}
							 elseif($order['Order']['priority'] == 2){ echo '<b>Priority :</b> Afternoon';}
							 elseif($order['Order']['priority'] == 3){ echo '<b>Priority :</b> Rush';}
							 echo '<br>';
							 if($order['Order']['recipient'] == 1){ echo '<b>Recipient :</b> Yes';}else{echo '<b>Recipient :</b> No';}
							 */
						 	if($orderdata['Order']['check_recieved']=='1' && $paystatus['Transactions']['payment_method']=='By Cheque' && $paystatus['Transactions']['chequenumber']!='')
							{
								echo "<br>";
								echo "<b>Check Number</b> :".$paystatus['Transactions']['chequenumber'];
								
							} 
							 ?>
							 
							 
							 
							 </td>
	</tr>						
					<?php }
	 			$i++; } } else {  ?> 
					<td colspan="4" style="color:red;" class="leftalign">No record found</td>  
				<?php }?>
				</tbody>
			</table>
		</div>
	<?php echo $this->Element("pagination"); ?>
	<?php echo $this->Form->end(); ?>
</div>

<div id="mahiModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">ORDER STATUS</h2>
        </div>
        <div id="tabble" class="modal-body">
			
          
          
          </table>
        </div>
        
      </div>
      
       </div>
</div>
