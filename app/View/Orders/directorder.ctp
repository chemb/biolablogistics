﻿<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.css" />
<style>
.add-box{
border: 2px solid black;
padding: 10px;
}
/*
span h3{
    background: #fff;
    position: relative;
    top: 22px;
    width: 170px;
    margin-left: 20px;

} */
.login-box input[type="checkbox"] {
margin: 0 0px 0 0;
}
.gm-style-mtc {
  display: none;
}
.wrapper {
  margin: auto;
  max-width: none;
  width: 96%;
}
.gm-svpc {
  display: none;
}
.login-box {
    max-width: none;
    color: #707475;
    margin: 0;
}
.row.drop-off-state {
    display: none;
}

</style>
<div class="row">
	<div class="col-sm-6">
		<div class="login-box">	
			<!--  <h2>Create Order</h2>
				  <p class="text-center">On-demand deliveries for your lab.</p> -->
					<?php echo $this->Session->flash(); 
				 $dropa = $this->Session->read('Auth.User.Userdetail.address');
				 $dropc = $this->Session->read('Auth.User.Userdetail.city');
				 $drops = $this->Session->read('Auth.User.Userdetail.state_id');
				 $dropz = $this->Session->read('Auth.User.Userdetail.zipcode');
				 $droplat = $this->Session->read('Auth.User.Userdetail.latitude');
				 $droplng = $this->Session->read('Auth.User.Userdetail.longitude');

					?>
					<?php echo $this->Form->create('Directorder', array("enctype" => "multipart/form-data", 'onsubmit'=>'return valid();')); ?>
       
				<span>
				<h3>Dropoff</h3>
				 <div class="add-box">
				 <div class="row">
					 <div class="col-sm-6">
						<span>Lab Name : <?php echo $labName ?></span>
					 </div>
				 </div>
				 </div></br>
				<div class="add-box">
				<div class="row">
					<div class="col-md-9">
						<?php echo $this->Form->input('pickup_address',array('value'=>$dropa, 'class'=>'form-control','type'=>'text','label'=>false, 'placeholder' => 'Dropoff Address')); ?>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->input('pickup_city',array('value'=>$dropc, 'class'=>'form-control','type'=>'text','label'=>false, 'placeholder' => 'Apt / Suite')); ?>
					</div>
				</div>
<div class="row drop-off-state">
	<div class="col-md-6">
		
<?php 
         echo $this->Form->input('pickup_state', array(
         'options' => array($state),
         'empty' => 'Select State',
       'value'=>$drops, 
       'style'=>'border: 2px solid #000000;
          background: #FFFFFF;
          border-radius: 0px;
          height: 42px;
          margin-top: 16px;
          color: #000000; opacity: 1;',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
      )); ?>
		</div>
	<div class="col-md-6">
<?php echo $this->Form->input('pickup_zip',array('value'=>$dropz, 'onkeypress'=>'return numbersonly(this, event)', 'class'=>'form-control','maxlength'=>'5','type'=>'text','label'=>false, 'placeholder' => 'Dropoff Zip Code')); ?>
</div>
</div> 
<?php echo $this->Form->input('pickup',array('class'=>'form-control','type'=>'number','label'=>false, 'onkeypress'=>'return numbersonly(this,event)','placeholder' => 'No of package', 'style'=>'width: 100%;','min'=>'0')); ?> 
<div class="row">
<div class="col-sm-12">
<?php echo $this->Form->input('pick_patient_name',array( 'class'=>'form-control','type'=>'text','label'=>false, 'placeholder' => 'Enter Patient Name(Example : Jhon, Smith, Allen)')); ?>
<?php echo $this->Form->input('pick_lat',array('value'=>$droplat, 'class'=>'form-control','type'=>'hidden','label'=>false)); ?>
<?php echo $this->Form->input('pick_lng',array('value'=>$droplng, 'class'=>'form-control','type'=>'hidden','label'=>false)); ?>
</div>
</div>
</div>
 </span>   
       
<span>
<h3>Pickup</h3>
<div class="add-box">
 <div class="row">
	 <div class="col-sm-6">
		<div class="input text">
<span>Select Doctor :</span>
		</div>
	 </div>
	 <div class="col-sm-6">
		
		
<?php 
         echo $this->Form->input('doctor_id', array(
         'options' => $docList,
         'empty' => 'Select Doctor',
       'value'=>$drops, 
       'style'=>'border: 2px solid #000000;
          background: #FFFFFF;
          border-radius: 0px;
          height: 42px;
          margin-top: 16px;
          color: #000000; opacity: 1;',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
      )); ?>
		
	 </div>
 </div>
 </div></br>
 <div class="row" style="text-align:center;font-weight: bolder;margin-bottom: 10px;">
 <span>-----OR-----</span>
 </div>
<div class="add-box" style="margin-bottom: 16px;">
<div class="row">
	<div class="col-md-9">
       <?php echo $this->Form->input('dropoff_address',array('class'=>'form-control placepicker','type'=>'text','label'=>false, 'placeholder' => 'Pickup Address',)); ?>
       </div>
	   
	<div class="col-md-3">
       <?php echo $this->Form->input('dropoff_city',array('class'=>'form-control city','type'=>'text','label'=>false, 'placeholder' => 'Apt / Suite')); ?>
</div>
</div>
<div class="row">
	<div class="col-md-6">
<?php 
        /*  echo $this->Form->input('dropoff_state', array(
         'options' => array($state),
         'empty' => 'Select State',
       
       'style'=>'border: 2px solid #000000;
          background: #FFFFFF;
          border-radius: 0px;
          height: 42px;
          margin-top: 16px;
          color: #000000; opacity: 1;',
          'class'=>'form-control drop_state',
          'label'=>false ,
          'div'=>'true',
      )); */ ?>
  </div>
	<div class="col-md-6">
       <?php //echo $this->Form->input('dropoff_zip',array('onkeypress'=>'return numbersonly(this, event)', 'class'=>'form-control zip','maxlength'=>'5','type'=>'text','label'=>false, 'placeholder' => 'Pickup Zip Code')); ?>
</div>
</div> 
<?php echo $this->Form->input('dropoff',array('class'=>'form-control','type'=>'number','label'=>false, 'onkeypress'=>'return numbersonly(this,event)','placeholder' => 'Dropoff', 'style'=>'width: 100%;','min'=>'0','placeholder' => 'No of package')); ?> 
<div class="row">
<div class="col-sm-12">
<?php echo $this->Form->input('drop_patient_name',array( 'class'=>'form-control','type'=>'text','label'=>false, 'placeholder' => 'Enter Patient Name(Example : Jhon, Smith, Allen)')); ?>
<?php echo $this->Form->input('drop_lat',array('value'=>'', 'class'=>'form-control drop_lat','type'=>'hidden','label'=>false)); ?>
<?php echo $this->Form->input('drop_lng',array('value'=>'', 'class'=>'form-control drop_lng','type'=>'hidden','label'=>false)); ?>
</div>
</div>
</div>
 </span>

<div class="add-box">
<div class="row">
	<div class="col-md-6">  
            <?php
            if (date('H', time()) >= 8 && date('H', time()) <= 10) {
                $option = array('Afternoon'=>'Afternoon (1 PM – 2 PM)','Rush'=>'Rush');                
            } elseif (date('H', time()) >= 12 && date('H', time()) <= 14) {
                $option = array('Morning'=>'Morning (9 AM – 10 AM)', 'Rush'=>'Rush');
            } else {
                $option = array('Morning'=>'Morning (9 AM – 10 AM)','Afternoon'=>'Afternoon (1 PM – 2 PM)','Rush'=>'Rush');
            }
            echo $this->Form->input('priority', array('options' => array($option), 'empty' => 'Select Priority', 'style'=>'border: 2px solid #000000; background: #FFFFFF; margin-top: 16px; border-radius: 0px; height: 42px; color: #000000; opacity: 1;', 'class'=>'form-control', 'label'=>false, 'div'=>'true'));
            ?>
      </div>
      <div class="col-md-6">
      <?php echo $this->Form->input('pick_date',array('class'=>'form-control date-picker', 'id'=>'datepicker','type'=>'text','label'=>false, 'placeholder' => 'Date', 'style'=>'width: 100%;cursor: default;', 'readonly'=>'readonly')); ?>
      </div>
      </div>
      </div>
      <br>
      <?php 
	     $option = array('Yes'=>'Yes','No'=>'No');
         echo $this->Form->input('recipient', array(
         'options' => array($option),
         'empty' => 'Recipient Needed',
       'default' => 'No',
       'style'=>'border: 2px solid #000000;
          background: #FFFFFF;
          border-radius: 0px;
          height: 42px;
          opacity: 1;',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
      )); ?>
      <?php echo $this->Form->input('order_status',array('type'=>'hidden', 'value' => 'Order Submitted')); ?>
      <!-- <?php echo $this->Form->input('price',array('class'=>'form-control','type'=>'number','label'=>false, 'placeholder' => 'Price', 'style'=>'width: 100%;' ,'onkeypress'=>'return numbersonly(this,event)')); ?> -->
      
      <?php echo $this->Form->input('note',array('class'=>'form-control','type'=>'textarea','label'=>false,'rows' => 3, 'placeholder' => 'Note: Here you may enter the name of the recipient or a best place to park, anything which will help us deliver faster or more efficiently.', 'style'=>'width: 100%;')); ?>
      <div style="text-align:center;padding: 17px;">
<?php echo $this->Form->input('condition',array('id'=>'term', 'type'=>'checkbox', 'label'=>false, 'div'=>false)); ?>
By selecting the "place order" button below, you accept the biolablogistics.com 
    <a href="#" id="red" class="red" data-target="#mahiModal" data-toggle="modal">Terms of Use</a> and the biolab terms of shipping in the applicable biolab Service Guide.
<div> 
        <div class="text-center">
            <div class="login-btn-outer">
            <?php echo $this->Form->submit(__('Place order', true), array('class' => 'login-btn', 'id'=>'orderBtn')); ?>
               
            </div>
        </div>
    

        <?php echo $this->Form->end(); ?>
   
</div>


<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script>
  
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function valid(){
var flag = true;
if(document.getElementById('DirectorderPickupAddress').value=='')
{
document.getElementById('DirectorderPickupAddress').style.border="1px solid red";
document.getElementById('DirectorderPickupAddress').placeholder = "Please enter the address";
flag = false;	
}else{document.getElementById('DirectorderPickupAddress').style.border="1px solid green";}

if(document.getElementById('DirectorderPickupCity').value=='')
{
document.getElementById('DirectorderPickupCity').style.border="1px solid red";
document.getElementById('DirectorderPickupCity').placeholder = "Please enter the city";
flag = false;
}else{document.getElementById('DirectorderPickupCity').style.border="1px solid green";}
if(document.getElementById('DirectorderPickupState').value=='')
{
document.getElementById('DirectorderPickupState').style.border="1px solid red";
document.getElementById('DirectorderPickupState').placeholder = "Please enter the city";
flag = false;

}else{document.getElementById('DirectorderPickupState').style.border="1px solid green";}
/*
if((document.getElementById('DirectorderPickupZip').value).length < 5)
{
document.getElementById('DirectorderPickupZip').style.border="1px solid red";
document.getElementById('DirectorderPickupZip').placeholder = "Please enter valid zipcode";
flag = false;	
} 
if(document.getElementById('DirectorderPickupZip').value=='')
{
document.getElementById('DirectorderPickupZip').style.border="1px solid red";
document.getElementById('DirectorderPickupZip').placeholder = "Please enter the zipcode";
flag = false;	
} 

if((document.getElementById('DirectorderPickupZip').value).length == 5)
{
document.getElementById('DirectorderPickupZip').style.border="1px solid green";}
*/
if(document.getElementById('DirectorderDropoffAddress').value=='')
{
document.getElementById('DirectorderDropoffAddress').style.border="1px solid red";
document.getElementById('DirectorderDropoffAddress').placeholder = "Please enter the address";
flag = false;	
}else{document.getElementById('DirectorderDropoffAddress').style.border="1px solid green";}

if(document.getElementById('DirectorderDropoffCity').value=='')
{
document.getElementById('DirectorderDropoffCity').style.border="1px solid red";
document.getElementById('DirectorderDropoffCity').placeholder = "Please enter the city";
flag = false;	
}else{document.getElementById('DirectorderDropoffCity').style.border="1px solid green";}
/*if((document.getElementById('DirectorderDropoffZip').value).length < 5)
{
document.getElementById('DirectorderDropoffZip').style.border="1px solid red";
document.getElementById('DirectorderDropoffZip').placeholder = "Please enter valid zipcode";
flag = false;	
}
if(document.getElementById('DirectorderDropoffZip').value=='')
{
document.getElementById('DirectorderDropoffZip').style.border="1px solid red";
document.getElementById('DirectorderDropoffZip').placeholder = "Please enter the zipcode";
flag = false;	
}
if((document.getElementById('DirectorderDropoffZip').value).length == 5)
{document.getElementById('DirectorderDropoffZip').style.border="1px solid green";}*/

if(document.getElementById('DirectorderPickup').value=='')
{
document.getElementById('DirectorderPickup').style.border="1px solid red";
document.getElementById('DirectorderPickup').placeholder = "Please enter no of packages";
flag = false;	
}else{document.getElementById('DirectorderPickup').style.border="1px solid green";}

if(document.getElementById('DirectorderPriority').value=='')
{
document.getElementById('DirectorderPriority').style.border="1px solid red";
flag = false;	
}else{document.getElementById('DirectorderPriority').style.border="1px solid green";}

if(document.getElementById('DirectorderRecipient').value=='')
{
document.getElementById('DirectorderRecipient').style.border="1px solid red";
flag = false;	
}else{document.getElementById('DirectorderRecipient').style.border="1px solid green";}
if(document.getElementById('DirectorderPickupState').value=='')
{
document.getElementById('DirectorderPickupState').style.border="1px solid red";
flag = false;	
}else{document.getElementById('DirectorderPickupState').style.border="1px solid green";}
/*if(document.getElementById('DirectorderDropoffState').value=='')
{
document.getElementById('DirectorderDropoffState').style.border="1px solid red";
flag = false;	
}else{document.getElementById('DirectorderDropoffState').style.border="1px solid green";}*/

if((document.getElementById('term').checked==false) && (document.getElementById('DirectorderRecipient').value !='')){

document.getElementById('red').click();
flag = false;
}
if(document.getElementById('datepicker').value=='')
{
document.getElementById('datepicker').style.border="1px solid red";
flag = false;	
}else{document.getElementById('datepicker').style.border="1px solid green";}

document.getElementById('DirectorderDropoff').style.border="1px solid green";

return flag;
}

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


</script>
</div>
        
      </div>
	  </div>
<div class="col-sm-6">
    <style type="text/css">
html { height: 100% }
body { height: 100%; margin: 0px; padding: 0px }
</style>

<script type="text/javascript">
  var directionDisplay;
  var directionsService = new google.maps.DirectionsService();
  var map;

  var origin = "<?php echo $origin?>";
  var destination = "<?php echo $destination?>";
  var startlat = "<?php echo $droplat?>";
  var startlng = "<?php echo $droplng?>";
  
  
  function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    //var chicago = new google.maps.LatLng(41.850033, -87.6500523);
	var start = new google.maps.LatLng(startlat, startlng);
	//alert(start);
    var myOptions = {
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: start
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
    //calcRoute();
  }
  function initializeWaypointsTo(toAddress){
	  //alert(toAddress);
	  calcRoute(toAddress);
	  
  }
  function calcRoute(toAddress) {

  var locationsAll=new Array();
  var someValue="<?php echo $this->Session->read('Auth.User.Userdetail.address'); ?>";
  locationsAll.push(someValue);
  locationsAll.push(toAddress);
  //alert(JSON.stringify(locationsAll));
    var request = {
        // from: Blackpool to: Preston to: Blackburn
        origin: someValue, 
        destination: toAddress, 
        
        travelMode: google.maps.DirectionsTravelMode.WALKING
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        var route = response.routes[0];
       //alert("Response got");
      } else {
        //alert("directions response "+status);
      }
    });
  }

     
google.maps.event.addDomListener(window, 'load', initialize);


</script>
<h3></h3>
<div id="map_canvas" style="margin-top:35px;float:left;width:100%;height:470px;"></div>


</div><!-- End Map Area-->
</div>
<div id="mahiModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">Terms of Use</h2>
          
        </div>
        <div id="tabble" class="modal-body">
		
	<div>Do you agree with terms and conditions?</div><br>
	
      <p style="margin:0px;padding:0px;">Yes<input id="yes" type="radio" name="terms" value="yes" style="  width: 9%;"></p>
      <p style="margin:0px;padding:0px;">No&nbsp;<input id="no" type="radio" name="terms" value="no" style="  width: 9%;"></p>     

        </div>
        
      </div>
      
       </div>
</div>

<script src="<?php echo $this->webroot; ?>assets/jquery.placepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(".placepicker").placepicker();

$('.placepicker').bind('change keyup',function(e){
var add=$('.placepicker').val();
 $.ajax({ url:"http://maps.googleapis.com/maps/api/geocode/json?address="+add+"&sensor=true",
         success: function(data){
       console.log(JSON.stringify(data));
                      /*for (var component in data.results[0]['address_components']) {
                         for (var i in data.results[0]['address_components'][component]['types']) {
                            if (data.results[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
                                state = data.results[0]['address_components'][component]['long_name'];
                                $('.city').val(data.results[0]['address_components'][4]['long_name']);
								if(data.results[0]['address_components'][5]['long_name'])
                                $('.zip').val(data.results[0]['address_components'][5]['long_name']);
                            }
                        }                     
                    }   */
					
            // alert(data.results[0].formatted_address);
			        for (var ii = 0; ii < data.results[0].address_components.length; ii++){
						var street_number = route = street = city = state = zipcode = country = formatted_address = '';
						var types = data.results[0].address_components[ii].types.join(",");
						if (types == "street_number"){
						  street_number = data.results[0].address_components[ii].long_name;
						}
						if (types == "route" || types == "point_of_interest,establishment"){
						  route = data.results[0].address_components[ii].long_name;
						}
						//if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political"){
						if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political"){
						  city = (city == '' || types == "locality,political") ? data.results[0].address_components[ii].long_name : city;
						 $('.city').val(city);
						}
						if (types == "administrative_area_level_1,political"){
						  state = data.results[0].address_components[ii].short_name;
						}
						if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
						  zipcode = data.results[0].address_components[ii].long_name;
						  $('.zip').val(zipcode);
						}
						if (types == "country,political"){
						  country = data.results[0].address_components[ii].long_name;
						}
					}
					
					var lat = data.results[0].geometry.location.lat;
					var lng = data.results[0].geometry.location.lng;
					$('.drop_lat').val(lat);
					$('.drop_lng').val(lng);
					//alert(add);
					$.ajax({   
						type: "POST",
						url:'<?php echo $this->Html->url(array('action' => 'ajaxmap', 'controller' => 'orders')); ?>',             
						data: {lat:lat, lng:lng, address:add},                 
						success: function(response){  
						                  
						  console.log(response);
						  //$("#orderMap").html(response);
						}
		            })
					
					initializeWaypointsTo(add);
					
         }
   });
});

$('#DirectorderDoctorId').on('change', function() {
   var ID = $(this).val();
 /*  $.ajax({   
        type: "POST",
        url:'<?php echo $this->Html->url(array('action' => 'ajaxmap', 'controller' => 'orders')); ?>',             
        data: {uid:$('#DirectorderDoctorId').val()},                 
        success: function(response){  
		                   
          console.log(response);
		  
			//$("#orderMap").html(response);
        }
	});
	*/
	$.ajax({   
        type: "POST",
        url:'<?php echo $this->Html->url(array('action' => 'getAddress', 'controller' => 'orders')); ?>',             
        data: {uid:$('#DirectorderDoctorId').val()},                 
        success: function(response){  
		  var obj = JSON.parse(response);
          //console.log(response );
          //console.log(response);
		  $('.placepicker').val(obj.Userdetail.address);
		  $('.city').val(obj.Userdetail.city);
		  $('.zip').val(obj.Userdetail.zipcode);
		  $('.drop_state').val(obj.Userdetail.state_id);
		  $('.drop_lat').val(obj.Userdetail.latitude);
		  $('.drop_lng').val(obj.Userdetail.longitude);
		  //alert(obj.Userdetail.address);
		  initializeWaypointsTo(obj.Userdetail.address);
			//$("#orderMap").html(response);
        }
	});
});


$('#datepicker').click(function(e){
//var date=document.getElementById('datepicker').value;

$('.datepicker').css({"display":"block"});
e.stopPropagation();
});
$('body').click(function(){
var date=document.getElementById('datepicker').value;
if(date!='')
{
$('.datepicker').css({"display":"none"});
}
});
</script>
<script type="text/javascript">
       $("input:radio").change(function(){
       //alert($(this).val());
       if ($(this).val() =='yes') {
    
     $("#term").prop("checked", true);
     document.getElementById('orderBtn').click();
      $('#mahiModal').modal('hide');
    }
    if ($(this).val() == 'no') {
  
     alert("Please Accept terms and conditions to proceed.");
     return false;
    }
       });
       </script> 
