<div class="new-order">
	<style>
		.statelist label{ display:none;}
		</style>
	<script type="text/javascript">
	
	$(document).ready(function(){
  $("#ordersSearchtype").prop("selectedIndex", 0);
});
	
	function get_search(val)
	{
		//alert(val);
		if(val==1)
		{
			document.getElementById('statelist').style.display='block';
			document.getElementById('citybox').style.display='none';
			
		}
		if(val==2)
		{
			document.getElementById('citybox').style.display='block';
			document.getElementById('statelist').style.display='none';
		}
		if(val==2)
		{
			return false;
		}
		
	}
	function changestatus(status_val,order_id)
	{
		if(status_val=='8')
		{
			//alert("ok");
					$("#OrderPaymentOrderId").val(order_id);
				$("#OrderPaymentStatusVal").val(status_val);
				$("#OrderPaymentOrderStatusId").val(order_status_id);
				$("#OrderPaymentSelectedVal").val(selected_val);
				$('#myModalForMember').modal('show');
		}
		
		//alert(status_val+'--'+order_id);return false;
		$.ajax({
		type: "POST",
		url: BASE_URL + "orders/update_status",
		data: {'status_id': status_val, 'order_id': order_id, 'selected_text': 'cancel'},
		success: function (resp) {	
			location.reload();
			 
		}
	});
	}
	
	
</script>
	<?php 
	echo $this->Html->script('sorttable');
	echo $this->Form->create("orders",array('action'=>'advancesearch','type'=>'post')); ?>	
		<h2 class="col-sm-2">Search by :</h2>
		<div class="col-sm-3" style="margin-top: 8px;"> 
		
				<?php $option = array('1'=>'State','2'=>'City',);
				echo $this->Form->input('searchtype', array(
                                'options' => array($option),
                                'empty' => 'Select',
                                'onchange'=> "get_search(this.value)",
                      
                                'div' => true,
                                'class' => 'selectpicker',
                                'label' => false
    
));?></div>
							
					
				<span id="statelist" style="display:none">
				<div class="col-sm-3" style="margin-left: 20px;margin-top: 8px;">
				<?php 	foreach($state as $val)
					{
						
					$option[$val['State']['id']] = $val['State']['name'];
					}		
							
							echo $this->Form->input('stateid', array(
                                'options' => array($option),
                                'empty' => 'Select State',
                                //'onchange'=> "get_search(this.value)",
                                
                                'div' => true,
                                'class' => 'selectpicker',
                                 'label' => false
)); ?>
</div>


		 
</span>

<span id="citybox" style="display:none">
<div class="col-sm-3">
<div class="login-box" style="margin-top: 8px;">
<?php echo $this->Form->input("cityname",array("type"=>'text',"placeholder"=>'Enter City Name','class'=>'form-control search_box','onkeyup'=>"doSearch();",'id'=>'searchTerm',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
</div>
</div>


		 
	</span>
<div class="col-sm-3" style="margin-top: 8px;">
<?php echo $this->Form->submit("Search",array("label"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); 
		 echo $this->Form->end(); ?>
		 </div>
		
		
			
			
	<br><br><br><br><br>
		
		<div class="responsive-table">
			<table id="dataTable" class="table table-striped sortable">
				<thead>
				<tr>
				
					<th class="st">Order ID</th>
					<th class="st">Pickup</th>
					<th class="st">Dropup</th>
					<th class="st">Driver</th>
					<th class="st">Action</th>
				</tr>
				</thead>
                            <tbody>
<?php

//print_r($order_data);
 
if(!empty($order_data)) { 

foreach ($order_data as $order){ 
	//print_r($order);echo "<hr>";
$ordee_st =	$this->requestAction('App/getstatus/'.$order['Order']['id']);
	if($ordee_st == 'Ready to Dispatch' || $ordee_st == 'Biolab Pickup'){
?>
<tr>
<td><?php $ordee_n = $order['Order']['order_manualid']; 
?>
<a href="<?php echo $this->Html->url('/view-order-staff/'.$order['Order']['id']); ?>" ><?php echo $ordee_n; ?></a>
</td>
<td><?php //echo $order['Lab']['user_id']; 
$state = $this->requestAction('App/getstate/'.$order['Lab']['state_id']);
$st = $state[0]['State']['name'].', '.$state[0]['State']['code'].' ';
echo $order['Lab']['address'].'<br>'.$order['Lab']['city'].', '.$st.''.$order['Lab']['zipcode'];

?></td>
<td><?php //echo $order['Doctor']['user_id']; 
$state = $this->requestAction('App/getstate/'.$order['Doctor']['state_id']);
$st = $state[0]['State']['name'].', '.$state[0]['State']['code'].' ';
echo $order['Doctor']['address'].'<br>'.$order['Doctor']['city'].', '.$st.''.$order['Doctor']['zipcode'];
?></td>
<td><?php $dri = $this->requestAction('App/getdriver/'.$order['Order']['id']); 
if($dri != ''){echo $dri;}
else{echo '<p style="color:red">Driver not assigned.</p>';}


?></td>
<td><?php if($ordee_st=='Ready to Dispatch')
							{
								$option = array('8'=>'Biolab Pickup');
								echo $this->Form->input('select_action', array(
                                'options' => array($option),
                                'empty' => 'Select Action',
                                'onchange'=> "update_status(this.value,".$order['Order']['id'].")",
                                'style'=>'height:30px; border-radius:2px; color:#b2312c; opacity:9;'
    
));
							}
							if($ordee_st=='Biolab Pickup')
							{
								$option = array('9'=>'Biolab Delivered');
								echo $this->Form->input('select_action', array(
                                'options' => array($option),
                                'empty' => 'Select Action',
                                'onchange'=> "update_status(this.value,".$order['Order']['id'].")",
                                'style'=>'height:30px; border-radius:2px; color:#b2312c; opacity:9;'
    
));
							}
							
							?></td>
</tr>
<?php
}
}
}
else{ ?>
	<td colspan="5" style="color:red;" class="leftalign">No record found. Only order ready to dispatch and biolab pickup are searchable.  </td> 
<?php
}
	?>			
				</tbody>
			</table>
		</div>

	
</div>

<div id="mahiModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">ORDER STATUS</h2>
        </div>
        <div id="tabble" class="modal-body">
			
          
          
          </table>
        </div>
        
      </div>
      
       </div>
</div>
<style>
.login-box {
margin:0;
}
</style>

				
