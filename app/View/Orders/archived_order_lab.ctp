<div class="new-order">
		<?php if($this->Session->read('Auth.User.profile_status') == 0) { ?>
	<label id="ww">Your profile has not been activated yet, To send request please <a href="<?php echo $this->Html->url("/send-email"); ?>" title="Send Approval Email">Click here</a></label>
	<?php } else { ?>
	<?php echo $this->Session->Flash();?>
		<?php echo $this->Form->create("User",array("div"=>false,'type' => 'get')); ?>	
		<h2 class="col-sm-2">Orders Requests</h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by Order Id, Status','class'=>'form-control',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
					
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<div class="add-btn-outer pull-right">		
		<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/view-orders') ?>" title="All orders">All orders</a>
		</div>
	<div class="responsive-table">
		<table class="table table-striped">
			<tr>
				<th>Doctor Details</th>
				<th>Order Reference Id</th>
				<th>Order Charges</th>
				<th>Order Status</th>
				<th>&nbsp;</th>
			</tr>			
			<?php foreach($orders as $order){
				if($order['Status']['id'] == 12 || $order['Status']['id'] == 13) {	
				if(  !empty($order['OrderRequest']['lab_id']) && $order['OrderRequest']['lab_id'] == $this->Session->read("Auth.User.id") ){ ?>
					<tr>
						<td>
							<a href="<?php echo $this->Html->url(array("controller" => "profile", "action" => $order['Order']['doctor_id'],$this->Common->makeurl(($order['Doctor']['first_name'] . ' ' . $order['Doctor']['last_name'])))); ?>"><?php echo $order['Doctor']['first_name'].' '.$order['Doctor']['last_name']; ?></a>
							<br />
							<?php echo $order['Doctor']['phone'];?><br />
							<?php echo $order['Doctor']['city'];?><br />
						</td>
						<td><?php echo $order['Order']['id'];?></td> 
						<td>
							<?php echo  !empty($order['OrderPayment']['payment']) ? '$'.$order['OrderPayment']['payment'] : 'N-A'; ?>
						</td> 
						<?php if(empty($order['Order']['lab_id'])) { ?>
							<?php if($order['Status']['id'] == 12){ ?>
								<td class="text-success">Cancel</td>								
								<?php } else if($order['Status']['id'] == 1) {?>
									<td class="text-success">Waiting</td>
									<?php } ?>
							<?php if($order['Status']['id'] == 3 && $order['OrderStatus']['updated_by'] == $this->Session->read("Auth.User.id") ) { ?>
								<td class="text-success"><?php echo $order['Status']['title'];?></td>
							<?php } ?>
						<?php } elseif($order['Status']['id'] == 11 && $order['OrderStatus']['updated_by'] != $this->Session->read("Auth.User.id")){?>
							<td class="text-success"><?php echo $order['Status']['title'];?></td>							
						<?php }	 elseif($order['Order']['lab_id'] == $order['OrderRequest']['lab_id']) { ?>
							<td class="text-success"><?php echo $order['Status']['title']; ?></td>
						<?php } else { ?>
							<td class="text-success"><?php echo 'Already picked by other lab';?></td>
						<?php } ?>
						<td>
							<div class="black-btn-outer pull-right">								
								<?php echo $this->Html->link("View Order",array("controller"=>"orders","action"=>'viewOrderForLab',$order['Order']['id']),array("class"=>"black-btn"));?> 
							</div>
						</td>
					</tr>
				<?php } 
			}
		 } ?>
			<?php if(empty($orders)){ ?>
				<tr>
					<td colspan="4" style="color:red;">No Order Found</td>
				</tr>
			<?php }?>
		</table>
	</div>
	<?php echo $this->Element("pagination"); ?>
	<?php } ?>
</div>
