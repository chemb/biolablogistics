<div class="categories form">
<?php echo $this->Form->create('Transaction',array('novalidate'=>true)); ?>
    <fieldset>
        <legend><?php echo __('Payment refrence form'); ?></legend>
	<?php
		
		echo $this->Form->input('payment_reference');
		echo $this->Form->input('payment_note', array('type' =>'textarea'));
		echo $this->Form->hidden('order_payment_id');
		

	?>
    </fieldset>
    <a class="btn btn-grey" onclick="history.go(-1);">Cancel</a>
<?php echo $this->Form->submit(); ?>
  
 <?php echo $this->Form->end(); ?>
  
</div>
