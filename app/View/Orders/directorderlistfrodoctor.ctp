<?php 
	echo $this->Html->script('sorttable');
	//echo $this->html->css('style2');
$script[] = '{title: "' . $dodata[0]["Directorder"]["doctor"]["UserDetail"]["address"] . '",'.
	'lat: "' . $dodata[0]["Directorder"]["doctor"]["UserDetail"]["latitude"] . '",' .
	'lng: "' . $dodata[0]["Directorder"]["doctor"]["UserDetail"]["longitude"] . '",' .
	'icon: "/icons/icon.php?t=doctor"}' .
	'';
foreach ($dodata as $order)	{
	$script[] = '{title: "' . $order["Directorder"]["lab"]["UserDetail"]["address"] . '",'.
		'lat: "' . $order["Directorder"]["lab"]["UserDetail"]["latitude"]  . '",' .
		'lng: "' . $order["Directorder"]["lab"]["UserDetail"]["longitude"]  . '",' .
		'icon: "/icons/icon.php?t=lab&h=' . $order["Directorder"]["oid"] . '"}' .
		'';
}

	$scriptOut = 'var markers = [' . implode(',', $script) . '];';

	?>

<style>
.select label{ display:none;}
table.sortable thead {
	    font-size: smaller;
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
.st a{
color :white !important;
}
.inputt {
    border-bottom: 1px solid #000 !important;
    border-left: 0;
    border-right: 0;
    border-top: 0;
    outline: none;
	background-color: transparent;
    width:98% !important;
    //line-height:20px;
}
.search-btn {
	    height: 38px !important;
}

.col-sm-3 {
    width: initial;
}
.mybutto {
	border: 1px solid darkgrey;
    padding: 7px;
    border-radius: 4px;
    background: white;
    color: #2a6496 !important;
    
}
.mybutto:hover {
	background: whitesmoke;
}
#pageNavPosition{
	float: right;
	margin-top: 20px;
}
.pg-selected {
	border: 1px solid darkgrey;
    padding: 7px;
    border-radius: 4px;
    background: white;
        color: maroon !important;
}
.wrapper {
    width: 96%;
    max-width: none;
    margin: auto;
}
.gm-style-mtc {
    display: none;
}
</style>

<div class="new-order ">
<?php if($archive == 1){ ?>
<h2 class="col-lg-3">Archive Orders</h2>
<?php }else { ?>
<h2 class="col-lg-3">Orders</h2>
<?php } ?>
		
		<div class="col-sm-5" style="width:initial;"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text','id'=>'search',"placeholder"=>'Search by any header','class'=>'form-control search_box',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				
				<span id="submitbtn" class="invite-button nomargin submitsearch search-btn">Search</span>
					
			</div>
			
		</div>
		<br>
		<div class="col-sm-3"> 
		<a id="add_btn" style="margin-top: 3px;background: #008080 !important;
	color: white !important;" class="add-btn" href="<?php echo $this->Html->url('directorder') ?>" >Schedule Delivery</a>
		</div>
		<div class="col-sm-2"> 
			<?php if($archive == 1){ ?>
		<a id="add_btn" style="mzargin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('directorderlist') ?>" >All Orders</a>
		<?php } else { ?>
			
			<a id="add_btn" style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('directorderlist/1') ?>" >Completed Delivery</a>
			<?php } ?>
		</div>
		<div class="clear"></div>
<?php echo $this->Session->flash(); ?>
<div class="row">
<div class="col-md-12">
<div class="col-md-8">
<div class="responsive-table">
		<table id="table" class="sortable table table-striped">
			<thead>
			<tr>
<th>&nbsp;</th>

                             <th class="st">Order ID</th>
                               <!-- <th class="st">From</th> -->
                                <th class="st">From</th>
                                <!--<th class="st">Pickup</th>
                                <th class="st">Dropoff</th>  
                                <th class="st">Priority</th>   
                                <th class="st">Recepient</th>-->   
                                <th class="st">Order Status</th>   
                                <th class="st">Patient</th>   
                                <th class="st">Price</th>   
                               <!-- <th class="st">Payment Status</th> -->
                                <th>&nbsp;</th>          
			</tr>	
			 </thead>
                   <tbody>
       <?php  

      if(!empty($dodata)){
      $i=0;
       foreach($dodata as $order){
       //print_r($order);
       ?>
			<tr>
			<td style="width:19px;" class="class_toggle plus"></td>
<!--<td><?php  
 //echo $this->Form->checkbox('arc', array('hiddenField' => false,'value'=>$order['Directorder']['oid'],'myarray'=>$order['Directorder']['payment_status'],'class'=>'arc','id'=>'arc_'.$i));
 $i++;

?></td> -->
			<td><?php echo '#'.$order['Directorder']['oid'];?></td>
			<!-- <td><?php 
			// $state1 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
			// $st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			// echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip']; ?></td> -->
			<td><?php 
			$state2 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st2.''.$order['Directorder']['pickup_zip'];
			?></td>
			<td><?php echo $order['Directorder']['order_status']; ?></td>
			<td><?php echo $order['Directorder']['drop_patient_name']; ?> </td>
			<td><?php echo ($order['Directorder']['aprice'] == '')? '$'.$order['Directorder']['price'] : '$'.$order['Directorder']['aprice']; ?></td>
			<!-- <td><?php // echo ($order['Directorder']['payment_status'] == 0)? 'Pending':'Payment Done'; ?></td> -->
	<td><?php /*if($order['Directorder']['payment_status'] == 0){ 
		echo '<div class="black-btn-outer pull-right">';
echo $this->Html->link("Make Payment",array("controller"=>"orders","action"=>'laborderpayment',$order['Directorder']['oid']),array("class"=>"black-btn"));
	echo '</div>';
}else{
	$ordeee = '#'.$order['Directorder']['oid'];
	$payments = $this->requestAction('App/labpriceshow/'.$ordeee);
	if($payments['Labtransaction']['adminpay_amt'] == ''){
	 echo 'Total : $'.$payments['Labtransaction']['pay_amount']; 
	 }
	 else{
	 echo '<span style="font-size:11px;">Initial Amt : $'.$payments['Labtransaction']['pay_amount'].'<br>Outstanding Amt : $'.$payments['Labtransaction']['adminpay_amt'].'<br>Total : $'.($payments['Labtransaction']['pay_amount'] + $payments['Labtransaction']['adminpay_amt']).'</span>';
	 }
}*/
		?>
		</td>
			</tr>
			<tr class="detail_data" style="display:none;">
			<td>&nbsp;</td>
			<td><?php
			$distance = $this->requestAction('App/getzipdistance/'.$order['Directorder']['pickup_zip'].'/'.$order['Directorder']['dropoff_zip']);
			 echo '<b>Date :</b><br>'.$distance; 
			 ?></td>
<td><?php
if(($order['Directorder']['apickup'] == '0')){
 echo '<b>Dropoff</b><br>'.$order['Directorder']['pickup'];}else {
echo '<b>Dropoff</b><br>'.$order['Directorder']['apickup'];}

?></td>
<td><?php 
if($order['Directorder']['adropoff'] == '0'){
echo '<b>Pickup</b><br>'.$order['Directorder']['dropoff'];} else {
echo '<b>Pickup</b><br>'.$order['Directorder']['adropoff']; }

?></td>
			<td><?php echo '<b>Priority</b><br>'.$order['Directorder']['priority']; ?></td>
			<td><?php echo '<b>Recepient</b><br>'.$order['Directorder']['recipient']; ?></td>
			<td><?php echo '<b>Note</b><br>'.$order['Directorder']['note']; ?></td>
			<td><?php echo '<b>Date</b><br>'.$order['Directorder']['pick_date']; ?></td>
			</tr>
			
			
			<?php } }else{ ?>
				<tr>
					<td colspan="7" style="color:red;">No Order Found</td>
				</tr> 
			<?php } ?>
			
			</tbody>
		</table>
	</div>

</div>

<div class="col-md-4" >
<style type="text/css">
html { height: 100% }
body { height: 100%; margin: 0px; padding: 0px }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
			  var directionDisplay;
			  var directionsService = new google.maps.DirectionsService();
			  var map;
			  //var origin = "<?php echo $origin?>";
			  var start_end = JSON.parse('<?php echo $start_end?>');;
			  var location_data = JSON.parse('<?php echo $locationData?>');
			  var len =  location_data.length;
			  /*for(var i=0;i<len;i++ ){
				alert(start_end.origin[i])  
                alert(location_data[i]);
              }*/
			  console.log(location_data);
              console.log(start_end);
			   function renderDirections(result) { 
			   //alert(JSON.stringify(result));
        var directionsRenderer = new google.maps.DirectionsRenderer({
			suppressMarkers: true
		}); 
        directionsRenderer.setMap(map); 
        directionsRenderer.setDirections(result); 
      }     

    function requestDirections(start, end,waypnt) { 
      directionsService.route({ 
        origin: start, 
        destination: end, 
		waypoints: waypnt,
		optimizeWaypoints: true,
		avoidTolls: true,
        travelMode: google.maps.DirectionsTravelMode.DRIVING 
      }, function(result) { 
        renderDirections(result); 
      }); 
    } 

	function initialize() {
		var start = new google.maps.LatLng(51.47482547819850,-0.37739553384529);
		var infowindow = new google.maps.InfoWindow();
		var myOptions = {
			zoom:7,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: start
		};
		<?php echo $scriptOut; ?>

		console.log(markers);
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

		markers.forEach(function(item, i, arr) {
			var image = {
				url: item.icon,
				size: new google.maps.Size(32, 48),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(16, 24)
			};
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(item.lat, item.lng),
				title: item.title,
				icon: image,
				map: map
			});
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.setContent(item.title);
				infowindow.open(map, marker);
			});
		});

		for(var i=0;i<len;i++){
		requestDirections(start_end.origin[i], start_end.destination[i],location_data[i]); 
		}
		//requestDirections('EC1V 0AH', '(51.47615506206120, -0.37795315370703)'); 	
	
				//calcRoute();
				showDriver();
    }
			  function plotDrivers(){
				  for(i=0;i<gpsArray.length;i++){
					  //alert(JSON.stringify(parseFloat(gpsArray[i].latitude)));
					
					  var latLng=new google.maps.LatLng(parseFloat(gpsArray[i].latitude),parseFloat(gpsArray[i].longitude));
					  var iconBase = 'http://biolablogistics.com/images/icon/';
				   var marker = new google.maps.Marker({
				position: latLng,
				map: map,
				icon: iconBase + 'person.png',
				title: 'Driver is here!'
			  });
			  }
			  }
			  function calcRoute() {
	
			  }     
			google.maps.event.addDomListener(window, 'load', initialize);
			</script>
			<script>
			var gpsArray;
			  var doctor_id=<?php echo $this->Session->read('Auth.User.id'); ?>;
			function showDriver() {
			  xhttp = new XMLHttpRequest();
			  xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
				gpsArray = JSON.parse(xhttp.responseText);
				//alert(JSON.stringify(gpsArray[0]));
				if(gpsArray.length>0)
					plotDrivers();
				}
			  };
			  xhttp.open("GET", "<?php echo Router::url('/', true)?>api/fetchdriverorder.php?driverGPSCoordinateforDoctor=true&doctor_id="+doctor_id, true);
			  xhttp.send();
			  
			}

			</script>
<div id="map_canvas" style="float:left;width:100%;height:500px;"></div>
</div>
</div>
</div>
</div>
</div>
<div id="pageNavPosition"></div>
<script>

var $rows = $('#table tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

$(document).ready(
		function(){
			$(".class_toggle").click(function(){
			    $(this).parent().next().toggle();
				
					
					if($(this).parent().next().css('display') == 'none')
					{if($(this).hasClass("minus")){$(this).removeClass("minus");$(this).addClass("plus");}}
					else{
						if($(this).hasClass("plus")){$(this).removeClass("plus");$(this).addClass("minus");}
					}
				
			});
			
		}
		
		);



</script>

<style type="text/css">
            .pg-normal {
                color: black;
                font-weight: normal;
                text-decoration: none;
                cursor: pointer;
            }
            .pg-selected {
                color: black;
                font-weight: bold;
                text-decoration: underline;
                cursor: pointer;
            }
            .header
            {
                background-color:gray;
            }
        </style>

<script type="text/javascript" src="/js/paging.js"></script>
<!--<script type='text/javascript' src='/js/jquery-migrate.js'></script>-->

<script type="text/javascript">
 
            var pager = new Pager('table', 40);
            pager.init();
            pager.showPageNav('pager', 'pageNavPosition');
            pager.showPage(1);
   $(document).ready(function(){
	  $('.detail_data').hide(); 
   });
 
        </script>

