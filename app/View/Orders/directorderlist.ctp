<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
	echo $this->Html->script('sorttable');
	//echo $this->html->css('style2');

    $script[] = '{title: "' . $dodata[0]["Directorder"]["lab"]["UserDetail"]["address"] . '",'.
                'lat: "' . $dodata[0]["Directorder"]["lab"]["UserDetail"]["latitude"] . '",' .
                'lng: "' . $dodata[0]["Directorder"]["lab"]["UserDetail"]["longitude"] . '",' .
                'icon: "/icons/icon.php?t=lab"}' .
                '';
    foreach ($dodata as $order)	{
        $script[] = '{title: "' . $order["Directorder"]["doctor"]["UserDetail"]["address"] . '",'.
            'lat: "' . $order["Directorder"]["doctor"]["UserDetail"]["latitude"]  . '",' .
            'lng: "' . $order["Directorder"]["doctor"]["UserDetail"]["longitude"]  . '",' .
            'icon: "/icons/icon.php?t=doctor&h=' . $order["Directorder"]["oid"] . '"}' .
            '';
    }

    $scriptOut = 'markers = [' . implode(',', $script) . '];';

?>
	

<style>
.select label{ display:none;}
table.sortable thead {
		font-size: smaller;
		background-color:#eee;
		color:#666666;
		//font-weight: bold;
		cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
.st a{
color :white !important;
}
.inputt {
    border-bottom: 1px solid #000 !important;
    border-left: 0;
    border-right: 0;
    border-top: 0;
    outline: none;
	background-color: transparent;
    width:98% !important;
    //line-height:20px;
}
.search-btn {
	    height: 38px !important;
}

.col-sm-3 {
    width: initial;
}
.mybutto {
	border: 1px solid darkgrey;
    padding: 7px;
    border-radius: 4px;
    background: white;
    color: #2a6496 !important;
    
}
.mybutto:hover {
	background: whitesmoke;
}
#pageNavPosition{
	float: right;
	margin-top: 20px;
}
.pg-selected {
	border: 1px solid darkgrey;
    padding: 7px;
    border-radius: 4px;
    background: white;
        color: maroon !important;
}
.wrapper {
    width: 96%;
    max-width: none;
    margin: auto;
}
.gm-style-mtc {
    display: none;
}
.gm-svpc {
  display: none;
}
#pageNavPosition {
  display: none;
}
.padding-remover{
  padding-left: 0;
  padding-right: 0;

}
.margin-remover{
  margin-left: 0;
  margin-right: 0;
}
.info-area .row .col-sm-3.padding-remover {
  margin: 14px 5px 15px;
}
.new-order {
    background: transparent;
}
.map-area .col-sm-3.padding-remover {
    margin: 14px 5px 15px;
	float:right;
}
.new-order {
border:0;
}
td[title]:hover:after{
	content: attr(title);
	padding: 8px 12px;
	color: #85003a;
	position: absolute;
	white-space: nowrap;
	z-index: 20;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	-moz-box-shadow: 0px 0px 2px #c0c1c2;
	-webkit-box-shadow: 0px 0px 2px #c0c1c2;
	box-shadow: 0px 0px 2px #c0c1c2;
	background-image: -moz-linear-gradient(top, #ffffff, #eeeeee);
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #ffffff),color-stop(1, #eeeeee));
	background-image: -webkit-linear-gradient(top, #ffffff, #eeeeee);
	background-image: -moz-linear-gradient(top, #ffffff, #eeeeee);
	background-image: -ms-linear-gradient(top, #ffffff, #eeeeee);
	background-image: -o-linear-gradient(top, #ffffff, #eeeeee);
}

</style>

<div class="new-order ">
<?php if($archive == 1){ ?>
<h2 class="col-lg-3">Archive Orders</h2>
<?php }else { ?>

<?php } ?>
<div class="row margin-remover">
	<div class="col-md-12">
		<div class="col-md-8 padding-remover">
			<div class="info-area">
				<div class="row margin-remover">
					<div class="col-sm-6 padding-remover" style="width:initial;">
						<div class="searchbox_ld">

							<?php echo $this->Form->input("searchval",array("type"=>'text','id'=>'search',"placeholder"=>'Search by any header','class'=>'form-control search_box',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>

							<span id="submitbtn" class="invite-button nomargin submitsearch search-btn">Search</span>

						</div>

					</div>
					<div class="col-sm-3 padding-remover">
							<a id="add_btn" style="margin-top: 3px;background: #008080 !important;
						color: white !important;" class="add-btn" href="<?php echo $this->Html->url('directorder') ?>" >Schedule Delivery</a>
					</div>
					<div class="col-sm-3 padding-remover">
							<?php if($archive == 1){ ?>
						<a id="add_btn" style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('directorderlist') ?>" >All Orders</a>
						<?php } else { ?>

							<a id="add_btn" style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('directorderlist/1') ?>" >Completed Delivery</a>
							<?php } ?>
					</div>
				</div>

				<?php echo $this->Session->flash(); ?>
				<div class="row margin-remover">
						<div class="responsive-table">
								<table id="table" class="sortable table table-striped">
									<thead>
										<tr>
												<th>&nbsp;</th>
												<th class="st">Order ID</th>
											   <!-- <th class="st">From</th> -->
												<th class="st">TO</th>
												<!--<th class="st">Pickup</th>
												<th class="st">Dropoff</th>
												<th class="st">Priority</th>
												<th class="st">Recepient</th>-->
												<th class="st">Status</th>
												<th class="st">Patient</th>
												<th class="st">Price</th>
											   <!-- <th class="st">Payment Status</th> -->
												<th>&nbsp;</th>
										</tr>
									 </thead>
										   <tbody>
												<?php  if(!empty($dodata)){$totalpickup=0;$totaldropoff=0; $i=0; foreach($dodata as $order){ //print_r($order); ?>
											<tr>
												<td style="width:19px;" class="class_toggle plus"></td>
												<!--<td><?php
												 //echo $this->Form->checkbox('arc', array('hiddenField' => false,'value'=>$order['Directorder']['oid'],'myarray'=>$order['Directorder']['payment_status'],'class'=>'arc','id'=>'arc_'.$i));
												 $i++;

												?></td> -->
												<td><?php echo '#'.$order['Directorder']['oid'];?></td>
												<!-- <td><?php
												// $state1 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
												// $st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
												// echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip']; ?></td> -->
												<td title="<?php echo $order['Directorder']['dropoff_address'].' '.$order['Directorder']['dropoff_city'].', '.$st2.' '.$order['Directorder']['dropoff_zip']; ?>"><?php
												/*$state2 = $this->requestAction('App/getstate/'.$order['Directorder']['dropoff_state']);
												$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
												echo $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'];*/
												echo $order['OrderDetail']['Userdetail']['full_name'];
												?></td>
												<td><?php echo $order['Directorder']['order_status']; ?></td>
												<td title="<?php echo 'Dropoff Patient'.'  '.$order['Directorder']['drop_patient_name'].'    '.'PickUp Patient'.'  '.$order['Directorder']['pick_patient_name']; ?> "><?php echo $order['Directorder']['drop_patient_name']; ?> </td>
												<td><?php echo ($order['Directorder']['aprice'] == '')? '$'.$order['Directorder']['price'] : '$'.$order['Directorder']['aprice']; ?></td>
												<!-- <td><?php // echo ($order['Directorder']['payment_status'] == 0)? 'Pending':'Payment Done'; ?></td> -->
												<td><?php /*if($order['Directorder']['payment_status'] == 0){
												echo '<div class="black-btn-outer pull-right">';
												echo $this->Html->link("Make Payment",array("controller"=>"orders","action"=>'laborderpayment',$order['Directorder']['oid']),array("class"=>"black-btn"));
												echo '</div>';
												}else{
												$ordeee = '#'.$order['Directorder']['oid'];
												$payments = $this->requestAction('App/labpriceshow/'.$ordeee);
												if($payments['Labtransaction']['adminpay_amt'] == ''){
												echo 'Total : $'.$payments['Labtransaction']['pay_amount'];
												}
												else{
												echo '<span style="font-size:11px;">Initial Amt : $'.$payments['Labtransaction']['pay_amount'].'<br>Outstanding Amt : $'.$payments['Labtransaction']['adminpay_amt'].'<br>Total : $'.($payments['Labtransaction']['pay_amount'] + $payments['Labtransaction']['adminpay_amt']).'</span>';
												}
												}
												*/?>
												</td>
											</tr>
											<tr class="detail_data" style="display:none;">
												<td>&nbsp;</td>
												<td><?php
												$distance = $this->requestAction('App/getzipdistance/'.$order['Directorder']['pickup_zip'].'/'.$order['Directorder']['dropoff_zip']);
												 //echo '<b>Date :</b><br>'.$distance;
												 echo '<b>Date</b><br>'.$order['Directorder']['pick_date'];
												 ?></td>
												<td><?php
												if(($order['Directorder']['apickup'] == '0')){
												 echo '<b>Dropoff</b><br>'.$order['Directorder']['pickup'];$totalpickup += $order['Directorder']['pickup'];}else {
												echo '<b>Dropoff</b><br>'.$order['Directorder']['apickup'];$totalpickup += $order['Directorder']['pickup'];}

												?></td>
												<td><?php
												if($order['Directorder']['adropoff'] == '0'){
												echo '<b>Pickup</b><br>'.$order['Directorder']['dropoff'];$totaldropoff +=$order['Directorder']['dropoff'];} else {
												echo '<b>Pickup</b><br>'.$order['Directorder']['adropoff']; $totaldropoff += $order['Directorder']['adropoff'];}

												?></td>
												<td><?php echo '<b>Priority</b><br>'.$order['Directorder']['priority']; ?></td>
												<td><?php echo '<b>Recepient</b><br>'.$order['Directorder']['recipient']; ?></td>
												<td><?php echo '<b>Note</b><br>'.$order['Directorder']['note']; ?></td>

												<!--<td><?php //echo '<b>Date</b><br>'.$order['Directorder']['pick_date']; ?></td>-->
											</tr>
									<?php } }else{ ?>
											<tr>
												<td colspan="7" style="color:red;">No Order Found</td>
											</tr>
									<?php } ?>

										</tbody>
								</table>
						</div><!-- /.responsive div -->
				</div><!--/.  row -->
			</div><!-- /. info div -->
			</div><!-- /. col-sm-6 -->

		<div class="col-md-4 padding-remover map-area">
		<div class="row margin-remover">

					<div class="col-sm-3 padding-remover">
							<a id="add_btn" style="margin-top: 3px;background: #008080 !important;
						color: white !important;" class="add-btn" href="javascript:void(0);" ><span>Total Drop Off&nbsp;(&nbsp;<?php echo $totalpickup; ?>
												&nbsp;)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Total PickUp&nbsp;(&nbsp;<?php echo $totaldropoff; ?>&nbsp;)</span></a>
					</div>

				</div>
			<div class="map-area">
				<style type="text/css">
				/*html { height: 100% }
				body { height: 100%; margin: 0px; padding: 0px }*/
				</style>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

				<script type="text/javascript">
				  var directionDisplay;
				  var markerArray = [];
				  var directions = [];
				  var directionsService = new google.maps.DirectionsService();
				  var map;
				  //var origin = "<?php echo $origin?>";
				  var start_end = JSON.parse('<?php echo $start_end?>');;
				  var location_data = JSON.parse('<?php echo $locationData?>');
				  var len =  location_data.length;

				  function renderDirections(result) {
					//alert(JSON.stringify(result));
					var directionsRenderer = new google.maps.DirectionsRenderer({
						suppressMarkers: true
					});
					directionsRenderer.setMap(map);
					directionsRenderer.setDirections(result);
				   }

				  function searchOptimalRoute() {
					  var direction;
					  var minDistance = 0;
					  var optimailDirection;
					  var legs ;
					  var totalDistance;
					  var totalDuration;

					  direction = directions[0];
					  legs = direction.routes[0].legs;
					  totalDistance = 0;
					  totalDuration = 0;
					  for(var n=0; n<legs.length; ++n) {
						  totalDistance += legs[n].distance.value;
						  totalDuration += legs[n].duration.value;
					  }
					  minDistance = totalDistance;
					  optimailDirection = direction;


					  for(var i=0;i<directions.length-1;i++){
						  direction = directions[i];
						  legs = direction.routes[0].legs;
						  totalDistance = 0;
						  totalDuration = 0;
						  for(var n=0; n<legs.length; ++n) {
							  totalDistance += legs[n].distance.value;
							  totalDuration += legs[n].duration.value;
						  }
						  if (totalDistance < minDistance) {
							  minDistance = totalDistance;
							  optimailDirection = directions[i];
						  }

					  }

					  renderDirections(optimailDirection);

				  }

				  function requestDirections(start, end,waypnt) {
					  directionsService.route({
						origin: start,
						destination: end,
						waypoints: waypnt,
						optimizeWaypoints: true,
						avoidTolls: true,
						travelMode: google.maps.DirectionsTravelMode.DRIVING,
					  }, function(result) {
						  directions.push(result);
						  if (directions.length == markers.length-1) searchOptimalRoute();
					  });
					}

				  function initialize() {
					  var start = new google.maps.LatLng(51.47482547819850,-0.37739553384529);
					  var infowindow = new google.maps.InfoWindow();
					  var myOptions = {
					zoom:7,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					center: start
					};
					  <?php echo $scriptOut; ?>

					map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

					markers.forEach(function(item, i, arr) {
						var image = {
							url: item.icon,
							size: new google.maps.Size(32, 48),
							origin: new google.maps.Point(0, 0),
							anchor: new google.maps.Point(16, 24)
						};
						var marker = new google.maps.Marker({
							position: new google.maps.LatLng(item.lat, item.lng),
							title: item.title,
							icon: image,
							map: map
						});
						google.maps.event.addListener(marker, 'click', function () {
							infowindow.setContent(item.title);
							infowindow.open(map, marker);
						});
					});

					calcRoute(markers);
					showDriver();
				  }
				  function plotDrivers(){
					  for(var i=0;i<gpsArray.length;i++){
						  //alert(JSON.stringify(parseFloat(gpsArray[i].latitude)));

						  var latLng=new google.maps.LatLng(parseFloat(gpsArray[i].latitude),parseFloat(gpsArray[i].longitude));
						  var iconBase = 'http://biolablogistics.com/images/icon/';
					   var marker = new google.maps.Marker({
					position: latLng,
					map: map,
					icon: iconBase + 'person.png',
					title: 'Driver is here!'
				  });
				  }
				  }
				  function calcRoute(markers) {
					  /* for(var i=0;i<len;i++){
					   requestDirections(start_end.origin[i], start_end.destination[i],location_data[i]);
					   }*/
					  for(var i=1;i<markers.length;i++){
					   var WayPoints = markers.slice();
						  WayPoints.splice(i,1);
						  WayPoints.splice(0,1);
						var waypnt = [];
						for(var n=0;n<WayPoints.length;n++){
						   waypnt.push({
							   location: new google.maps.LatLng(WayPoints[n].lat, WayPoints[n].lng),
							   stopover: true
							});
						}
						var origin = new google.maps.LatLng(markers[0].lat, markers[0].lng);
						var destination = new google.maps.LatLng(markers[i].lat, markers[i].lng);

					   requestDirections(origin,destination,waypnt);
					  }


				  }
				google.maps.event.addDomListener(window, 'load', initialize);
				</script>
				<script>
				var gpsArray;
				  var lab_id=<?php echo $this->Session->read('Auth.User.id'); ?>;
				function showDriver() {

				  xhttp = new XMLHttpRequest();
				  xhttp.onreadystatechange = function() {
					if (xhttp.readyState == 4 && xhttp.status == 200) {
					gpsArray = JSON.parse(xhttp.responseText);
					//alert(JSON.stringify(gpsArray[0]));
					if(gpsArray.length>0)
						plotDrivers();
					}
				  };
				  xhttp.open("GET", "<?php echo Router::url('/', true)?>api/fetchdriverorder.php?driverGPSCoordinate=true&lab_id="+lab_id, true);
				  xhttp.send();

				}

				</script>
				<div id="map_canvas" style="float:left;width:100%;height:470px;"></div>
			</div>
		</div>
	</div>
</div>


</div>
</div>
<div id="pageNavPosition"></div>
<script>

var $rows = $('#table tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

$(document).ready(
		function(){
			$(".class_toggle").click(function(){
			    $(this).parent().next().toggle();
				
					
					if($(this).parent().next().css('display') == 'none')
					{if($(this).hasClass("minus")){$(this).removeClass("minus");$(this).addClass("plus");}}
					else{
						if($(this).hasClass("plus")){$(this).removeClass("plus");$(this).addClass("minus");}
					}
				
			});
			
		}
		
		);



</script>

<style type="text/css">
            .pg-normal {
                color: black;
                font-weight: normal;
                text-decoration: none;
                cursor: pointer;
            }
            .pg-selected {
                color: black;
                font-weight: bold;
                text-decoration: underline;
                cursor: pointer;
            }
            .header
            {
                background-color:gray;
            }
        </style>

<script type="text/javascript" src="/js/paging.js"></script>
<!--<script type='text/javascript' src='/js/jquery-migrate.js'></script>-->

<script type="text/javascript">
 
            var pager = new Pager('table', 40);
            pager.init();
            pager.showPageNav('pager', 'pageNavPosition');
            pager.showPage(1);
   $(document).ready(function(){
	  $('.detail_data').hide(); 
   });
 
        </script>

