<?php 
echo $this->Html->script('sorttable');
 ?>
<script type="text/javascript">
	
	function doSearch() {
    var searchText = document.getElementById('searchTerm').value;
    var targetTable = document.getElementById('dataTable');
    var targetTableColCount;
            
    //Loop through table rows
    for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
        var rowData = '';

        //Get column count from header row
        if (rowIndex == 0) {
           targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
           continue; //do not execute further code for header row.
        }
                
        //Process data rows. (rowIndex >= 1)
        for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
            rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent;
        }

        //If search term is not found in row data
        //then hide the row, else show
        if (rowData.indexOf(searchText) == -1)
            targetTable.rows.item(rowIndex).style.display = 'none';
        else
            targetTable.rows.item(rowIndex).style.display = 'table-row';
    }
}
	</script>
<style>
table.sortable thead {
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
</style>
<?php echo $this->Paginator->options(array('url'=>$this->passedArgs)); ?>
<?php echo $this->Session->flash(); ?>
<div class="new-order">
		<?php if($this->Session->read('Auth.User.profile_status') == 0) { ?>
	<label id="ww">Your profile has not been activated yet, To send request please <a href="<?php echo $this->Html->url("/send-email"); ?>" title="Send Approval Email">Click here</a></label>
	<?php } else { ?>
	<?php echo $this->Session->Flash();?>
		<?php echo $this->Form->create("Payment",array("div"=>false,'type' => 'get')); ?>	
		<h2 class="col-sm-3">Orders Requests</h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by any header below', 'id'=>'searchTerm','class' => 'form-control search_box','onkeyup'=>"doSearch()","div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
					
			</div>
		</div>		
		<?php echo $this->Form->end(); ?>
				<div class="add-btn-outer pull-right">
			<?php if(isset($data) && !empty($data)) { ?>
				<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/pending-payment') ?>" title="Pending payment">Pending payment</a>
			<?php } else {	?>
		       <a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/archive-payments') ?>" title="Archive payment">Archive payment</a>
		     <?php } ?>
		</div>
    <?php if ( $this->Session->read("Auth.User.user_type") == 1 ) { ?>
	<div class="responsive-table">
		<table class="table table-striped">
			<tr>
				<th><?php echo $this->Paginator->sort('order_id','Order Id');?></th>
				<th>Patient name</th>
				<th>Lab name</th>
				<!--<th>Payment to lab</th>
				<th>Delivery charges</th>-->
				<th>Payment</th>
				<th>Payment status</th>				
				<th>&nbsp;</th>				
			</tr>			
			<?php foreach($payments as $key=>$payment) {  
				$r = unserialize($payment['OrderPayment']['order_detail']); ?>
				<tr>					
					<td><?php echo !empty($payment['OrderPayment']['order_id']) ? $payment['OrderPayment']['order_id'] : 'N-A'; ?></td>					
					<td><?php echo !empty($r[0]['patientName']) ? $r[0]['patientName'] : 'N-A'; ?></td>
					<td><?php echo !empty($payment['OrderPayment']['lab_name']) ? $payment['OrderPayment']['lab_name'] : 'N-A'; ?></td>
					<!--<td><?php echo "$".number_format($payment['OrderPayment']['order_payment'],2,'.',','); ?></td>
					<td><?php echo "$".number_format($payment['OrderPayment']['payment'],2,'.',','); ?></td>-->
					<td><?php echo "$".number_format(($payment['OrderPayment']['payment'] + $payment['OrderPayment']['order_payment']),2,'.',','); ?></td>
					<td><?php 
					
					echo $payment['OrderPayment']['payment_status'] == 0 ? 'Pending':'Completed'; ?>
					
					</td>
					<?php
					if($payment['OrderPayment']['order_status'] != 11) { 
					if($payment['OrderPayment']['payment_status'] != 1){ ?>
					<td><div class="black-btn-outer">								
								<?php echo $this->Html->link("Click to Pay",array("controller"=>"orders","action"=>'payment',$payment['OrderPayment']['order_id']),array("class"=>"black-btn"));?> 
					</div></td>
					<?php } else { ?>
						<td> &nbsp; </td>
					<?php } } else { echo "<td>Reorder</td>"; }?>
				</tr>
			<?php } ?>	
			<?php if(empty($payments)){ ?>
				<tr>
					<td colspan="5" style="color:red;">No Record Found</td>
				</tr>
			<?php }?>
		</table>
	</div>
	<?php echo $this->Element("pagination"); ?>
	<?php } else { ?>
			<div class="responsive-table">
		<table id="result_table" class="sortable table table-striped">
			<thead>
			<tr>
				<th class="st" style="color:#FFFFFF;">Order Id</th>
				<th class="st">Payment</th>				
				<th class="st">Payment status</th>				
			</tr>	
			</thead>
                            <tbody>				
			<?php foreach($payments as $key=>$payment) { ?>
				
					<td><?php echo $this->requestAction('App/getorderid/'.$payment['OrderPayment']['order_id']); ?></td>
					<td><?php echo "$".number_format($payment['OrderPayment']['payment'],2,'.',','); ?></td>					
					<td><?php echo $payment['OrderPayment']['adminpayment'] == 0 ? 'Pending':'Completed'; ?></td>					
				</tr>
			<?php } ?>	
			<?php if(empty($payments)){ ?>
				<tr>
					<td colspan="3" style="color:red;">No Record Found</td>
				</tr>
			<?php }?>
		<tbody>	
		</table>
	</div>
	<?php echo $this->Element("pagination"); ?>
	<?php } ?>
	
	
	
	
	
	<?php } ?>
</div>

