 <?PHP error_reporting(0); print_r($this->Form->create('Order', array("enctype" => "multipart/form-data")));?>
 <?php echo $this->Session->flash(); ?>
    <?php echo $this->Form->hidden("auto"); ?>
    <!--<?php if (empty($users)) { ?><p class="text-center" id="no_lab">Please select atleast one Lab to place your order. Click <?php echo $this->Html->link(__(__d('labels', 'HERE')), array('plugin' => false, 'controller' => 'users', 'action' => 'search', '2'), array('title' => 'All Labs')); ?> to add Labs.</p>  <?php } ?> -->
<?php
$i = -1;
$k = -1;
$j = 147;
echo $this->html->css('style2');
echo $this->html->script('bootstrap-datepicker.min');
echo $this->html->css('jsDatePick_ltr.min');
echo $this->html->script('jquery.1.4.2');
echo $this->html->script('jsDatePick.jquery.min.1.3');
echo $this->html->script('excanvas');
echo $this->html->script('drawing');
echo $this->html->script('html2canvas');
echo $this->html->css('datepicker');
?>
<style>
::-webkit-input-placeholder {
 
 color: black;margin-left:2px;
 
}
:-moz-placeholder { /* older Firefox*/
 color: black;margin-left:2px;
}
::-moz-placeholder { /* Firefox 19+ */ 
 color: black;margin-left:2px;
} 
:-ms-input-placeholder { 
 color: black;margin-left:2px;
}
input{
 line-height: normal !important;
 padding-left: 5px;
}
#prize {
background:yellow;
}
</style>
<script type="text/javascript">



function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
	window.onload = function(){
	
	
	
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
		new JsDatePick({
			useMode:2,
			target:"inputField2",
			dateFormat:"%d-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
	
	

</script> 


<div class="container">
 <div class="logo1">
  <img src="/images/logo.jpg" style="width:142px;">
  <p style="padding-bottom:20px;">
  1875 Connecticut Ave NW,<br/>
  Washington, DC 20008<br/>
  PH: (202) 400 5170<br/>
  anaqvi@biolablogistics.com
  www.biolablogistics.com   
  </p>
 </div>
 <div class="upper-box diable-doc">
	 <?php echo $this->Form->create('Order', array("enctype" => "multipart/form-data")); ?>
    <?php if(isset($or_id)) { ?>	
    <?php echo $this->Form->hidden('order_id',array('value'=>$or_id)); } ?>
 	<table width="400" style="padding: 0 10px;">
  <tr>
    <td colspan="6" class="red" data-target="#mahiModal" data-toggle="modal"><h5 style="margin-top:5px; margin-bottom:0;">Doctor's Name / Account Number or Referring Dental Lab</h5></td>
  </tr>
  <tr>
    <td colspan="6"><?php 
    $docname=$this->Session->read("Auth.User.Userdetail.first_name").' '.$this->Session->read("Auth.User.Userdetail.last_name");
    echo $this->Form->input('OrderDetail.' . ++$j . '.doctorname', array("type" => "text",'value'=>$docname, 'class' => 'inputt', 'label' => false)); ?>
		<!--input type="text" name="doc_name" style="color:#555;" value=""--></td>
  </tr>
  
 <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
  
  <tr>
    <td style="padding-right:8px;">Address</td>
    <td colspan="5"><!--input type="text" name="doc_address" style="color:#555;" value="<?php //echo ' '.$user_d['address'];?>"-->
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctoradd', array("type" => "text",'value'=>$user_d['address'], 'class' => 'inputt', 'label' => false)); ?>
    </td>
  </tr>
  <tr>
    <td>City</td>
    <td colspan="5"><!--input type="text" name="doc_city" style="color:#555;" value="<?php //echo $user_d['city'];?>"-->
     <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorcity', array("type" => "text",'value'=>$user_d['city'], 'class' => 'inputt', 'label' => false)); ?>
    </td>
  </tr>
  <tr>
    <td>State</td>
    <td colspan="5"><!--input type="text" name="doc_state" style="color:#555;" value="<?php //echo $state['State']['name'];?>"-->
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorstate', array("type" => "text",'value'=>$state['State']['name'], 'class' => 'inputt', 'label' => false)); ?>
    </td>
  </tr>
  

  <tr>
    <td>Zip</td>
    <td colspan="2"><!--input type="text" name="zip_no" style="color:#555;" value="<?php// echo $user_d['zipcode'] ;?>"-->
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorzip', array("type" => "text",'value'=>$user_d['zipcode'], 'class' => 'inputt', 'label' => false)); ?>
    </td>
    <td class="red">Phone</td>
    <td colspan="2"><!--input type="text" name="phone_no" style="color:#555;" value=""-->
    <?php 
    $pphone = $this->Session->read("Auth.User.Userdetail.phone");
    $phone = (empty("(".substr($pphone, 0, 3).") ".substr($pphone, 3, 3)."-".substr($pphone,6))?'**********':(empty($user['Userdetail']['privacy'])?"(".substr($pphone, 0, 3).") ".substr($pphone, 3, 3)."-".substr($pphone,6):'************')) ;
    echo $this->Form->input('OrderDetail.' . ++$j . '.doctorphone', array("type" => "text",'value'=>$phone, 'class' => 'inputt', 'label' => false)); ?>
    
    </td>
  </tr>
  <tr>
    <td>Email</td>
    <td colspan="5"><!--input type="text" name="email_id" style="color:#555;" value="<?php// echo $stuser['User']['username'];?>"-->
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctormail', array("type" => "text",'value'=>$stuser['User']['username'], 'class' => 'inputt', 'label' => false)); ?>
    </td>
  </tr>
</table>
</div>
 <div class="upper-box">
 	<table width="400" class="table1" style="margin-left:10px;">
  <tr>
    <td colspan="6"><h5 style="margin-top:5px; margin-bottom:0;">Patients's Name</h5>
    </td>
  </tr>
  <tr>
	  
    <td colspan="6">
		<?php 
		//mahavir
		$ii = 156;
		echo $this->Form->hidden('OrderDetail.' . $ii . '.lab_id',array('value'=>$usersl));
		//echo $this->Form->hidden('lab_id',array('value'=>$usersl));
		echo $this->Form->input('OrderDetail.' . ++$i . '.patientName', array("type" => "text", 'id' =>'pname', 'class' => 'inputt', 'label' => false)); ?>
		<!--input type="text" name="patient_name"-->
		</td>
  </tr>
  <tr>
    <td style="width:90px;">Date of RX</td>
    <td colspan="1" >
		<?php $date=date('m-d-Y');  echo $this->Form->input('OrderDetail.' . ++$i . '.dateofRx', array("type" => "text", 'class' => false, 'label' => false,'value'=>$date)); ?>
    <!--input type="text" size="12" id="inputField"/-->
    </td>
    <td   class="red" style="width:90px;">Return Date</td>
    <td colspan="1">
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.requestedReturnDate', array("type" => "text", 'class' => false, 'label' => false,'value'=>$date)); ?>
    <!--input type="text" size="12" id="inputField2" /-->
    </td>
  </tr>
  <tr>
    <td colspan="6" class="red" >
		<!--input type="checkbox" style="width: 13px;"> RUSH CASE -->
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.RUSHCASE', array('type' => 'checkbox', 'label' => 'RUSH CASE', 'div'=>false)); ?>
    <span style="color:#333; font-size:10px;">(Rush Fee Accepted)</span></td>
  </tr>
</table>
<h5 style="margin: 0px 4px; font-weight:bold;margin-left:10px;   padding: 2px;">For Lab Use</h5>
<table width="410" id="office" class="diable-labuse" style="border: 1px solid #ddd; text-transform:capitalize;margin-left:10px;background: #eee;">
   	
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.IMPRESSION', array('type' => 'checkbox', 'label' => 'IMPRESSION')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.WAXUP', array('type' => 'checkbox', 'label' => 'WAXUP')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.DENTURE', array('type' => 'checkbox', 'label' => 'DENTURE')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.CROWN', array('type' => 'checkbox', 'label' => 'CROWN')); ?></td>
  </tr>
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ARTICULATOR', array('type' => 'checkbox', 'label' => 'ARTICULATOR')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.BITE', array('type' => 'checkbox', 'label' => 'BITE')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.FRAMEWRK', array('type' => 'checkbox', 'label' => 'FRAMEWRK')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.PYMT', array('type' => 'checkbox', 'label' => 'PYMT')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.MODELS', array('type' => 'checkbox', 'label' => 'MODELS')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.BITEBLK', array('type' => 'checkbox', 'label' => 'BITE BLK')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.PARTIAL', array('type' => 'checkbox', 'label' => 'PARTIAL')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.CASESPOSTAGE', array('type' => 'checkbox', 'label' => 'CASES POSTAGE')); ?>	</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.REGULAR', array('type' => 'checkbox', 'label' => 'REGULAR')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.OVERNIGHT', array('type' => 'checkbox', 'label' => 'OVERNIGHT')); ?></td>
    <td colspan="2">&nbsp;</td>
    
  </tr>
</table>
 </div>
</div>
<!--container-->
<div class="dropdown">
	<div style="float:left;width:300px; padding-left:30px; color:#555; font-weight:600; ">Select Restoration Type</div>
    <div style="float:left;width:300px;">
	<select id="change_restorations" name="ftype" style="height:22px; border-radius:2px; color:#b2312c; opacity:9;" onChange="changediv();">
    	<option selected value="1">FIXED RESTORATIONS</option>
        <option  value="2">REMOVABLE RESTORATIONS</option>
    </select>
    
    </div>
    <span class="col-lg-4"><label class="label-text" style="margin-left:60px;font-size:16px;font-weight: bold;color:#555;">Cost : </label>
		<?php echo $this->Form->input('Order.budget', array("type" => "text",'id' => 'prize', 'onKeyPress' => 'return numbersonly(this, event)', "placeholder"=>"Price in $",'style'=>'width: 190px;margin-left: 5px;', 'class' => false, 'label' => false,'div'=>false)); ?>
        </span>
    </div>
<script type="text/javascript">
	
function changediv(){
var selectbox = document.getElementById('change_restorations').value;
if(selectbox=='1'){
	document.getElementById('fixed_restorations').style.display="block";
	document.getElementById('removable_restorations').style.display="none";	
}
else{
	document.getElementById('fixed_restorations').style.display="none";
	document.getElementById('removable_restorations').style.display="block";	
}
}
</script>
<!--container-->
<div class="container2">
 <div class="box1">
 <div style="display:block;" id="fixed_restorations">
  <h4>FIXED RESTORATIONS</h4>
  <table width="550" style="color:#333; font-size:12px; padding: 16px 0;   margin-top: 11px;">
	  
            
  <tr>
    <td class="red" style="padding-left:5px;">Shade</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.shade_val', array("type" => "text", 'class' => false, 'label' => false));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.OcclusalStaining', array('type' => 'checkbox', 'label' => 'Occlusal Staining'));?></td>
  </tr>
  <tr>
    <td colspan="1" class="red" style="padding-left:5px;">Tooth Number(s)</td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.toothnumber', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
</table>
 <div style="height:112px; border-bottom:1px dotted #f33; border-top:1px dotted #f33; padding:10px 0px;margin-top: 20px;">
 <div class="half">
 <table style="color:#333; width:100%; font-size:12px;margin-left:5px;">
  <tr>
    <td colspan="4" class="red" style="padding-left: 5px;"><b>Restoration</b></td>
  </tr>
                    
  <tr>
    <td width="22px" ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.crown', array('type' => 'checkbox', 'label' => 'Crown'));?></td>
    <td width="22px"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.inlayOnlay', array('type' => 'checkbox', 'label' => 'Inlay/Onlay'));?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.veneer', array('type' => 'checkbox', 'label' => 'Veneer')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.postCore', array('type' => 'checkbox', 'label' => 'Post & Core')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bridge', array('type' => 'checkbox', 'label' => 'Bridge')); ?></td>
  </tr>
</table>
 </div>
 <div class="half">
 <table width="100"  style="color:#333;margin-left: 20px; width: 150px; font-size:12px;" >
  <tr>
    <td colspan="6" class="red" style="padding-left: 8px;"><b>Pontic Design</b></td>
  </tr>

    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth1', array('type' => 'checkbox', 'label' =>'<img src="/images/th-1.png" width="22" height="27">','div' =>false)) ?></td>
    <!--td><img src="/images/th-1.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth2', array('type' => 'checkbox','label' =>'<img src="/images/th-2.png" width="22" height="27">','div' =>false)) ?></td>
    <!--td><img src="/images/th-2.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth3', array('type' => 'checkbox','label' =>'<img src="/images/th-3.png" width="22" height="27">','div' =>false)) ?></td>
    <!--td><img src="/images/th-3.png" width="22" height="27"></td-->
  </tr>
  <tr>
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth4', array('type' => 'checkbox','label' =>'<img src="/images/th-4.png" width="22" height="27">','div' =>false)) ?></td>
    <!--td><img src="/images/th-4.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox','label' =>'<img src="/images/th-5.png" width="22" height="27">','div' =>false)) ?></td>
    <!--td colspan="3"><img src="/images/th-5.png" width="22" height="27"></td-->
  </tr>
  
</table>
 </div>
</div>
 <div style="margin-top:12px; margin-bottom:18px">
 <table width="260px" style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:5px;"><b>Design Details</b></td>
 </tr>
  <tr>
    <td colspan='3' ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.360degree', array('type' => 'checkbox','style'=>'margin-left:8px;', 'label' => ' 360º metal margin', 'div' => false)); ?></td>
    
    <td colspan='1'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalmargin', array("type" => "text", 'onKeyPress' => 'return numbersonly(this, event)','style'=>'width:78% !important;', 'class' => false, 'label' => false, 'div' => false)); ?>mm</td>
  </tr>
  <tr>
    <td colspan='4'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.porcelainButtMargin', array('type' => 'checkbox', 'label' => 'Porcelain Butt Margin*')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingualCollar', array('type' => 'checkbox', 'label' => 'Metal Lingual Collar')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => ' 3/4 Metal Occlusal*')); ?></td>
  </tr>
    <tr style="visibility:hidden;">
    <td><input type="checkbox" style="float:left; width:13px;margin-left:8px !important;"></td>
    <td colspan="1">Other</td>
    <td><input type="text" name="doc_name" ></td>
  </tr>
</table>
 <table width="260px" style="color:#333; font-size:12px; float:left; margin-top:22px;  margin-bottom: 9px;   margin-left: 12px;">

  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingual', array('type' => 'checkbox', 'label' => 'Metal Lingual*')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => 'Metal Occlusal*')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diagnosticWaxup', array('type' => 'checkbox', 'label' => 'Diagnostic Wax up*')); ?></td>
  </tr>
  <tr>
    <td colspan='3'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox','style'=>'margin-left:8px;', 'label' => 'other', 'div' => false)); ?></td>
    <td colspan='1'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other_val1', array("type" => "text", 'class' => false, 'label' => false, 'div' => false)); ?>
</td>
  </tr>
  <tr>
    <td colspan="5" style="text-align:right; font-size:10px;padding-right: 5px;">*Additional Charges</td>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 10px 0px; clear:both;  padding: 18px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px"><b>ALL-CERAMIC</b></td>
 </tr>
 
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechLayeredZirconia', array('type' => 'checkbox', 'label' => 'Suntech Layered Zirconia')); ?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechFullZirconia', array('type' => 'checkbox', 'label' => 'Suntech Full Zirconia')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.maxPressed', array('type' => 'checkbox', 'label' => 'e.max Pressed')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechZirconiaCopingOnly', array('type' => 'checkbox', 'label' => ' Suntech Zirconia Coping Only')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px;background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>COMPOSITES/TEMP</b></td>
 </tr>
  
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gardia', array('type' => 'checkbox', 'label' => 'Gardia (composite)')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechTemporaryCrown', array('type' => 'checkbox', 'label' => 'Suntech Temporary Crown')); ?></td>
  </tr>
  
</table>
 </div>
 <div style=" display: flex; padding: 10px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px;"><b>PFM CROWNS</b></td>
 </tr>
                            	
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.captek', array('type' => 'checkbox', 'label' => 'Captek', 'class' => 'checkbox-group')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
<tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>FULL CAST</b></td>
 </tr>              
                    	
 <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castPostCore', array('type' => 'checkbox', 'label' => 'Cast Post & Core')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.y2Gold', array('type' => 'checkbox', 'label' => 'Y+ 2% Gold', 'class' => 'checkbox-group')); ?>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 16px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>IMPLANT ABUTMENTS</b></td>
 </tr>
 
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.stock', array('type' => 'checkbox', 'label' => 'Stock')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.customMilled', array('type' => 'checkbox', 'label' => 'Custom Milled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.haderBar', array('type' => 'checkbox', 'label' => 'Hader Bar')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castOverdentureFrame', array('type' => 'checkbox', 'label' => 'Cast Overdenture Frame')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.screwRetained', array('type' => 'checkbox', 'label' => 'Screw Retained')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconia', array('type' => 'checkbox', 'label' => 'Zirconia')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.titanium', array('type' => 'checkbox', 'label' => 'Titanium')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconiainsertHybrid', array('type' => 'checkbox', 'label' => 'Zirconia w/Ti insert-Hybrid')); ?></td>
  </tr>
  <tr>
  <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' => 'Tooth5')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="2">Implant System</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.implantSystem', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Diameter</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diameter', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"># Chips or locators</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.clipsorLocator', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  
</table>
 </div>
</div>
                                                           <!--extra left div for display none property-->

<div id="removable_restorations" style="display:none;">
 <h4>REMOVABLE RESTORATIONS</h4>
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
	 
	
	 
  <tr>
  	<td style="display:none;"><input type="checkbox" style="float:left; width:12px;"></td>
   	<td colspan="2" class="red" style="padding-left: 5px;",>Check all that apply</td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox','style'=>'margin-left:8px;', 'label' => 'Other','div' => false ));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other1', array('type' => 'textbox', 'label' => false,'div' => false)); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.upper', array('type' => 'checkbox', 'label' => 'Upper'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lower', array('type' => 'checkbox', 'label' => 'Lower'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.try-in', array('type' => 'checkbox', 'label' => 'Try-in'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.finish', array('type' => 'checkbox', 'label' => 'Finish')); ?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.cusil', array('type' => 'checkbox', 'label' => 'Cusil'));?></td>
   
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bite_block', array('type' => 'checkbox', 'label' => 'Bite Block')); ?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.denture', array('type' => 'checkbox', 'label' => 'Denture'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.immediate', array('type' => 'checkbox', 'label' => 'Immediate/Surgical Denture '));?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot', array('type' => 'checkbox','style'=>'margin-left:8px;', 'label' => 'Extract Toot #','div' => false));?></td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot1', array('type' => 'textbox', 'label' => false,'div' => false)); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_all', array('type' => 'checkbox', 'label' => 'Extract All'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_now', array('type' => 'checkbox', 'label' => 'Extract Now'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_try', array('type' => 'checkbox', 'label' => 'Extract After Try-In'));?></td>
  </tr>
  
</table>
<div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
  <table width="541" style="color:#333; margin-left:8px; font-size:12px; float:left; margin-top:10px;">
  <tr>
   	<td colspan="6" class="red"><b>Teeth</b><span style="font-size:10px; color:#333;"> (stock teeth used if no option is selected)</span></td>
  </tr>
   
  <tr>
	
   	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.tooth_shad', array('type' => 'textbox','style'=>'width:466px;', 'label' => 'Tooth Shad', 'div'=>false)); ?></td>
  </tr>
  <tr>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ipn_portrait', array('type' => 'checkbox', 'label' => 'IPN Portrait*')); ?></td>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gold_face', array('type' => 'checkbox', 'label' => 'Gold Open Face*')); ?></td>
    <td><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.full_gold', array('type' => 'checkbox', 'label' => 'Full Gold*'));?></td>
  </tr>
  
  
</table>
  </div>
 <table width="520" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Sunflex Partials</b></td>
    <td colspan="2" class="red"><b>Sunflex Shade</b></td>
    <td colspan="2" class="red"><b>Clasp Type</b></td>
  </tr>
    
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex', array('type' => 'checkbox', 'label' => 'Sunflex')); ?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.cast', array('type' => 'checkbox', 'label' => 'Cast*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_cast_frame', array('type' => 'checkbox', 'label' => 'Sunflex Cast Frame Combo'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.pink', array('type' => 'checkbox', 'label' => 'Pink'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.wire', array('type' => 'checkbox', 'label' => 'Wire*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_vitallium_frame', array('type' => 'checkbox', 'label' => 'Sunflex Vitallium 2000+ Frame Combo'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.flexible', array('type' => 'checkbox', 'label' => 'Flexible*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_unilateral', array('type' => 'checkbox', 'label' => 'Sunflex Unilateral(Maximum 2 teeth)'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear', array('type' => 'checkbox', 'label' => 'sunclear*'));?></td>
  </tr>
</table>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Acrylic partials</b></td>
    <td colspan="2" class="red"><b>Acrylic Shade</b></td>
  </tr>
  
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_frame', array('type' => 'checkbox', 'label' => 'Suncast Frame w/Acrylic'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lucitone', array('type' => 'checkbox', 'label' => 'Lucitone 199*'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_acrylic', array('type' => 'checkbox', 'label' => ' Vitallium 2000+ w/Acrylic'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_pink', array('type' => 'checkbox', 'label' => 'Pink'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear_frame', array('type' => 'checkbox', 'label' => 'Sunclear Frame w/Acrylic'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.flipper', array('type' => 'checkbox', 'label' => ' Flipper (1 Tooth All Acrylic)'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_meharry', array('type' => 'checkbox', 'label' => 'Light Meharry'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_partial', array('type' => 'checkbox', 'label' => ' Acrylic Partial (No Frame)','div'=> false,'style'=>'margin-left:8px;'));?><span style="font-size:10px;">Wrought Wire Clasps</span></td>
    <td><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));?></td>
  </tr>
  <tr>
  	<td colspan="4"  style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    
    <td style="vertical-align: top !important;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));?></td>
  </tr>
</table>
 </div>
 <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Metal Frameworks</b></td>
    <td colspan="2" class="red"><b>Reinforcement</b></td>
  </tr>
       
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_framework', array('type' => 'checkbox', 'label' => 'Suncast Framework Only'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_wire', array('type' => 'checkbox', 'label' => 'Wire*'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_framework', array('type' => 'checkbox', 'label' => 'Vitallium 2000+ Framework Only'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_mesh', array('type' => 'checkbox', 'label' => 'Mesh*'));?></td>
  </tr>
</table>
  <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Design</b></td>
    <td colspan="2" class="red">&nbsp;</td>
  </tr>
            
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.horseshoe_palate', array('type' => 'checkbox', 'label' => 'Horseshoe Palate'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.open_palate', array('type' => 'checkbox', 'label' => 'AP Open Palate'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.full_palatal_metal_coverage', array('type' => 'checkbox', 'label' => 'Full Palatal Metal Coverage '));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.palatal_trap', array('type' => 'checkbox', 'label' => 'Palatal Strap'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metal_occlusion', array('type' => 'checkbox', 'label' => 'Metal Occlusion'));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rests', array('type' => 'checkbox', 'label' => 'Rests'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_pron', array('type' => 'checkbox', 'label' => 'Lingual Apron'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.precision_attachments', array('type' => 'checkbox', 'label' => 'Precision Attachments*'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_bar', array('type' => 'checkbox', 'label' => 'Lingual Bar'));?></td>
    <td style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" style="padding-left:5px;">(Best design is fabricated if no option is selected)</td>
  </tr>
</table>
 </div>
  <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Valplast Partials</b></td>
    <td colspan="2" class="red"><b>Sunclear Frameworks</b></td>
  </tr>
  
          
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast', array('type' => 'checkbox', 'label' => 'Valplast'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.framework_only', array('type' => 'checkbox', 'label' => ' Framework Only')); ?></td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_cast_combo', array('type' => 'checkbox', 'label' => 'Valplast Cast Combo'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_vitallium', array('type' => 'checkbox', 'label' => 'Valplast Vitallium 2000+ Combo'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
  <div style=" border-top:1px dotted #f33; float:left; display: 
  flex; margin-top:8px; padding-bottom:8px; margin-bottom:5px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="2" class="red" style="padding-left:5px;"><b>Attachments*</b></td>
    <td colspan="3" class="red">&nbsp;</td>
  </tr>
 
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.era', array('type' => 'checkbox', 'label' => 'ERA'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.pd', array('type' => 'checkbox', 'label' => 'PD'));?></td>
  </tr>
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hader_bar', array('type' => 'checkbox', 'label' => 'Hader Bar'));?></td>
    <td colspan="1" style="  width: 66px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'checkbox', 'label' => 'Other'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'text', 'label'=>false));?></td>
  </tr>
  
</table>
 </div>
  <div style=" text-align:center; background:#b2312c; color:#fff; margin: -4px auto; clear:both;padding: 3px;">OTHER</div>
 <div style="border-bottom:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px;  margin-top:4px; margin-bottom:4px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Repair</b></td>
    <td colspan="3" class="red"><b>Night Guards/Bite Splints</b></td>
    <td colspan="2" class="red"><b>Other*</b></td>
  </tr>
	
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reline', array('type' => 'checkbox', 'label' => 'Reline'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft', array('type' => 'checkbox', 'label' => 'Soft'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.base_plate_bite_rim', array('type' => 'checkbox', 'label' => 'Base Plate/Bite Rim'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rebase', array('type' => 'checkbox', 'label' => 'Rebase')); ?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard', array('type' => 'checkbox', 'label' => 'Hard'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.custom_tray', array('type' => 'checkbox', 'label' => 'Custom Tray'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.basic_repair', array('type' => 'checkbox', 'label' => 'Basic Repair'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_2mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 2mm'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.duplicate_model', array('type' => 'checkbox', 'label' => 'Duplicate Model'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft_liner', array('type' => 'checkbox', 'label' => 'Soft Liner'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_3mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 3mm'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.epoxy_model', array('type' => 'checkbox', 'label' => 'Epoxy Model'));?></td>
  </tr>
  <tr>
  	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth', array('type' => 'checkbox', 'label' => 'Add Tooth #'));?></td><br/>
    <td colspan="2" style="width: 84px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth1', array('type' => 'text','div'=>false,'label'=>false));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.surgical_stent', array('type' => 'checkbox', 'label' => 'Surgical Stent'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.patient_name', array('type' => 'checkbox', 'label' => 'Patient Name in Denture'));?></td>
  </tr>
  <tr>
  	<td colspan="8" style="text-align:right;padding-right:24px; padding-top:5px;">*Additional charge</td>
  </tr>
  
</table>
 </div>
 </div><!--second form for display none property-->

 </div><!--box 1-->
 <div class="box2">
<p style="text-align:center">Select Denture By Drawing</p>
 <canvas id="canvasInAPerfectWorld" width="460" height="300"  >
 <!-- <img src="images/teeth-img.png" style="display:block; margin:auto; margin-top:80px; margin-bottom:60px;">--> 
  </canvas>
  
 <a id="divid" onclick="redraw12();" class="clr_btn">clear</a>
 
   <a onclick="cantoimg();" class="save_btn" id="svbtn" >Save</a>
   <div style="color:green; text-align:center;" class="" id="img" ></div>
  <textarea class="notebook" rows="7" cols="20" style="resize:none;"></textarea>
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:0px;">
  <tr>
	  
    <td colspan="2">If an adjustment is needed:</td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_opposing', array('type' => 'checkbox', 'label' => 'Adjust opposing'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_abument', array('type' => 'checkbox', 'label' => 'Adjust abument'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.call_office', array('type' => 'checkbox', 'label' => 'Call the office'));?></b></td>
  </tr>
  
</table>
	
   <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:40px;">

  <tr>
    <td width="18" valign="top">
    	<!--<input type="checkbox" id="condition" style="float:left; width:12px;">-->
    	<?php echo $this->Form->input('OrderDetail.condition', array('type'=>'checkbox', "style"=>"float:left; width:12px;", "label" => false));?>
    </td>
    <td colspan="2" style="line-height: 17px;">By selecting the "place an order" button below, you accept the biolablogistics.com 
    <a href="#" id="red" class="red" data-target="#mahiModal" data-toggle="modal">Terms of Use</a> and the biolab terms of shipping in the applicable biolab Service Guide.</td>
  </tr>
  <?PHP //echo $this->Form->input('image_id', array('type' => 'text', 'id' => 'imginsertid'));?>
 <?php  echo $this->Form->hidden('image_id',array('id'=>'imginsertid')); ?>
  
</table> 
    
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:60px;">
  <tr>
   <td>Doctor's Name</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctornamem', array("type" => "text",'id'=>'doctornamem', 'class' => 'inputt', 'label' => false,'value'=>$docname,'readonly' => 'readonly')); ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
   <td>License#</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorlicense', array("type" => "text", 'id'=>'doctorlicense', 'class' => 'inputt', 'label' => false,'value'=>$user_d['licence'],'readonly' => 'readonly')); ?></td>
  </tr>
  
</table>
  </div><!--box 2-->

 
 
 <div style="margin-top:10px; clear:both; padding-top:12px;font-weight: bold;  margin-left:40%;  width:24%;">
	 <?php echo $this->Form->submit(__($heading, true), array('class' => 'login-btn', 'onclick' => 'return valid();', 'id' => 'orderBtn','style'=>'border:1px solid #09978B !important;border-radius:4px;')); ?>
	 
	 
	 <div id="mahiModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">Terms of Use</h2>
          
        </div>
        <div id="tabble" class="modal-body">
		
	<div>Are you agree with terms and conditions?</div><br>
	
      <p style="margin:0px;padding:0px;">Yes<input id="yes" type="radio" name="terms" value="yes" style="  width: 9%;"></p>
      <p style="margin:0px;padding:0px;">No&nbsp;<input id="no" type="radio" name="terms" value="no" style="  width: 9%;"></p>     
         
          
 
        </div>
        
      </div>
      
       </div>
</div>
 	
 	<?php echo $this->Form->end(); ?>
 </div>
 <div id="error"></div>
 
<script type="text/javascript">

function cantoimg(){
		//alert("hello");
		var canvas=document.getElementById('canvasInAPerfectWorld');
		html2canvas($("#canvasInAPerfectWorld")).then(function(canvas) {
         dataURL = canvas.toDataURL("image/png");
         <?//php echo Router::url(array('controller' => 'orders', 'action' => 'canvas')); ?>
$.ajax({
type: 'POST',
url: 'http://biolablogistics.com/saveimage.php',
data:'img='+dataURL+'&id=2',
success:function(res){
document.getElementById('img').innerHTML='Image Successfully Saved.';
$("#svbtn").css({'display':'none'});
$("#divid").css({'display':'none'});
$("#canvasInAPerfectWorld").css({'pointer-events': 'none'});
$("#imginsertid").val(res);
/*$('#teethpic').attr('src','/biolab/'+res);
$("#canvasInAPerfectWorld").css({'display':'none'});
$("#teethpic").css({'display':'block'});
$("#divid").css({'display':'none'});
$("#svbtn").css({'display':'none'});
$("#cnbtn").css({'display':'inline-block'});
$('#upload_frame').attr('src',dataURL); */
}
});
});
	}
	
	
context = document.getElementById('canvasInAPerfectWorld').getContext("2d");
$('#canvasInAPerfectWorld').mousedown(function(e){
  var mouseX = e.pageX - this.offsetLeft;
  var mouseY = e.pageY - this.offsetTop;
		
  paint = true;
  addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
  redraw();
});
$('#canvasInAPerfectWorld').mousemove(function(e){
  if(paint){
    addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
    redraw();
  }
});
$('#canvasInAPerfectWorld').mouseup(function(e){
  paint = false;
});
$('#canvasInAPerfectWorld').mouseleave(function(e){
  paint = false;
});
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;

function addClick(x, y, dragging)
{
  clickX.push(x);
  clickY.push(y);
  clickDrag.push(dragging);
}

function redraw(){
	//if(curLoadResNum < totalLoadResources){ return; }
	
	//clearCanvas();
  context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
  
  context.strokeStyle = "#f44";
  context.lineJoin = "round";
  context.lineWidth = 1;
			
  for(var i=0; i < clickX.length; i++) {		
    context.beginPath();
    if(clickDrag[i]){
      context.moveTo(clickX[i-1], clickY[i-1]);
     }else{
       context.moveTo(clickX[i]-1, clickY[i]);
     }
     context.lineTo(clickX[i], clickY[i]);
     context.closePath();
     context.stroke();
  }
}
function redraw12(){
	context.canvas.width=context.canvas.width;
	clickX=[];
  clickY=[];
  clickDrag=[];
}
function valid()
{
var name= document.getElementById("pname").value;
var prize= document.getElementById("prize").value;
var myDate=document.getElementById("OrderDetail1DateofRx").value;
var myDate2=document.getElementById("OrderDetail2RequestedReturnDate").value;
//alert(myDate);
//alert(myDate2);

if(name == '' ||name == null){

document.getElementById('error').innerHTML = "Please Enter the Patient's Name";
//document.getElementById('pname').style.background="#d14836";
//alert('Please Enter the Patient name');
return false ;
}

else if(document.getElementById('OrderDetail1DateofRx').value=='')
{
document.getElementById('error').innerHTML = "Please Enter RX Date";
//document.getElementById('inputField').style.background="#d14836";
alert("Please input RX Date");
return false;
}

else if(document.getElementById('OrderDetail2RequestedReturnDate').value=='')
{
document.getElementById('error').innerHTML= "Please Enter Return Date";
//document.getElementById('inputField2').style.background="#d14836";
//alert('Please input Return Date');
return false;

}
else if(parseInt(myDate2.replace(/-/g,""),10) < parseInt(myDate.replace(/-/g,""),10))
{
document.getElementById('error').innerHTML= "Return date should be greater than Rx date.";
return false;
}

else if(prize == '' ||prize == null){
document.getElementById('error').innerHTML= "Please Enter Cost";
//document.getElementById('prize').style.background="#d14836";
//alert('Please input cost');
return false;
}
else if(document.getElementById('imginsertid').value==''){
document.getElementById('error').innerHTML= "Please Save the Denture";
//document.getElementById('inputField2').style.background="#d14836";
//alert('Please input Return Date');
return false;

}
else if(document.getElementById('OrderDetailCondition').checked==false){
document.getElementById('red').click();

//document.getElementById('inputField2').style.background="#d14836";
//alert('Please input Return Date');
return false;
}

else
{
return true;
}
}

$("form input:radio").change(function () {
//alert($(this).val());
    if ($(this).val() =='yes') {
    
     $("#OrderDetailCondition").prop("checked", true);
     document.getElementById('orderBtn').click();
    }
   
    if ($(this).val() == 'no') {
     //$("#OrderDetailCondition").prop("checked", true);
     alert("Please Accept terms and conditions to proceed.");
     return false;
    }
  });
  
  
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


</script> 
<style>
    .diable-doc{
    pointer-events: none;
    
    }
    .diable-labuse{
    pointer-events: none;
    
    }
    #OrderDetail2RequestedReturnDate{
   
    }
    #error
    {
    text-align: center;
  margin-top: 11px;
  color: red;
  font-weight: bold;
  margin-left: 33px;
    }
    #img
    {
    color: green;
  
  font-size: 11px;
  margin-left: 11px;
    }
    
    </style>
 
</div>


