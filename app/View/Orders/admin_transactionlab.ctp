<div class="users view">
	<h2><?php  echo __('Transaction detail'); ?></h2>
	<dl>
		<dt><?php echo __('Order Id'); ?></dt>
		<dd>
			 <?php if(!empty($transaction['OrderPayment']['order_id'])) {?> <a href="<?php echo $this->Html->url("/admin/orders/view/".$transaction['OrderPayment']['order_id']) ?>"><?php echo $transaction['OrderPayment']['order_id']; ?></a><?php } ?>
			 &nbsp;
		</dd>
		<dt><?php echo __('Order Payment'); ?></dt>
		<dd>
			<?php echo (empty($transaction['OrderPayment']['payment'])? 'N-A' : ($transaction['OrderPayment']['payment'])); ?>
		</dd>
		<dt><?php echo __('Order reference'); ?></dt>
		<dd>
			<?php echo (empty($transaction['Transactions']['payment_reference'])? 'N-A' : ($transaction['Transactions']['payment_reference'])); ?>
		</dd>
		<dt><?php echo __('Lab details'); ?></dt>
		<dd>
			<?php echo (empty($transaction['Lab']['first_name'])? 'N-A' : ($transaction['Lab']['first_name'])); ?>
			<?php echo (empty($transaction['Lab']['last_name'])? 'N-A' : ($transaction['Lab']['last_name'])); ?>
			
		</dd>
		<dt><?php echo __('Doctor phone'); ?></dt>
		<dd>
			<?php echo (empty($transaction['Lab']['phone'])? 'N-A' : ($transaction['Lab']['phone'])); ?>
		</dd>
		
		<dt><a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a></dt>
	</dl>
</div>

