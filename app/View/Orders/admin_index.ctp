<div class="emailtemplates index">
	<?php echo $this->Form->create("Order",array("div"=>false)); ?>
	<?php echo $this->element("admins/common",array("place"=>'Search by order id, status',"flag"=>false,"pageheader"=>'Transactions',"buttontitle"=>'no',"listflag"=>true,"action"=>"no","selflag"=>false)); ?>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th class="leftalign"><?php echo $this->Paginator->sort('id','Order ID');?></th>
			<th class="leftalign">Order Cost</th>
			<th class="leftalign">Status</th>
			<th class="leftalign">Action</th>
		</tr>
		<?php 
		$i = 0;
		foreach ($orders as $order):  //pr($order);
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<tr <?php echo $class;?>>
				<td class="leftalign"><?php echo h($order['Order']['order_manualid']); ?>&nbsp;</td>
				<td class="leftalign">
					<?php foreach ($order['OrderPayment'] as $orderpayment ){
						if($orderpayment['receiver'] == $order['Order']['lab_id']){
							echo h($orderpayment['payment']); 	
						}
					}
					?>&nbsp;
				</td>
				<td class="leftalign"><?php echo h($order['Order']['status']);?></td>
				<td class="leftalign">
					<!--a href="<?php echo $this->Html->url("/admin/orders/viewtransaction/".$order['Order']['id']) ?>">View Detail</a-->&nbsp;
					<a href="<?php echo $this->Html->url("/admin/orders/view/".$order['Order']['id']) ?>">View Order Detail</a>&nbsp;
					<a href="<?php echo $this->Html->url("/admin/orders/invoice_for_lab/".$order['Order']['id']) ?>">Invoice From Lab</a>&nbsp;
					<a href="<?php echo $this->Html->url("/admin/orders/invoice_for_doctor/".$order['Order']['id']) ?>">Invoice For Doctor</a>&nbsp;
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
	</p>
	<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>
<?php echo $this->Form->end(); ?>
