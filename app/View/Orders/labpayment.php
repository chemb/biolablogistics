<?php error_reporting(0);  ?>
<?php echo $this->Html->script("jquery"); ?>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#accordion" ).accordion();
  });
</script>
<div class="login-box payment-info">		
<?php echo $this->Session->Flash();  ?>
<table style="border:1px solid #000;" class="sortable table table-striped">
		  <thead>
		  <tr>
			 <th> Order ID</th>
			  <th>Lab Name</th>
			  <th>Patient Name</th>
			  <th>Price</th>
		  </tr>
		 
		  </thead>
      <tbody>
		
		  <?php     if(is_array($orderids)){
			      foreach ($orderids as $orderid){?>
		  <tr>
		  <td><?php echo $this->requestAction('App/getorderid/'.$orderid); ?></td>
		  <td><?php 
		  
		  $lab_in = $this->requestAction('App/getlabid/'.$orderid);
		  
		  $lab_id = unserialize($lab_in['OrderDetail']['details']);
		  $lab = $lab_id[156]['lab_id'];
		  echo $this->requestAction('App/companyname/'.$lab);
		   ?></td>
		  <td><?php echo $this->requestAction('App/getpatient/'.$orderid); ?></td>
		  <td> <?php echo "$ ".$this->requestAction('App/getdeliveryprize/'.$orderid); ?></td>
		  </tr>
		  <?php }}
		   else
		    { 
			   ?>
			  
			  <tr>
		  <td><?php echo $this->requestAction('App/getorderid/'.$orderids); ?></td>
		  <td><?php 
		  $lab_in = $this->requestAction('App/getlabid/'.$orderids);
		  $lab_id = unserialize($lab_in['OrderDetail']['details']);
		  $lab = $lab_id[156]['lab_id'];
		  echo $this->requestAction('App/companyname/'.$lab);
		   ?></td>
		  <td><?php echo $this->requestAction('App/getpatient/'.$orderids); ?></td>
		  <td> <?php echo "$ ".$this->requestAction('App/getdeliveryprize/'.$orderids); ?></td>
		  </tr>
			  
			  <?php }?>
		 
		  </tbody>
</table>
		 <div id="accordion">
			 <h3 style="cursor:pointer;" id="braintree" class="text-center"><b>Pay with Braintree</b></h3>
		 <div>
<p>
		
      <form action="<?php echo $url; ?>" name="myform" method="post" onsubmit="return validate()" autocomplete="off">
    
       <span class="col-sm-4">&nbsp;</span><span class="payment-errors"></span></p>
      
      <div class="row">
    <label>
      <span class="col-sm-4">First Name</span>
      <?php echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"transaction[customer][first_name]","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'fname')); ?>
    </label>
  </div>
   <div class="row">
    <label>
      <span class="col-sm-4">Last Name</span>
      <?php echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"transaction[customer][last_name]","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'lname')); ?>
    </label>
  </div>
   <div class="row">
    <label>
      <span class="col-sm-4">Email</span>
      <?php echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"transaction[customer][email]","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'email')); ?>
    </label>
  </div>
  <div class="row">
  
          
    <label>
      <span class="col-sm-4">Card Number</span>
       <?php echo $this->Form->input("number",array("type"=>"text","data-stripe"=>"number","class"=>"card-number col-sm-8",'autocomplete'=>false,'div'=>false,'label'=>false,'name'=>'transaction[credit_card][number]' ,'id'=>'cardnumber')); ?>
    </label>
  </div>
 
  <div class="row">
    <label>
      <span class="col-sm-4">CVC</span>
      <?php echo $this->Form->input("cvc",array("type"=>"text","data-stripe"=>"cvc","name"=>"transaction[credit_card][cvv]","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'cvc')); ?>
    </label>
  </div>
  
  <div class="row">
    <label>
      <span class="col-sm-4">Expiration (MM/YYYY)</span>
      <span class="col-sm-4 padding-lt">
      <?php echo $this->Form->input("exp-month",array("options"=>$months,"empty"=>'Select Month',"data-stripe"=>"exp-month","class"=>"card-expiry-month selectpicker", 'label'=>false)); ?></span>
   <span class="col-sm-4 padding-lt">
    <?php echo $this->Form->input("exp-year",array("options"=>$years,"empty"=>'Select Year',"data-stripe"=>"exp-year","class"=>"card-expiry-year selectpicker", 'label'=>false)); ?>
   </span>
       </label>
  </div>
  <div class="row">
    <label>
      <span class="col-sm-4">Pay Amount</span>
      <span class="col-sm-4 padding-lt">
     
   <span style="color:#000; width:100%;" class="col-sm-4 padding-lt">
    <span class=""> $ <?php echo $amount; ?></span>
   </span>
       </label>
  </div>

 
       <?php echo $this->Form->input("tr_data",array("type"=>"hidden","data-stripe"=>"cvc","name"=>"transaction[credit_card][expiration_date]","class"=>"card-cvc col-sm-8",'id'=>'exp','div'=>false,'label'=>false,'value'=>"")); ?>
  
        <?php echo $this->Form->input("tr_data",array("type"=>"hidden","data-stripe"=>"cvc","name"=>"tr_data","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'value'=>$tr_data)); ?>
  
 
 <div class="text-center clear">
     <div class="login-btn-outer">
         <button type="submit" class="login-btn">Pay Now</button>
     </div>
 </div>
 <script type="text/javascript">
	function validate()
	{
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var re16digit = /^\d{16}$/;
var cvc=/^[0-9]{3,4}$/;
var exp2=document.getElementById('exp-month').value+'/'+document.getElementById('exp-year').value;
document.getElementById('exp').value=exp2;
//alert(document.getElementById('exp').value);

if (document.getElementById('fname').value=='') {
        alert("Please enter First Name");
        return false;
    }
   else if (document.getElementById('lname').value=='') {
        alert("Please enter Last Name");
        return false;
    }
    else if (document.getElementById('email').value=='') {
        alert("Please enter Email");
        return false;
    }
    else if (document.getElementById('email').value=='') {
        alert("Please enter Email");
        return false;
    }

    else if (!filter.test(document.getElementById('email').value)) {
    alert('Please provide a valid email address');
    email.focus;
    return false;
        }	
		
	
   else if (!re16digit.test(document.getElementById('cardnumber').value)) {
        alert("Please enter your 16 digit credit card numbers");
        return false;
    }
    else if(!cvc.test(document.getElementById('cvc').value))
    {
	 alert("Please enter correct CVC number");
        return false;	
	}
	else if(document.getElementById('exp-month').value==''||document.getElementById('exp-year').value=='')
    {
	 alert("Please enter expiry date");
        return false;	
	}
	else
	{
		return true;
	}
	}
	function check(val)
	{
		//alert(val);
		var mystr=val.length;
		//alert(mystr);
		if(mystr==2)
		{
			if(document.getElementById('exp').value<=31)
			{
		var newstr=document.getElementById('exp').value;
		document.getElementById('exp').value=newstr+'/';
	      }
	      else
	      {
			  alert("Enter Valid date");
		  }
		}
	}
	</script>
<?php echo $this->Form->end(); ?>

</p>
</div>

<!--<h3 style="cursor:pointer;" id="cheque" class="text-center"><b>Pay By Check</b></h3>

	 <div> 
		  <p>
	 <form action="/orders/paybycheque" name="myform" method="post" onsubmit="return validate2()" autocomplete="off">
          <span class="col-sm-4">&nbsp;</span><span class="payment-errors"></span>
          
           <div class="row">
    <label>
      <span class="col-sm-4">Check Number</span>
      <?php //echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"cqnumber","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'cheque2')); ?>
    </label>
  </div>
      
      <div class="row">
    <label>
      <span class="col-sm-4">First Name</span>
      <?php //echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"first_name","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'fname2')); ?>
    </label>
  </div>
   <div class="row">
    <label>
      <span class="col-sm-4">Last Name</span>
      <?php //echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"last_name","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'lname2')); ?>
    </label>
  </div>
   <div class="row">
    <label>
      <span class="col-sm-4">Email</span>
      <?php //echo $this->Form->input("",array("type"=>"text","data-stripe"=>"cvc","name"=>"email","class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false,'id'=>'email2')); ?>
    </label>
  </div>
 
  <div class="row">
    <label>
      <span class="col-sm-4">Pay Amount</span>
      <span class="col-sm-4 padding-lt">
     
   <span style="color:#000; width:100%;" class="col-sm-4 padding-lt">
    <span class=""> $ <?php //echo $amount; ?></span>
   </span>
       </label>
  </div>
       
  
    <?php //echo $this->Form->input("",array("type"=>"hidden","data-stripe"=>"cvc","name"=>"amount",'value'=>$amount,"class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false)); ?>
    <?php //echo $this->Form->input("",array("type"=>"hidden","data-stripe"=>"cvc","name"=>"orderids","value"=>$orderids,"class"=>"card-cvc col-sm-8",'div'=>false,'label'=>false)); ?>
    

 <div class="text-center clear">
     <div class="login-btn-outer">
         <button type="submit" class="login-btn">Pay Now</button>
     </div>
 </div>-->
 <script type="text/javascript">
	function validate2()
	{
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var re16digit = /^\d{16}$/;
var cvc=/^[0-9]{3,4}$/;
var exp2=document.getElementById('exp-month').value+'/'+document.getElementById('exp-year').value;
document.getElementById('exp').value=exp2;
//alert(document.getElementById('exp').value);
if(document.getElementById('cheque2').value=='')
{
	 alert("Please enter Cheque Number");
        return false;
}
else if (document.getElementById('fname2').value=='') {
        alert("Please enter First Name");
        return false;
    }
   else if (document.getElementById('lname2').value=='') {
        alert("Please enter Last Name");
        return false;
    }
    else if (document.getElementById('email2').value=='') {
        alert("Please enter Email");
        return false;
    }
    else if (document.getElementById('email2').value=='') {
        alert("Please enter Email");
        return false;
    }

    else if (!filter.test(document.getElementById('email2').value)) {
    alert('Please provide a valid email address');
    email.focus;
    return false;
        }	
		else
	{
		return true;
	}
	}
	</script>
<?php echo $this->Form->end(); ?>	 
</p>
</div>


</div>

</div>
