<div class="users view">
	<h2><?php  echo __('Order'); ?></h2>
	<dl>
		<dt><?php echo __('Order Id'); ?></dt>
		<dd>
			 <?php echo empty($order['Order']['id']) ? 'N-A':$order['Order']['order_manualid']; ?>
			 &nbsp;
		</dd>
		<dt><?php echo __('Order Detail'); ?></dt>
		<dd><?php $res = unserialize($order['OrderDetail'][0]['details']); ?>
			<?php echo (empty($res[18]['details'])? 'N-A' : ($res[18]['details'])); ?>
		</dd>
		<dt><?php echo __('Order By');?></dt>
		<dd>
			<?php  echo (empty($order['Doctor']['first_name']) ? 'N-A' : ($order['Doctor']['first_name'])); ?>
			<?php  echo $order['Doctor']['last_name']; ?>
			
		</dd>
		<dt><?php echo __('Doctor Address');?></dt>
		<dd>
			<?php echo (empty($order['Doctor']['address'])? 'N-A' : ($order['Doctor']['address'])); ?>
			<?php echo (empty($order['Doctor']['city'])? 'N-A' : ($order['Doctor']['city'])); ?>
			<?php echo (empty($order['Doctor']['phone'])? 'N-A' : ($order['Doctor']['phone'])); ?>
		</dd>
		<dt><?php echo __('Order To');?></dt>
		<dd>
			<?php  echo (empty($order['Lab']['first_name']) ? 'N-A' : ($order['Lab']['first_name'])); ?>
			<?php  echo $order['Lab']['last_name']; ?>
		</dd>
		<dt><?php echo __('Lab Address');?></dt>
		<dd>
			<?php echo (empty($order['Lab']['address'])? 'N-A' : ($order['Lab']['address'])); ?>
			<?php echo (empty($order['Lab']['city'])? 'N-A' : ($order['Lab']['city'])); ?>
			<?php echo (empty($order['Lab']['phone'])? 'N-A' : ($order['Lab']['phone'])); ?>
		</dd>
		<dt><?php echo __('Order Status'); ?></dt>
		<dd>
			<?php echo (empty($order['Order']['status'])? 'N-A' : ($order['Order']['status'])); ?>
		</dd>
		<dt><a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a></dt>
	</dl>
</div>
