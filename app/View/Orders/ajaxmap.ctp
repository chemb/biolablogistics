<?php   
				/* Set Default Map Area Using First Location */
		$map_area_lat = $lat;
		$map_area_lng = $lng;
				?>

<div id="map" style="width: 600px; height: 1024px; margin-top: 616px;"></div>
	
	<div class="google-map-wrap" itemscope itemprop="hasMap" >
					<div id="google-map" class="google-map" style="width:600px;height:1024px;margin-top:-1024px">
					</div><!-- #google-map -->
         <script>
				jQuery( document ).ready( function($) {

					/* Do not drag on mobile. */
					var is_touch_device = 'ontouchstart' in document.documentElement;

					var map = new GMaps({
						el: '#google-map',
						lat: '<?php echo $map_area_lat; ?>',
						lng: '<?php echo $map_area_lng; ?>',
						scrollwheel: true,
                        zoom:1,
						draggable: ! is_touch_device
					});

					/* Map Bound */
					var bounds = [];

						/* Set Bound Marker */
						var latlng = new google.maps.LatLng(<?php echo $map_area_lat; ?>, <?php echo $map_area_lng; ?>);
						bounds.push(latlng);
						/* Add Marker */
						map.addMarker({
							lat: <?php echo $map_area_lat; ?>,
							lng: <?php echo $map_area_lng; ?>,
							title: 'Doctor Info',
							infoWindow: {
								content: '<p><?php echo $add;?></p>'
							}
						}); 

					/* Fit All Marker to map */
					map.fitLatLngBounds(bounds);

					/* Make Map Responsive */
					var $window = $(window);
					function mapWidth() {
						var size = $('.google-map-wrap').width();
						$('.google-map').css({width: size + 'px', height: size + 'px'});
					}
					mapWidth();
					$(window).resize(mapWidth);

				});
				</script>
				</div>
	
	</div>