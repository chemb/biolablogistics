	<h2 style="font-size: 23px;text-align: center;font-weight: bold;  margin-top: 25px;">Order Detail</h2>
    <p style="margin-bottom:10px; "class="text-center">Moving science forward. Literally.</p>
    <?php if ($this->Session->read("Auth.User.user_type") == 1) {   ?>
		<?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create(null,array("enctype" => "multipart/form-data",'url' => array('controller' => 'orders', 'action' => 'updateorder',$order_data['Order']['id'])));?>
        <div class="row"style="margin-bottom:10px;">
            <div id="or_id" class="col-sm-6 text-bold txt-lrg" style="color:#555;">Order ID: <?php echo empty($order_data['Order']['id']) ? 'N-A' : $this->requestAction('App/getorderid/'.$order_data['Order']['id']); ?></div>
            <div class="col-sm-6"> <?php if($order_data['OrderDetail']['status_id'] == 13) { ?>
			<a href="<?php echo $this->Html->url('/new-order/'.$order_data['Order']['id']); ?>" class="black-btn pull-right">Place a new order</a>
			<?php }?></div>
        </div>
          <div class="row" style="margin-bottom:10px;">			
            <div class="col-sm-12 text-bold txt-lrg" style="color:#555;">Lab Name: <?php echo empty($lab_name['User']['company_name']) ? 'N-A' : $lab_name['User']['company_name']; ?></div><br/>
			<?php echo $this->Form->hidden("Order.lab_name",array("value"=>$lab_name['User']['company_name'])); ?>
          </div>
       <?php  if ($order_data['OrderDetail']['status_id'] == 1 && !empty($order_data['Order']['change_request'])) {  ?>
			  <div class="row">			
				<div class="col-sm-3 text-bold txt-lrg" style="color: #555;">Change Request: </div>
<div class="col-sm-9"style="color: #555;font-size:20px;margin-left:-90px;margin-bottom: 16px;"><?php echo $order_data['Order']['change_request']; ?></div>
			</div>
		<?php }	?>
     <?php // echo $this->Form->hidden("auto"); ?>
    <?php
$i = -1;
$k = -1;
$j = 147;
echo $this->html->css('style2');
echo $this->html->script('bootstrap-datepicker.min');
echo $this->html->css('jsDatePick_ltr.min');
echo $this->html->script('jquery.1.4.2');
echo $this->html->script('jsDatePick.jquery.min.1.3');
echo $this->html->script('excanvas');
echo $this->html->script('drawing');
echo $this->html->script('functionality');
?>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
		new JsDatePick({
			useMode:2,
			target:"inputField2",
			dateFormat:"%d-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
	
	

</script>

<div class="container">
 <div class="logo1">
  <img src="/images/logo.jpg" style="width:142px;">
 <p style="padding-bottom:20px;">
  1875 Connecticut Ave NW,<br/>
  Washington, DC 20008<br/>
  PH: (202) 400 5170<br/>
  anaqvi@biolablogistics.com
  www.biolablogistics.com   
  </p>
 </div>
 <div class="upper-box ">
	 <?php echo $this->Form->create('Order', array("enctype" => "multipart/form-data")); ?>
    <?php if(isset($or_id)) { ?>	
    <?php echo $this->Form->hidden('order_id',array('value'=>$or_id)); } ?>
 	<table width="400" style="padding: 0 10px;">
  <tr>
    <td colspan="6" class="red"><h5 style="margin-top:5px; margin-bottom:0;">Doctor's Name / Account Number or Referring Dental Lab</h5></td>
  </tr>
  <tr>
    <td colspan="6">
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorname', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?>
    </td>
  </tr>
  
 <tr>
    <td colspan="6">&nbsp;</td>
  </tr>

  <tr>
    <td style="padding-right: 8px;">Address</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctoradd', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>City</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorcity', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>State</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorstate', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  

  <tr>
    <td>Zip</td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorzip', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
    <td class="red">Phone</td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$j . '.doctorphone', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctormail', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
</table>
</div>
 <div class="upper-box">
 	<table width="400" class="table1" style="margin-left:10px;">
  <tr>
    <td colspan="6"><h5 style="margin-top:5px; margin-bottom:0;">Patients's Name</h5>
    </td>
  </tr>
  <tr>
	  
    <td colspan="6">
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.patientName', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?>
		<!--input type="text" name="patient_name"-->
		</td>
  </tr>
  <tr>
    <td style="width:90px;">Date of RX</td>
    <td colspan="1" >
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.dateofRx', array("type" => "text", 'class' => false, 'label' => false)); ?>
    <!--input type="text" size="12" id="inputField"/-->
    </td>
    <td   class="red" style="width:90px;">Return Date</td>
    <td colspan="1">
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.requestedReturnDate', array("type" => "text", 'class' => false, 'label' => false)); ?>
    <!--input type="text" size="12" id="inputField2" /-->
    </td>
  </tr>
  <tr>
    <td colspan="6" class="red" >
    <!--input type="checkbox" style="width: 13px;"> RUSH CASE -->
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.RUSHCASE', array('type' => 'checkbox', 'label' => 'RUSH CASE', 'class' => false, 'div'=>false)); ?> 
    <span style="color:#333; font-size:10px;">(Rush Fee Accepted)</span></td>
  </tr>
</table>
<h5 style="margin: 0px 4px; font-weight:bold;margin-left:10px;   padding: 2px;">For Lab Use</h5>
<table width="410" id="office" style="border: 1px solid #ddd; text-transform:capitalize;margin-left:10px;background: #eee;">
   	
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.IMPRESSION', array('type' => 'checkbox', 'label' => 'IMPRESSION','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.WAXUP', array('type' => 'checkbox', 'label' => 'WAXUP','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.DENTURE', array('type' => 'checkbox', 'label' => 'DENTURE','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.CROWN', array('type' => 'checkbox', 'label' => 'CROWN','disabled'=>'disabled')); ?></td>
  </tr>
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ARTICULATOR', array('type' => 'checkbox', 'label' => 'ARTICULATOR','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.BITE', array('type' => 'checkbox', 'label' => 'BITE','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.FRAMEWRK', array('type' => 'checkbox', 'label' => 'FRAMEWRK','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.PYMT', array('type' => 'checkbox', 'label' => 'PYMT','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.MODELS', array('type' => 'checkbox', 'label' => 'MODELS','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.BITEBLK', array('type' => 'checkbox', 'label' => 'BITE BLK','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.PARTIAL', array('type' => 'checkbox', 'label' => 'PARTIAL','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.CASESPOSTAGE', array('type' => 'checkbox', 'label' => 'CASES POSTAGE','disabled'=>'disabled')); ?>	</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.REGULAR', array('type' => 'checkbox', 'label' => 'REGULAR','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.OVERNIGHT', array('type' => 'checkbox', 'label' => 'OVERNIGHT','disabled'=>'disabled')); ?></td>
    <td colspan="2">&nbsp;</td>
    
  </tr>
</table>
 </div>
</div>
<!--container-->
<div class="dropdown">
	<div style="float:left;width:300px; padding-left:30px; color:#555; font-weight:600; ">Select Restoration Type</div>
    <div style="float:left;width:300px;">
	<select id="change_restorations" name="ftype" style="height:22px; border-radius:2px; color:#b2312c; opacity:9;" onChange="changediv();">
    	<option selected value="1">FIXED RESTORATIONS</option>
        <option  value="2">REMOVABLE RESTORATIONS</option>
    </select>
    
    </div>
    <span class="col-lg-4"><label class="label-text" style="margin-left:60px;font-size:16px;font-weight: bold;color:#555;">Cost : </label>
		<?php echo $this->Form->input('Order.budget', array("type" => "text","placeholder"=>"Price in $",'style'=>'position: relative;top: -18px;margin-left:125px;width: 190px;background:yellow;', 'class' => false, 'label' => false,'value'=>$order_data['Order']['budget'])); ?>
        </span>
    </div>
<script type="text/javascript">
	
function changediv(){
var selectbox = document.getElementById('change_restorations').value;
if(selectbox=='1'){
	document.getElementById('fixed_restorations').style.display="block";
	document.getElementById('removable_restorations').style.display="none";	
}
else{
	document.getElementById('fixed_restorations').style.display="none";
	document.getElementById('removable_restorations').style.display="block";	
}
}
</script>
<!--container-->
<div class="container2">
 <div class="box1">
 <div style="display:block;" id="fixed_restorations">
  <h4>FIXED RESTORATIONS</h4>
  <table width="550" style="color:#333; font-size:12px; padding: 16px 0;   margin-top: 11px;">
	  
            
  <tr>
    <td class="red" style="padding-left:5px;">Shade</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.shade_val', array("type" => "text", 'class' => false, 'label' => false));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.OcclusalStaining', array('type' => 'checkbox', 'label' => 'Occlusal Staining'));?></td>
  </tr>
  <tr>
    <td colspan="1" class="red" style="padding-left:5px;">Tooth Number(s)</td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.toothnumber', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
</table>
 <div style="height:112px; border-bottom:1px dotted #f33; border-top:1px dotted #f33; padding:10px 0px;margin-top: 20px;">
 <div class="half">
 <table style="color:#333; width:100%; font-size:12px;margin-left:5px;">
  <tr>
    <td colspan="4" class="red" style="padding-left: 5px;"><b>Restoration</b></td>
  </tr>
                    
  <tr>
    <td width="22px" ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.crown', array('type' => 'checkbox', 'label' => 'Crown'));?></td>
    <td width="22px"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.inlayOnlay', array('type' => 'checkbox', 'label' => 'Inlay/Onlay'));?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.veneer', array('type' => 'checkbox', 'label' => 'Veneer')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.postCore', array('type' => 'checkbox', 'label' => 'Post & Core')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bridge', array('type' => 'checkbox', 'label' => 'Bridge')); ?></td>
  </tr>
</table>
 </div>
 <div class="half">
 <table width="100"  style="color:#333;margin-left: 20px; width: 150px; font-size:12px;" >
  <tr>
    <td colspan="6" class="red" style="padding-left: 8px;"><b>Pontic Design</b></td>
  </tr>

    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth1', array('type' => 'checkbox', 'label' =>false,'div' =>false)); ?><label for="OrderDetail27Tooth1"><?php echo $this->Html->image("th-1.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-1.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth2', array('type' => 'checkbox', 'label' =>false,'div' =>false)); ?><label for="OrderDetail28Tooth2"><?php echo $this->Html->image("th-2.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-2.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth3', array('type' => 'checkbox', 'label' =>false,'div' =>false)); ?><label for="OrderDetail29Tooth3"><?php echo $this->Html->image("th-3.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-3.png" width="22" height="27"></td-->
  </tr>
  <tr>
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth4', array('type' => 'checkbox', 'label' =>false,'div' =>false)); ?><label for="OrderDetail30Tooth4"><?php echo $this->Html->image("th-4.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-4.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' =>false,'div' =>false)); ?><label for="OrderDetail31Tooth5"><?php echo $this->Html->image("th-5.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td colspan="3"><img src="/images/th-5.png" width="22" height="27"></td-->
  </tr>
  
</table>
 </div>
</div>
 <div style="margin-top:12px; margin-bottom:18px">
 <table width="260px" style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:5px;"><b>Design Details</b></td>
 </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.360degree', array('type' => 'checkbox', 'label' => ' 360º metal margin', 'div' => false, 'style'=>'margin-left: 8px;')); ?></td>
    <td colspan='1'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalmargin', array("type" => "text",'style'=>'width:78% !important;', 'class' => false, 'label' => false, 'div' => false)); ?>mm</td>
  </tr>
  <tr>
    <td colspan='4'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.porcelainButtMargin', array('type' => 'checkbox', 'label' => 'Porcelain Butt Margin*')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingualCollar', array('type' => 'checkbox', 'label' => 'Metal Lingual Collar')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => ' 3/4 Metal Occlusal*')); ?></td>
  </tr>
    <tr style="visibility:hidden;">
    <td><input type="checkbox" style="float:left; width:12px;"></td>
    <td colspan="1">Other</td>
    <td><input type="text" name="doc_name" ></td>
  </tr>
</table>
 <table width="260px" style="color:#333; font-size:12px; float:left; margin-top:22px;  margin-bottom: 9px;   margin-left: 12px;">

  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingual', array('type' => 'checkbox', 'label' => 'Metal Lingual*')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => 'Metal Occlusal*')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diagnosticWaxup', array('type' => 'checkbox', 'label' => 'Diagnostic Wax up*')); ?></td>
  </tr>
  <tr>
    <td colspan='3'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'other', 'div' => false,'style'=>'margin-left: 8px;')); ?></td>
    <td colspan='1'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other_val1', array("type" => "text", 'class' => false, 'label' => false, 'div' => false)); ?>
</td>
  </tr>
  <tr>
    <td colspan="5" style="text-align:right; font-size:10px;padding-right: 5px;">*Additional Charges</td>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 10px 0px; clear:both;  padding: 18px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px"><b>ALL-CERAMIC</b></td>
 </tr>
 
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechLayeredZirconia', array('type' => 'checkbox', 'label' => 'Suntech Layered Zirconia')); ?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechFullZirconia', array('type' => 'checkbox', 'label' => 'Suntech Full Zirconia')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.maxPressed', array('type' => 'checkbox', 'label' => 'e.max Pressed')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechZirconiaCopingOnly', array('type' => 'checkbox', 'label' => ' Suntech Zirconia Coping Only')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px;background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>COMPOSITES/TEMP</b></td>
 </tr>
  
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gardia', array('type' => 'checkbox', 'label' => 'Gardia (composite)')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechTemporaryCrown', array('type' => 'checkbox', 'label' => 'Suntech Temporary Crown')); ?></td>
  </tr>
  
</table>
 </div>
 <div style=" display: flex; padding: 10px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px;"><b>PFM CROWNS</b></td>
 </tr>
                            	
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.captek', array('type' => 'checkbox', 'label' => 'Captek', 'class' => 'checkbox-group')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
<tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>FULL CAST</b></td>
 </tr>              
                    	
 <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castPostCore', array('type' => 'checkbox', 'label' => 'Cast Post & Core')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.y2Gold', array('type' => 'checkbox', 'label' => 'Y+ 2% Gold', 'class' => 'checkbox-group')); ?>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 16px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>IMPLANT ABUTMENTS</b></td>
 </tr>
 
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.stock', array('type' => 'checkbox', 'label' => 'Stock')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.customMilled', array('type' => 'checkbox', 'label' => 'Custom Milled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.haderBar', array('type' => 'checkbox', 'label' => 'Hader Bar')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castOverdentureFrame', array('type' => 'checkbox', 'label' => 'Cast Overdenture Frame')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.screwRetained', array('type' => 'checkbox', 'label' => 'Screw Retained')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconia', array('type' => 'checkbox', 'label' => 'Zirconia')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.titanium', array('type' => 'checkbox', 'label' => 'Titanium')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconiainsertHybrid', array('type' => 'checkbox', 'label' => 'Zirconia w/Ti insert-Hybrid')); ?></td>
  </tr>
  <tr>
  <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' => 'Tooth5')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="2">Implant System</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.implantSystem', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Diameter</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diameter', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"># Chips or locators</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.clipsorLocator', array("type" => "text", 'class' => false, 'label' => false)); ?></td>
  </tr>
  
</table>
 </div>
</div>
                                                           <!--extra left div for display none property-->

<div id="removable_restorations" style="display:none;">
 <h4>REMOVABLE RESTORATIONS</h4>
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
	 
	
	 
  <tr>
  	<td style="display:none;"><input type="checkbox" style="float:left; width:12px;"></td>
   	<td colspan="2" class="red" style="padding:5px;">Check all that apply</td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'Other','div' => false, 'style'=>'margin-left: 8px;' ));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other1', array('type' => 'textbox', 'label' => false,'div' => false)); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.upper', array('type' => 'checkbox', 'label' => 'Upper'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lower', array('type' => 'checkbox', 'label' => 'Lower'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.try-in', array('type' => 'checkbox', 'label' => 'Try-in'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.finish', array('type' => 'checkbox', 'label' => 'Finish')); ?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.cusil', array('type' => 'checkbox', 'label' => 'Cusil'));?></td>
   
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bite_block', array('type' => 'checkbox', 'label' => 'Bite Block')); ?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.denture', array('type' => 'checkbox', 'label' => 'Denture'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.immediate', array('type' => 'checkbox', 'label' => 'Immediate/Surgical Denture '));?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot', array('type' => 'checkbox', 'label' => 'Extract Tooth #','div' => false, 'style'=>'margin-left: 8px;'));?></td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot1', array('type' => 'textbox', 'label' => false,'div' => false)); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_all', array('type' => 'checkbox', 'label' => 'Extract All'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_now', array('type' => 'checkbox', 'label' => 'Extract Now'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_try', array('type' => 'checkbox', 'label' => 'Extract After Try-In'));?></td>
  </tr>
  
</table>
<div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
  <table width="541" style="color:#333; margin-left:8px; font-size:12px; float:left; margin-top:10px;">
  <tr>
   	<td colspan="6" class="red"><b>Teeth</b><span style="font-size:10px; color:#333;"> (stock teeth used if no option is selected)</span></td>
  </tr>
   
  <tr>
	
   	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.tooth_shad', array('type' => 'textbox','style'=>'  width: 466px;', 'label' => 'Tooth Shad', 'div'=>false)); ?></td>
  </tr>
  <tr>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ipn_portrait', array('type' => 'checkbox', 'label' => 'IPN Portrait*')); ?></td>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gold_face', array('type' => 'checkbox', 'label' => 'Gold Open Face*')); ?></td>
    <td><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.full_gold', array('type' => 'checkbox', 'label' => 'Full Gold*'));?></td>
  </tr>
  
  
</table>
  </div>
 <table width="520" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Sunflex Partials</b></td>
    <td colspan="2" class="red"><b>Sunflex Shade</b></td>
    <td colspan="2" class="red"><b>Clasp Type</b></td>
  </tr>
    
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex', array('type' => 'checkbox', 'label' => 'Sunflex')); ?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.cast', array('type' => 'checkbox', 'label' => 'Cast*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_cast_frame', array('type' => 'checkbox', 'label' => 'Sunflex Cast Frame Combo'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.pink', array('type' => 'checkbox', 'label' => 'Pink'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.wire', array('type' => 'checkbox', 'label' => 'Wire*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_vitallium_frame', array('type' => 'checkbox', 'label' => 'Sunflex Vitallium 2000+ Frame Combo'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.flexible', array('type' => 'checkbox', 'label' => 'Flexible*'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_unilateral', array('type' => 'checkbox', 'label' => 'Sunflex Unilateral(Maximum 2 teeth)'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear', array('type' => 'checkbox', 'label' => 'sunclear*'));?></td>
  </tr>
</table>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Acrylic partials</b></td>
    <td colspan="2" class="red"><b>Acrylic Shade</b></td>
  </tr>
  
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_frame', array('type' => 'checkbox', 'label' => 'Suncast Frame w/Acrylic'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lucitone', array('type' => 'checkbox', 'label' => 'Lucitone 199*'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_acrylic', array('type' => 'checkbox', 'label' => ' Vitallium 2000+ w/Acrylic'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_pink', array('type' => 'checkbox', 'label' => 'Pink'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear_frame', array('type' => 'checkbox', 'label' => 'Sunclear Frame w/Acrylic'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.flipper', array('type' => 'checkbox', 'label' => ' Flipper (1 Tooth All Acrylic)'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_meharry', array('type' => 'checkbox', 'label' => 'Light Meharry'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_partial', array('type' => 'checkbox', 'div'=>false, 'label' => ' Acrylic Partial (No Frame)', 'style'=>'margin-left: 8px;'));?><span style="font-size:10px;"><?php echo "(Wrought Wire Clasps)";?></span></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));?></td>
  </tr>
  <tr>
  	<td colspan="4"  style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));?></td>
  </tr>
</table>
 </div>
 <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Metal Frameworks</b></td>
    <td colspan="2" class="red"><b>Reinforcement</b></td>
  </tr>
       
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_framework', array('type' => 'checkbox', 'label' => 'Suncast Framework Only'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_wire', array('type' => 'checkbox', 'label' => 'Wire*'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_framework', array('type' => 'checkbox', 'label' => 'Vitallium 2000+ Framework Only'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_mesh', array('type' => 'checkbox', 'label' => 'Mesh*'));?></td>
  </tr>
</table>
  <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Design</b></td>
    <td colspan="2" class="red">&nbsp;</td>
  </tr>
            
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.horseshoe_palate', array('type' => 'checkbox', 'label' => 'Horseshoe Palate'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.open_palate', array('type' => 'checkbox', 'label' => 'AP Open Palate'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.full_palatal_metal_coverage', array('type' => 'checkbox', 'label' => 'Full Palatal Metal Coverage '));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.palatal_trap', array('type' => 'checkbox', 'label' => 'Palatal Strap'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metal_occlusion', array('type' => 'checkbox', 'label' => 'Metal Occlusion'));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rests', array('type' => 'checkbox', 'label' => 'Rests'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_pron', array('type' => 'checkbox', 'label' => 'Lingual Apron'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.precision_attachments', array('type' => 'checkbox', 'label' => 'Precision Attachments*'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_bar', array('type' => 'checkbox', 'label' => 'Lingual Bar'));?></td>
    <td style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" style="padding:5px;">(Best design is fabricated if no option is selected)</td>
  </tr>
</table>
 </div>
  <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Valplast Partials</b></td>
    <td colspan="2" class="red"><b>Sunclear Frameworks</b></td>
  </tr>
  
          
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast', array('type' => 'checkbox', 'label' => 'Valplast'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.framework_only', array('type' => 'checkbox', 'label' => ' Framework Only')); ?></td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_cast_combo', array('type' => 'checkbox', 'label' => 'Valplast Cast Combo'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_vitallium', array('type' => 'checkbox', 'label' => 'Valplast Vitallium 2000+ Combo'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
  <div style=" border-top:1px dotted #f33; float:left; display: 
  flex; margin-top:8px; padding-bottom:8px; margin-bottom:5px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="2" class="red" style="padding-left:5px;"><b>Attachments*</b></td>
    <td colspan="3" class="red">&nbsp;</td>
  </tr>
 
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.era', array('type' => 'checkbox', 'label' => 'ERA'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.pd', array('type' => 'checkbox', 'label' => 'PD'));?></td>
  </tr>
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hader_bar', array('type' => 'checkbox', 'label' => 'Hader Bar'));?></td>
    <td colspan="1" style="  width: 66px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'checkbox', 'label' => 'Other'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'text', 'label'=>false));?></td>
  </tr>
  
</table>
 </div>
  <div style=" text-align:center; background:#b2312c; color:#fff; margin: -4px auto; clear:both;padding: 3px;">OTHER</div>
 <div style="border-bottom:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px;  margin-top:4px; margin-bottom:4px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Repair</b></td>
    <td colspan="3" class="red"><b>Night Guards/Bite Splints</b></td>
    <td colspan="2" class="red"><b>Other*</b></td>
  </tr>
	
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reline', array('type' => 'checkbox', 'label' => 'Reline'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft', array('type' => 'checkbox', 'label' => 'Soft'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.base_plate_bite_rim', array('type' => 'checkbox', 'label' => 'Base Plate/Bite Rim'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rebase', array('type' => 'checkbox', 'label' => 'Rebase')); ?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard', array('type' => 'checkbox', 'label' => 'Hard'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.custom_tray', array('type' => 'checkbox', 'label' => 'Custom Tray'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.basic_repair', array('type' => 'checkbox', 'label' => 'Basic Repair'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_2mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 2mm'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.duplicate_model', array('type' => 'checkbox', 'label' => 'Duplicate Model'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft_liner', array('type' => 'checkbox', 'label' => 'Soft Liner'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_3mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 3mm'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.epoxy_model', array('type' => 'checkbox', 'label' => 'Epoxy Model'));?></td>
  </tr>
  <tr>
  	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth', array('type' => 'checkbox', 'label' => 'Add Tooth #'));?></td><br/>
    <td colspan="2" style="width: 84px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth1', array('type' => 'text','div'=>false,'label'=>false));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.surgical_stent', array('type' => 'checkbox', 'label' => 'Surgical Stent'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.patient_name', array('type' => 'checkbox', 'label' => 'Patient Name in Denture'));?></td>
  </tr>
  <tr>
  	<td colspan="8" style="text-align:right;padding-right:24px; padding-top:5px;">*Additional charge</td>
  </tr>
  
</table>
 </div>
 </div><!--second form for display none property-->

 </div><!--box 1-->
 <div class="box2">
<p style="text-align:center">Select Denture By Drawing</p>
 <!--<canvas id="canvasInAPerfectWorld" width="460" height="300"  > </canvas>-->
 <img src="/shareimages/<?php echo $OrderAttachment[0]['OrderAttachment']['filename'];?>" width="460" height="300">
 <!-- <img src="images/teeth-img.png" style="display:block; margin:auto; margin-top:80px; margin-bottom:60px;">--> 
  </canvas>
  
 <!-- <a id="divid" onclick="redraw12();" class="clr_btn">clear</a>-->
 <!-- <a class="save_btn">Save</a>-->
  
  <textarea class="notebook" rows="7" cols="20" style="resize:none;"></textarea>
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:0px;">
  <tr>
	  
    <td colspan="2">If an adjustment is needed:</td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_opposing', array('type' => 'checkbox', 'label' => 'Adjust opposing'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_abument', array('type' => 'checkbox', 'label' => 'Adjust abument'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.call_office', array('type' => 'checkbox', 'label' => 'Call the office'));?></b></td>
  </tr>
  
</table>
	
   <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top: 24px;">

  <tr>
    <td width="18" valign="top">
    	<!-- <input type="checkbox" style="float:left; width:12px;"> -->
    	<?php echo $this->Form->input('OrderDetail.condition', array('type'=>'checkbox', "style"=>"left:-2px;float:left; width:12px;", "label" => false));?>
    </td>
    <td colspan="2" style="line-height: 17px;">By selecting the "place an order" button below, you accept the biolablogistics.com 
    <a href="#" class="red" data-target="#mahiModal" data-toggle="modal">Terms of Use</a> and the biolab terms of shipping in the applicable biolab Service Guide.</td>
  </tr>
  
  
</table> 
    
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:37px;">
  <tr>
   <td>Doctor's Name</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctornamem', array("type" => "text",'id'=>'doctornamem', 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
   <td>License#</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorlicense', array("type" => "text", 'id'=>'doctorlicense', 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  
</table>
  </div><!--box 2-->

								<!--------------------------------------change----------------------------------->
        <div class="clear">&nbsp;</div>    
        <div class="row form-group" style="margin-bottom:0;">
            <div class="col-sm-2 text-bold txt-lrg" style="margin-left: 27%;color:#555;">Order Status</div>
            <div class="col-sm-2" id="statusID"><?php echo empty($order_data['OrderDetail']['status']) ? 'N-A' : $order_data['OrderDetail']['status']; ?></div>
        </div><br>
        <div class="row form-group" style="margin-bottom:0;">
            <div class="col-sm-2 text-bold txt-lrg" style="margin-left: 27%;color:#555;">Created Date</div>
            <div class="col-sm-5"><?php echo empty($order_data['Order']['created']) ? 'N-A' : date('m/d/y', strtotime($order_data['Order']['created'])); ?></div>
        </div><br>
        <!--div class="row form-group">
            <div class="col-sm-2 text-bold txt-lrg">Attachment Files</div>
        <?php  if (!empty($OrderAttachment)) {
            foreach ($OrderAttachment as $files) { // pr($files);
                ?>							
                    <div class="col-sm-5 download_file glyphicon glyphicon-save show-glyphicon"  ><span class="downd_txt download_file" data-attr="dest_<?php echo $files['OrderAttachment']['id']; ?>"><?php echo $files['OrderAttachment']['filename']; ?></span> 
                   <?php  if ($order_data['OrderDetail']['status_id'] == 1) { ?>
                    <span class="remove_attach glyphicon-remove" data-attrr="dest_<?php echo $files['OrderAttachment']['id']; ?>_<?php echo $order_data['Order']['id']; ?>_<?php echo $files['OrderAttachment']['destination']; ?>"></span>
                    <?php } ?>
		</div>	
        <?php }
    } else { ?>
     <div class="col-sm-5">N-A</div>
   <?php  }
    ?>  </div-->        

    <div class="row form-group" style="margin-bottom:0;">
    <?php if ($order_data['OrderDetail']['status_id'] == 1) { ?>
                <div class="col-sm-2 text-bold txt-lrg" style="margin-left: 27%;color:#555;">Change Status</div>
                <div class="col-sm-3 select-top-mrgn">
        <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('12' => $status[12]), 'selected' => $order_data['OrderDetail']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>
                </div> 
            </div>  			   
                <?php } elseif ($order_data['OrderDetail']['status_id'] == 12) { ?>
            <!--div class="col-sm-5 text-bold txt-lrg">Change Status</div>
                    <div class="col-sm-5 select-top-mrgn">

                    </div>
            </div--> 
                <?php } elseif ($order_data['OrderDetail']['status_id'] == 11) { ?>		

                <?php } elseif ($order_data['OrderDetail']['status_id'] == 9) { ?>
              <div class="col-sm-2 text-bold txt-lrg" style="margin-left: 27%;color:#555;">Change Status</div>
            <div class="col-sm-3 select-top-mrgn">
                    <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('11' => $status[11], '13' => $status[13]), 'selected' => $order_data['OrderDetail']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>
            </div>            
     
		 <?php } ?>
		<!-----------------------------------------------------------NEW------------------------------------------------------------------>
		 
		 	 <div class="text-center clear">	
				<?php  if ($order_data['OrderDetail']['status_id'] == 1) {  ?>		
				
				 
                <div class="login-btn-outer" style="background:#09978B;" style="margin-left: 40%;">
            <?php echo $this->Form->submit(__('Update Order', true), array('class' => 'login-btn', 'id' => 'orderBtn')); ?>
                </div>								
              
                <?php } ?>
					&nbsp; 
				<?php echo $this->Form->end(); ?>
				
				
			<script type="text/javascript">
context = document.getElementById('canvasInAPerfectWorld').getContext("2d");
$('#canvasInAPerfectWorld').mousedown(function(e){
  var mouseX = e.pageX - this.offsetLeft;
  var mouseY = e.pageY - this.offsetTop;
		
  paint = true;
  addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
  redraw();
});
$('#canvasInAPerfectWorld').mousemove(function(e){
  if(paint){
    addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
    redraw();
  }
});
$('#canvasInAPerfectWorld').mouseup(function(e){
  paint = false;
});
$('#canvasInAPerfectWorld').mouseleave(function(e){
  paint = false;
});
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;

function addClick(x, y, dragging)
{
  clickX.push(x);
  clickY.push(y);
  clickDrag.push(dragging);
}

function redraw(){
	//if(curLoadResNum < totalLoadResources){ return; }
	
	//clearCanvas();
  context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
  
  context.strokeStyle = "#f44";
  context.lineJoin = "round";
  context.lineWidth = 1;
			
  for(var i=0; i < clickX.length; i++) {		
    context.beginPath();
    if(clickDrag[i]){
      context.moveTo(clickX[i-1], clickY[i-1]);
     }else{
       context.moveTo(clickX[i]-1, clickY[i]);
     }
     context.lineTo(clickX[i], clickY[i]);
     context.closePath();
     context.stroke();
  }
}
function redraw12(){
	context.canvas.width=context.canvas.width;
	clickX=[];
  clickY=[];
  clickDrag=[];
}
</script> 
 
</div>	
<!------------------------------------------------------------------------SECOND FORM-------------------------------------------------------------------------------->					
					<!--  Rating section start-->
				
				 <?php if(($this->Session->read('Auth.User.user_type') == 1) && (empty($user_rating_already)) && ($order_data['OrderDetail']['status_id'] == 13)) { ?>
       <?php } ?>
       
					<!--  Rating section end-->
					
					<div class="login-btn-outer" style="margin-left: 40%;">
					<?php echo $this->Html->link("Back", "javascript:history.back(1)", array("class" => "login-btn")); ?>
					</div>
					</div>
				<?php echo $this->Form->end(); ?>
            <?php } else if ($this->Session->read("Auth.User.user_type") == 2) { ?>
				<?php echo $this->Session->flash(); ?>
        <div class="row form-group"> 
             <div class="col-sm-12 text-bold txt-lrg" id="ord_id" style="color:#363E41;">OrderID :<?php echo empty($order_data['Order']['id']) ? 'N-A' : $this->requestAction('App/getorderid/'.$order_data['Order']['id']); ?></div>
             <div class="col-sm-12 text-bold txt-lrg" id="ord_id" style="color:#363E41; margin-top:10px;">Doctor Name :<?php echo empty($doc_name['User']['company_name']) ? 'N-A' : $doc_name['User']['company_name']; ?></div>
        </div><br>
        <?php    echo $this->Form->create('Labform');		 ?>
        
        
        
             <!--  order details form start  -->
             
             			
                <?php $i = -1;
                $j = 147;
                 ?>
                
                
                
                
        <!------------------------------------------------------------------------------------------------------------------------------->
        
   
    <?php

echo $this->html->css('style2');
echo $this->html->css('jsDatePick_ltr.min');
/*
echo $this->html->script('bootstrap-datepicker.min');
echo $this->html->script('jquery.1.4.2');
echo $this->html->script('jsDatePick.jquery.min.1.3');

echo $this->html->script('excanvas');
echo $this->html->script('drawing');
echo $this->html->script('functionality');
*/
?>


<div class="container">
 <div class="logo1">
  <img src="/images/logo.jpg" style="width:142px;">
 <p style="padding-bottom:20px;">
  1875 Connecticut Ave NW,<br/>
  Washington, DC 20008<br/>
  PH: (202) 400 5170<br/>
  anaqvi@biolablogistics.com
  www.biolablogistics.com   
  </p>
 </div>
 <div class="upper-box diable-doc">
	 
 	<table width="400" style="padding: 0 10px;">
  <tr>
    <td colspan="6" class="red"><h5 style="margin-top:5px; margin-bottom:0;">Doctor's Name / Account Number or Referring Dental Lab</h5></td>
  </tr>
  <tr>
     <td colspan="6">
    <?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorname', array("type" => "text", 'class' => 'inputt', 'label' => false,'div'=>false)); ?>
    </td>
  </tr>
  
 <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
  
  <tr>
    <td style="padding-right: 8px;">Address</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctoradd', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>City</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorcity', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>State</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorstate', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  

  <tr>
    <td>Zip</td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorzip', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
    <td class="red">Phone</td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$j . '.doctorphone', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctormail', array("type" => "text", 'class' => 'inputt', 'label' => false)); ?></td>
  </tr>
</table>
</div>
 <div class="upper-box">
 	<table width="400" class="table1" style="margin-left:10px;">
  <tr>
    <td colspan="6"><h5 style="margin-top:5px; margin-bottom:0;">Patients's Name</h5>
    </td>
  </tr>
  <tr>
	  
    <td colspan="6">
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.patientName', array("type" => "text", 'class' => 'inputt', 'label' => false,'disabled'=>'disabled')); ?>
		<!--input type="text" name="patient_name"-->
		</td>
  </tr>
  <tr>
    <td style="width:90px;">Date of RX</td>
    <td colspan="1" >
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.dateofRx', array("type" => "text", 'class' => false, 'label' => false , 'id' =>'inputField','disabled'=>'disabled')); ?>
    <!--input type="text" size="12" id="inputField"/-->
    </td>
    <td   class="red" style="width:90px;">Return Date</td>
    <td colspan="1">
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.requestedReturnDate', array("type" => "text", 'class' => false, 'label' => false,'id' =>'inputField2','disabled'=>'disabled')); ?>
    <!--input type="text" size="12" id="inputField2" /-->
    </td>
  </tr>
  <tr>
    <td colspan="6" class="red" >
    <!--input type="checkbox" style="width: 13px;"> RUSH CASE -->
		<?php echo $this->Form->input('OrderDetail.' . ++$i . '.RUSHCASE', array('type' => 'checkbox', 'label' => 'RUSH CASE', 'class' => false,'disabled'=>'disabled','div'=>false)); ?> 
    <span style="color:#333; font-size:10px;">(Rush Fee Accepted)</span></td>
  </tr>
</table>
<?php 
$val =$order_data['Order']['id'];
echo $this->Form->hidden('orderid',array('value'=>$val, 'id'=>'orderid')); ?>
<h5 style="margin: 0px 4px; font-weight:bold;margin-left:10px;   padding: 2px;">For Lab Use</h5>
<table width="410" id="office" style="border: 1px solid #ddd; text-transform:capitalize;margin-left:10px;background: #eee;">
   	
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.IMPRESSION', array('type' => 'checkbox', 'value'=>'1', 'id'=>'im', 'label' => 'IMPRESSION')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.WAXUP', array('type' => 'checkbox', 'value'=>'1', 'id'=>'wa', 'label' => 'WAXUP')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.DENTURE', array('type' => 'checkbox', 'value'=>'1', 'id'=>'de', 'label' => 'DENTURE')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.CROWN', array('type' => 'checkbox', 'value'=>'1', 'id'=>'cr', 'label' => 'CROWN')); ?></td>
  </tr>
  <tr style="margin-left:10px;">
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ARTICULATOR', array('type' => 'checkbox', 'value'=>'1', 'id'=>'ar', 'label' => 'ARTICULATOR')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.BITE', array('type' => 'checkbox', 'value'=>'1', 'id'=>'bi', 'label' => 'BITE')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.FRAMEWRK', array('type' => 'checkbox', 'value'=>'1', 'id'=>'fr', 'label' => 'FRAMEWRK')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.PYMT', array('type' => 'checkbox', 'value'=>'1', 'id'=>'py', 'label' => 'PYMT')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.MODELS', array('type' => 'checkbox', 'value'=>'1', 'id'=>'mo', 'label' => 'MODELS')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.BITEBLK', array('type' => 'checkbox', 'id'=>'bit', 'value'=>'1', 'label' => 'BITE BLK')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.PARTIAL', array('type' => 'checkbox', 'id'=>'par', 'value'=>'1', 'label' => 'PARTIAL')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.CASESPOSTAGE', array('type' => 'checkbox', 'value'=>'1', 'id'=>'cas', 'label' => 'CASES POSTAGE')); ?>	</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.REGULAR', array('type' => 'checkbox', 'label' => 'REGULAR','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.OVERNIGHT', array('type' => 'checkbox', 'label' => 'OVERNIGHT','disabled'=>'disabled')); ?></td>
    <td colspan="2"></td>
    
  </tr>
</table>
 </div>
</div>
<!--container-->  
<div class="dropdown">
	<div style="float:left;width:300px; padding-left:30px; color:#555; font-weight:600; ">Select Restoration Type</div>
    <div style="float:left;width:300px;">
	<select id="change_restorations" name="ftype" style="height:22px; border-radius:2px; color:#b2312c; opacity:9;" onChange="changediv();">
    	<option selected value="1">FIXED RESTORATIONS</option>
        <option  value="2">REMOVABLE RESTORATIONS</option>
    </select>
    
    </div>
    <span class="col-lg-4"><label class="label-text" style="margin-left:60px;font-size:16px;font-weight: bold;color:#555;">Cost : </label>
		<?php echo $this->Form->input('Order.budget', array("type" => "text","placeholder"=>"Price in $",'style'=>'position: relative;top: -18px;margin-left:125px;width: 190px;background:yellow;', 'class' => false, 'label' => false,'value'=>$order_data['Order']['budget'],'disabled'=>'disabled')); ?>
        </span>
    </div>
<script type="text/javascript">
	
function changediv(){
var selectbox = document.getElementById('change_restorations').value;
if(selectbox=='1'){
	document.getElementById('fixed_restorations').style.display="block";
	document.getElementById('removable_restorations').style.display="none";	
}
else{
	document.getElementById('fixed_restorations').style.display="none";
	document.getElementById('removable_restorations').style.display="block";	
}
}
</script>
<!--container-->
<div class="container2">
 <div class="box1">
 <div style="display:block;" id="fixed_restorations">
  <h4>FIXED RESTORATIONS</h4>
  <table width="550" style="color:#333; font-size:12px; padding: 16px 0;   margin-top: 11px;">
	  
            
  <tr>
    <td class="red" style="padding-left:5px;">Shade</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.shade_val', array("type" => "text", 'class' => false, 'label' => false,'disabled'=>'disabled'));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.OcclusalStaining', array('type' => 'checkbox', 'label' => 'Occlusal Staining','disabled'=>'disabled'));?></td>
  </tr>
  <tr>
    <td colspan="1" class="red" style="padding-left:5px;">Tooth Number(s)</td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.toothnumber', array("type" => "text", 'class' => false, 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
</table>
 <div style="height:112px; border-bottom:1px dotted #f33; border-top:1px dotted #f33; padding:10px 0px;margin-top: 20px;">
 <div class="half">
 <table style="color:#333; width:100%; font-size:12px;margin-left:5px;">
  <tr>
    <td colspan="4" class="red" style="padding-left: 5px;"><b>Restoration</b></td>
  </tr>
                    
  <tr>
    <td width="22px" ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.crown', array('type' => 'checkbox', 'label' => 'Crown','disabled'=>'disabled'));?></td>
    <td width="22px"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.inlayOnlay', array('type' => 'checkbox', 'label' => 'Inlay/Onlay','disabled'=>'disabled'));?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.veneer', array('type' => 'checkbox', 'label' => 'Veneer','disabled'=>'disabled')); ?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.postCore', array('type' => 'checkbox', 'label' => 'Post & Core','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bridge', array('type' => 'checkbox', 'label' => 'Bridge','disabled'=>'disabled')); ?></td>
  </tr>
</table>
 </div>
 <div class="half">
 <table width="100"  style="color:#333;margin-left: 20px; width: 150px; font-size:12px;" >
  <tr>
    <td colspan="6" class="red" style="padding-left: 8px;"><b>Pontic Design</b></td>
  </tr>

    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth1', array('type' => 'checkbox', 'label' =>false,'div' =>false,'disabled'=>'disabled')); ?><label for="OrderDetail27Tooth1"><?php echo $this->Html->image("th-1.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-1.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth2', array('type' => 'checkbox', 'label' =>false,'div' =>false,'disabled'=>'disabled')); ?><label for="OrderDetail28Tooth2"><?php echo $this->Html->image("th-2.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-2.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth3', array('type' => 'checkbox', 'label' =>false,'div' =>false,'disabled'=>'disabled')); ?><label for="OrderDetail29Tooth3"><?php echo $this->Html->image("th-3.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-3.png" width="22" height="27"></td-->
  </tr>
  <tr>
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth4', array('type' => 'checkbox', 'label' =>false,'div' =>false,'disabled'=>'disabled')); ?><label for="OrderDetail30Tooth4"><?php echo $this->Html->image("th-4.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td><img src="/images/th-4.png" width="22" height="27"></td-->
    <td colspan='2'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' =>false,'div' =>false,'disabled'=>'disabled')); ?><label for="OrderDetail31Tooth5"><?php echo $this->Html->image("th-5.png", array("alt" => "Tooth1", "width" => "22", "height" => "27")); ?> </label></td>
    <!--td colspan="3"><img src="/images/th-5.png" width="22" height="27"></td-->
  </tr>
  
</table>
 </div>
</div>
 <div style="margin-top:12px; margin-bottom:18px">
 <table width="260px" style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:5px;"><b>Design Details</b></td>
 </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.360degree', array('type' => 'checkbox', 'label' => ' 360º metal margin', 'div' => false,'disabled'=>'disabled', 'style'=>'margin-left:8px;')); ?></td>
    <td colspan='1'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalmargin', array("type" => "text",'style'=>'width:78% !important;', 'class' => false, 'label' => false, 'div' => false,'disabled'=>'disabled')); ?>mm</td>
  </tr>
  <tr>
    <td colspan='4'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.porcelainButtMargin', array('type' => 'checkbox', 'label' => 'Porcelain Butt Margin*','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingualCollar', array('type' => 'checkbox', 'label' => 'Metal Lingual Collar','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan='3'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => ' 3/4 Metal Occlusal*','disabled'=>'disabled')); ?></td>
  </tr>
    <tr style="visibility:hidden;">
    <td><input type="checkbox" style="float:left; width:12px;"></td>
    <td colspan="1">Other</td>
    <td><input type="text" name="doc_name" ></td>
  </tr>
</table>
 <table width="260px" style="color:#333; font-size:12px; float:left; margin-top:22px;  margin-bottom: 9px;   margin-left: 12px;">

  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingual', array('type' => 'checkbox', 'label' => 'Metal Lingual*','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => 'Metal Occlusal*','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan='4'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diagnosticWaxup', array('type' => 'checkbox', 'label' => 'Diagnostic Wax up*','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan='3'> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'other', 'div' => false,'disabled'=>'disabled', 'style'=>'margin-left:8px;')); ?></td>
    <td colspan='1'><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other_val1', array("type" => "text", 'class' => false, 'label' => false, 'div' => false,'disabled'=>'disabled')); ?>
</td>
  </tr>
  <tr>
    <td colspan="5" style="text-align:right; font-size:10px;padding-right: 5px;">*Additional Charges</td>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 10px 0px; clear:both;  padding: 18px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px"><b>ALL-CERAMIC</b></td>
 </tr>
 
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechLayeredZirconia', array('type' => 'checkbox', 'label' => 'Suntech Layered Zirconia','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td> <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechFullZirconia', array('type' => 'checkbox', 'label' => 'Suntech Full Zirconia','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.maxPressed', array('type' => 'checkbox', 'label' => 'e.max Pressed','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechZirconiaCopingOnly', array('type' => 'checkbox', 'label' => ' Suntech Zirconia Coping Only','disabled'=>'disabled')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px;background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>COMPOSITES/TEMP</b></td>
 </tr>
  
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gardia', array('type' => 'checkbox', 'label' => 'Gardia (composite)','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechTemporaryCrown', array('type' => 'checkbox', 'label' => 'Suntech Temporary Crown','disabled'=>'disabled')); ?></td>
  </tr>
  
</table>
 </div>
 <div style=" display: flex; padding: 10px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff; padding:4px 9px;"><b>PFM CROWNS</b></td>
 </tr>
                            	
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.captek', array('type' => 'checkbox', 'label' => 'Captek', 'class' => 'checkbox-group','disabled'=>'disabled')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
<tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>FULL CAST</b></td>
 </tr>              
                    	
 <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castPostCore', array('type' => 'checkbox', 'label' => 'Cast Post & Core','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.y2Gold', array('type' => 'checkbox', 'label' => 'Y+ 2% Gold', 'class' => 'checkbox-group','disabled'=>'disabled')); ?>
  </tr>
  
</table>
 </div>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; display: flex; padding: 16px 0px;">
 <table width="254"  style="color:#333; font-size:12px; float:left;margin-left:5px;">
 <tr>
 	<td class="red" colspan="3" style="padding-left:8px; background:#b2312c; border-radius:4px; color:#fff;padding:4px 9px;"><b>IMPLANT ABUTMENTS</b></td>
 </tr>
 
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.stock', array('type' => 'checkbox', 'label' => 'Stock','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.customMilled', array('type' => 'checkbox', 'label' => 'Custom Milled','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.haderBar', array('type' => 'checkbox', 'label' => 'Hader Bar','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.castOverdentureFrame', array('type' => 'checkbox', 'label' => 'Cast Overdenture Frame','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.screwRetained', array('type' => 'checkbox', 'label' => 'Screw Retained','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconia', array('type' => 'checkbox', 'label' => 'Zirconia','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.titanium', array('type' => 'checkbox', 'label' => 'Titanium','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconiainsertHybrid', array('type' => 'checkbox', 'label' => 'Zirconia w/Ti insert-Hybrid','disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
  <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' => 'Tooth5','disabled'=>'disabled')); ?></td>
  </tr>
</table>
 <table width="254" style="color:#333; font-size:12px; float:left; margin-left:10px;">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="2">Implant System</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.implantSystem', array("type" => "text", 'class' => false, 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Diameter</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.diameter', array("type" => "text", 'class' => false, 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"># Chips or locators</td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.clipsorLocator', array("type" => "text", 'class' => false, 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  
</table>
 </div>
</div>
                                                           <!--extra left div for display none property-->

<div id="removable_restorations" style="display:none;">
 <h4>REMOVABLE RESTORATIONS</h4>
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
	 
	
	 
  <tr>
  	<td style="display:none;"><input type="checkbox" style="float:left; width:12px;"></td>
   	<td colspan="2" class="red" style="padding:5px;">Check all that apply</td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'Other','div' => false ,'disabled'=>'disabled', 'style'=>'margin-left:8px;'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.other1', array('type' => 'textbox', 'label' => false,'div' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.upper', array('type' => 'checkbox', 'label' => 'Upper','disabled'=>'disabled'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lower', array('type' => 'checkbox', 'label' => 'Lower','disabled'=>'disabled'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.try-in', array('type' => 'checkbox', 'label' => 'Try-in','disabled'=>'disabled'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.finish', array('type' => 'checkbox', 'label' => 'Finish','disabled'=>'disabled')); ?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.cusil', array('type' => 'checkbox', 'label' => 'Cusil','disabled'=>'disabled'));?></td>
   
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.bite_block', array('type' => 'checkbox', 'label' => 'Bite Block','disabled'=>'disabled')); ?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.denture', array('type' => 'checkbox', 'label' => 'Denture','disabled'=>'disabled'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.immediate', array('type' => 'checkbox', 'label' => 'Immediate/Surgical Denture ','disabled'=>'disabled'));?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot', array('type' => 'checkbox', 'label' => 'Extract Toot #','div' => false,'disabled'=>'disabled', 'style'=>'margin-left:8px;'));?></td>
    <td colspan="5"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot1', array('type' => 'textbox', 'label' => false,'div' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
   	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_all', array('type' => 'checkbox', 'label' => 'Extract All','disabled'=>'disabled'));?></td>
    <td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_now', array('type' => 'checkbox', 'label' => 'Extract Now','disabled'=>'disabled'));?></td>
    <td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_try', array('type' => 'checkbox', 'label' => 'Extract After Try-In','disabled'=>'disabled'));?></td>
  </tr>
  
</table>
<div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
  <table width="541" style="color:#333; margin-left:8px; font-size:12px; float:left; margin-top:10px;">
  <tr>
   	<td colspan="6" class="red"><b>Teeth</b><span style="font-size:10px; color:#333;"> (stock teeth used if no option is selected)</span></td>
  </tr>
   
  <tr>
	
   	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.tooth_shad', array('type' => 'textbox','style'=>'  width: 466px;', 'label' => 'Tooth Shad', 'div'=>false)); ?></td>
  </tr>
  <tr>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.ipn_portrait', array('type' => 'checkbox', 'label' => 'IPN Portrait*','disabled'=>'disabled')); ?></td>
   	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.gold_face', array('type' => 'checkbox', 'label' => 'Gold Open Face*','disabled'=>'disabled')); ?></td>
    <td><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.full_gold', array('type' => 'checkbox', 'label' => 'Full Gold*','disabled'=>'disabled'));?></td>
  </tr>
  
  
</table>
  </div>
 <table width="520" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Sunflex Partials</b></td>
    <td colspan="2" class="red"><b>Sunflex Shade</b></td>
    <td colspan="2" class="red"><b>Clasp Type</b></td>
  </tr>
    
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex', array('type' => 'checkbox', 'label' => 'Sunflex','disabled'=>'disabled')); ?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_pink', array('type' => 'checkbox', 'label' => 'Light Pink','disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.cast', array('type' => 'checkbox', 'label' => 'Cast*','disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_cast_frame', array('type' => 'checkbox', 'label' => 'Sunflex Cast Frame Combo','disabled'=>'disabled'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.pink', array('type' => 'checkbox', 'label' => 'Pink','disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.wire', array('type' => 'checkbox', 'label' => 'Wire*' ,'disabled'=>'disabled'));?></td>

<style>
.diable-doc{
    pointer-events: none;
    
    }
</style>
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_vitallium_frame', array('type' => 'checkbox', 'label' => 'Sunflex Vitallium 2000+ Frame Combo' ,'disabled'=>'disabled'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.flexible', array('type' => 'checkbox', 'label' => 'Flexible*' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_unilateral', array('type' => 'checkbox', 'label' => 'Sunflex Unilateral(Maximum 2 teeth)' ,'disabled'=>'disabled'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear', array('type' => 'checkbox', 'label' => 'sunclear*' ,'disabled'=>'disabled'));?></td>
  </tr>
</table>
 <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Acrylic partials</b></td>
    <td colspan="2" class="red"><b>Acrylic Shade</b></td>
  </tr>
  
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_frame', array('type' => 'checkbox', 'label' => 'Suncast Frame w/Acrylic' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lucitone', array('type' => 'checkbox', 'label' => 'Lucitone 199*'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_acrylic', array('type' => 'checkbox', 'label' => ' Vitallium 2000+ w/Acrylic' ,'disabled'=>'disabled'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_pink', array('type' => 'checkbox', 'label' => 'Pink' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear_frame', array('type' => 'checkbox', 'label' => 'Sunclear Frame w/Acrylic' ,'disabled'=>'disabled'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_light_pink', array('type' => 'checkbox', 'label' => 'Light Pink' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.flipper', array('type' => 'checkbox', 'label' => ' Flipper (1 Tooth All Acrylic)' ,'disabled'=>'disabled'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.light_meharry', array('type' => 'checkbox', 'label' => 'Light Meharry' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_partial', array('type' => 'checkbox','div'=>false, 'label' => ' Acrylic Partial (No Frame)' ,'disabled'=>'disabled', 'style'=>'margin-left:8px;'));?><span style="font-size:10px;"><?php echo "(Wrought Wire Clasps)";?></span></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="4"  style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry' ,'disabled'=>'disabled'));?></td>
  </tr>
</table>
 </div>
 <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Metal Frameworks</b></td>
    <td colspan="2" class="red"><b>Reinforcement</b></td>
  </tr>
       
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_framework', array('type' => 'checkbox', 'label' => 'Suncast Framework Only' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_wire', array('type' => 'checkbox', 'label' => 'Wire*' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="4"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_framework', array('type' => 'checkbox', 'label' => 'Vitallium 2000+ Framework Only' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_mesh', array('type' => 'checkbox', 'label' => 'Mesh*' ,'disabled'=>'disabled'));?></td>
  </tr>
</table>
  <div style="border-bottom:1px dotted #f33; border-top:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="4" class="red" style="padding-left:5px;"><b>Design</b></td>
    <td colspan="2" class="red">&nbsp;</td>
  </tr>
            
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.horseshoe_palate', array('type' => 'checkbox', 'label' => 'Horseshoe Palate' ,'disabled'=>'disabled'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.open_palate', array('type' => 'checkbox', 'label' => 'AP Open Palate' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.full_palatal_metal_coverage', array('type' => 'checkbox', 'label' => 'Full Palatal Metal Coverage ' ,'disabled'=>'disabled'));?></td>
    <td ><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.palatal_trap', array('type' => 'checkbox', 'label' => 'Palatal Strap' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.metal_occlusion', array('type' => 'checkbox', 'label' => 'Metal Occlusion' ,'disabled'=>'disabled'));?></td>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rests', array('type' => 'checkbox', 'label' => 'Rests' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_pron', array('type' => 'checkbox', 'label' => 'Lingual Apron' ,'disabled'=>'disabled'));?></td>
    <td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.precision_attachments', array('type' => 'checkbox', 'label' => 'Precision Attachments*' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td ><?php echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_bar', array('type' => 'checkbox', 'label' => 'Lingual Bar' ,'disabled'=>'disabled'));?></td>
    <td style="visibility:hidden;"><input type="checkbox" style="float:left; width:12px;"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" style="padding:5px;">(Best design is fabricated if no option is selected)</td>
  </tr>
</table>
 </div>
  <table width="550"  style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Valplast Partials</b></td>
    <td colspan="2" class="red"><b>Sunclear Frameworks</b></td>
  </tr>
  
          
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast', array('type' => 'checkbox', 'label' => 'Valplast' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.framework_only', array('type' => 'checkbox', 'label' => ' Framework Only' ,'disabled'=>'disabled')); ?></td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_cast_combo', array('type' => 'checkbox', 'label' => 'Valplast Cast Combo' ,'disabled'=>'disabled'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td><?php echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_vitallium', array('type' => 'checkbox', 'label' => 'Valplast Vitallium 2000+ Combo' ,'disabled'=>'disabled'));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
  <div style=" border-top:1px dotted #f33; float:left; display: 
  flex; margin-top:8px; padding-bottom:8px; margin-bottom:5px;">
 <table width="550" style="color:#333; font-size:12px; float:left; margin-top:10px;">
  <tr>
  	<td colspan="2" class="red" style="padding-left:5px;"><b>Attachments*</b></td>
    <td colspan="3" class="red">&nbsp;</td>
  </tr>
 
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.era', array('type' => 'checkbox', 'label' => 'ERA' ,'disabled'=>'disabled'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.pd', array('type' => 'checkbox', 'label' => 'PD' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hader_bar', array('type' => 'checkbox', 'label' => 'Hader Bar' ,'disabled'=>'disabled'));?></td>
    <td colspan="1" style="  width: 66px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'checkbox', 'label' => 'Other' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'text', 'label'=>false ,'disabled'=>'disabled'));?></td>
  </tr>
  
</table>
 </div>
  <div style=" text-align:center; background:#b2312c; color:#fff; margin: -4px auto; clear:both;padding: 3px;">OTHER</div>
 <div style="border-bottom:1px dotted #f33; float:left; display: flex; margin-top:8px; padding-bottom:8px;">
 <table width="550" style="color:#333; font-size:12px;  margin-top:4px; margin-bottom:4px;">
  <tr>
  	<td colspan="3" class="red" style="padding-left:5px;"><b>Repair</b></td>
    <td colspan="3" class="red"><b>Night Guards/Bite Splints</b></td>
    <td colspan="2" class="red"><b>Other*</b></td>
  </tr>
	
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.reline', array('type' => 'checkbox', 'label' => 'Reline' ,'disabled'=>'disabled'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft', array('type' => 'checkbox', 'label' => 'Soft' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.base_plate_bite_rim', array('type' => 'checkbox', 'label' => 'Base Plate/Bite Rim' ,'disabled'=>'disabled'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.rebase', array('type' => 'checkbox', 'label' => 'Rebase' ,'disabled'=>'disabled')); ?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard', array('type' => 'checkbox', 'label' => 'Hard' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.custom_tray', array('type' => 'checkbox', 'label' => 'Custom Tray' ,'disabled'=>'disabled'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.basic_repair', array('type' => 'checkbox', 'label' => 'Basic Repair' ,'disabled'=>'disabled'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_2mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 2mm' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.duplicate_model', array('type' => 'checkbox', 'label' => 'Duplicate Model' ,'disabled'=>'disabled'));?></td>
    
  </tr>
  <tr>
  	<td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.soft_liner', array('type' => 'checkbox', 'label' => 'Soft Liner' ,'disabled'=>'disabled'));?></td>
    <td colspan="3"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_3mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 3mm' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.epoxy_model', array('type' => 'checkbox', 'label' => 'Epoxy Model' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="1"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth', array('type' => 'checkbox', 'label' => 'Add Tooth #' ,'disabled'=>'disabled'));?></td><br/>
    <td colspan="2" style="width: 84px;"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth1', array('type' => 'text','div'=>false,'label'=>false ,'disabled'=>'disabled'));?></td>
    <td colspan="3"><?php  echo $this->Form->input('OrderDetail.' . ++$i . '.surgical_stent', array('type' => 'checkbox', 'label' => 'Surgical Stent' ,'disabled'=>'disabled'));?></td>
    <td colspan="2"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.patient_name', array('type' => 'checkbox', 'label' => 'Patient Name in Denture' ,'disabled'=>'disabled'));?></td>
  </tr>
  <tr>
  	<td colspan="8" style="text-align:right;padding-right:24px; padding-top:5px;">*Additional charge</td>
  </tr>
  
</table>
 </div>
 </div><!--second form for display none property-->

 </div><!--box 1-->
 <div class="box2">
<p style="text-align:center">Select Denture By Drawing</p>
 <!--<canvas id="canvasInAPerfectWorld" width="460" height="300"  > </canvas>-->
 <img src="/shareimages/<?php echo $OrderAttachment[0]['OrderAttachment']['filename'];?>" width="460" height="300">
 
  
 
  
  <textarea class="notebook" rows="7" cols="20" style="resize:none;"></textarea>
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:0px;">
  <tr>
	  
    <td colspan="2">If an adjustment is needed:</td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_opposing', array('type' => 'checkbox', 'label' => 'Adjust opposing' ,'disabled'=>'disabled'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_abument', array('type' => 'checkbox', 'label' => 'Adjust abument' ,'disabled'=>'disabled'));?></b></td>
  </tr>
  <tr>
    <td width="18"><?PHP echo $this->Form->input('OrderDetail.' . ++$i . '.call_office', array('type' => 'checkbox', 'label' => 'Call the office' ,'disabled'=>'disabled'));?></b></td>
  </tr>
  
</table>
	
   <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top: 24px;">

  <tr>
    <td width="18" valign="top">
    	<!-- <input type="checkbox" style="float:left; width:12px;"> -->
    	<?php echo $this->Form->input('OrderDetail.condition', array('type'=>'checkbox', 'style'=>'left:-2px;
    top: 0px;', "label" => false));?>
    </td>
    <td colspan="2" style="line-height: 17px;">By selecting the "place an order" button below, you accept the biolablogistics.com 
    <a href="#" class="red" data-target="#mahiModal" data-toggle="modal">Terms of Use</a> and the biolab terms of shipping in the applicable biolab Service Guide.</td>
  </tr>
  
  
</table> 
    
  <table width="460" style="color:#333; font-size:12px; float:left; margin-left:10px; margin-top:37px;">
  <tr>
   <td>Doctor's Name</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctornamem', array("type" => "text",'id'=>'doctornamem', 'class' => 'inputt', 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
   <td>License#</td>
  </tr>
  <tr>
    <td><?php echo $this->Form->input('OrderDetail.' . ++$j . '.doctorlicense', array("type" => "text", 'id'=>'doctorlicense', 'class' => 'inputt', 'label' => false,'disabled'=>'disabled')); ?></td>
  </tr>
  
</table>
  </div><!--box 2-->
            
   <!-------------------------------------------------------------DO NOT CHANGE THIS------------------------------------------------------------------------------>         
        <div class="clear">&nbsp;</div>    
        <div class="row form-group" style="margin-top:20px;">
			<div class="col-sm-4"></div>
            <div class="col-sm-3 text-bold txt-lrg" style="margin-left: -32px; color:#555;">Order Status</div>
            <div class="col-sm-5" id="statusID">
        <?php //pr($order_data);
        //die; 
        ?>
        <?php if (empty($order_data['OrderDetail']['is_reject_by']) && $order_data['OrderDetail']['status_id'] == 3) { ?>
        <?php echo "Waiting"; ?>
    <?php
    } elseif (empty($order_data['Order']['lab_id'])) {
        echo empty($order_data['OrderDetail']['status']) ? 'N-A' : $order_data['OrderDetail']['status'];
    } elseif (!empty($order_data['Order']['lab_id']) && ($order_data['Order']['lab_id'] == $this->Session->read('Auth.User.id'))) {
        ?>
        <?php echo empty($order_data['OrderDetail']['status']) ? 'N-A' : $order_data['OrderDetail']['status']; ?>
    <?php } else {
        echo "Order has already been accepted by another lab";
    } ?>
            </div>
        </div>
        <div class="row form-group">
			<div class="col-sm-4"></div>
            <div class="col-sm-3 text-bold txt-lrg" style="margin-left: -32px; color:#555;">Created Date</div>
            <div class="col-sm-5"><?php echo empty($order_data['Order']['created']) ? 'N-A' : date('m/d/y', strtotime($order_data['Order']['created'])); ?></div>
        </div>

        <!-- <div class="row form-group">
			<div class="col-sm-4"></div>
            <div class="col-sm-3 text-bold txt-lrg" style="margin-left: -32px; color:#555;">Attachment Files</div>
        <?php  if (!empty($OrderAttachment)) {
            foreach ($OrderAttachment as $files) { // pr($files);
                ?>							
                    <div class="col-sm-3 download_file glyphicon glyphicon-save show-glyphicon" data-attr="dest_<?php echo $files['OrderAttachment']['id']; ?>" ><?php echo $files['OrderAttachment']['filename']; ?></div>	
        <?php }
    } else { ?>
       <div class="col-sm-3">N-A</div>
 <?php   }
    ?>

        </div>
        -->
        <div class="row form-group" style="margin-top:20px;">					
            <?php if (!empty($order_data['Order']['lab_id']) && ($order_data['Order']['lab_id'] == $this->Session->read('Auth.User.id'))) { ?>
                <?php if ($order_data['OrderDetail']['status_id'] != 3) { ?>
                        <?php if ($order_data['OrderDetail']['status_id'] == 2) { ?>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3 text-bold txt-lrg" style="margin-left: -32px; color:#555;">Change Status</div>
                        <div class="col-sm-3 select-top-mrgn">
                        <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('4' => $status[4]), 'selected' => $order_data['OrderDetail']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>
                        </div>
                        <?php } else if ($order_data['OrderDetail']['status_id'] == 4) { ?>
                        <div class="col-sm-4"></div>		
                        <div class="col-sm-3 text-bold txt-lrg" style="margin-left: -32px; color:#555;">Change Status</div>
                        <div class="col-sm-3 select-top-mrgn">
                        <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('7' => $status[7]), 'selected' => $order_data['OrderDetail']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>        
                        </div>
                        
						
            <?php
            } /* else if($order_data['OrderDetail']['status_id'] == 7){ ?>
              <div class="col-sm-5 text-bold txt-lrg">Change Status</div>
              <div class="col-sm-5 select-top-mrgn">
              <?php echo $this->Form->input('status',array('class'=>"selectpicker",'label'=>false,'div'=>false,"options"=>array('7'=>$status[7]),'selected'=>$order_data['OrderDetail']['status_id'], 'empty'=>'Select Status','onChange' =>'update_status(this.value,"'.$order_data['Order']['id'].'")')); ?>
              </div>
              <?php } */
        }
    }
    ?>

        </div>    
    <?php if (empty($order_data['Order']['lab_id']) && $order_data['OrderDetail']['is_reject_by'] != $this->Session->read("Auth.User.id")) { ?>
        <?php if ($order_data['OrderDetail']['status_id'] != 12 && $order_data['OrderDetail']['status_id'] != 3) { ?>
			            
            <div class="text-center">
                <div class="black-btn-outer">
			<?php	echo $this->Form->button('Request for Change', array('type'=>'button',"class" => "black-btn","id" => "sendemailforDoctor"));
			  ?>
			</div> &nbsp;
                    <div class="black-btn-outer">
                    
  <?php  echo $this->Form->button('Accept Order', array('type' => 'button', 'class' => 'black-btn accpt_order','onclick'=> 'return pro();')); ?>
  
            <?php echo $this->Html->link("Accept Order", array("controller" => "orders","action" => 'orderstatus', $order_data['Order']['id'], 2), array("class" => "black-btn accpt_order",'id'=>'link' ,'style'=>'display:none')); ?>
                    </div> &nbsp; 
                    <div class="black-btn-outer">
            <?php echo $this->Html->link("Reject Order", array("controller" => "orders", "action" => 'orderstatus', $order_data['Order']['id'], 3), array("class" => "black-btn rejct_order")); ?>
                    </div>
                </div>
        <?php }
    } ?>
    <div class="text-center">
<div class="login-btn-outer">
<?php echo $this->Html->link("Back", "javascript:history.back(1)", array("class" => "login-btn")); ?>
</div>
</div>
<?php echo $this->Form->hidden('doc_email',array('value'=>$doc_email['User']['username']));    ?>
<?php echo $this->Form->hidden('doc_name',array('value'=>$order_data['Doctor']['first_name']));    ?>

<?php echo $this->Form->end(); ?>
    
<?php } else if ($this->Session->read("Auth.User.user_type") == 3) { ?>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Order ID</div>
            <div class="col-sm-5 txt-lrg"><?php echo empty($order_data['Order']['id']) ? 'N-A' : $order_data['Order']['order_manualid']; ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Order Status</div>
            <div class="col-sm-5" id="statusID"><?php echo empty($order_data['Order']['status']) ? 'N-A' : $order_data['Order']['status']; ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Created Date</div>
            <div class="col-sm-5"><?php echo empty($order_data['Order']['created']) ? 'N-A' : date('m/d/y',strtotime($order_data['Order']['created'])); ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Lab Address</div>
            <div class="col-sm-5"><?php 
            $statel = $this->requestAction('App/getstate/'.$order_data['Lab']['state_id']); 
            $state_l = $statel[0]['State']['name'].','.$statel[0]['State']['code'];
            echo empty($order_data['Lab']['address']) ? 'N-A' : $order_data['Lab']['address'].'<br>'.$order_data['Lab']['city'].', '.$state_l.' '.$order_data['Lab']['zipcode']; 
           
            
            ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Lab Phone</div>
            <div class="col-sm-5"><?php echo empty($order_data['Lab']['phone']) ? 'N-A' : /* $order_data['Lab']['phone'] */(empty("(".substr($order_data['Lab']['phone'], 0, 3).") ".substr($order_data['Lab']['phone'], 3, 3)."-".substr($order_data['Lab']['phone'],6))?'**********':(empty($order_data['Lab']['privacy'])?"(".substr($order_data['Lab']['phone'], 0, 3).") ".substr($order_data['Lab']['phone'], 3, 3)."-".substr($order_data['Lab']['phone'],6):'************')) ; ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Doctor Address</div>
            <div class="col-sm-5"><?php 
            $stated = $this->requestAction('App/getstate/'.$order_data['Doctor']['state_id']); 
            $state_d = $stated[0]['State']['name'].','.$stated[0]['State']['code'];
            echo empty($order_data['Doctor']['address']) ? 'N-A' : $order_data['Doctor']['address'].'<br>'.$order_data['Doctor']['city'].', '.$state_l.' '.$order_data['Doctor']['zipcode']; ?></div>
        </div>
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Assigned Driver</div>
            <div class="col-sm-5"><?php $lab_id = $order_data['Order']['lab_id'];
            $driver_detail = $this->requestAction('App/getdriver/'.$lab_id); 
			
           echo empty($driver_detail['Userdetail']['first_name']) ? '<div style="color:red">Driver not assigned.</div>' : $driver_detail; 
            
            ?></div></div>
        
        <div class="row form-group">
            <div class="col-sm-5 text-bold txt-lrg">Doctor Phone</div>
            <div class="col-sm-5"><?php echo empty($order_data['Doctor']['phone']) ? 'N-A' : /* $order_data['Doctor']['phone'] */(empty("(".substr($order_data['Doctor']['phone'], 0, 3).") ".substr($order_data['Doctor']['phone'], 3, 3)."-".substr($order_data['Doctor']['phone'],6))?'**********':(empty($order_data['Doctor']['privacy'])?"(".substr($order_data['Doctor']['phone'], 0, 3).") ".substr($order_data['Doctor']['phone'], 3, 3)."-".substr($order_data['Doctor']['phone'],6):'************')) ; ?></div>
        </div><br>
    <?php if (!empty($status)) { ?>
            <div class="row form-group">
                <div class="col-sm-5 text-bold txt-lrg">Change Status</div>
        <?php if ($order_data['Order']['status_id'] == 7) {  ?> 
                    <div class="col-sm-5 select-top-mrgn">
            <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('8' => $status[8]), 'selected' => $order_data['Order']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>                      
                    </div>
        <?php } elseif ($order_data['Order']['status_id'] == 8) {  ?>
                    <div class="col-sm-5 select-top-mrgn">
            <?php echo $this->Form->input('status', array('class' => "selectpicker", 'label' => false, 'div' => false, "options" => array('9' => $status[9]), 'selected' => $order_data['Order']['status_id'], 'empty' => 'Select Status', 'onChange' => 'update_status(this.value,"' . $order_data['Order']['id'] . '")')); ?>
        <?php } ?>
                </div>
    <?php } ?> 
    				<div class="text-center clear">
					<div class="login-btn-outer" style="margin-left: 40%;">
					<?php echo $this->Html->link("Back", "javascript:history.back(1)", array("class" => "login-btn")); ?>
					</div>
					</div>
<?php } ?>
        <br>

    </div>
 
</div>

<div id="mahiModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">TERMS OF USE</h2>
        </div>
        <div id="tabble" class="modal-body">
			
          
          
          </table>
        </div>
        
      </div>
      
       </div>
</div>
<script type="text/javascript">
 $( document ).ready(function() {
 var v2=document.getElementById('wa').checked;var v3=document.getElementById('de').checked;
       var v4=document.getElementById('cr').checked;var v5=document.getElementById('ar').checked;
var v6=document.getElementById('bi').checked;var v7=document.getElementById('fr').checked;
var v8=document.getElementById('py').checked;var v9=document.getElementById('mo').checked;
var v10=document.getElementById('bit').checked;var v11=document.getElementById('par').checked;
var v12=document.getElementById('cas').checked;var v1=document.getElementById('im').checked;
      if(v1 == true || v2 == true|| v3 == true|| v4 == true|| v5 == true|| v6 == true|| v7 == true|| v8 == true|| v9 == true|| v10 == true|| v11 == true|| v12 == true){
       document.getElementById('im').disabled = true;
       document.getElementById('wa').disabled = true;
       document.getElementById('de').disabled = true;
       document.getElementById('cr').disabled = true;
       document.getElementById('ar').disabled = true;
       document.getElementById('bi').disabled = true;
       document.getElementById('fr').disabled = true;
       document.getElementById('py').disabled = true;
       document.getElementById('mo').disabled = true;
       document.getElementById('bit').disabled = true;
       document.getElementById('par').disabled = true;
       document.getElementById('cas').disabled = true;
       document.getElementById('im').disabled = true;
       
       }
    });
 

$('table').on('click', 'input:checkbox', function() {
    $(this)
    .parents('table')
    .find('input:checkbox:not(:checked)')
    .attr('disabled', $(this).is(':checked'));
});
function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}

function pro(){
var order=document.getElementById('orderid').value;
//alert(order);
document.getElementById('orderidee').value = order;

 

var v2=document.getElementById('wa').checked;
var v3=document.getElementById('de').checked;

var v4=document.getElementById('cr').checked;
var v5=document.getElementById('ar').checked;
var v6=document.getElementById('bi').checked;
var v7=document.getElementById('fr').checked;
var v8=document.getElementById('py').checked;
var v9=document.getElementById('mo').checked;
var v10=document.getElementById('bit').checked;
var v11=document.getElementById('par').checked;
var v12=document.getElementById('cas').checked;
var v1=document.getElementById('im').checked;
if(v1==true ||v2==true ||v3==true || v4==true ||v5==true ||v5==true || v6==true || v7==true || v8==true || v9==true ||v10==true ||v11==true ||v12==true)
{

if(v1 == true){v1=1;}
else{v1=0;} 
if(v2 == true){v2=1;}
else{v2=0;} if(v3 == true){v3=1;}else{v3=0;} if(v4 == true){v4=1;}else{v4=0;} 
if(v5 == true){v5=1;}else{v5=0;} if(v6 == true){v6=1;}else{v6=0;} if(v7 == true){v7=1;}else{v7=0;} if(v8 == true){v8=1;}else{v8=0;}
if(v9 == true){v9=1;}else{v9=0;} if(v10 == true){v10=1;}else{v10=0;} if(v11 == true){v11=1;}else{v11=0;} if(v12 == true){v12=1;}else{v12=0;}

//alert("ok");

$.ajax({
  type: 'POST',
  url: 'http://biolablogistics.com/updatepro.php',
  data: { 'v1': v1 , 'v2': v2, 'v3': v3, 'v4': v4, 'v5': v5, 'v6': v6, 'v7': v7, 'v8': v8, 'v9': v9, 'v10': v10, 'v11': v11, 'v12': v12, 'order':order},
success:function(res){
//alert(res);
location.assign(document.getElementById("link").getAttribute("href"));

}
});

}
else 
{
alert("Please Select a procedure.");
//alert(document.getElementsById("link").getAttribute("href"));
return false;
}
}
function valid(){
	var s1 = document.getElementById('labdoctor').value;
	if(s1 == ''){
		document.getElementById('error').innerHTML='Please select atleast one doctor';
		return false;
	}
	
	}
</script>

<style>



</style> 
<div id="mahiModaldoc" class="modal fade bs-example-modal-sm" role="dialog">
  <div class="modal-dialog modal-sm">
   <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="text-align:center;" class="modal-title">Select Doctor</h2>
        </div>
        <div id="tabble" class="modal-body">
        <?php 
        echo $this->Form->create('order', array('action' => 'adddoctor','onsubmit'=>'return valid()'));
        echo $this->Form->input('orderid', array('type' => 'hidden','id'=>'orderidee'));
		echo '<div id="labdoctor2"></div>';
		echo '<p style="color:red;" id="error"></p>';
		echo $this->Form->submit();
		echo $this->Form->end(); 
        ?>
        
        </div>
        
      </div>
      
       </div>
</div>

