<?php
$i = -1;
$k = -1;
?>
<div class="place-order">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Form->hidden("auto"); ?>
    <!--<?php if (empty($users)) { ?><p class="text-center" id="no_lab">Please select atleast one Lab to place your order. Click <?php echo $this->Html->link(__(__d('labels', 'HERE')), array('plugin' => false, 'controller' => 'users', 'action' => 'search', '2'), array('title' => 'All Labs')); ?> to add Labs.</p>  <?php } ?> -->
    
    <h2><?php echo $heading; ?></h2>
    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    <?php echo $this->Form->create('Order', array("enctype" => "multipart/form-data")); ?>
    <?php if(isset($or_id)) { ?>	
    <?php echo $this->Form->hidden('order_id',array('value'=>$or_id)); } ?>
    <div class="row">
        <div class="col-lg-5"><label class="label-text">Patient’s Name</label>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.patientName', array("type" => "text", 'class' => false, 'label' => false)); ?>
        </div>	

        <div class="col-lg-3 no-padding date-icon"><label class="label-text">Date of RX</label>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.dateofRx', array("type" => "text", 'class' => false, 'label' => false)); ?>
        </div>


        <div class="col-lg-4 no-padding date-icon"><label class="label-text">Requested Return Date</label>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.requestedReturnDate', array("type" => "text", 'class' => false, 'label' => false)); ?>
        </div>
		<div class="col-lg-4"><label class="label-text">Cost : </label>
            <?php echo $this->Form->input('Order.budget', array("type" => "text","placeholder"=>"Price in $", 'class' => false, 'label' => false)); ?>
        </div>
    </div>
    
    <br><br>





    <div class="lab-use"><label class="label-text labuse">Lab use only:</label>
        <div class="check-boxes">
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.IMPRESSION ', array('type' => 'checkbox', 'label' => 'IMPRESSION ')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.WAXUP', array('type' => 'checkbox', 'label' => 'WAXUP')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.DENTURE', array('type' => 'checkbox', 'label' => 'DENTURE')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.CROWN', array('type' => 'checkbox', 'label' => 'CROWN')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.ARTICULATOR', array('type' => 'checkbox', 'label' => 'ARTICULATOR')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.BITE', array('type' => 'checkbox', 'label' => 'BITE')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.FRAMEWRK', array('type' => 'checkbox', 'label' => 'FRAMEWRK')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.PYMT', array('type' => 'checkbox', 'label' => 'PYMT')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.MODELS', array('type' => 'checkbox', 'label' => 'MODELS')); ?>			
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.BITEBLK', array('type' => 'checkbox', 'label' => 'BITE BLK')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.PARTIAL', array('type' => 'checkbox', 'label' => 'PARTIAL')); ?>
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.CASESPOSTAGE', array('type' => 'checkbox', 'label' => 'CASES POSTAGE')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.REGULAR', array('type' => 'checkbox', 'label' => 'REGULAR')); ?>	
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.OVERNIGHT', array('type' => 'checkbox', 'label' => 'OVERNIGHT')); ?>	
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.RUSHCASE', array('type' => 'checkbox', 'label' => 'RUSH CASE')); ?>	
        </div>
        <div class="col-lg-5">
            <div class="input"><br>
                <label class="label-text col-lg-4 no-padding">Order Detail</label>
                <div class=" col-lg-8 no-padding">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.details', array("type" => "textarea", 'class' => "gray-bg form-control", 'label' => false, 'div' => false)); ?>
                </div>
            </div>
        </div>
         <div class="col-lg-5">
            <div class="input"><br>
                <label class="label-text col-lg-4 no-padding">Other Specifications</label>
                <div class=" col-lg-8 no-padding">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.specification', array("type" => "textarea", 'class' => "gray-bg form-control", 'label' => false, 'div' => false)); ?>
                </div>
            </div>
        </div>
        
        
        
        
      <!--   <div class="col-lg-5">
            <div class="input" style="width:100%" ><br>
                <label class="label-text col-lg-4 no-padding">Select Labs</label>
                 <div class="col-lg-8 no-padding">
                <?php if (!empty($accepted_user) && isset($accepted_user)) { ?>
                        <label id="chek_msg" class="error">Selected LAb</label> 		
                        <?php
                        $j = 0;
                        foreach ($users as $user) {
                            if ($user['DoctorLab']['lab_id'] == $accepted_user) {
                                echo $this->Form->input('OrderRequest.' . $j++ . '.lab_id', array('type' => 'checkbox', 'checked' => $accepted_user, 'label' => false, 'div' => false, 'value' => $user['DoctorLab']['lab_id'], 'class' => 'checkbox-group-lab'));
                                echo $user['Userdetail']['first_name'];
                            } else {
                                //echo $this->Form->input('OrderRequest.'.$j++.'.lab_id', array('type' => 'checkbox','checked'=>false,'label' => false, 'value'=>$user['DoctorLab']['lab_id'] ,'class'=>'checkbox-group-lab'));
                                //echo $user['Userdetail']['first_name'];				
                            }
                        }
                        ?>
                    <?php } else { ?>
                        			
                        <?php
                        $j = 0;
                        foreach ($users as $user) {
                            echo $this->Form->input('OrderRequest.' . $j++ . '.lab_id', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => $user['DoctorLab']['lab_id'], 'class' => 'checkbox-group-lab'));
                            echo $user['Userdetail']['first_name'];
                            ?> &nbsp;  
                            <?php
                        }
                        ?> 
                            <label id="chek_msg" class="error">Please select atleast one Lab</label> 
                    <?php } ?> 
                </div>
            </div>
        </div>-->
    </div>
    <div class="row">
		<div class="col-lg-5 qty">
            <div class="input">
                <label class="label-text">Quantity</label>               
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.quantity', array("type" => "text", 'class' => false, 'label' => false, 'div' => false)); ?>                
            </div>
        </div>
    </div>
     <!--  <div class="clear">&nbsp;</div>-->
    <h3 class='resc'><span>FIXED RESTORATIONS</span></h3>

        <div><label class="label-text">Shade</label> 
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.shade_val', array("type" => "text", 'class' => false, 'label' => false));
            echo $this->Form->input('OrderDetail.' . ++$i . '.OcclusalStaining', array('type' => 'checkbox', 'label' => 'Occlusal Staining'));
            ?>  &nbsp; 
            <label class="label-text">Tooth Number(s)</label>
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.toothnumber', array("type" => "text", 'class' => false, 'label' => false)); ?>
        </div><br><br>

        <div class="restoration"><label class="label-text">Restoration</label>
            <div class="check-boxes">
                <?php echo $this->Form->input('OrderDetail.' . ++$i . '.crown', array('type' => 'checkbox', 'label' => 'Crown'));?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.inlayOnlay', array('type' => 'checkbox', 'label' => 'Inlay/Onlay'));?>
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.veneer', array('type' => 'checkbox', 'label' => 'Veneer')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.postCore', array('type' => 'checkbox', 'label' => 'Post & Core')); ?>
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.bridge', array('type' => 'checkbox', 'label' => 'Bridge')); ?>	
            </div>
        </div>
            <br><br>

            <div class="restoration"><label class="label-text">Pontic Design</label>
                <div class="check-boxes">
                    <div class="checkbox">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth1', array('type' => 'checkbox', 'label' =>false,'div' =>false)) ?> <label for="<?php echo 'OrderDetail' .$i. 'Tooth1'; ?>"><?php echo $this->Html->image("th-1.png", array("alt" => "Tooth1", "width" => "15", "height" => "21")); ?> </label> 
                    </div>
                    <div class="checkbox"><?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth2', array('type' => 'checkbox','label' =>false,'div' =>false)) ?> <label for="<?php echo 'OrderDetail' .$i. 'Tooth2'; ?>"><?php echo $this->Html->image("th-2.png", array("alt" => "Tooth1", "width" => "15", "height" => "21")); ?> </label></div>
                    <div class="checkbox">
                            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth3', array('type' => 'checkbox','label' =>false,'div' =>false)) ?> <label for="<?php echo 'OrderDetail' .$i. 'Tooth3'; ?>"><?php echo $this->Html->image("th-3.png", array("alt" => "Tooth1", "width" => "16", "height" => "21")); ?> </label></div>
                    <div class="checkbox">
                                <?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth4', array('type' => 'checkbox','label' =>false,'div' =>false)) ?> <label for="<?php echo 'OrderDetail' .$i. 'Tooth4'; ?>"><?php echo $this->Html->image("th-4.png", array("alt" => "Tooth1", "width" => "14", "height" => "21")); ?> </label></div>
                    <div class="checkbox">
                                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox','label' =>false,'div' =>false)) ?> <label for="<?php echo 'OrderDetail' .$i. 'Tooth5'; ?>"><?php echo $this->Html->image("th-5.png", array("alt" => "Tooth1", "width" => "11", "height" => "21")); ?> </label></div>
                </div>
            </div>
            <br><br>

            <div class="restoration"><label class="label-text">Design Details</label>
                <div class="check-boxes">
                    <div class="checkbox">
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.360degree', array('type' => 'checkbox', 'label' => ' 360º', 'div' => false)); ?>
                        <label>metal margin</label>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalmargin', array("type" => "text", 'class' => false, 'label' => false, 'div' => false)); ?>mm
                    </div>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => ' 3/4 Metal Occlusal*')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.porcelainButtMargin', array('type' => 'checkbox', 'label' => 'Porcelain Butt Margin*')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingual', array('type' => 'checkbox', 'label' => 'Metal Lingual*')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalLingualCollar', array('type' => 'checkbox', 'label' => 'Metal Lingual Collar')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.metalOcclusal', array('type' => 'checkbox', 'label' => 'Metal Occlusal*')); ?>
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.diagnosticWaxup', array('type' => 'checkbox', 'label' => 'Diagnostic Wax up*')); ?>
                    <div class="checkbox">
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'other', 'div' => false)); ?>
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.other_val1', array("type" => "text", 'class' => false, 'label' => false, 'div' => false)); ?>
                    </div>
                </div>
                <label>*Additional Charge</label><br><br>		
            </div>

            <div class="restoration"><label class="label-text">ALL-CERAMIC</label>
                <div class="check-boxes">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechLayeredZirconia', array('type' => 'checkbox', 'label' => 'Suntech Layered Zirconia')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechFullZirconia', array('type' => 'checkbox', 'label' => 'Suntech Full Zirconia')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.maxPressed', array('type' => 'checkbox', 'label' => 'e.max Pressed')); ?><br>
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechZirconiaCopingOnly', array('type' => 'checkbox', 'label' => ' Suntech Zirconia Coping Only')); ?>	
                </div>
            </div><br><br>
            <div class="restoration"><label class="label-text">COMPOSITES/ TEMP</label>
                <div class="check-boxes">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.gardia', array('type' => 'checkbox', 'label' => 'Gardia (composite)')); ?>		
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.suntechTemporaryCrown', array('type' => 'checkbox', 'label' => 'Suntech Temporary Crown')); ?>		
                </div>
            </div>
            <br><br>				
            <div class="restoration"><label class="label-text">PFM CROWNS</label>
                <div class="check-boxes">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?>
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?>
                            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?>
                                <?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?>
                                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.captek', array('type' => 'checkbox', 'label' => 'Captek', 'class' => 'checkbox-group')); ?>	
                </div>
            </div>
            <br><br>
            <div class="restoration"><label class="label-text"> FULL CAST</label>
                <div class="check-boxes">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.nonPrecious', array('type' => 'checkbox', 'label' => 'Non-Precious')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.semiPrecious', array('type' => 'checkbox', 'label' => 'Semi-Precious')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleWhiteGold', array('type' => 'checkbox', 'label' => 'High Noble White Gold')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.highNobleYellowGold', array('type' => 'checkbox', 'label' => 'High Noble Yellow Gold')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.castPostCore', array('type' => 'checkbox', 'label' => 'Cast Post & Core')); ?>	
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.y2Gold', array('type' => 'checkbox', 'label' => 'Y+ 2% Gold', 'class' => 'checkbox-group')); ?>	
                </div>
            </div><br><br>

            <div class="restoration"><label class="label-text">IMPLANT ABUTMENTS</label>
                <div class="check-boxes">
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.stock', array('type' => 'checkbox', 'label' => 'Stock')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconia', array('type' => 'checkbox', 'label' => 'Zirconia')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.customMilled', array('type' => 'checkbox', 'label' => 'Custom Milled')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.titanium', array('type' => 'checkbox', 'label' => 'Titanium')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.haderBar', array('type' => 'checkbox', 'label' => 'Hader Bar')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.zirconiainsertHybrid', array('type' => 'checkbox', 'label' => 'Zirconia w/Ti insert-Hybrid')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.castOverdentureFrame', array('type' => 'checkbox', 'label' => 'Cast Overdenture Frame')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.screwRetained', array('type' => 'checkbox', 'label' => 'Screw Retained')); ?>
                    <?php echo $this->Form->input('OrderDetail.' . ++$i . '.tooth5', array('type' => 'checkbox', 'label' => 'Tooth5')); ?><br>
                    <div class="implant"><label>Implant System</label>	
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.implantSystem', array("type" => "text", 'class' => false, 'label' => false)); ?>

                    <label># of Clips or Locator</label>	
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.clipsorLocator', array("type" => "text", 'class' => false, 'label' => false)); ?>

                    <label>Implant System</label>	
<?php echo $this->Form->input('OrderDetail.' . ++$i . '.diameter', array("type" => "text", 'class' => false, 'label' => false)); ?>
                </div>
                </div>
            </div>
            <br><br>

            <h3 class='resc'><span>REMOVABLE RESTORATIONS</span></h3><br>
            <div class="restoration"><label class="label-text">Check all that apply</label>
                <div class="check-boxes">
                     <div class="checkbox check-all">
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.other', array('type' => 'checkbox', 'label' => 'Other','div' => false ));
                        echo $this->Form->input('OrderDetail.' . ++$i . '.other1', array('type' => 'textbox', 'label' => false,'div' => false)); ?>
                     </div><?php echo $this->Form->input('OrderDetail.' . ++$i . '.upper', array('type' => 'checkbox', 'label' => 'Upper'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.lower', array('type' => 'checkbox', 'label' => 'Lower'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.try-in', array('type' => 'checkbox', 'label' => 'Try-in'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.finish', array('type' => 'checkbox', 'label' => 'Finish'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.cusil', array('type' => 'checkbox', 'label' => 'Cusil'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.bite_block', array('type' => 'checkbox', 'label' => 'Bite Block'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.denture', array('type' => 'checkbox', 'label' => 'Denture'));
                         echo $this->Form->input('OrderDetail.' . ++$i . '.immediate', array('type' => 'checkbox', 'label' => 'Immediate/Surgical Denture '));?>
                    <div class="checkbox check-all">
                        <?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot', array('type' => 'checkbox', 'label' => 'Extract Toot #','div' => false));
                        echo $this->Form->input('OrderDetail.' . ++$i . '.extract_toot1', array('type' => 'textbox', 'label' => false,'div' => false)); ?>
                    </div><?php echo $this->Form->input('OrderDetail.' . ++$i . '.extract_all', array('type' => 'checkbox', 'label' => 'Extract All'));
                        echo $this->Form->input('OrderDetail.' . ++$i . '.extract_now', array('type' => 'checkbox', 'label' => 'Extract Now'));
                        echo $this->Form->input('OrderDetail.' . ++$i . '.extract_try', array('type' => 'checkbox', 'label' => 'Extract After Try-In'));
                        ?>
                </div>
            </div><br><br>
            <div class="restoration">
                <label class="label-text">Teeth</label> 
                <div class="check-boxes">
                    <p>(Stock teeth used if no option is selected)</p>
                    <div class="checkbox check-all check-all-label">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.tooth_shad', array('type' => 'textbox', 'label' => 'Tooth Shad', 'div'=>false)); ?>
                    </div>
             <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.ipn_portrait', array('type' => 'checkbox', 'label' => 'IPN Portrait*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.gold_face', array('type' => 'checkbox', 'label' => 'Gold Open Face*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.full_gold', array('type' => 'checkbox', 'label' => 'Full Gold*'));
            ?>
                </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Sunflex Partials</label>
            <div class="check-boxes">
                    <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex', array('type' => 'checkbox', 'label' => 'Sunflex'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_cast_frame', array('type' => 'checkbox', 'label' => 'Sunflex Cast Frame Combo'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_vitallium_frame', array('type' => 'checkbox', 'label' => 'Sunflex Vitallium 2000+ Frame Combo'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunflex_unilateral', array('type' => 'checkbox', 'label' => 'Sunflex Unilateral(Maximum 2 teeth)'));
            ?>
            </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Sunflex Shade</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.pink', array('type' => 'checkbox', 'label' => 'Pink'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));
            ?>
               </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Clasp Type</label>
            <div class="check-boxes">
                <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.cast', array('type' => 'checkbox', 'label' => 'Cast*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.wire', array('type' => 'checkbox', 'label' => 'Wire*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.flexible', array('type' => 'checkbox', 'label' => 'Flexible*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear', array('type' => 'checkbox', 'label' => 'sunclear*'));
            ?>
                </div></div><br> <br>
            <div class="restoration">
                <label class="label-text">Acrylic Partials</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_frame', array('type' => 'checkbox', 'label' => 'Suncast Frame w/Acrylic'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_acrylic', array('type' => 'checkbox', 'label' => ' Vitallium 2000+ w/Acrylic'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.sunclear_frame', array('type' => 'checkbox', 'label' => 'Sunclear Frame w/Acrylic'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.flipper', array('type' => 'checkbox', 'label' => ' Flipper (1 Tooth All Acrylic)'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_partial', array('type' => 'checkbox', 'label' => ' Acrylic Partial (No Frame)'));
            echo "(Wrought Wire Clasps)";
            ?>
                </div></div><br> <br>
            <div class="restoration">
                <label class="label-text">Acrylic Shade</label>
            <div class="check-boxes">
                <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.lucitone', array('type' => 'checkbox', 'label' => 'Lucitone 199*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_pink', array('type' => 'checkbox', 'label' => 'Pink'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_light_pink', array('type' => 'checkbox', 'label' => 'Light Pink'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.light_meharry', array('type' => 'checkbox', 'label' => 'Light Meharry'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_medium_meharry', array('type' => 'checkbox', 'label' => 'Medium Meharry'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.acrylic_dark_meharry', array('type' => 'checkbox', 'label' => 'Dark Meharry'));
            ?>
            </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Reinforcement</label>
            <div class="check-boxes">
                    <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_wire', array('type' => 'checkbox', 'label' => 'Wire*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.reinforcement_mesh', array('type' => 'checkbox', 'label' => 'Mesh*'));
            ?>
            </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Metal Frameworks</label>
            <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.suncast_framework', array('type' => 'checkbox', 'label' => 'Suncast Framework Only'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.vitallium_framework', array('type' => 'checkbox', 'label' => 'Vitallium 2000+ Framework Only'));
            ?>
            </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Sunclear <br>Frameworks</label>
                <div class="check-boxes">
            <?php echo $this->Form->input('OrderDetail.' . ++$i . '.framework_only', array('type' => 'checkbox', 'label' => ' Framework Only')); ?>
                </div>
            </div>
            <br> <br>
            <div class="restoration">
                <label class="label-text">Valplast Partials</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.valplast', array('type' => 'checkbox', 'label' => 'Valplast'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_cast_combo', array('type' => 'checkbox', 'label' => 'Valplast Cast Combo'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.valplast_vitallium', array('type' => 'checkbox', 'label' => 'Valplast Vitallium 2000+ Combo'));
            ?>
                </div>
            </div>
            <br> <br>
            <div class="restoration">
                <label class="label-text">Design</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.horseshoe_palate', array('type' => 'checkbox', 'label' => 'Horseshoe Palate'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.open_palate', array('type' => 'checkbox', 'label' => 'AP Open Palate'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.full_palatal_metal_coverage', array('type' => 'checkbox', 'label' => 'Full Palatal Metal Coverage '));
            echo $this->Form->input('OrderDetail.' . ++$i . '.palatal_trap', array('type' => 'checkbox', 'label' => 'Palatal Strap'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.metal_occlusion', array('type' => 'checkbox', 'label' => 'Metal Occlusion'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.rests', array('type' => 'checkbox', 'label' => 'Rests'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_pron', array('type' => 'checkbox', 'label' => 'Lingual Apron'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.precision_attachments', array('type' => 'checkbox', 'label' => 'Precision Attachments*'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.lingual_bar', array('type' => 'checkbox', 'label' => 'Lingual Bar'));?>
                    <p><?php echo "(Best design is fabricated if no option is selected)";
            ?></p>
                </div>
            </div><br> <br>
            <div class="restoration">
                <label class="label-text">Attachments*</label>
            <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.era', array('type' => 'checkbox', 'label' => 'ERA'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.pd', array('type' => 'checkbox', 'label' => 'PD'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.hader_bar', array('type' => 'checkbox', 'label' => 'Hader Bar'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'checkbox', 'label' => 'Other'));
            ?>
                <div class="checkbox check-all check-all-label">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.attachments_other', array('type' => 'text','div'=>false));
            ?>
                </div>
            </div>
            </div>
            <h2>OTHER</h2>
            <div class="restoration">
                <label class="label-text">Repair</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.reline', array('type' => 'checkbox', 'label' => 'Reline'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.rebase', array('type' => 'checkbox', 'label' => 'Rebase'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.basic_repair', array('type' => 'checkbox', 'label' => 'Basic Repair'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.soft_liner', array('type' => 'checkbox', 'label' => 'Soft Liner'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth', array('type' => 'checkbox', 'label' => 'Add Tooth #')); ?>
                    <div class="checkbox check-all check-all-label">
                    <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.add_tooth1', array('type' => 'text','div'=>false));
            ?>
                    </div>
                </div>
            </div>
            <br> <br>
            <div class="restoration">
                <label class="label-text">Night Guards/<br>Bite Splints</label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.soft', array('type' => 'checkbox', 'label' => 'Soft'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.hard', array('type' => 'checkbox', 'label' => 'Hard'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_2mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 2mm'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.hard_soft_3mm', array('type' => 'checkbox', 'label' => 'Hard/Soft 3mm'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.surgical_stent', array('type' => 'checkbox', 'label' => 'Surgical Stent'));
            ?>
                </div>
            </div>
            <br> <br>
            <div class="restoration">
                <label class="label-text">Other* </label>
                <div class="check-boxes">
            <?php
            echo $this->Form->input('OrderDetail.' . ++$i . '.base_plate_bite_rim', array('type' => 'checkbox', 'label' => 'Base Plate/Bite Rim'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.custom_tray', array('type' => 'checkbox', 'label' => 'Custom Tray'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.duplicate_model', array('type' => 'checkbox', 'label' => 'Duplicate Model'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.epoxy_model', array('type' => 'checkbox', 'label' => 'Epoxy Model'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.patient_name', array('type' => 'checkbox', 'label' => 'Patient Name in Denture'));
            echo "* Additional Charge";
            ?>
                </div>
            </div>
            <br><br>
            <h3 class='resc'><span>Design</span></h3><br>
            <div class="restoration"><label class="label-text">
                    <?php echo $this->Html->image("teeth-img.png", array("alt" => "Teeth design", "width" => "134", "height" => "204")); ?> 
                   </label>
                <div class="check-boxes">
                    <?php // echo $this->Form->input('OrderDetail.' . ++$i . '.details', array("type" => "textarea", 'class' => "gray-bg form-control", 'label' => false,)); ?>
                <?php
			echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_opposing', array('type' => 'checkbox', 'label' => 'Adjust opposing'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.adjust_abument', array('type' => 'checkbox', 'label' => 'Adjust abument'));
            echo $this->Form->input('OrderDetail.' . ++$i . '.call_office', array('type' => 'checkbox', 'label' => 'Call the office'));?>
                </div>
            </div>
            <br>


            <div class="file-outer"> <?php echo $this->Form->input('OrderAttachment..filename', array('placeholder' => 'Attatch file', 'multiple', "type" => "file")); ?>
            <p></p>  <span class="filename" > files must be .jpg, .jpeg, .gif, .png, .bmp, .stl, .cad</span>
            </div>
            <div class="text-center">
                <div class="login-btn-outer">
            <?php echo $this->Form->submit(__($heading, true), array('class' => 'login-btn', 'id' => 'orderBtn')); ?>
                </div>
            </div>
<?php echo $this->Form->end(); ?>
        </div>
