<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />


	<table class="table table-striped table-bordered" id="example">
	
	<thead>
	<tr>
	<th>Order ID</th>
	<th>Lab name</th>
	<th>Delivery charges</th> 
	<th>Payment status</th> 
	<th>Payment method</th>
	</tr>
	</thead>
	<tbody>
	<?php if(!empty($payment)){ 
	foreach($payment as $payments){
	?>
	<tr>
	
	 <td><?php echo $id = $this->requestAction('admins/getorderid/'.$payments['Labtransaction']['order_id']); 
	 if($id == ''){
	 echo $payments['Labtransaction']['order_id'];
	 }
	 
	 ?></td>
	 <td><?php echo $payments['Labtransaction']['lab_name']; ?></td>
	 <td><?php 
	 if($payments['Labtransaction']['adminpay_amt'] == ''){
	 echo '$ '.$payments['Labtransaction']['pay_amount']; 
	 }
	 else{
	 echo 'Initial Amt : $'.$payments['Labtransaction']['pay_amount'].'<br>Outstanding Amt : $'.$payments['Labtransaction']['adminpay_amt'].'<br>Total : $'.($payments['Labtransaction']['pay_amount'] + $payments['Labtransaction']['adminpay_amt']);
	 }
	 ?></td>
	 <td><?php echo 'Payment Done'; ?></td>
	 <td><?php echo $payments['Labtransaction']['pay_method']; ?></td>
	 </tr>
	<?php } }
	else{
	 ?>
		<td colspan='8' class="leftalign" style='color:red; text-align:center;'>No record found</td>
	<?php } ?>
<tbody>
	</table>
	




<script>
$(document).ready(function() {
    $('#example').DataTable({pageLength: 100});
   
} );
</script>

<style>
#example_length{
margin-top: 20px;
}
input{
width: 70%;
    padding: 7px;
}

</style>
