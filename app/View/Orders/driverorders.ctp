<?php 
	echo $this->Html->script('sorttable');

	?>

<style>
.select label{ display:none;}
table.sortable thead {
	    font-size: smaller;
    background-color:#eee;
    color:#666666;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
.st a{
color :white !important;
}
.inputt {
    border-bottom: 1px solid #000 !important;
    border-left: 0;
    border-right: 0;
    border-top: 0;
    outline: none;
	background-color: transparent;
    width:98% !important;
}
.search-btn {
	    height: 38px !important;
}
.new-order table{
font-size: 12px;
}
select{
opacity: 1;
}
</style>
<div class="new-order ">

		<h2 class="col-lg-3">Orders</h2>
		<div class="col-sm-5" style="width:initial;"> 
			<div class="searchbox_ld">
			
	<?php echo $this->Form->input("searchval",array("type"=>'text','id'=>'search',"placeholder"=>'Search by any header','class'=>'form-control search_box',"div"=>false,"label"=>false)); ?>
				
				<span id="submitbtn" class="invite-button nomargin submitsearch search-btn">Search</span>
					
			</div>
			
		</div>
		<br>
		<div class="col-sm-3"> 
                    <div id="archive-unarchive">
                    </div>
		</div>
		<div class="clear"></div>

	<div class="responsive-table">
		<table id="table" class="sortable table table-striped">
			<thead>
			<tr>

<th>&nbsp;</th>
                             <th class="st">Order ID</th>
                             <th class="st">Date</th>
                             <th class="st">From</th>
                                <th class="st">TO</th>
                                <th class="st">Dropoff</th>
                                <th class="st">Pickup</th>  
                                <th class="st">Priority</th>   
                                <th class="st">Recepient</th> 
                                  
                                <th class="st">Action</th>          
			</tr>	
			 </thead>
                   <tbody>
       <?php  
      if(!empty($orders)){
      $i=0;
       foreach($orders as $order){
       //print_r($order);
       
       ?>
			<tr>
<td style="width:19px;" class="class_toggle plus"></td>
			<td><?php echo '#'.$order['Directorder']['oid'];?></td>
			<td><?php echo $order['Directorder']['pick_date']; ?></td>
			<td><?php 
			$state1 = $this->requestAction('App/getstate/'.$order['Directorder']['pickup_state']);
			$st1 = $state1[0]['State']['name'].', '.$state1[0]['State']['code'].' ';
			echo $order['Directorder']['pickup_address'].'<br>'.$order['Directorder']['pickup_city'].', '.$st1.''.$order['Directorder']['pickup_zip']; ?></td>
			<td><?php 
			$state2 = $this->requestAction('App/getstate/'.$order['Directorder']['dropoff_state']);
			$st2 = $state2[0]['State']['name'].', '.$state2[0]['State']['code'].' ';
			echo $order['Directorder']['dropoff_address'].'<br>'.$order['Directorder']['dropoff_city'].', '.$st2.''.$order['Directorder']['dropoff_zip'];
			?></td>
			
	
			<td><?php echo $order['Directorder']['pickup']; ?></td>
			<td><?php echo $order['Directorder']['dropoff']; ?></td>
			<td><?php echo $order['Directorder']['priority']; ?></td>
			<td><?php echo $order['Directorder']['recipient']; ?></td>
		
			<td>
			<?php

if($order['Directorder']['order_status'] == 'Pickup'){
  $select = array('Delivered'=>'Delivered'); 
  echo $this->Form->input('action', array(
         'options' => array($select),
         'empty' => 'Action',
         'myatt' => $order['Directorder']['oid'],
         'onchange' => 'update_st("'.$order['Directorder']['oid'].'")',
          'class'=>'form-control',
          'label'=>false ,
          'div'=>'true',
          'id' =>'action_'.$order['Directorder']['oid']
      ));
			}
			else{
			echo $order['Directorder']['order_status'];
			}
			
			?>
			</td>
			</tr>
			<tr class="detail_data" style="display:none;">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><?php
			$distance = $this->requestAction('App/getzipdistance/'.$order['Directorder']['pickup_zip'].'/'.$order['Directorder']['dropoff_zip']);
			 echo '<b>Distance</b><br>'.$distance; 
			 ?></td>
			<td><?php echo '<b>Order Status</b><br>'.$order['Directorder']['order_status']; ?></td>
			<td><?php echo '<b>Payment Status</b><br>'; echo ($order['Directorder']['payment_status'] == 0)? 'Pending':'Payment Done'; ?></td>
			<td colspan="5"><?php echo '<b>Note</b><br>'.$order['Directorder']['note']; ?></td>
			</tr>
			
			<?php  	} }else{ ?>
				<tr>
					<td colspan="10" style="color:red;">No Order Found</td>
				</tr> 
			<?php } ?>
			
			</tbody>
		</table>
	</div>

</div>


<script>
    var $rows = $('#table tr');
    $('#search').keyup(function() {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });

    $(document).ready(function() {
        $(".class_toggle").click(function() {
            $(this).parent().next().toggle();
            if ($(this).parent().next().css('display') == 'none') {
                if ($(this).hasClass("minus")) {
                    $(this).removeClass("minus");
                    $(this).addClass("plus");
                }
            } else {
                if ($(this).hasClass("plus")) {
                    $(this).removeClass("plus");
                    $(this).addClass("minus");
                }
            }
        });
    });
    
    function update_st(orderid) {
        action = $('#action_'+orderid).val();
        $.ajax({
            url: "<?php echo Router::url(array('controller'=>'admins','action'=>'updateassignlabdriver'));?>",
            type: "POST",
            data: {'action': action, 'orderid':orderid},
            success: function(data) {// console.log(data); return false;
                $('#alert-text').html(data);
                $('#custom-modal').modal('show');
                location.reload();
            },
            error: function(data) {
                alert(data);
            }
        });
    }
    
    $(document).ready(function() {
        <?php if ($button == 'Archive Orders') : ?>
        $('#archive-unarchive').html('<a  style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('/orders/driverorders/archive') ?>" >Archive Orders</a>');
        <?php else : ?>
        $('#archive-unarchive').html('<a  style="margin-top: 3px;" class="add-btn" href="<?php echo $this->Html->url('/orders/driverorders/') ?>" >Unarchive Orders</a>');
        <?php endif; ?>    
    });
</script>




