<div class="new-order">
	<?php echo $this->Form->create("User",array("div"=>false,'type' => 'get')); ?>
	<?php echo $this->Session->Flash();?>
	
		<h2 class="col-sm-2"><?php echo 'View Orders'; ?></h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by Order Id, Status','class'=>'form-control',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
					
			</div>
		</div>
		
		<div class="add-btn-outer pull-right">
		
		<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/preview-orders') ?>" title="All orders">All orders</a>
		</div>
			
		<!--<div class="col-sm-3">
                    <div class="sorting-select">
			<?php echo $this->Form->input('sort_rating',array("options"=>array("All"=>"All","Rating"=>"Rating"),"empty"=>"Sort By",'class'=>"selectpicker",'id'=>'sort_rating',"value"=>(isset($set_val['sort_rating']) && !empty($set_val['sort_rating']))?$set_val['sort_rating']:'','label'=>false,'div'=>false)); ?></div>
		</div> -->
		<div class="clear"></div>
		<div class="responsive-table">
			<table class="table table-striped">
                            <tr>
                                <th>Order Id</th>
                                <th>Status</th>
                                <th>Order Charges</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th>&nbsp;</th>
                            </tr>
				<?php if(!empty($order_data)) {  
					foreach ($order_data as $order) { 
					if($order['Order']['status_id'] == 12 || $order['Order']['status_id'] == 13) {	
						?>
					
						<tr> 
								<td><?php echo $order['Order']['id']; ?></td>
								<td><?php echo $order['Order']['status']; ?></td>
								<td><?php
								if(count($order['OrderPayment']) == 0){
									echo "N-A";
								}                          
								else if(count($order['OrderPayment']) == 1) { 
									echo "Order Charge: $".$order['OrderPayment'][0]['payment'];

								} else{
									echo "Order Charge: $".$order['OrderPayment'][0]['payment']."<br/>"; 
									echo "Delivery Charge: $".$order['OrderPayment'][1]['payment']."<br/>"; 
									echo "Total: $".($order['OrderPayment'][0]['payment'] + $order['OrderPayment'][1]['payment']); 					    

								}	?></td>
								 <td><?php echo $order['Order']['created']; ?></td>
							<td><?php echo $order['OrderStatus'][0]['modified']; ?></td> 
						<td>   <div class="black-btn-outer pull-right">								
									<?php echo $this->Html->link("View Order",array("controller"=>"orders","action"=>'viewOrder',$order['Order']['id']),array("class"=>"black-btn"));?> 
									
								</div>
							</td>
						</tr> 
					<?php } else { ?>
					 
					<?php }  } 
				} else {  ?> 
						<td colspan="5" style="color:red;" class="leftalign">No record found</td>  
				<?php } ?>
			</table>
		</div>
	<?php echo $this->Element("pagination"); ?>
	<?php echo $this->Form->end(); ?>
</div>
   

