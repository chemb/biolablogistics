<?php 
echo $this->Html->script('sorttable');
?>
<style>
table.sortable thead {
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 

}

@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) { 
.responsive-table{ overflow:hidden;}

 }



</style>
<script type="text/javascript">
	
	function doSearch() {
    var searchText = document.getElementById('searchTerm').value;
    var targetTable = document.getElementById('dataTable');
    var targetTableColCount;
            
    //Loop through table rows
    for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
        var rowData = '';

        //Get column count from header row
        if (rowIndex == 0) {
           targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
           continue; //do not execute further code for header row.
        }
                
        //Process data rows. (rowIndex >= 1)
        for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
            rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent;
        }

        //If search term is not found in row data
        //then hide the row, else show
        if (rowData.indexOf(searchText) == -1)
            targetTable.rows.item(rowIndex).style.display = 'none';
        else
            targetTable.rows.item(rowIndex).style.display = 'table-row';
    }
}
	</script>

<div class="new-order">
	<?php //pr($orders); //die; ?>
	<div class="responsive-table">
		<h2 class="col-sm-3">Track Order</h2>
		<?php echo $this->Form->create('Order',array("div"=>false,'type' => 'get'));?>
			<div class="col-sm-5"> 
				<div class="searchbox_ld">
					<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by any header below','class'=>'form-control search_box','onkeyup'=>"doSearch()",'id'=>'searchTerm',"div"=>false,"label"=>false, "value" => (isset($set_val['searchval']) && !empty($set_val['searchval'])) ? $set_val['searchval'] : '' )); ?>
					<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
		<table id="dataTable" class="sortable table table-striped">
			<thead>		
			<?php if(!empty($orders)) { ?>
				<tr>
					<th style="text-align:center;" class="st">View Order</th>
					<th   style="text-align:center;" class="st">Order id</th>
					<th  style="text-align:center;" class="st">Patient name</th>
					<th  class="st" style="width:200px; text-align:center;">Track Status</th>
					<th  style="text-align:center;" class="st">Delivery Date</th>
					<th  style="text-align:center;" class="st">Price</th>		
					<!-- <th class="st">Total</th>	-->	
				</tr>
			</thead>
                            <tbody>
			<?php foreach ( $orders as $orders ) {
			 if($orders['Order']['lab_id']==$this->Session->read("Auth.User.id")){//pr($orders); die;  ?>
				<tr>
					<td style="text-align:center;">
						<?php if($this->Session->read("Auth.User.user_type") == 1){ 
							echo $this->Html->link(__("View Order"),array("action"=>"viewOrder", $orders['Order']['id']));
						}elseif($this->Session->read("Auth.User.user_type") == 2){
							echo $this->Html->link(__("View Order"),array("action"=>"viewOrderForLab", $orders['Order']['id']));	
						}elseif($this->Session->read("Auth.User.user_type") == 3){
							echo $this->Html->link(__("View Order"),array("action"=>"viewOrderStaff", $orders['Order']['id']));	
						}?>
					</td>
					<td style="text-align:center;"><?php echo $orders['Order']['order_manualid']; ?></td>
					<td style="text-align:center;"><?php $r = unserialize($orders['Order']['order_details']); echo $r[0]['patientName']; ?></td>
					<td >
						<ul id="track_order">
							<?php $deliverydate = '';
							$j = 0;							
							foreach($orders['OrderStatus'] as $key=>$order) {
									//pr($order);	die;			    
							?>
								<a class="bottom trackOrder" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="<?php echo "Status : ".$status[$order['status_id']].', Updated by '.(!empty($order['updated_company_by'])?ucfirst($order['updated_company_by']):ucfirst($order['updated_name_by']))." on ".$order['created']; ?>">	&nbsp;&nbsp;</a>
								
							<?php if(!empty($order['status_id']) && $order['status_id'] == 9){
									 $deliverydate = (empty($order['created'])? '' : $order['created']);
								}
								?>							
							<?php	
								/*if($order['status_id'] != 8 && $order['status_id'] != 9 && $order['status_id'] != 11 && $order['status_id'] != 13 && $order['status_id'] != 12) { 
								if($order['updated_company_by'] != $j) { ?>
								<a class="bottom trackOrder" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="<?php if($order['status_id'] == 1) {									
									$string = $order['Status']['title']. ' Updated by '.$order['updated_company_by']." on ".$order['created'];;
									echo str_replace("Waiting Updated by", "Order placed by Doctor", $string);									
									} 
									else if($order['status_id'] == 2){
									echo 'At Lab '.$order['updated_company_by']." on ".$order['created'];							
										}
																									
								 ?>">								
									<?php echo '&nbsp;&nbsp;'; ?>
								
								</a>
								<?php } } else { ?> 								
									
									<a class="bottom trackOrder" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="<?php 
									 if($order['status_id'] == 8)	{
										echo 'On the way by '.$order['updated_company_by']." on ".$order['created'];		
										}
									else if($order['status_id'] == 9) {
										echo 'Delivered by '.$order['updated_company_by']." on ".$order['created'];	
										}
									else if($order['status_id'] == 12) {
										echo 'Cancelled by '.$order['updated_company_by']." on ".$order['created'];	
										}							
									  ?>">								
									<?php if($order['status_id'] != 11 && $order['status_id'] != 13) { echo "&nbsp;&nbsp;"; } ?>
								
								</a>
									
								
									   <?php } ?>
								<?php if(!empty($order['status_id']) && $order['status_id'] == 9){
									 $deliverydate = (empty($order['created'])? '' : $order['created']);
								} 
								$j = $order['updated_company_by'];*/
							}
							 ?>
						</ul>
					</td>
					<td style="text-align:center;"><?php echo !empty($deliverydate)?date('m/d/y', strtotime($deliverydate)):'N-A';?></td>
					<td style="text-align:center;">
						<label>Order Cost:</label>
						<?php echo empty($orders['OrderStatus'][0]['order_payment']) ? 'N-A' : '$'.$orders['OrderStatus'][0]['order_payment']	; ?><br />
						
						<label>Delivery:</label>
						<?php echo empty($orders['OrderStatus'][0]['deliver_charges']) ? 'N-A' : '$'.$orders['OrderStatus'][0]['deliver_charges']; ?>
						
					</td>
					<!-- <td><?php $res = $orders['OrderStatus'][0]['order_payment'] + $orders['OrderStatus'][0]['deliver_charges']; 
					echo empty($res) ? 'N-A': '$'.$res; ?></td> -->
				</tr>
			<?php } }?>
			<?php } else{ ?>
				<tr>
					<td colspan="4" style="color:red;">No record found</td>
				</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>
