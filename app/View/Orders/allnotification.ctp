<?php 
error_reporting(0);
echo $this->Html->script('sorttable');
 ?>
<style>
table.sortable thead {
    background-color:#eee;
    color:#666666;
    //font-weight: bold;
    cursor: pointer;
}
table.sortable th.st:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
    content: " \25B4\25BE" 
}
table th {
    background: #1F2427;
    color: #fff;
    text-transform: uppercase;
}
</style>
<script type="text/javascript">
function callajaxdel(uid){

var checkedValue = []; 
var inputElements = document.getElementsByClassName('arc');
for(var i=0; inputElements[i]; ++i){
      if(inputElements[i].checked){
           checkedValue[i] = inputElements[i].value;
        
          //alert(checkedValue[i]);
          var x = checkedValue[i];
var status_id = inputElements[i].getAttribute("myatt");
//alert(status_id);
//alert(uid);
          $.ajax({
		url: "<?php echo Router::url(array('controller'=>'orders','action'=>'delnotification'));?>",
		
		type: "POST",
		data:'status_id='+status_id+'&id='+checkedValue[i]+'&uid='+uid ,
		success:function(data){
//alert(data);
$('#msg').html('');
              $('#msg').append(data);
             location.reload();
             // $('#tabble').html('');
             // $('#tabble').append(data);
            },
         error:function(data){
			 alert('Waiting for data...');
		 }
        });
      }
}
	
		
	
	
}


</script>


<div class="responsive-table">
<table class="sortable table table-striped">
<thead>
<tr>
<th>&nbsp;</th>
<th class="st">Date</th>
<th class="st">Order ID</th>
<th class="st">Type</th> 
<th>Message</th>
</tr>
<thead>
<tbody>
<?php
$i=0;
foreach($notification_data as $data){
?>
<tr>
<td><?php

 echo $this->Form->checkbox('arc', array('hiddenField' => false,'value'=>$data['Notification']['order_id'],'myatt'=>$data['Notification']['status_id'],'class'=>'arc','id'=>'arc_'.$i));
 $i++;
 ?></td>
<td><?php echo date('m/d/y', strtotime($data['Notification']['update_on']));  ?></td>
<td><?php echo $this->requestAction('App/getorderid/'.$data['Notification']['order_id']); ?></td>
<td><?php $msg = $this->requestAction('App/notificationmsg/'.$data['Notification']['status_id']); 
echo $msg[0]['Notificationtitle']['name'];
?></td>

<td>
<?php

 echo $msg[0]['Notificationtitle']['detail'];
//echo '<pre>';
//print_r($msg);
//echo '<pre>';
}
?>
</td>
</tr>
</tbody>
</table>
</div>
<button class="btn btn-danger" onclick="callajaxdel(<?php echo $this->Session->read("Auth.User.id"); ?>);">Delete Selected</button>


