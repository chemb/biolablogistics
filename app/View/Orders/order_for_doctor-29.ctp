<?php error_reporting(0); ?><div class="new-order">
	<?php echo $this->Form->create("User",array("div"=>false,'type' => 'get')); ?>
	<?php echo $this->Session->Flash();?>
	
		<h2 class="col-sm-2"><?php echo 'View Orders'; ?></h2>
		<div class="col-sm-5"> 
			<div class="searchbox_ld">
			
				<?php echo $this->Form->input("searchval",array("type"=>'text',"placeholder"=>'Search by Order Id, Status','class'=>'form-control',"div"=>false,"label"=>false,"value"=>(isset($set_val['searchval']) && !empty($set_val['searchval']))?$set_val['searchval']:'' )); ?>
				<?php echo $this->Form->submit("Search",array("label"=>false,"div"=>false,"class"=>'invite-button nomargin submitsearch search-btn',"id"=>'submitbtn',"attr"=>'search')); ?>
					
			</div>
		</div>
		
		<div class="add-btn-outer pull-right">
			<?php if(isset($data) && !empty($data)) { ?>
				<a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/preview-orders') ?>" title="All orders">All orders</a>
			<?php } else {	?>
		       <a id="add_btn" class="add-btn" href="<?php echo $this->Html->url('/archive-orders') ?>" title="Archive old orders">Archive old orders</a>
		     <?php } ?>
		</div>
			
		<!--<div class="col-sm-3">
                    <div class="sorting-select">
			<?php echo $this->Form->input('sort_rating',array("options"=>array("All"=>"All","Rating"=>"Rating"),"empty"=>"Sort By",'class'=>"selectpicker",'id'=>'sort_rating',"value"=>(isset($set_val['sort_rating']) && !empty($set_val['sort_rating']))?$set_val['sort_rating']:'','label'=>false,'div'=>false)); ?></div>
		</div> -->
		<div class="clear"></div>
		<div class="responsive-table">
			<table class="table table-striped">
                            <tr>
                                <th><?php echo $this->Paginator->sort('id',"Order Id"); ?></th>
                                <th>Patient name</th>
                                <th>Status</th>
                                <th>Order Charges</th>
                                <th>Lst Updated</th>
                                <th>&nbsp;</th>
                            </tr>
				<?php if(!empty($order_data)) {  
					foreach ($order_data as $order):
					//pr($order);
					?>
						<tr> 
								<td><?php echo $order['Order']['id']; ?></td>
								<td><?php $r = unserialize($order['OrderDetail'][0]['details']); echo $r[0]['patientName']; ?></td>
								<td class="<?php echo "status_".$order['Order']['status_id'];  ?>"><?php echo $order['Order']['status']; ?></td>
								<td><?php
								if(count($order['OrderPayment']) == 0){
									echo "N-A";
								}   								                
								else if(count($order['OrderPayment']) == 1) {
									echo "Order Charge: $".number_format($order['OrderPayment'][0]['payment'],2,'.',','); 
									
								} else{
									echo "Order Charge: $".number_format($order['OrderPayment'][0]['payment'],2,'.',',')."<br/>";
									echo "Delivery Charge: $".number_format($order['OrderPayment'][1]['payment'],2,'.',',')."<br/>";
									echo "Total: $".number_format(($order['OrderPayment'][0]['payment'] + $order['OrderPayment'][1]['payment']),2,'.',',')."<br/>";
								}	?></td>
								<td><?php $lastData = end($order['OrderStatus']); echo (date("Y-m-d",strtotime($lastData['modified']))); ?></td> 
						<td>   <div class="black-btn-outer pull-right">								
									<?php echo $this->Html->link("View Order",array("controller"=>"orders","action"=>'viewOrder',$order['Order']['id']),array("class"=>"black-btn"));?> 
									
								</div>
							</td>
						</tr> 
					<?php endforeach;  
				} else {  ?> 
						<td colspan="6" style="color:red;" class="leftalign">No record found</td>  
				<?php }?>
			</table>
		</div>
	<?php echo $this->Element("pagination"); ?>
	<?php echo $this->Form->end(); ?>
</div>
   

