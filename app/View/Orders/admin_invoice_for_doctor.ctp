<?php //pr($labInvoices); die; ?>
<div class="users view">
	<h2><?php  echo __('Invoice From Lab'); ?></h2>
	<dl>
		<?php if(!empty($labInvoices)) { foreach($labInvoices as $labInvoice){ //pr($labInvoice); ?>
			<dt><?php echo __('Order Id'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['Order']['id'])? 'N-A' : ($this->requestAction('admins/getorderid/'.$labInvoice['Order']['id']))); ?>
			</dd>
			<dt><?php echo __('Payment'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['OrderPayment']['payment'])? 'N-A' : ($labInvoice['OrderPayment']['payment'])); ?>
			</dd>
			<dt><?php echo __('Delivery Charges'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['DeliverCharges']['payment'])? 'N-A' : ($labInvoice['DeliverCharges']['payment'])); ?>
			</dd>
			<dt><?php echo __('Total'); ?></dt>
			<dd>
				<?php echo $totla = $labInvoice['OrderPayment']['payment'] + $labInvoice['DeliverCharges']['payment'];
					//echo (empty($labInvoice['DeliverCharges']['payment'])? 'N-A' : ($labInvoice['DeliverCharges']['payment'])); ?>
			</dd>
			<dt><?php echo __('Payment From'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['Doctor']['first_name'])? 'N-A' : (ucwords($labInvoice['Doctor']['first_name'].' '.$labInvoice['Doctor']['last_name']))); ?>
			</dd>
			<dt><?php echo __('Payment To'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['OrderPayment']['payer'])? 'N-A' : 'RRL'); ?>
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo (empty($labInvoice['OrderPayment']['created'])? 'N-A' : ($labInvoice['OrderPayment']['created'])); ?>
			</dd>
		<?php } } else { ?>
		<dt>&nbsp;</dt>
			<dd>
				<?php echo 'No record found.'; ?>
			</dd>
		
		<?php } ?>
		<dt><a style="margin-left:6px;"class="btn btn-grey" onclick="history.go(-1);">Cancel</a></dt>
	</dl>
</div>
