<?php 
error_reporting(0);
 ?>
<div class="row">
	<?php //pr($timings); ?>
    <?php echo $this->Session->flash(); ?>
    <div class="clear">&nbsp;</div>
    <?php echo $this->element('userdetailsleft'); ?>
    <section class="col-sm-8 right-panel">
        <h2>Profile<br />
            <span>&nbsp;</span>
        </h2>


        <?php echo $this->Form->create('Userdetail'); ?>

        <h3>Basics</h3>

        <div class="row input">
            <div class="col-sm-12  margin-btm">
                <?php echo $this->Form->input('User.company_name', array("placeholder" => ($this->Session->read("Auth.User.user_type") == 2)?"Lab Name":"Hospital Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 200)); ?>
            </div>
        </div>
        <div class="row input">
            <div class="col-sm-6  margin-btm">
                <?php echo $this->Form->input('first_name', array("placeholder" => "First Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>
            </div>
            <div class="col-sm-6">

                <?php echo $this->Form->input('last_name', array("placeholder" => "Last Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>
            </div>
        </div>
        <div class="row input">
            <div class="col-sm-6  margin-btm">
                <?php echo $this->Form->input('phone', array("placeholder" => "Phone", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 15)); ?>

            </div>
        </div>
        
     <!----------------------------turnaround ----------------------------->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>

        var placeSearch, autocomplete;
        var componentForm = {
            UserdetailCity: 'long_name',
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById('UserdetailLongitude').value = longitude;
            document.getElementById('UserdetailLatitude').value = latitude;
            validLocation = true;
            doValidation();

        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function addressChanged() {
            validLocation = false;
            doValidation();

        }

        function doValidation() {
            if (validLocation) {
                $('.input .address input').removeClass('invalid');
                $("#submit-btn").prop('disabled', false);
            } else {
                $('.input .address input').addClass('invalid');;
                $("#submit-btn").prop('disabled', true);
            }

        }

    </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADwlDF4ePt7FLfn-9fHS-XAXLM8lrHwdw&amp;libraries=places&amp;callback=initAutocomplete" async="" defer=""></script>
        <script>
        $(document).ready(function () {
            validLocation = $('UserdetailLatitude').val() != '' && $('UserdetailLongitude').val() != '';
            doValidation();
  //called when key is pressed in textbox


   $(".form-control-num").keypress(function (e) {
   //alert("ok");
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });


});
        </script>
        <?php if ( $this->Session->read("Auth.User.user_type") == 2 ) { ?>
        <h3>Turnarounds</h3>
        <div class="row input" id="turnarounds_container">
			<?php if ( empty($turnarounds) ) { ?>
            <div class="col-sm-4  margin-btm form-group">
                <?php echo $this->Form->input('Userdetail.turnarounds.0.name', array("placeholder" => "Procedure", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>
            </div>
            <div class="col-sm-4 form-group">
				<?php echo $this->Form->input('Userdetail.turnarounds.0.value', array("placeholder" => "Timings", "class" => "form-control-num", "id" => "quantity", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div><span id="errmsg" style="color:red"></span>

            <div class="col-sm-4 form-group">
				<?php echo $this->Form->input('Userdetail.turnarounds.0.price', array("placeholder" => "Price", "class" => "form-control-num", "id" => "quant", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div>
            <?php } else { ?>
				<?php
				foreach ( $turnarounds as $key => $val ) {
				?>
					<div class="col-sm-4  margin-btm form-group">
						<?php echo $this->Form->input('Userdetail.turnarounds.'.$key.'.name', array("placeholder" => "Procedure", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50,'value'=>$val['name'])); ?>
					</div>

					<div class="col-sm-4 form-group">
						<?php echo $this->Form->input('Userdetail.turnarounds.'.$key.'.value', array("placeholder" => "Timings", "class" => "form-control-num", "id" => "quantity", "div" => false, "label" => false, "type" => "text", "maxlength" => 60,'value'=>$val['value'])); ?>
					</div>

					<div class="col-sm-4 form-group">
						<?php echo $this->Form->input('Userdetail.turnarounds.'.$key.'.price', array("placeholder" => "Price", "class" => "form-control-num", "id" => "quant", "div" => false, "label" => false, "type" => "text", "maxlength" => 60,'value'=>$val['price'])); ?>
					</div>
				<?php
				}

				?>
			<?php } ?>
        </div>
        <?php echo $this->Form->hidden("turn_count",array("value"=>empty($turnarounds)?1:count($turnarounds))); ?>
        <a href="javascript:void(0);" class="add_turnarounds" >Add More Procedures</a>
        <!-----------turnaround end here ------------------>
        <?php } ?>
        <!-- timings -------------------------------------->
        <br/>
        <h3>Timings (leave blank if close)</h3>
        <div class="timing-con">
        <?php for ( $i = 0; $i < 7 ; $i++ ) { ?>
			<span style="float:left;width:100%;margin:10px;"><?php echo $weekArray[$i]; ?></span>
			<div class="row input">
			<div class="col-sm-6  margin-btm">
				<?php echo $this->Form->input('Userdetail.timings.'.$i.'.from', array("class" => "selectpicker", "div" => false, "label" => false, "maxlength" => 50,'options'=>$time,'empty'=>"From",'value'=>isset($timings[0])?$timings[$i]['from']:'')); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $this->Form->input('Userdetail.timings.'.$i.'.to', array("class" => "selectpicker", "div" => false, "label" => false, "maxlength" => 60,'options'=>$time,'empty'=>"To",'value'=>isset($timings[0])?$timings[$i]['to']:'')); ?>
			</div>
			</div>
		<?php } ?>
        </div>
        <!-- timings end here------------------------------>

         <div class="row input">
			<div class="col-sm-12">
				<div class="col-sm-12">
				<?php $data = explode(',',$this->data['Userdetail']['serviceTags']); ?>
				<?php echo $this->Form->input('serviceTags', array("options"=>$services,"div" => false, "label" => false, "class" => "selectpicker","selected" => $data,"empty"=>"Select Specialization"));
			?>
			</div>
			</div>
        </div>
		 <div class="row input">
			<div class="col-sm-12">
				<?php echo $this->Form->input('webLink', array("placeholder" => "Website link", "div" => false, "label" => false, "class" => "form-control", "type" => "text"));
			?>
			</div>
        </div>

<?php /*
        <div class="row input">
            <div class="col-sm-12">
                <?php echo $this->Form->input('about', array("placeholder" => "About", "div" => false, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
            </div>
        </div>
*/ ?>
        <div class="row input">
			<div class="col-sm-12 address">
				<?php echo $this->Form->input('address', array("placeholder" => "Address", "div" => false, "label" => false, "id" => "autocomplete", "class" => "form-control", "type" => "text", "onFocus" =>"geolocate()", "onchange" =>"addressChanged()")); ?>
            </div>
		</div>
		<!--<div class="row input">
			<div class="col-sm-4">
				<?php echo $this->Form->input('zipcode', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text", "maxlength" => 6));
				echo $this->Form->hidden('latitude', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text", 'value'=>$val['latitude']));
				echo $this->Form->hidden('longitude', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text", 'value'=>$val['longitude'])); ?>
			</div>
            <div class="col-sm-4 margin-btm">
				<?php //pr($state); ?>
                <?php echo $this->Form->input('state_id', array('options' => $state,'class' => 'State selectpicker', "label" => false, 'selected' => $this->data['Userdetail']['state_id'], 'empty' => 'Select State')); ?>
            </div>
            <div class="col-sm-4">

                <?php echo $this->Form->input('city', array("placeholder" => "City", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div>
        </div> -->
        <?php
        echo $this->Form->hidden('latitude', array("div" => false, "label" => false, "class" => "form-control", "type" => "text"));
        echo $this->Form->hidden('longitude', array("div" => false, "label" => false, "class" => "form-control", "type" => "text")); ?>
        <?php /*
        <div class="row input">
            <div class="col-sm-12">
                <h3>Biography</h3><br>
                <?php echo $this->Form->input('biography', array("placeholder" => "Biography", "div" => false, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
            </div>
        </div>
*/ ?>
        <div class="text-center">
            <div class="login-btn-outer">
                <?php echo $this->Form->Submit("Save", array('label' => false, 'div' => false, 'class' => 'login-btn', 'id' => 'submit-btn')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>
</div>






