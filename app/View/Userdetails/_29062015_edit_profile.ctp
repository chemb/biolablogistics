<div class="row">
	<?php //pr($timings); ?>
    <?php echo $this->Session->flash(); ?>
    <div class="clear">&nbsp;</div>
    <?php echo $this->element('userdetailsleft'); ?>
    <section class="col-sm-8 right-panel">
        <h2>Profile<br />
            <span>&nbsp;</span>
        </h2>


        <?php echo $this->Form->create('Userdetail'); ?>

        <h3>Basics</h3>

        <div class="row input">
            <div class="col-sm-12  margin-btm">
                <?php echo $this->Form->input('User.company_name', array("placeholder" => ($this->Session->read("Auth.User.user_type") == 2)?"Lab Name":"Hospital Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 200)); ?>
            </div>
        </div>
        <div class="row input">
            <div class="col-sm-6  margin-btm">
                <?php echo $this->Form->input('first_name', array("placeholder" => "First Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>
            </div>
            <div class="col-sm-6">

                <?php echo $this->Form->input('last_name', array("placeholder" => "Last Name", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>
            </div>
        </div>
        <div class="row input">
            <div class="col-sm-6  margin-btm">
                <?php echo $this->Form->input('phone', array("placeholder" => "Phone", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 10)); ?>

            </div>
            <div class="col-sm-6">
				<?php echo $this->Form->input('designation', array("placeholder" => "Designation", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div>
        </div>
        <?php if ( $this->Session->read("Auth.User.user_type") == 2 ) { ?>
        <!-----------turnaround ------------------>
        <h3>Turnarounds</h3>
        <div class="row input" id="turnarounds_container">
			<?php if ( empty($turnarounds) ) { ?>
            <div class="col-sm-6  margin-btm form-group">
                <?php echo $this->Form->input('Userdetail.turnarounds.0.name', array("placeholder" => "Procedure", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50)); ?>

            </div>
            <div class="col-sm-6 form-group">
				<?php echo $this->Form->input('Userdetail.turnarounds.0.value', array("placeholder" => "Timings", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div>
            <?php } else { ?>
				<?php 
				foreach ( $turnarounds as $key => $val ) {
				?>
					<div class="col-sm-6  margin-btm form-group">
						<?php echo $this->Form->input('Userdetail.turnarounds.'.$key.'.name', array("placeholder" => "Procedure", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 50,'value'=>$val['name'])); ?>

					</div>
					<div class="col-sm-6 form-group">
						<?php echo $this->Form->input('Userdetail.turnarounds.'.$key.'.value', array("placeholder" => "Timings", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 60,'value'=>$val['value'])); ?>
					</div>
				<?php	
				}
				
				?>
			<?php } ?>
        </div>
        <?php echo $this->Form->hidden("turn_count",array("value"=>empty($turnarounds)?1:count($turnarounds))); ?>
        <a href="javascript:void(0);" class="add_turnarounds" >Add More Procedures</a>
        <!-----------turnaround end here ------------------>
        
        <!-- timings -------------------------------------->
        <br/>
        <h3>Timings (leave blank if close)</h3>
        <div class="timing-con">
        <?php for ( $i = 0; $i < 7 ; $i++ ) { ?>
			<span style="float:left;width:100%;margin:10px;"><?php echo $weekArray[$i]; ?></span>
			<div class="row input">
			<div class="col-sm-6  margin-btm">
				<?php echo $this->Form->input('Userdetail.timings.'.$i.'.from', array("class" => "selectpicker", "div" => false, "label" => false, "maxlength" => 50,'options'=>$time,'empty'=>"From",'value'=>isset($timings[0])?$timings[$i]['from']:'')); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $this->Form->input('Userdetail.timings.'.$i.'.to', array("class" => "selectpicker", "div" => false, "label" => false, "maxlength" => 60,'options'=>$time,'empty'=>"To",'value'=>isset($timings[0])?$timings[$i]['to']:'')); ?>
			</div>
			</div>
		<?php } ?>
        </div>
        <!-- timings end here------------------------------>
        <?php } ?>
         <div class="row input">
			<div class="col-sm-12">
				<div class="col-sm-12">
				<?php $data = explode(',',$this->data['Userdetail']['serviceTags']); ?>
				<?php echo $this->Form->input('serviceTags', array("options"=>$services,"div" => false, "label" => false, "class" => "selectpicker","selected" => $data,"empty"=>"Select Specialization"));
			?>
			</div>
			</div>
        </div>
		 <div class="row input">
			<div class="col-sm-12">
				<?php echo $this->Form->input('webLink', array("placeholder" => "Website link", "div" => false, "label" => false, "class" => "form-control", "type" => "text"));
			?>
			</div>
        </div>
        
<?php /*
        <div class="row input">
            <div class="col-sm-12">
                <?php echo $this->Form->input('about', array("placeholder" => "About", "div" => false, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
            </div>
        </div>
*/ ?>
        <div class="row input">
			<div class="col-sm-12">
				<?php echo $this->Form->input('address', array("placeholder" => "Address", "div" => false, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
			</div>
		</div>
		<div class="row input">
			<div class="col-sm-4">
				<?php echo $this->Form->input('zipcode', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text", "maxlength" => 6));
				echo $this->Form->hidden('latitude', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text")); 
				echo $this->Form->hidden('longitude', array("placeholder" => "Zipcode", "div" => false, "label" => false, "class" => "form-control", "type" => "text")); ?> 
			</div>
            <div class="col-sm-4 margin-btm">
				<?php //pr($state); ?>
                <?php echo $this->Form->input('state_id', array('options' => $state,'class' => 'State selectpicker', "label" => false, 'selected' => $this->data['Userdetail']['state_id'], 'empty' => 'Select State')); ?>
            </div>
            <div class="col-sm-4">

                <?php echo $this->Form->input('city', array("placeholder" => "City", "class" => "form-control", "div" => false, "label" => false, "type" => "text", "maxlength" => 60)); ?>
            </div>
        </div>

<?php /*
        <div class="row input">
            <div class="col-sm-12">
                <h3>Biography</h3><br>
                <?php echo $this->Form->input('biography', array("placeholder" => "Biography", "div" => false, "label" => false, "class" => "form-control", "type" => "textarea", "maxlength" => 500,"rows" =>4)); ?>
            </div>
        </div>
*/ ?>
        <div class="text-center">
            <div class="login-btn-outer">
                <?php echo $this->Form->Submit("Save", array('label' => false, 'div' => false, 'class' => 'login-btn')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>		
</div>






