<div class="row">
	<?php echo $this->Session->flash(); ?>
	<div class="clear-fix">&nbsp;</div>
	<?php echo $this->element('userdetailsleft');?>
	<section class="col-sm-8 right-panel">
		<h2>Account Settings</h2><br>
		<?php echo $this->Form->create('User',array("enctype"=>"multipart/form-data","class"=>"profile_bx")); ?>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $this->Form->input("currentpassword",array("type"=>'password','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter Current Password', "class" => "form-control")); 
				 echo $this->Form->input("password",array("type"=>'password','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter New Password', "class" => "form-control"));
				 echo $this->Form->input("confirmpassword",array("type"=>'password','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Re-type New Password', "class" => "form-control"));
				 echo $this->Form->input("id",array("type"=>'hidden','label'=>false,"value"=>$this->Session->read("Auth.User.id")));?>
				<div class="text-center">
					<div class="login-btn-outer">
						<?php echo $this->Form->Submit("Change Password",array('label'=>false,'div'=>false,'class'=>'login-btn l_b_p')); ?>
					</div>
				</div>
			</div>
		</div>
	</section>		
</div>

