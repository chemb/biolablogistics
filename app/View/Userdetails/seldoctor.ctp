<div class="row">
	<?php echo $this->Session->flash(); ?>
	<div class="clear-fix">&nbsp;</div>
	<?php echo $this->element('userdetailsleft');?>
	<section class="col-sm-8 right-panel">
		<h2>Add Doctor</h2><br>
		<?php echo $this->Form->create('User',array("enctype"=>"multipart/form-data","class"=>"profile_bx")); ?>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $this->Form->input("name",array("type"=>'text','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter Doctor Name', "class" => "form-control"));
				
				
				echo $this->Form->input("email",array("type"=>'text','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter Email', "class" => "form-control"));
				
				echo $this->Form->input("city",array("type"=>'text','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter City ', "class" => "form-control"));  
				
				echo $this->Form->input("address",array("type"=>'text','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter Address', "class" => "form-control")); 
				
				//print_r($state);
				foreach($state as $data)
				{
				$option[$data['State']['id']]=$data['State']['name'];
				
				}
				
				 echo $this->Form->input('state', array('options' => $option,'class' => 'State selectpicker', "label" => false,"div" => true, 'empty' => 'Select State')); 
				
				 echo $this->Form->input("phone",array("type"=>'text','label'=>false,'div'=>'chng_pswd_flds input','placeholder'=>'Enter Phone Number', "class" => "form-control")); 
				
				 echo $this->Form->input("id",array("type"=>'hidden','label'=>false,"value"=>$this->Session->read("Auth.User.id")));?>
				 
				<div class="text-center">
					<div class="login-btn-outer">
						<?php echo $this->Form->Submit("ADD",array('label'=>false,'div'=>false,'class'=>'login-btn l_b_p')); ?>
					</div>
				</div>
			</div>
		</div>
	</section>		
</div>

