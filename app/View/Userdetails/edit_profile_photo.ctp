<div class="row">
    <?php echo $this->Session->flash(); ?>
    <div class="clear-fix">&nbsp;</div>
    <?php echo $this->element("userdetailsleft"); ?>
    <section class="col-sm-8 right-panel">

        <h2>Profile Photo</h2>

        <div class="image-outer">

            <div class="img-content">
                <div class="img-frame">
                    <?php
                    // use thumb path from helper
                    if ( $this->Session->read("Auth.User.user_type") == 1 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/docprofile.jpg");
					} else if ( $this->Session->read("Auth.User.user_type") == 2 ) { 
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/labprofile.jpg");
					} else {
						$profileImgPathThumb1 = (($this->Session->read("Auth.User.Userdetail.image") && file_exists(WWW_ROOT . $this->Session->read("Auth.User.Userdetail.image"))) ? $this->Session->read("Auth.User.Userdetail.image") : "/img/no_img.png");
					}
                    $profileImgThumb1 = $this->Common->getImageName($profileImgPathThumb1, LargeProfileImagePrefix);
                    echo $this->Html->image($profileImgThumb1, array("alt" => $this->Session->read("Auth.User.Userdetail.first_name") . ' ' . $this->Session->read("Auth.User.Userdetail.last_name"), "width" => "244px", "height" => "244px"));
                    ?>
                    <p class="pro-prview">Profile Preview</p>
                </div>
                <!-- <p><strong>TIP:</strong> Your beautiful, clean, non-pixelated image should be at minimum 200x200 pixels.</p> -->
            </div>
            <div class="upload-pic row">

                           <div class="col-md-8"> <?php echo $this->Form->create('Userdetail', array("enctype" => "multipart/form-data", "class" => "profile_bx")); ?>				
                            <?php echo $this->Form->input('image', array("type" => "file", "class" => "TxtFld upload_default", "label" => false, "div" => true, "id" => "PhotoFile",'placeholder'=>'Company name')); ?>
                            <?php echo $this->Form->hidden('id', array('id' => $this->data['Userdetail']['id'])); ?>	
                           </div>
                            <div class="col-md-4">
                                <div class="login-btn-outer">
                                <?php echo $this->Form->Submit("Upload Image File", array('label' => false, 'div' => false, 'class' => 'login-btn')); ?>
                                </div>
                         </div>
                             </div>
                            <span class="filename" >Image files must be .jpg, .jpeg, .gif, .png, .bmp</span>
                       
                  
        </div>


    </section>		
</div>
