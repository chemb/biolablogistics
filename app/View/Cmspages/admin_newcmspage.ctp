<?php echo $this->element("admins/common",array("place"=>'Search by title,content',"flag"=>false,"pageheader"=>'',"buttontitle"=>'Add Blog',"listflag"=>false)); ?>
<div class="pages form">
<?php echo $this->Form->create('Cmspage');?>
	<fieldset>
 		<legend><?php echo __('New Page'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('content');
		echo $this->Form->input('metatitle',array("label"=>"Meta Title"));
		echo $this->Form->input('seourl',array("label"=>"Seo Url"));
		echo $this->Form->input('metadesc',array("label"=>"Meta Description"));
		echo $this->Form->input('metakeyword',array("label"=>"Meta Keyword"));
		echo $this->Form->input('status',array("type"=>"hidden",'value'=>1));
                echo $this->Form->input('showinheader',array("type"=>"checkbox","label"=>"Header Menu"));
		echo $this->Form->input('showinfooter',array("type"=>"checkbox","label"=>"Footer Menu"));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true)); echo $this->Html->link(__('Cancel', true), array('action' => 'index'),array('class'=>'btn btn-grey'));
echo $this->Fck->load("CmspageContent");
//echo $this->Fck->load("CmspageMetakeyword");
//echo $this->Fck->load("CmspageMetadesc");
?>
</div>
