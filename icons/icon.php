<?php

header("Content-type: image/png");
$image = $_GET['t'];
$hint = $_GET['h'];
$im     = imagecreatefrompng($image.".png");
$orange = imagecolorallocate($im, 30, 90, 184);

imagealphablending($im, false);
imagesavealpha($im, true);
if (isset($hint)) {
	$px     = (imagesx($im) - 7.5 * strlen($hint)) / 2;
	imagestring($im, 3, $px, 0, $hint, $orange);
}
imagepng($im);
imagedestroy($im);

?>
